<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="YakAssets" version="1.0" date="27/06/2009" >
    <VersionSettings gameVersion="1.4.1" />
		<Author name="Dunar" email="dunar@darkmages.de" />
		<Description text="Textures from YakUI LibSharedAssets. Don't turn this mod off!" />
        
		<Files>
			<File name="LibStub.lua" />
			<File name="LibSharedAssets.lua" />
      <File name="textures.xml" />
			<File name="textures.lua" />
		</Files>
		
		<OnInitialize/>
		<OnUpdate/>
		<OnShutdown/>
	<WARInfo>
			<Categories>
				<Category name="SYSTEM" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
	</WARInfo>		
	</UiMod>
</ModuleFile>
