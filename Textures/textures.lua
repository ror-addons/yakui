local textures = {
	[1]="tint_square",
	[2]="SharedMediaYGroupIconShape",
	[3]="SharedMediaYGroupIconGloss",
	
	[4]="SharedMediaYSet1CBBar",
	[5]="SharedMediaYSet1CBBarEnd",
	[6]="SharedMediaYSet1CBBG",
	[7]="SharedMediaYSet1CBFX",
	[8]="SharedMediaYSet1IPBG",
	[9]="SharedMediaYSet1IPFX",
	[10]="SharedMediaYSet1UFBar",
	[11]="SharedMediaYSet1UFBG",
	[12]="SharedMediaYSet1UFFX",
	
	[13]="SharedMediaYSet2CBBar",
	[14]="SharedMediaYSet2CBBarEnd",
	[15]="SharedMediaYSet2CBBG",
	[16]="SharedMediaYSet2CBFX",
	[17]="SharedMediaYSet2IPBG",
	[18]="SharedMediaYSet2IPFX",
	[19]="SharedMediaYSet2UFBar",
	[20]="SharedMediaYSet2UFBG",
	[21]="SharedMediaYSet2UFFX",

	[22]="SharedMediaYSet3CBBar",
	[23]="SharedMediaYSet3CBBarEnd",
	[24]="SharedMediaYSet3CBBG",
	[25]="SharedMediaYSet3CBFX",
	[26]="SharedMediaYSet3IPBG",
	[27]="SharedMediaYSet3IPFX",
	[28]="SharedMediaYSet3UFBar",
	[29]="SharedMediaYSet3UFBG",
	[30]="SharedMediaYSet3UFFX",

	[31]="SharedMediaYSet4CBBar",
	[32]="SharedMediaYSet4CBBarEnd",
	[33]="SharedMediaYSet4CBBG",
	[34]="SharedMediaYSet4CBFX",
	[35]="SharedMediaYSet4IPBG",
	[36]="SharedMediaYSet4IPFX",
	[37]="SharedMediaYSet4UFBar",
	[38]="SharedMediaYSet4UFBG",
	[39]="SharedMediaYSet4UFFX",

	[40]="SharedMediaYSet5CBBar",
	[41]="SharedMediaYSet5CBBarEnd",
	[42]="SharedMediaYSet5CBBG",
	[43]="SharedMediaYSet5CBFX",
	[44]="SharedMediaYSet5IPBG",
	[45]="SharedMediaYSet5IPFX",
	[46]="SharedMediaYSet5UFBar",
	[47]="SharedMediaYSet5UFBG",
	[48]="SharedMediaYSet5UFFX"
}

local displaynames = {
	[1]="Plain",
	[2]="SM-YGroupIconShape",
	[3]="SM-YGroupIconGloss",

	[4]="SM-YSet1CBBar",
	[5]="SM-YSet1CBBarEnd",
	[6]="SM-YSet1CBBG",
	[7]="SM-YSet1CBFX",
	[8]="SM-YSet1IPBG",
	[9]="SM-YSet1IPFX",
	[10]="SM-YSet1UFBar",
	[11]="SM-YSet1UFBG",
	[12]="SM-YSet1UFFX",
	
	[13]="SM-YSet2CBBar",
	[14]="SM-YSet2CBBarEnd",
	[15]="SM-YSet2CBBG",
	[16]="SM-YSet2CBFX",
	[17]="SM-YSet2IPBG",
	[18]="SM-YSet2IPFX",
	[19]="SM-YSet2UFBar",
	[20]="SM-YSet2UFBG",
	[21]="SM-YSet2UFFX",

	[22]="SM-YSet3CBBar",
	[23]="SM-YSet3CBBarEnd",
	[24]="SM-YSet3CBBG",
	[25]="SM-YSet3CBFX",
	[26]="SM-YSet3IPBG",
	[27]="SM-YSet3IPFX",
	[28]="SM-YSet3UFBar",
	[29]="SM-YSet3UFBG",
	[30]="SM-YSet3UFFX",

	[31]="SM-YSet4CBBar",
	[32]="SM-YSet4CBBarEnd",
	[33]="SM-YSet4CBBG",
	[34]="SM-YSet4CBFX",
	[35]="SM-YSet4IPBG",
	[36]="SM-YSet4IPFX",
	[37]="SM-YSet4UFBar",
	[38]="SM-YSet4UFBG",
	[39]="SM-YSet4UFFX",

	[40]="SM-YSet5CBBar",
	[41]="SM-YSet5CBBarEnd",
	[42]="SM-YSet5CBBG",
	[43]="SM-YSet5CBFX",
	[44]="SM-YSet5IPBG",
	[45]="SM-YSet5IPFX",
	[46]="SM-YSet5UFBar",
	[47]="SM-YSet5UFBG",
	[48]="SM-YSet5UFFX"
}

local tiled = {
	["tint_square"] = true
}

local dims = {
	["tint_square"] = {32, 32},
	["SharedMediaYGroupIconShape"] = { 50, 50 },
	["SharedMediaYGroupIconGloss"] = { 50, 50 },
	
	["SharedMediaYSet1CBBar"] = { 211, 10 },
	["SharedMediaYSet1CBBarEnd"] = { 20, 25 },
	["SharedMediaYSet1CBBG"] = { 225, 20 },
	["SharedMediaYSet1CBFX"] = { 225, 20 },
	["SharedMediaYSet1IPBG"] = { 600, 70 },
	["SharedMediaYSet1IPFX"] = { 600, 70 },
	["SharedMediaYSet1UFBar"] = { 256, 32 },
	["SharedMediaYSet1UFBG"] = { 512, 128 },
	["SharedMediaYSet1UFFX"] = { 512, 128 },

	["SharedMediaYSet2CBBar"] = { 211, 10 },
	["SharedMediaYSet2CBBarEnd"] = { 20, 25 },
	["SharedMediaYSet2CBBG"] = { 225, 20 },
	["SharedMediaYSet2CBFX"] = { 225, 20 },
	["SharedMediaYSet2IPBG"] = { 600, 70 },
	["SharedMediaYSet2IPFX"] = { 600, 70 },
	["SharedMediaYSet2UFBar"] = { 256, 32 },
	["SharedMediaYSet2UFBG"] = { 512, 128 },
	["SharedMediaYSet2UFFX"] = { 512, 128 },

	["SharedMediaYSet3CBBar"] = { 211, 10 },
	["SharedMediaYSet3CBBarEnd"] = { 20, 25 },
	["SharedMediaYSet3CBBG"] = { 225, 20 },
	["SharedMediaYSet3CBFX"] = { 225, 20 },
	["SharedMediaYSet3IPBG"] = { 600, 70 },
	["SharedMediaYSet3IPFX"] = { 600, 70 },
	["SharedMediaYSet3UFBar"] = { 256, 32 },
	["SharedMediaYSet3UFBG"] = { 512, 128 },
	["SharedMediaYSet3UFFX"] = { 512, 128 },

	["SharedMediaYSet4CBBar"] = { 211, 10 },
	["SharedMediaYSet4CBBarEnd"] = { 20, 25 },
	["SharedMediaYSet4CBBG"] = { 225, 20 },
	["SharedMediaYSet4CBFX"] = { 225, 20 },
	["SharedMediaYSet4IPBG"] = { 600, 70 },
	["SharedMediaYSet4IPFX"] = { 600, 70 },
	["SharedMediaYSet4UFBar"] = { 256, 32 },
	["SharedMediaYSet4UFBG"] = { 512, 128 },
	["SharedMediaYSet4UFFX"] = { 512, 128 },

	["SharedMediaYSet5CBBar"] = { 211, 10 },
	["SharedMediaYSet5CBBarEnd"] = { 20, 25 },
	["SharedMediaYSet5CBBG"] = { 225, 20 },
	["SharedMediaYSet5CBFX"] = { 225, 20 },
	["SharedMediaYSet5IPBG"] = { 600, 70 },
	["SharedMediaYSet5IPFX"] = { 600, 70 },
	["SharedMediaYSet5UFBar"] = { 256, 32 },
	["SharedMediaYSet5UFBG"] = { 512, 128 },
	["SharedMediaYSet5UFFX"] = { 512, 128 }
}



local tags = {
	["tint_square"] = {statusbar=true,background=true,verticalbar=true,border=true},
	["SharedMediaYGroupIconShape"] = {statusbar=true,background=true,verticalbar=true},
	["SharedMediaYGroupIconGloss"] = {statusbar=true,background=true,verticalbar=true},

	["SharedMediaYSet1CBBar"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet1CBBarEnd"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet1CBBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet1CBFX"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet1IPBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet1IPFX"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet1UFBar"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet1UFBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet1UFFX"] = {statusbar=true,background=false,verticalbar=true},

	["SharedMediaYSet2CBBar"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet2CBBarEnd"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet2CBBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet2CBFX"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet2IPBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet2IPFX"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet2UFBar"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet2UFBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet2UFFX"] = {statusbar=true,background=false,verticalbar=true},

	["SharedMediaYSet3CBBar"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet3CBBarEnd"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet3CBBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet3CBFX"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet3IPBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet3IPFX"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet3UFBar"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet3UFBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet3UFFX"] = {statusbar=true,background=false,verticalbar=true},

	["SharedMediaYSet4CBBar"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet4CBBarEnd"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet4CBBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet4CBFX"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet4IPBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet4IPFX"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet4UFBar"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet4UFBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet4UFFX"] = {statusbar=true,background=false,verticalbar=true},

	["SharedMediaYSet5CBBar"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet5CBBarEnd"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet5CBBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet5CBFX"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet5IPBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet5IPFX"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet5UFBar"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet5UFBG"] = {statusbar=true,background=false,verticalbar=true},
	["SharedMediaYSet5UFFX"] = {statusbar=true,background=false,verticalbar=true}
}

local LSA = LibStub("LibSharedAssets")
if not LSA then return end

for k,texName in ipairs(textures) do
	local metadata = {
			displayname = displaynames[k],
			size = dims[texName],
			tiled = tiled[texName] or false,
			tags = tags[texName],
		}
	local result = LSA:RegisterTexture(texName, metadata)
	if not result then
		LSA:AddMetadata(texName, {displayname = displaynames[k]})
	end
end