-- yakui.lua build20100524

yakui = {}

local yperftoggle = 1
local ymenutoggle = 1
local ytextureset
yakui.panelxscale = 1
yakui.panelyscale = 1
--[[mwmxanch = 1
mwmyanch = 1
mwmxscale = 1
mwmyscale = 1]]--

MacroIcons = {}
local oldOnPlayerLinkLButtonUp
local oldOnPlayerLinkRButtonUp

local macroId = EA_Window_Macro.MACRO_ICONS_ID_BASE
local oldFlightPointLButtonUp = nil
local selectedFlightButton = nil
--[[local BASE_ICON         = 0
local BUTTON_TEXT       = 1
local COOLDOWN          = 2
local COOLDOWN_TIMER    = 3
local FLASH_ANIM        = 4
local ACTIVE_ANIM       = 5
local GLOW_ANIM         = 6
local STACK_COUNT_TEXT  = 7]]--

local moraleInitScale
local moraleScaleFactor = 1.7
local moraleEventhandler = false

local booltowstring={[false]=L"Off",[true]=L"On"} 

function yakui.Print(args)
	if args == nil then 
		args = L"nil"
	elseif type(args) == "boolean" then 
		args = args and L"true" or L"false"
	elseif type(args) == "string" then
		args = StringToWString(args)
	end
	EA_ChatWindow.Print(L"YakUI: "..(args))
end
--------------------------------------------------------------------------------------------------------

function yakui.initialize()
	if not yakuiVar then return end	-- do nothing on first install when yakuiFirstrun is running
	
	yakuiUpdate.load()
		
------------------------------------------------------ Event Handlers

	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_HEALER , "yakui.healall")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "yakui.onreload")
	RegisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED, "yakui.zonechange")
	RegisterEventHandler(SystemData.Events.LOADING_END, "yakui.onload")
	RegisterEventHandler(SystemData.Events.RESOLUTION_CHANGED, "yakui.HandleReso")
	RegisterEventHandler(SystemData.Events.RESOLUTION_CHANGED, "yakui.RenderPanel")
	yakui.ychatrenameEA=EA_ChatWindow.OnRename 
	EA_ChatWindow.OnRename=yakui.ychatrename
	
	oldFlightPointLButtonUp = EA_InteractionFlightMasterWindow.FlightPointLButtonUp
    EA_InteractionFlightMasterWindow.FlightPointLButtonUp = yakui.guildscrollhook

    --[[RegisterEventHandler("TargetLast", SystemData.Events.PLAYER_TARGET_UPDATED, "yakui.TargetLUpdateTargets")
    RegisterEventHandler("TargetLast", SystemData.Events.ESCAPE_KEY_PROCESSED, "yakui.TargetLSetEscapeFlag")
    
    yakui.TargetLoldHostileTarget = nil
    yakui.TargetLlastKnownFriendlyTarget = nil
    yakui.TargetLlastKnownHostileTarget = nil
    yakui.TargetLlockedFriendlyTarget = nil
    yakui.TargetLisFriendlyTargetLocked = false
    yakui.TargetLoldFriendlyTarget = GameData.Player.name]]--

------------------------------------------------------ building yakui

-- Chatdisplay
	for k,v in pairs(ChatSettings.Channels) do
		if v.serverCmd == L"/t" then
			ChatSettings.Channels[k].remember = true
		end
	end

	oldOnPlayerLinkLButtonUp = EA_ChatWindow.OnPlayerLinkLButtonUp
	oldOnPlayerLinkRButtonUp = EA_ChatWindow.OnPlayerLinkRButtonUp

	EA_ChatWindow.OnPlayerLinkLButtonUp = function(playerName, flags, x, y)
		if flags == 8 then
			-- ctrl left click : invite to group
			playerName = wstring.gsub( playerName, L"(^.)", L"" )
			SendChatText( L"/invite "..playerName, L"" )
		elseif flags == 4 then
			-- shift left click : who
			playerName = wstring.gsub( playerName, L"(^.)", L"" )
			SendChatText( L"/who "..playerName, L"" )
		else
			oldOnPlayerLinkLButtonUp(playerName, flags, x, y)
		end
	end

	EA_ChatWindow.OnPlayerLinkRButtonUp = function(playerName, flags, x, y)
		if flags == 4 then
			-- shift right click : target
			playerName = wstring.gsub( playerName, L"(^.)", L"" )
			SendChatText( L"/target "..playerName, L"" )
		elseif flags == 8 then
			-- ctrl right click: join
			playerName = wstring.gsub( playerName, L"(^.)", L"" )
			SendChatText( L"/join "..playerName, L"" )
		else
			oldOnPlayerLinkRButtonUp(playerName, flags, x, y)
		end
	end


	CreateWindow("YakChatdisplay", true)
	LayoutEditor.RegisterWindow("YakChatdisplay", L"Y Chatframe Display", L"Options.",false,false,true,nil )
	WindowSetFontAlpha ("YakChatdisplay", 0.5)
	WindowSetFontAlpha ("YakChatdisplayText", 0.3)
	LabelSetTextColor("YakChatdisplayText", 200, 200 , 200 )
	WindowSetMovable( "YakChatdisplay", false)
	yakui.ychattoggle(yakuiVar.Panel.chat,1)
	
-- enable more macro symbols
	
	yakui.macroshookselect = EA_Window_Macro.SelectionIconLButtonDown
	EA_Window_Macro.SelectionIconLButtonDown = yakui.macrosselect
	WindowSetShowing("IconsScrollbar", false)
	CreateWindow("MacroIconsUpArrowButton", true)
	CreateWindow("MacroIconsDownArrowButton", true)
	WindowSetParent("MacroIconsUpArrowButton", "MacroIconSelectionWindow")
	WindowSetParent("MacroIconsDownArrowButton", "MacroIconSelectionWindow")
	WindowClearAnchors("MacroIconsUpArrowButton")
	WindowClearAnchors("MacroIconsDownArrowButton")
	WindowAddAnchor("MacroIconsUpArrowButton", "topright", "MacroIconSelectionWindow", "topright", -5,35)
	WindowAddAnchor("MacroIconsDownArrowButton", "bottomright", "MacroIconSelectionWindow", "bottomright", -5,-5)			

------------------------------------------------------ building yakui: resolution dependencies
	
------------------------------------------------------ building yakui: post resolution 

	if Vectors and Vectors.CalcRatio() == "1.25" then -- "5:4"
		ytextureset = "1280x1024"
	elseif SystemData.screenResolution.x > 1680 then
		ytextureset = "1920x1200"
	else
		ytextureset = "1680x1050"
	end
	
---------------------------------------------------------------------------------------------------------------------------------------	
	
			
--Buttons (YuiButton, War Commander Button, Alpha Toggle Button)
	--[[CreateWindow("YakuiButton", true)
	LayoutEditor.RegisterWindow( "YakuiButton", L"YakUI Button", L"YakUI Functions", false, false, true, nil )]]--
	CreateWindow("YakuiWCButton", true)
	LayoutEditor.RegisterWindow( "YakuiWCButton", L"YakUIWC Button", L"Warcommander Functions", false, false, true, nil )
	CreateWindow("YakuiFWButton", true)
	LayoutEditor.RegisterWindow( "YakuiFWButton", L"YakUIFW Button", L"FW Button", false, false, true, nil )
	CreateWindow("YakuiBWButton", true)
	LayoutEditor.RegisterWindow( "YakuiBWButton", L"YakUIBW Button", L"BW Button", false, false, true, nil )
	CreateWindow("YakuiSORButton", true)
	LayoutEditor.RegisterWindow( "YakuiSORButton", L"YakUIKeep Button", L"RvR Map Functions", false, false, true, nil )
	CreateWindow("YakuiATButton", true)
	LayoutEditor.RegisterWindow( "YakuiATButton", L"YakUIAT Button", L"Alpha Toggle", false, false, true, nil )
	--[[CreateWindow("YakuiTTButton", true)
	LayoutEditor.RegisterWindow( "YakuiTTButton", L"YakUITT Button", L"Effigy Targets Toggle", false, false, true, nil )]]--

-- appearance custom: panel r64,g64,b80 background r16,0,0
	--colors
   	if Map then
		Map.SetBorderBackgroundColor(yakuiVar.Colors.colprintr, yakuiVar.Colors.colprintg, yakuiVar.Colors.colprintb)
		Map.SetBorderForegroundColor(yakuiVar.Colors.fx.r, yakuiVar.Colors.fx.g, yakuiVar.Colors.fx.b)
	end
	WindowSetTintColor("YakuiATButton", yakuiVar.Colors.colprintr, yakuiVar.Colors.colprintg, yakuiVar.Colors.colprintb)
	--WindowSetTintColor("YakuiTTButton", 255, 255, 255)
	yakui.CreatePanel()
	yakui.HandleReso()
	
	
	yakuislash.init()
	yakuiTactic.init()
	--yakuiActionBars_Initialize()
	yakuiconfig.initialize()
	yakui.POInit()
	yakuiInfo.entry_point()
	yakui.alertlimitInit()
	CreateWindow("MoraleEditor", true)
	yakuiWHL.initialize()
	--yakui.minefilter()
	-- startup checks
	--RegisterEventHandler(SystemData.Events.LOADING_END, "yakui.handle_loading_end_function")


	if yakuiVar.counter == 1 then
		yakui.relogafterprofileselection()
	else
		yakui.delay(30, yakui.postloading)
	end
end


--------------------------------------------------------------------------------------------------------

--[[function yakui.ybuttonr()
	if yakuiVar.Panel.config == "on" then yakuiconfig.hide() end
	if Map then Map.ToggleMap() end
end
function yakui.ybuttonl() yakuiconfig.infopaneltoggle() end
function yakui.ybuttonmo()
    local windowName	= SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"YakUI rocks!")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end]]--



--[[function yakui.ytargetsbtl()
	if yakuiVar.DEVmode == true then yakui.Print("YakUI: ytargetsbtl") end
	--if yakuiVar.Huduf.ignore == false then
		if Effigy.Bars["TargetsHostile1"] then
			local show = not Effigy.Bars["TargetsHostile1"].show
			for i = 1,6 do
				bar =  Effigy.Bars["TargetsHostile"..i]
				bar.show = show
				bar:setup()
				bar:render()
			end
			yakui.Print(L"Effigy Targets: Hostile "..booltowstring[show])
		end
	--end
end

function yakui.ytargetsbtr()
	--if yakuiVar.Huduf.ignore == false then
		if Effigy.Bars["TargetsFriendly1"] then
			local show = not Effigy.Bars["TargetsFriendly1"].show
			for i = 1,6 do
				bar =  Effigy.Bars["TargetsFriendly"..i]
				bar.show = show
				bar:setup()
				bar:render()
			end
			yakui.Print(L"Effigy Targets: Friendly "..booltowstring[show])
		end
	--end
end

function yakui.ytargetsbtmo()
    local windowName	= SystemData.ActiveWindow.name
    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Toggle Effigy Targets")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end]]--

--yakuiVar.RvRTrackerMode: 0 auto, 1 on, 2 off
function yakui.OnLButtonUpKeep()
	if yakuiVar.RvRTrackerMode == nil then
		--UnregisterEventHandler(SystemData.Events.PLAYER_RVRLAKE_CHANGED, "EA_Window_RvRTracker.UpdateShowing")
		WindowUnregisterEventHandler( "EA_Window_RvRTracker", SystemData.Events.PLAYER_RVRLAKE_CHANGED)
	end
	local newMode = not WindowGetShowing("EA_Window_RvRTracker")
	WindowSetShowing("EA_Window_RvRTracker", newMode)
	if newMode then EA_Window_RvRTracker.RefreshObjectiveTimers() end
	yakuiVar.RvRTrackerMode = newMode
	yakui.Print(L"RvRTracker is now "..booltowstring[newMode])
end

-- MapSetMapView( "EA_Window_RvRTrackerMapDisplay", GameDefs.MapLevel.ZONE_MAP, 209 )209 Eataine

function yakui.OnMButtonUpKeep()
	if yakuiVar.RvRTrackerMode ~= nil then
		--RegisterEventHandler(SystemData.Events.PLAYER_RVRLAKE_CHANGED, "EA_Window_RvRTracker.UpdateShowing")
		WindowRegisterEventHandler( "EA_Window_RvRTracker", SystemData.Events.PLAYER_RVRLAKE_CHANGED, "EA_Window_RvRTracker.UpdateShowing" )
		EA_Window_RvRTracker.UpdateShowing()
		yakuiVar.RvRTrackerMode = nil
		yakui.Print(L"RvRTracker is now automatic.")
	end
end

function yakui.OnRButtonUpKeep() EA_Window_WorldMap.ToggleHUDCampaignTracker() end

function yakui.ywarcmdbtmo()
    local windowName	= SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"Toggle WarCommander")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

function yakui.alphatoggle()
	local bars = {"YBackPanel1920x1200", "YBackPanel1680x1050", "YBackPanel1280x1024"}
	if yakuiVar.Panel.alphatoggle == true then
		for _, v in ipairs (bars) do
			if Effigy.Bars[v] then 
				bar = Effigy.Bars[v]
				bar.images["Panel"].alpha = yakuiVar.Panel.alpha
				if bar.setup then
					bar:setup()
					bar:render()
				end
			end
		end
		Map.SetMapAlpha(yakuiVar.Panel.alpha)
		--WindowSetAlpha("YPanel",1)
		--WindowSetAlpha("YMPanel",1)
		WindowSetTintColor("yCustomcolorsliders_BGSliderA", 120, 120, 120 )
		yakuiVar.Panel.alphatoggle = false
	else
		for _, v in ipairs (bars) do
			if Effigy.Bars[v] then 
				bar = Effigy.Bars[v]
				bar.images["Panel"].alpha = 1
				if bar.setup then
					bar:setup()
					bar:render()
				end
			end
		end
		Map.SetMapAlpha(1)
		--WindowSetAlpha("YPanel",yakuiVar.Panel.alpha)
		--WindowSetAlpha("YMPanel",yakuiVar.Panel.alpha)
		WindowSetTintColor("yCustomcolorsliders_BGSliderA", 255, 255, 255 )
		yakuiVar.Panel.alphatoggle = true
	end
	ButtonSetPressedFlag("yCustomcolorsliders_AlphaToggle", yakuiVar.Panel.alphatoggle)
	SliderBarSetDisabledFlag("yCustomcolorsliders_BGSliderA", not yakuiVar.Panel.alphatoggle )
	yakuiSkin.newbackground()
end

function yakui.ychatfwbt() yakui.ychattoggle(yakuiVar.Panel.chat+1,#EA_ChatTabManager.Tabs) end
function yakui.ychatbwbt() yakui.ychattoggle(yakuiVar.Panel.chat-1,1) end

function yakui.ychattoggle(start,stop)
    local x=1
    local t
    local tabs=EA_ChatTabManager.Tabs
    if(start>stop)then x=-1 end
    for i=start,stop,x
    do
        if(tabs[i].used)
        then
            t=i
            break
        end
    end
    if(t~=nil)
    then
        yakuiVar.Panel.chat=t
        EA_ChatWindow.SetToTab(t)
        LabelSetText("YakChatdisplayText",towstring(EA_ChatWindowGroups[tabs[t].wndGroupId].Tabs[t].tabText))
    end
end 

function yakui.ychatmenu()
	EA_ChatTabManager.activeTabId = yakuiVar.Panel.chat 
	EA_Window_ContextMenu.CreateContextMenu("YakChatdisplaytext", EA_Window_ContextMenu.CONTEXT_MENU_1) 
	EA_Window_ContextMenu.AddMenuItem(GetChatString(StringTables.Chat.LABEL_CHAT_FILTERS), EA_ChatWindow.OnViewChatFilters, false, true, EA_Window_ContextMenu.CONTEXT_MENU_1) 
	EA_Window_ContextMenu.AddMenuItem(GetChatString(StringTables.Chat.LABEL_CHAT_TAB_TEXTCOLORS), EA_ChatWindow.OnViewChatOptions, false, true, EA_Window_ContextMenu.CONTEXT_MENU_1) 
	EA_Window_ContextMenu.AddCascadingMenuItem(GetChatString(StringTables.Chat.LABEL_CHAT_TAB_TABOPTIONS), EA_ChatWindow.SpawnTabOptionsMenu, false, EA_Window_ContextMenu.CONTEXT_MENU_1) 
	EA_Window_ContextMenu.AddCascadingMenuItem(GetChatString(StringTables.Chat.LABEL_CHAT_TAB_COMMANDLISTS), EA_ChatWindow.SpawnCommandListsMenu, false, EA_Window_ContextMenu.CONTEXT_MENU_1) 
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_1) 
end

function yakui.ychatrename()
	yakui.ychatrenameEA()
	yakui.ychattoggle(yakuiVar.Panel.chat,1)
end


---------------------------------------------------------------------------------------------

function yakui.perftoggle()
	if yperftoggle == 1 then
		SystemData.Settings.Performance.perfLevel = SystemData.Settings.Performance.PERF_LEVEL_CUSTOM2
		BroadcastEvent(SystemData.Events.USER_SETTINGS_CHANGED)
		TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, L"Performance settings changed to CUSTOM2...")	
		yperftoggle = 2
	else
		SystemData.Settings.Performance.perfLevel = SystemData.Settings.Performance.PERF_LEVEL_CUSTOM1
		BroadcastEvent(SystemData.Events.USER_SETTINGS_CHANGED)
		TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, L"Performance settings changed to CUSTOM1...")
		yperftoggle = 1
	end
end

function yakui.color()
	--if yakuiVar.Huduf.ignore == false and Effigy then
	if Effigy then
		local bars = {
			"PlayerHP", "FriendlyTarget", "HostileTarget", "Pet", "Castbar", "MouseoverTarget",
			"GroupMember1", "GroupMember2", "GroupMember3", "GroupMember4", "GroupMember5",
			"TargetsFriendly1", "TargetsFriendly2", "TargetsFriendly3", "TargetsFriendly4", "TargetsFriendly5", "TargetsFriendly6",
			--"TargetsHostile1", "TargetsHostile2", "TargetsHostile3", "TargetsHostile4", "TargetsHostile5", "TargetsHostile6"
		}
		local bar
		for _, v in ipairs (bars) do
			if Effigy.Bars[v] then 
				bar = Effigy.Bars[v]
				bar.images["Background"].colorsettings.color.r = yakuiVar.Colors.colprintr
				bar.images["Background"].colorsettings.color.g = yakuiVar.Colors.colprintg
				bar.images["Background"].colorsettings.color.b = yakuiVar.Colors.colprintb
				bar.images["Background"].alpha = yakuiVar.Panel.Panelalpha
				bar.images["Foreground"].colorsettings.color.r = yakuiVar.Colors.fx.r
				bar.images["Foreground"].colorsettings.color.g = yakuiVar.Colors.fx.g
				bar.images["Foreground"].colorsettings.color.b = yakuiVar.Colors.fx.b
				bar.images["Foreground"].alpha = yakuiVar.Panel.fxalpha
				if v == "PlayerHP" or v == "GroupMember1" or v == "GroupMember2" or v == "GroupMember3" or v == "GroupMember4" or v == "GroupMember5" or v == "Pet" or v == "MouseoverTarget" then 
					bar.fg.colorsettings.color.r = yakuiVar.Colors.unitframes.player.r
					bar.fg.colorsettings.color.g = yakuiVar.Colors.unitframes.player.g
					bar.fg.colorsettings.color.b = yakuiVar.Colors.unitframes.player.b
				elseif v == "FriendlyTarget" or v == "TargetsFriendly1" or v == "TargetsFriendly2" or v == "TargetsFriendly3" or v == "TargetsFriendly4" or v == "TargetsFriendly5" or v == "TargetsFriendly6" then
					bar.fg.colorsettings.color.r = yakuiVar.Colors.unitframes.friendly.r
					bar.fg.colorsettings.color.g = yakuiVar.Colors.unitframes.friendly.g
					bar.fg.colorsettings.color.b = yakuiVar.Colors.unitframes.friendly.b
				elseif v == "HostileTarget" or v == "TargetsHostile1" or v == "TargetsHostile2" or v == "TargetsHostile3" or v == "TargetsHostile4" or v == "TargetsHostile5" or v == "TargetsHostile6" then
					bar.fg.colorsettings.color.r = yakuiVar.Colors.unitframes.hostile.r
					bar.fg.colorsettings.color.g = yakuiVar.Colors.unitframes.hostile.g
					bar.fg.colorsettings.color.b = yakuiVar.Colors.unitframes.hostile.b
				end
				if bar.setup then
					bar:setup()
					bar:render()
				end
			end
		end
		if Effigy.Bars["Castbar"] then
			bar = Effigy.Bars["Castbar"]
			bar.fg.colorsettings.color.r = yakuiVar.Colors.unitframes.castbar.r
			bar.fg.colorsettings.color.g = yakuiVar.Colors.unitframes.castbar.g
			bar.fg.colorsettings.color.b = yakuiVar.Colors.unitframes.castbar.b
			if bar.setup then
				bar:setup()
				bar:render()
			end
		end
		if Effigy.Bars["PlayerAP"] then
			bar =  Effigy.Bars["PlayerAP"]
			bar.fg.colorsettings.color.r = yakuiVar.Colors.unitframes.apbar.r
			bar.fg.colorsettings.color.g = yakuiVar.Colors.unitframes.apbar.g
			bar.fg.colorsettings.color.b = yakuiVar.Colors.unitframes.apbar.b
			if bar.setup then
				bar:setup()
				bar:render()
			end
		end
		if Effigy.Bars["PlayerCareer"] then
			bar =  Effigy.Bars["PlayerCareer"]
			bar.fg.colorsettings.color.r = yakuiVar.Colors.unitframes.careerbar.r
			bar.fg.colorsettings.color.g = yakuiVar.Colors.unitframes.careerbar.g
			bar.fg.colorsettings.color.b = yakuiVar.Colors.unitframes.careerbar.b
			if bar.setup then
				bar:setup()
				bar:render()
			end
		end

		if Effigy.Bars["InfoPanel"] then
			bar = Effigy.Bars["InfoPanel"]
			bar.images["Background"].colorsettings.color.r = yakuiVar.Colors.colprintr
			bar.images["Background"].colorsettings.color.g = yakuiVar.Colors.colprintg
			bar.images["Background"].colorsettings.color.b = yakuiVar.Colors.colprintb
			bar.images["Background"].alpha = yakuiVar.Panel.Panelalpha
			bar.images["Foreground"].colorsettings.color.r = yakuiVar.Colors.fx.r
			bar.images["Foreground"].colorsettings.color.g = yakuiVar.Colors.fx.g
			bar.images["Foreground"].colorsettings.color.b = yakuiVar.Colors.fx.b
			bar.images["Foreground"].alpha = yakuiVar.Panel.fxalpha
			if bar.setup then
				bar:setup()
				bar:render()
			end
		end
		
		bars = {"YBackPanel1920x1200", "YBackPanel1680x1050", "YBackPanel1280x1024"}
		for _, v in ipairs (bars) do
			if Effigy.Bars[v] then
				bar = Effigy.Bars[v]
				bar.images["Border"].colorsettings.color.r = yakuiVar.Colors.colprintr
				bar.images["Border"].colorsettings.color.g = yakuiVar.Colors.colprintg
				bar.images["Border"].colorsettings.color.b = yakuiVar.Colors.colprintb
				bar.images["Border"].alpha = yakuiVar.Panel.Panelalpha
				bar.images["Panel"].colorsettings.color.r = yakuiVar.Colors.background.r
				bar.images["Panel"].colorsettings.color.g = yakuiVar.Colors.background.g
				bar.images["Panel"].colorsettings.color.b = yakuiVar.Colors.background.b
				if yakuiVar.Panel.alphatoggle == true then 
					bar.images["Panel"].alpha = yakuiVar.Panel.alpha
				else
					bar.images["Panel"].alpha = 1
				end
				bar.images["FX"].colorsettings.color.r = yakuiVar.Colors.fx.r
				bar.images["FX"].colorsettings.color.g = yakuiVar.Colors.fx.g
				bar.images["FX"].colorsettings.color.b = yakuiVar.Colors.fx.b
				bar.images["FX"].alpha = yakuiVar.Panel.fxalpha
				if bar.setup then
					bar:setup()
					bar:render()
				end
			end
		end
	end
	
	WindowSetTintColor("YakuiATButton", yakuiVar.Colors.colprintr, yakuiVar.Colors.colprintg, yakuiVar.Colors.colprintb)
	--WindowSetTintColor("YakuiTTButton", 255, 255, 255)
	
	if yakuiVar.Skin.active == true then
		yakuiSkin.UpdateAllBorders()
		if yakuiVar.Skin.newbackgrounds.linkwpanel == true then	yakuiSkin.newbackground() end
	end
	
	if Map then
		Map.SetBorderBackgroundColor(yakuiVar.Colors.colprintr, yakuiVar.Colors.colprintg, yakuiVar.Colors.colprintb)
		Map.SetBorderForegroundColor(yakuiVar.Colors.fx.r, yakuiVar.Colors.fx.g, yakuiVar.Colors.fx.b)
		Map.SetBorderBackgroundAlpha(yakuiVar.Panel.Panelalpha)
		Map.SetBorderForegroundAlpha(yakuiVar.Panel.fxalpha)
		if yakuiVar.Panel.alphatoggle == true then 
			Map.SetMapAlpha(yakuiVar.Panel.alpha)
		else
			Map.SetMapAlpha(1)
		end	
	end
end

function yakui.texture()
	--if yakuiVar.Huduf.ignore == false and Effigy then
	if Effigy then
		--unitframes
		local bars = {
			"PlayerHP", "FriendlyTarget", "HostileTarget", "Pet", "MouseoverTarget",
			"GroupMember1", "GroupMember2", "GroupMember3", "GroupMember4", "GroupMember5",
			"TargetsFriendly1", "TargetsFriendly2", "TargetsFriendly3", "TargetsFriendly4", "TargetsFriendly5", "TargetsFriendly6",
			--"TargetsHostile1", "TargetsHostile2", "TargetsHostile3", "TargetsHostile4", "TargetsHostile5", "TargetsHostile6"
		}
		for _, v in ipairs (bars) do
			bar =  Effigy.Bars[v]
			bar.images["Background"].texture.name = "SharedMediaY"..yakuiVar.Panel.tex.."UFBG"
			bar.images["Foreground"].texture.name = "SharedMediaY"..yakuiVar.Panel.tex.."UFFX"
			if bar.setup then
				bar:setup()
				bar:render()
			end
		end
		--bars
		bars = {
			"PlayerHP", "FriendlyTarget", "HostileTarget", "Pet", "MouseoverTarget", "PlayerCareer", "PlayerAP",
			"GroupMember1", "GroupMember2", "GroupMember3", "GroupMember4", "GroupMember5",
			"TargetsFriendly1", "TargetsFriendly2", "TargetsFriendly3", "TargetsFriendly4", "TargetsFriendly5", "TargetsFriendly6",
			--"TargetsHostile1", "TargetsHostile2", "TargetsHostile3", "TargetsHostile4", "TargetsHostile5", "TargetsHostile6"
		}
		for _, v in ipairs (bars) do
			bar = Effigy.Bars[v]
			bar.fg.texture.name = "SharedMediaY"..yakuiVar.Panel.tex.."UFBar"
			if bar.setup then
				bar:setup()
				bar:render()
			end
		end
		--castbar
		Effigy.Bars["Castbar"].images["Background"].texture.name = "SharedMediaY"..yakuiVar.Panel.tex.."CBBG"
		Effigy.Bars["Castbar"].images["Foreground"].texture.name = "SharedMediaY"..yakuiVar.Panel.tex.."CBFX"
		Effigy.Bars["Castbar"].images["CBEnd"].texture.name = "SharedMediaY"..yakuiVar.Panel.tex.."CBBarEnd"
		Effigy.Bars["Castbar"].fg.texture.name = "SharedMediaY"..yakuiVar.Panel.tex.."CBBar"
		if Effigy.Bars["Castbar"].setup then
			Effigy.Bars["Castbar"]:setup()
			Effigy.Bars["Castbar"]:render()
		end
		--infopanel
		Effigy.Bars["InfoPanel"].images["Background"].texture.name = "SharedMediaY"..yakuiVar.Panel.tex.."IPBG"
		Effigy.Bars["InfoPanel"].images["Foreground"].texture.name = "SharedMediaY"..yakuiVar.Panel.tex.."IPFX"
		if Effigy.Bars["InfoPanel"].setup then
			Effigy.Bars["InfoPanel"]:setup()
			Effigy.Bars["InfoPanel"]:render()
		end

		yakui.RenderPanel()
	end
	if Map then Map.SetBorderForegroundFRI("Map_YakBorderForeground"..yakuiVar.Panel.tex) end
	if yakuiVar.Skin.active == true then yakuiSkin.UpdateAllBorders() end
end

function yakui.healall()
	--d(".Heal")
	cost = MoneyFrame.FormatMoneyString(EA_Window_InteractionHealer.costToRemoveSinglePenalty * EA_Window_InteractionHealer.penaltyCount)
	yakui.Print(towstring(L"You have cleansed yourself of "..EA_Window_InteractionHealer.penaltyCount.. L" penalties" ))
	yakui.Print(towstring(L"This cost you "..cost ))
	EA_Window_InteractionHealer.HealAllPenalties()
end

function yakui.macrosdw() yakui.macrosnewtex(18) end
function yakui.macrosup() yakui.macrosnewtex(-18) end

function yakui.macrosnewtex(startId)
	if macroId+startId+EA_Window_Macro.NUM_MACRO_ICONS > 1688 or macroId+startId < 1 then return end
	macroId = macroId + startId
	for  slot = 1, EA_Window_Macro.NUM_MACRO_ICONS do
		local texture, x, y = GetIconData( macroId + slot )
		DynamicImageSetTexture( "MacroIconSelectionWindowIconSlot"..slot.."IconBase", texture, x, y )
	end
end

function yakui.macrosselect(...)
	yakui.macroshookselect(...)
	pcall(yakui.macrosnewicon)
end

function yakui.macrosnewicon()
	local slot = WindowGetId(SystemData.ActiveWindow.name)
	EA_Window_Macro.iconNum = macroId + slot
	local texture, x, y = GetIconData( EA_Window_Macro.iconNum )
	DynamicImageSetTexture( "EA_Window_MacroDetailsIconIconBase", texture, x, y )
end

function yakui.guildscroll()
    if not (GuildWindow and GuildWindow.IsPlayerInAGuild()) then
        return false
    end
    if (not GameData.Guild.m_GuildRank) or GameData.Guild.m_GuildRank < 17 then
        return false
    end
    -- 178 = The Viper Pit, 198 = Sigmar's Hammer
    if GameData.Player.zone == 178 or GameData.Player.zone == 198 then
        local itemData = DataUtils.GetItems()
        for k,v in pairs(itemData) do
            -- 65824 = order GRS, 65825 = destro GRS
            if v.uniqueID == 65824 or v.uniqueID == 65825 then
                -- Found a scroll, don't need to buy one.
                return false
            end
        end
        -- We didn't find any scrolls in inventory, so yes, we should probably buy some.
        return true
    else
        -- We're not in a guild tavern, so we don't care about buying scrolls.
        return false
    end
end

function yakui.guildscrollfly()
    SystemData.ActiveWindow.name = selectedFlightButton
    oldFlightPointLButtonUp()
end

function yakui.guildscrollhook()
    if not yakui.guildscroll() then
        oldFlightPointLButtonUp()
    else
        selectedFlightButton = SystemData.ActiveWindow.name
        DialogManager.MakeTwoButtonDialog(
            L"You're out of Guild Recall Scrolls!",
            L"Continue", yakui.guildscrollfly,
            L"Cancel", nil)
    end
end

--
-- TargetLast
--
-- ToDo: Move to Effigy State
--[[TargetLastSettings = {}

yakui.TargetLoldFriendlyTarget = nil
TargetLastSettings.isFriendlyTargetSticky = false
TargetLastSettings.isHostileTargetSticky = false

local escapeFlag = false

local function GetTargetName(target)
    if not TargetInfo:UnitIsNPC(target) then
        return TargetInfo:UnitName(target)
    end
	return nil
end

local function TargetPlayer(playerName) SendChatText( L"/target "..playerName:match(L"([^^]+)^?([^^]*)"), L"" ) end
function yakui.TargetLSetEscapeFlag() escapeFlag = true end

function yakui.TargetLUpdateTargets(targetClass, targetId, targetType)
    
    TargetInfo:UpdateFromClient()
    
    if targetClass == "selffriendlytarget" then
        local escaped = escapeFlag
        escapeFlag = false
        
        if yakui.TargetLisFriendlyTargetLocked then
            targetname = GetTargetName(targetClass)
            if targetname ~= yakui.TargetLlockedFriendlyTarget then
                TargetPlayer(yakui.TargetLlockedFriendlyTarget)
            end
        else
            targetname = GetTargetName(targetClass)
            if targetname == nil then return end
            if targetname == L"" then
                if TargetLastSettings.isFriendlyTargetSticky and yakui.TargetLlastKnownFriendlyTarget and (not escaped) then
                    TargetPlayer(yakui.TargetLlastKnownFriendlyTarget)
                    return
                else
                    targetname = GameData.Player.name
                end
            end
            if targetname ~= yakui.TargetLlastKnownFriendlyTarget then
                  yakui.TargetLoldFriendlyTarget = yakui.TargetLlastKnownFriendlyTarget
                  yakui.TargetLlastKnownFriendlyTarget = targetname
            end
        end
    end
    
    if targetClass == "selfhostiletarget" then
        targetname = GetTargetName(targetClass)
        if targetname == nil then return end
        if targetname == L"" then
            if TargetLastSettings.isHostileTargetSticky and yakui.TargetLlastKnownHostileTarget then
                TargetPlayer(yakui.TargetLlastKnownHostileTarget)
            end
            return
        end
        if targetname ~= yakui.TargetLlastKnownHostileTarget then
            if yakui.TargetLlastKnownHostileTarget ~= L"" then
                yakui.TargetLoldHostileTarget = yakui.TargetLlastKnownHostileTarget
            end
            yakui.TargetLlastKnownHostileTarget = targetname
        end
    end
    
end

function TargetLastFriendlyTarget()
    if yakui.TargetLoldFriendlyTarget then
        TargetPlayer(yakui.TargetLoldFriendlyTarget)
    end
end

function TargetLastHostileTarget()
    if yakui.TargetLoldHostileTarget then
        TargetPlayer(yakui.TargetLoldHostileTarget)
    end
end

function ToggleFriendlyTargetLock()
    if yakui.TargetLisFriendlyTargetLocked then
        yakui.TargetLisFriendlyTargetLocked = false
        yakui.Print(L"Friendly target unlocked.")
    else
        targetname = GetTargetName("selffriendlytarget")
        if targetname == nil then
            yakui.Print(L"Can't lock an NPC friendly target.")
            return
        end
        if targetname == L"" then targetname = GameData.Player.name end
        yakui.TargetLlockedFriendlyTarget = targetname
        yakui.TargetLisFriendlyTargetLocked = true
        yakui.Print(L"Friendly target locked to "..targetname..L".")
    end
end

function ToggleFriendlyTargetSticky()
    TargetLastSettings.isFriendlyTargetSticky = not TargetLastSettings.isFriendlyTargetSticky
    if TargetLastSettings.isFriendlyTargetSticky then
        yakui.Print(L"Friendly target is now sticky.")
    else
        yakui.Print(L"Friendly target is now nonsticky.")
    end
end

function ToggleHostileTargetSticky()
    TargetLastSettings.isHostileTargetSticky = not TargetLastSettings.isHostileTargetSticky
    if TargetLastSettings.isHostileTargetSticky then
        yakui.Print(L"Hostile target is now sticky.")
    else
        yakui.Print(L"Hostile target is now nonsticky.")
    end
end
]]--
-- /TargetLast

--
-- Alert Limit
--
local TIME_OUT = 5
ALRTLMT = {}
local alertlimitlastMessage = L""
local alertlimitsinceMessage = 0
function yakui.alertlimitInit()
	RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakui.alertlimitOnUpdate")
	alertlimitlastMessage = L""
	alertlimitsinceMessage = 0

	-- Hooks
	yakui.alertlimitSetAlertDataHook = AlertTextWindow.SetAlertData
	AlertTextWindow.SetAlertData = yakui.alertlimitSetAlertData
end

function yakui.alertlimitOnUpdate(elapsed)
	-- Only if we have a message
	if (alertlimitlastMessage ~= L"") then
		-- How long has it been?
		alertlimitsinceMessage = alertlimitsinceMessage + elapsed
		if (alertlimitsinceMessage > TIME_OUT) then
			-- Time to reset
			alertlimitlastMessage = L""
			alertlimitsinceMessage = 0
		end
	end
end

function yakui.alertlimitSetAlertData(type, text)
	local displayMessage = false
	for _, message in pairs(text) do
		if (message ~= alertlimitlastMessage) then
			displayMessage = true
			alertlimitlastMessage = message
			alertlimitsinceMessage = 0
		end
	end
	if yakuiVar.limitalert == false then displayMessage = true end
	if (displayMessage) then
		yakui.alertlimitSetAlertDataHook(type, text)
	end
end
--
-- /Alert Limit
--

function yakui.relogafterprofileselection()
	-- KwestorTracker.NubLButtonUp()
	WindowSetShowing("UiModVersionMismatchWindow", false)
	if EA_Window_UiProfileImport then EA_Window_UiProfileImport.End() end
	if EA_Window_UiProfileIntroDialog then EA_Window_UiProfileIntroDialog.End() end
	TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"YakUI will now log off to complete the installation.")
	CreateWindow("YOnScreenMessage",true)
	LabelSetText("YOnScreenMessage_Label", L"YakUI will now log off to complete the installation.")
	--LabelSetText("YOnScreenMessage_Label", L"YakUI will now reload to complete the installation.")
	yakui.CareerResourceHack()
	--[[for _, dataIndex in ipairs( UiModWindow.modsListDataDisplayOrder ) do    
        local modData = UiModWindow.modsListData[ dataIndex ]
        if (yakui.modslist[modData.name]) and ( modData.isEnabled ~= nil ) and ( modData.isEnabled ~= true ) then          
            modData.isEnabled = true
        end    	
    end 
	UiModWindow.SaveAndReloadMods()]]--
	Effigy.LoadLayout("YakUI_140")
	BroadcastEvent( SystemData.Events.LOG_OUT )
end

function yakui.postloading()
	if yakuiVar.Skin.active == true then yakuiSkin.initialize() end
	TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, towstring(yakuiVar.ver)..L" initialized...")
	--core addon check
	if Effigy and Effigy.Initialize then 
		--yakuiVar.Warnings.Huduf = 1
	else 
		--if yakuiVar.Warnings.Huduf < 4 then 
			--yakuiVar.Warnings.Huduf = yakuiVar.Warnings.Huduf+1
			--TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: Effigy not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.Huduf)..L" more times)")
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: Effigy not found!")
		--end
	end

	-- Enemy hook
	if Enemy then
		DynamicImageSetTexture ("EnemyIcon", "YakuiEnemyRed", 0, 0)
		yakui.enemyiconhook()
	end

	if TidyChat then 
		yakuiVar.Warnings.TChat = 1
		WindowSetShowing("YakuiFWButton", true)
		WindowSetShowing("YakuiBWButton", true)
		WindowSetShowing("YakChatdisplay", true)
		yakui.tidychathook()
	else 
		if yakuiVar.Warnings.TChat < 4 then 
			yakuiVar.Warnings.TChat = yakuiVar.Warnings.TChat+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: TidyChat not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.TChat)..L" more times)")
		end
		WindowSetShowing("YakuiFWButton", false)
		WindowSetShowing("YakuiBWButton", false)
		WindowSetShowing("YakChatdisplay", false)
	end

	if TidyRoll then 
		yakuiVar.Warnings.TRoll = 1 
	else 
		if yakuiVar.Warnings.TRoll < 4 then 
			yakuiVar.Warnings.TRoll = yakuiVar.Warnings.TRoll+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: TidyRoll not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.TRoll)..L" more times)")
		end
	end

	if ScenarioStats then 
		yakuiVar.Warnings.ScenarioStats = 1 
	else 
		if yakuiVar.Warnings.ScenarioStats < 4 then 
			yakuiVar.Warnings.ScenarioStats = yakuiVar.Warnings.ScenarioStats+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: ScenarioStats not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.ScenarioStats)..L" more times)")
		end
	end

	if Phantom then 
		yakuiVar.Warnings.Phantom = 1 
	else 
		if yakuiVar.Warnings.Phantom < 4 then 
			yakuiVar.Warnings.Phantom = yakuiVar.Warnings.Phantom+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: Phantom not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.Phantom)..L" more times)")
		end
	end

	if Dascore then 
		yakuiVar.Warnings.DaScore = 1 
	else 
		if yakuiVar.Warnings.DaScore < 4 then 
			yakuiVar.Warnings.DaScore = yakuiVar.Warnings.DaScore+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: DaScore not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.DaScore)..L" more times)")
		end
	end

	--[[if Effigy then 
		yakuiVar.Warnings.Targets = 1
		WindowSetShowing("YakuiTTButton", true)
	else 
		if yakuiVar.Warnings.Targets < 4 then 
			yakuiVar.Warnings.Targets = yakuiVar.Warnings.Targets+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: Effigy not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.Targets)..L" more times)")
		end
		WindowSetShowing("YakuiTTButton", false)
	end]]--

	--[[if SOR then 
		yakuiVar.Warnings.SOR = 1]]--
		WindowSetShowing("YakuiSORButton", true)
	--[[else 
		if yakuiVar.Warnings.SOR < 4 then 
			yakuiVar.Warnings.SOR = yakuiVar.Warnings.SOR+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: State Of Realm not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.SOR)..L" more times)")
		end
		WindowSetShowing("YakuiSORButton", false)
	end]]--

	if Squared then 
		yakuiVar.Warnings.Squared = 1 
	else 
		if yakuiVar.Warnings.Squared < 4 then 
			yakuiVar.Warnings.Squared = yakuiVar.Warnings.Squared+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: Squared not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.Squared)..L" more times)")
		end
	end

	if DAoCBuff then 
		yakuiVar.Warnings.DaocBuff = 1
	else 
		if yakuiVar.Warnings.DaocBuff < 4 then 
			yakuiVar.Warnings.DaocBuff = yakuiVar.Warnings.DaocBuff+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: DaoCBuff not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.DaocBuff)..L" more times)")
		end
	end

	if WARCommander then 
		yakuiVar.Warnings.Warcommander = 1
		WindowSetShowing("YakuiWCButton", true)
	else 
		if yakuiVar.Warnings.Warcommander < 4 then 
			yakuiVar.Warnings.Warcommander = yakuiVar.Warnings.Warcommander+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: WarCommander not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.Warcommander)..L" more times)")
		end
		WindowSetShowing("YakuiWCButton", false)
	end

	--[[if Minmap then 
		yakuiVar.Warnings.Minmap = 1
		yakui.minmapmwmtogglehook()
		--RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakui.minmaphookdel")
	else 
		if yakuiVar.Warnings.Minmap < 4 then 
			yakuiVar.Warnings.Minmap = yakuiVar.Warnings.Minmap+1
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: Minmap not found! (This warning will be shown "..towstring(4-yakuiVar.Warnings.Minmap)..L" more times)")
		end
	end]]--
	
	--[[local screenWidth, screenHeight = GetScreenResolution()
	local resocheck = towstring(screenWidth)..L"x"..towstring(screenHeight)
	if resocheck == not towstring(yakuiVar.Panel.res) then
		TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"WARNING: Game resolution and YakUI profile don't match!")
	end]]--

	-- dammaz kron stuff
	if DK_Config and DK_Config.OnUpdateSettingButton and Moth and Moth.Initialize then
			DK_Config.ToggleSetting( "MiscMoth" , 0 )
	end
	
	-- 2nd Start only
	if yakuiVar.counter == 2 then
		yakuiUpdate.afterloading()
	else
		-- was firststeps open before reloading ui?
		if yakuiVar.firststepsopen == true then yakuifirststeps.show() end	
	end

	-- infobar loc module needs delay or libsurveyor won't be available at start
	yakuiInfo.loc_moduleentry_point()
	if yakuiVar.loc == true then
		WindowSetShowing("i_loc", true)
		RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED,"yakuiInfo.loc_moduleupdate")
	else
		WindowSetShowing("i_loc", false)
	end
	if DoesWindowExist("DammazKronHTS") == true then WindowSetShowing("DammazKronHTS", false) end
	if DoesWindowExist("DammazKronHTS") == true and DoesWindowExist("HostileTarget") == true then
		WindowClearAnchors("DammazKronHTS")
		WindowAddAnchor("DammazKronHTS", "bottomright", "HostileTarget", "bottomleft" , 20 , -20 )
		LayoutEditor.UnregisterWindow("DammazKronHTS")
	end
	
	RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakui.getinitmoralscale")
end

function yakui.getinitmoralscale()
	if DoesWindowExist("EA_MoraleBarContentsButton1CooldownTimer") then
		UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakui.getinitmoralscale")
		moraleInitScale = WindowGetScale("EA_MoraleBarContentsButton1CooldownTimer")
		if moraleEventhandler == false then
			RegisterEventHandler(SystemData.Events.PLAYER_MORALE_UPDATED, "yakui.setmoraleCDsize")
			moraleEventhandler = true
		end
	end
end

function yakui.setmoraleCDsize()
	if moraleEventhandler == true then 
		UnregisterEventHandler(SystemData.Events.PLAYER_MORALE_UPDATED, "yakui.setmoraleCDsize")
		moraleEventhandler = false
	end
	if yakuiVar.moraleCDsize == true then 
		WindowSetScale("EA_MoraleBarContentsButton1CooldownTimer", 1.7 * moraleInitScale)
		WindowSetScale("EA_MoraleBarContentsButton2CooldownTimer", 1.7 * moraleInitScale)
		WindowSetScale("EA_MoraleBarContentsButton3CooldownTimer", 1.7 * moraleInitScale)
		WindowSetScale("EA_MoraleBarContentsButton4CooldownTimer", 1.7 * moraleInitScale)
	end
end

function yakui.tidychathook()
	oldTidyChatOptionsOnApply = TidyChat.Options.OnApply
	TidyChat.Options.OnApply =
		function(...)
			oldTidyChatOptionsOnApply(...)
			if TidyChat.Settings[1].chatwindow_tabs_showing == false then
				WindowSetShowing("YakuiFWButton", true)
				WindowSetShowing("YakuiBWButton", true)
				WindowSetShowing("YakChatdisplay", true)
			else
				WindowSetShowing("YakuiFWButton", false)
				WindowSetShowing("YakuiBWButton", false)
				WindowSetShowing("YakChatdisplay", false)
			end
		end
end

local delaycounter
local targetfunc

function yakui.delay(del, func) --del = #of updates, func = functionname without ""!!!
	delaycounter = del
	targetfunc = func
	RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakui.delay2")
end

function yakui.delay2()
	delaycounter = delaycounter - 1
	if delaycounter == 0 then
		UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakui.delay2")
		targetfunc()
	end
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------

function yakui.zonechange()																							-- Morale
	if moraleEventhandler == false then 
		RegisterEventHandler(SystemData.Events.PLAYER_MORALE_UPDATED, "yakui.setmoraleCDsize") 
		moraleEventhandler = true
	end
end

function yakui.onload()																								-- Morale
	WindowSetShowing("EA_MoraleBarContentsBackground", false)
	WindowSetShowing("EA_MoraleBarContentsOverlay", false)
	WindowClearAnchors("EA_MoraleBarContentsButton3")
	WindowAddAnchor( "EA_MoraleBarContentsButton3", "bottomleft", "EA_MoraleBar", "bottomleft", 181, -10 )
	WindowClearAnchors("EA_MoraleBarContentsButton4")
	WindowAddAnchor( "EA_MoraleBarContentsButton4", "bottomleft", "EA_MoraleBar", "bottomleft", 249, -10 )
	if moraleEventhandler == false then 
		RegisterEventHandler(SystemData.Events.PLAYER_MORALE_UPDATED, "yakui.setmoraleCDsize") 
		moraleEventhandler = true
	end
end

function yakui.onreload()																							-- Morale + Dammaz Kron
	WindowSetShowing("EA_MoraleBarContentsBackground", false)
	WindowSetShowing("EA_MoraleBarContentsOverlay", false)
	WindowClearAnchors("EA_MoraleBarContentsButton3")
	WindowAddAnchor( "EA_MoraleBarContentsButton3", "bottomleft", "EA_MoraleBar", "bottomleft", 181, -10 )
	WindowClearAnchors("EA_MoraleBarContentsButton4")
	WindowAddAnchor( "EA_MoraleBarContentsButton4", "bottomleft", "EA_MoraleBar", "bottomleft", 249, -10 )
	if DoesWindowExist("DammazKronHTS") == true then WindowSetShowing("DammazKronHTS", false) end
	if DoesWindowExist("DammazKronHTS") == true and DoesWindowExist("HostileTarget") == true then
		WindowClearAnchors("DammazKronHTS")
		WindowAddAnchor("DammazKronHTS", "bottomright", "HostileTarget", "bottomleft" , 20 , 0 )
		LayoutEditor.UnregisterWindow("DammazKronHTS")
	end
end

function yakui.enemyiconhook()																						-- Enemy
	if yakuiVar.DEVmode == true then yakui.Print("YakUI: enemyiconhook") end
	local oldEnemyUI_Icon_Switch = Enemy.UI_Icon_Switch
	Enemy.UI_Icon_Switch =
		function(on,...)
			oldEnemyUI_Icon_Switch(on,...)
			if (on) then
				DynamicImageSetTexture ("EnemyIcon", "YakuiEnemyGreen", 0, 0)
			else
				DynamicImageSetTexture ("EnemyIcon", "YakuiEnemyRed", 0, 0)
			end
		end
end

function yakui.reloadui() InterfaceCore.ReloadUI() end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
