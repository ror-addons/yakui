-- yakuiFirststeps.lua build20100524

yakuifirststeps = {}

local tourcounter = 0

local function CreateOrShowWindow(name)
	if not DoesWindowExist(name) then
		CreateWindow(name, true)
	else
		WindowSetShowing(name, true)
	end
end

function yakuifirststeps.show()
	yakuiVar.firststepsopen = true
	CreateOrShowWindow("YPointer", false)
	CreateOrShowWindow("YFirstSteps", true)
	if yakuiVar.Skin.active == true then
		yakuiSkin.FirstStepsCreate()
	end
	CreateOrShowWindow("YFirstStepsHyperText1")
	WindowClearAnchors("YFirstStepsHyperText1")
	WindowAddAnchor("YFirstStepsHyperText1", "topleft", "YFirstSteps", "topleft", 185, 230)
	WindowSetParent("YFirstStepsHyperText1","YFirstSteps")
	CreateOrShowWindow("YFirstStepsHyperText2", true)
	WindowClearAnchors("YFirstStepsHyperText2")
	WindowAddAnchor("YFirstStepsHyperText2", "topleft", "YFirstSteps", "topleft", 185, 260)
	WindowSetParent("YFirstStepsHyperText2","YFirstSteps")
	CreateOrShowWindow("YFirstStepsHyperText3", true)
	WindowClearAnchors("YFirstStepsHyperText3")
	WindowAddAnchor("YFirstStepsHyperText3", "topleft", "YFirstSteps", "topleft", 185, 290)
	WindowSetParent("YFirstStepsHyperText3","YFirstSteps")
	CreateOrShowWindow("YFirstStepsHyperText4", true)
	WindowClearAnchors("YFirstStepsHyperText4")
	WindowAddAnchor("YFirstStepsHyperText4", "topleft", "YFirstSteps", "topleft", 185, 320)
	WindowSetParent("YFirstStepsHyperText4","YFirstSteps")
	CreateOrShowWindow("YFirstStepsHyperText5", true)
	WindowClearAnchors("YFirstStepsHyperText5")
	WindowAddAnchor("YFirstStepsHyperText5", "topleft", "YFirstSteps", "topleft", 185, 350)
	WindowSetParent("YFirstStepsHyperText5","YFirstSteps")
	LabelSetText("YFirstSteps_Heading", towstring(yakuiVar.ver)..L" First Steps:")
	LabelSetText("YFirstSteps_Text1", L"YakUI has been installed to your characters profile.")
	LabelSetText("YFirstSteps_Text2", L"Now it is time to set the most important settings")
	LabelSetText("YFirstSteps_Text3", L"and have a look at the basic functions:")
	LabelSetText("YFirstSteps_Text4", L"")
	LabelSetText("YFirstSteps_Text5", L"")
	LabelSetText("YFirstSteps_TextBottom", L"This window can be opened with the YakUI Config")
	LabelSetText("YFirstStepsHyperText1_Text", L"Keybindings")
	LabelSetText("YFirstStepsHyperText2_Text", L"Loot Options")
	LabelSetText("YFirstStepsHyperText3_Text", L"Manage Addons")
	LabelSetText("YFirstStepsHyperText4_Text", L"YakUI Config")
	LabelSetText("YFirstStepsHyperText5_Text", L"Yaks Tour")
	if tourcounter == 0 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 204,192,43)
	else
		LabelSetTextColor("YFirstStepsHyperText5_Text", 160,0,0)
	end
end

function yakuifirststeps.close()
	if yakuiVar.Skin.active == true then
		yakuiSkin.FirstStepsDestroy()
	end
	DestroyWindow("YFirstSteps")
	DestroyWindow("YFirstStepsHyperText1")
	DestroyWindow("YFirstStepsHyperText2")
	DestroyWindow("YFirstStepsHyperText3")
	DestroyWindow("YFirstStepsHyperText4")
	DestroyWindow("YFirstStepsHyperText5")
	DestroyWindow("YPointer")
	yakuiVar.firststepsopen = nil
end

function yakuifirststeps.firststepshypertext1()
	WindowUtils.ToggleShowing( "KeyMappingWindow" )
end

function yakuifirststeps.firststepshypertext1mon()
	LabelSetTextColor("YFirstStepsHyperText1_Text", 255,255,255)
	LabelSetText("YFirstSteps_Text1", L"YakUI installation resets all keybindings. Open the")
	LabelSetText("YFirstSteps_Text2", L"keybind window and alter the settings, or logoff and")
	LabelSetText("YFirstSteps_Text3", L"replace the following file with your backup file:")
	LabelSetText("YFirstSteps_Text4", L"/user/settings/<server>/<character>/<profilename>")
	LabelSetText("YFirstSteps_Text5", L"-> ProfileSettings.xml")
end

function yakuifirststeps.firststepshypertext1moff()
	LabelSetTextColor("YFirstStepsHyperText1_Text", 204,192,43)
	LabelSetText("YFirstSteps_Text1", L"YakUI has been installed to your characters profile.")
	LabelSetText("YFirstSteps_Text2", L"Now it is time to set the most important settings")
	LabelSetText("YFirstSteps_Text3", L"and have a look at the basic functions:")
	LabelSetText("YFirstSteps_Text4", L"")
	LabelSetText("YFirstSteps_Text5", L"")
end

function yakuifirststeps.firststepshypertext2()
	yakuiInfo.menu_module.rclick4()
end

function yakuifirststeps.firststepshypertext2mon()
	LabelSetTextColor("YFirstStepsHyperText2_Text", 255,255,255)
	WindowSetShowing("YPointer", true)
	WindowClearAnchors("YPointer")
	WindowAddAnchor("YPointer", "topright", "i_menu4_icon", "bottomleft", -5, 0)
	LabelSetText("YFirstSteps_Text1", L"All autoloot settings are off after the installation")
	LabelSetText("YFirstSteps_Text2", L"and you will have to configure them. They can easily")
	LabelSetText("YFirstSteps_Text3", L"be accessed by rightclicking on the bag icon in the")
	LabelSetText("YFirstSteps_Text4", L"infobar.")
	LabelSetText("YFirstSteps_Text5", L"")
end

function yakuifirststeps.firststepshypertext2moff()
	LabelSetTextColor("YFirstStepsHyperText2_Text", 204,192,43)
	WindowSetShowing("YPointer", false)
	LabelSetText("YFirstSteps_Text1", L"YakUI has been installed to your characters profile.")
	LabelSetText("YFirstSteps_Text2", L"Now it is time to set the most important settings")
	LabelSetText("YFirstSteps_Text3", L"and have a look at the basic functions:")
	LabelSetText("YFirstSteps_Text4", L"")
	LabelSetText("YFirstSteps_Text5", L"")
end

function yakuifirststeps.firststepshypertext3()
	yakuiInfo.menu_module.rclick6()
end

function yakuifirststeps.firststepshypertext3mon()
	LabelSetTextColor("YFirstStepsHyperText3_Text", 255,255,255)
	WindowSetShowing("YPointer", true)
	WindowClearAnchors("YPointer")
	WindowAddAnchor("YPointer", "topright", "i_menu6_icon", "bottomleft", -5, 0)
	LabelSetText("YFirstSteps_Text1", L"YakUI brings several additional addons, that are")
	LabelSetText("YFirstSteps_Text2", L"inactive at the first run. Open the addon manager to")
	LabelSetText("YFirstSteps_Text3", L"activate those you need. The addon manager can be")
	LabelSetText("YFirstSteps_Text4", L"accessed by rightclicking on the computer icon in the")
	LabelSetText("YFirstSteps_Text5", L"infobar.")
end

function yakuifirststeps.firststepshypertext3moff()
	LabelSetTextColor("YFirstStepsHyperText3_Text", 204,192,43)
	WindowSetShowing("YPointer", false)
	LabelSetText("YFirstSteps_Text1", L"YakUI has been installed to your characters profile.")
	LabelSetText("YFirstSteps_Text2", L"Now it is time to set the most important settings")
	LabelSetText("YFirstSteps_Text3", L"and have a look at the basic functions:")
	LabelSetText("YFirstSteps_Text4", L"")
	LabelSetText("YFirstSteps_Text5", L"")
end

function yakuifirststeps.firststepshypertext4()
	yakuiconfig.toggle()
end

function yakuifirststeps.firststepshypertext4mon()
	LabelSetTextColor("YFirstStepsHyperText4_Text", 255,255,255)
	WindowSetShowing("YPointer", true)
	WindowClearAnchors("YPointer")
	WindowAddAnchor("YPointer", "topright", "YakuiButton", "bottomleft", -5, 0)
	LabelSetText("YFirstSteps_Text1", L"YakUI can be customized in many ways. Open the")
	LabelSetText("YFirstSteps_Text2", L"menu by middlemouse click on the y-button, or")
	LabelSetText("YFirstSteps_Text3", L"/yui config. Also the settings of the core addons can")
	LabelSetText("YFirstSteps_Text4", L"be entered there.")
	LabelSetText("YFirstSteps_Text5", L"")
end

function yakuifirststeps.firststepshypertext4moff()
	LabelSetTextColor("YFirstStepsHyperText4_Text", 204,192,43)
	WindowSetShowing("YPointer", false)
	LabelSetText("YFirstSteps_Text1", L"YakUI has been installed to your characters profile.")
	LabelSetText("YFirstSteps_Text2", L"Now it is time to set the most important settings")
	LabelSetText("YFirstSteps_Text3", L"and have a look at the basic functions:")
	LabelSetText("YFirstSteps_Text4", L"")
	LabelSetText("YFirstSteps_Text5", L"")
end

function yakuifirststeps.firststepshypertext5()
	tourcounter = tourcounter + 1
	if tourcounter > 34 then tourcounter = 0 end
	if tourcounter == 0 then 
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
	else
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,0,0)
	end
	yakuifirststeps.firststepshypertext5mon()
end

function yakuifirststeps.firststepshypertext5r()
	tourcounter = 0
	LabelSetTextColor("YFirstStepsHyperText5_Text", 204,192,43)
	WindowSetShowing("YPointer", false)
	yakuifirststeps.firststepshypertext5mon()
end

function yakuifirststeps.firststepshypertext5mon()
	if tourcounter == 0 then 
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Yaks Tour will introduce more elements of the")
		LabelSetText("YFirstSteps_Text2", L"interface and provides further information.")
		LabelSetText("YFirstSteps_Text3", L"Leftclick to advance to the next element.")
		LabelSetText("YFirstSteps_Text4", L"Rightclick to reset the tour.")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", false)

	elseif tourcounter == 1 then
		if Effigy then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Unitframes:")
		LabelSetText("YFirstSteps_Text2", L"Your HP will be displayed here. Above")
		LabelSetText("YFirstSteps_Text3", L"the HP the buffs will be displayed. All")
		LabelSetText("YFirstSteps_Text4", L"debuffs are displayed seperatly below your")
		LabelSetText("YFirstSteps_Text5", L"character.")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "PlayerHP", "bottomleft", -85, 0)
		else
			yakui.Print("Effigy not found: skipping Effigy elements...")
			tourcounter = 6
		end
	elseif tourcounter == 2 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Unitframes:")
		LabelSetText("YFirstSteps_Text2", L"Your AP and career resources will be")
		LabelSetText("YFirstSteps_Text3", L"displayed here. If you have the addon")
		LabelSetText("YFirstSteps_Text4", L"CDown active, all your actions on CD")
		LabelSetText("YFirstSteps_Text5", L"will be listed below those bars.")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "PlayerAP", "bottomleft", -85, 0)
	elseif tourcounter == 3 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Unitframes:")
		LabelSetText("YFirstSteps_Text2", L"Hostile Target will be displayed here. Above")
		LabelSetText("YFirstSteps_Text3", L"the HP your debuffs will be displayed, below")
		LabelSetText("YFirstSteps_Text4", L"the HP all other debuffs except your own.")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "HostileTarget", "bottomleft", -85, 0)
	elseif tourcounter == 4 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Unitframes:")
		LabelSetText("YFirstSteps_Text2", L"Your Friendly Target will be displayed")
		LabelSetText("YFirstSteps_Text3", L"here. Your own buffs on the friendly")
		LabelSetText("YFirstSteps_Text4", L"target will be displayed above the bar,")
		LabelSetText("YFirstSteps_Text5", L"all other buffs and debuffs below it.")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "FriendlyTarget", "bottomleft", -85, 0)
	elseif tourcounter == 5 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Unitframes:")
		LabelSetText("YFirstSteps_Text2", L"Your Pets HP will be displayed here.")
		LabelSetText("YFirstSteps_Text3", L"")
		LabelSetText("YFirstSteps_Text4", L"")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "Pet", "bottomleft", -85, 0)
	elseif tourcounter == 6 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Unitframes:")
		LabelSetText("YFirstSteps_Text2", L"Your party members can be displayed here.")
		LabelSetText("YFirstSteps_Text3", L"You can toggle the display on/off with")
		LabelSetText("YFirstSteps_Text4", L"YakUI Config -> Unitframes -> group frames")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "GroupMember3", "bottomleft", -85, 0)


	elseif tourcounter == 7 then
		if Squared then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Squared:")
		LabelSetText("YFirstSteps_Text2", L"Party, Warband and Scenario groups are")
		LabelSetText("YFirstSteps_Text3", L"displayed with Squared. Their HP bar")
		LabelSetText("YFirstSteps_Text4", L"is colored according to the archtype of")
		LabelSetText("YFirstSteps_Text5", L"that class (tanks = red, etc.)")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "SquaredAnchor", "bottomleft", 100, 70)
		else
			yakui.Print("Squared not found: skipping squared element...")
		end
	elseif tourcounter == 8 then
		if ClosetGoblin then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"ClosetGoblin:")
		LabelSetText("YFirstSteps_Text2", L"ClosetGoblin is an equipment manager")
		LabelSetText("YFirstSteps_Text3", L"that lets you define gear presets")
		LabelSetText("YFirstSteps_Text4", L"that can be swapped with a single")
		LabelSetText("YFirstSteps_Text5", L"one mouseclick.")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "ClosetGoblinOptionWindowIcon", "bottomleft", -5, 0)
		else
			yakui.Print("ClosetGoblin not found: skipping ClosetGoblin element...")
		end

	elseif tourcounter == 9 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"The MoraleSet Button lets you chose from up to")
		LabelSetText("YFirstSteps_Text2", L"5 morale configurations. Each configuration")
		LabelSetText("YFirstSteps_Text3", L"can be edited by selecting it and then using")
		LabelSetText("YFirstSteps_Text4", L"the moralebar to change morales for this set.")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "MoraleEditorSetMenu", "bottomleft", -5, 0)
	elseif tourcounter == 10 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"The TacticsEditor is like the original one")
		LabelSetText("YFirstSteps_Text2", L"from the standard ui, but with rightclicking")
		LabelSetText("YFirstSteps_Text3", L"it, you can toggle the visibility of the used")
		LabelSetText("YFirstSteps_Text4", L"set.")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "EA_TacticsEditor", "bottomleft", -5, 10)
	elseif tourcounter == 11 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"The two arrows will switch the chat channels.")
		LabelSetText("YFirstSteps_Text2", L"If you prefer the classic tab appearance, you")
		LabelSetText("YFirstSteps_Text3", L"can use the YakUI config to open the TidyChat")
		LabelSetText("YFirstSteps_Text4", L"options and reactivate the chattabs. This will")
		LabelSetText("YFirstSteps_Text5", L"automatically turn the YakUI chat elements off.")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "YakuiBWButton", "bottomleft", -5, 0)
	elseif tourcounter == 12 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"The name of the current chat tab is displayed")
		LabelSetText("YFirstSteps_Text2", L"here. All functions, that can be accessed by")
		LabelSetText("YFirstSteps_Text3", L"rightclicking on the chattabs in the standard ui")
		LabelSetText("YFirstSteps_Text4", L"can also be accessed here with a rightclick.")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "YakChatdisplay", "bottomleft", -80, 0)
	elseif tourcounter == 13 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		--if SOR then
		--[[LabelSetText("YFirstSteps_Text1", L"State of Realm keeps track of a variaty of")
		LabelSetText("YFirstSteps_Text2", L"RvR information, and allows you to broadcast")
		LabelSetText("YFirstSteps_Text3", L"to Region, Party and Warband.")
		LabelSetText("YFirstSteps_Text4", L"Leftclick: Toggles the SOR mainframe")
		LabelSetText("YFirstSteps_Text5", L"Rightclick: Toggles SOR Options")]]--
		LabelSetText("YFirstSteps_Text1", L"Toggle RvR information")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Leftclick: Toggles the RvR Tracker manual")
		LabelSetText("YFirstSteps_Text4", L"Middleclick: Toggles the RvR Tracker automatic")
		LabelSetText("YFirstSteps_Text5", L"Rightclick: Toggles the Campaign Map Tracker")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "YakuiSORButton", "bottomleft", -5, 0)
		--else
		--	yakui.Print("State Of Realm not found: skipping SOR element...")
		--end
	elseif tourcounter == 14 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		if WARCommander then
		LabelSetText("YFirstSteps_Text1", L"WarCommander is a tool to broadcast RvR infos")
		LabelSetText("YFirstSteps_Text2", L"Spotted enemy, ress request, call target etc.")
		LabelSetText("YFirstSteps_Text3", L"")
		LabelSetText("YFirstSteps_Text4", L"Leftclick: Toggles the WarCommander mainframe")
		LabelSetText("YFirstSteps_Text5", L"Rightclick: Toggles WarCommander Options")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "YakuiWCButton", "bottomleft", -5, 0)
		else
			yakui.Print("WarCommander not found: skipping WarCommander element...")
		end
	--[[elseif tourcounter == 15 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		if Effigy then
		LabelSetText("YFirstSteps_Text1", L"Effigy Targets builds and maintains custom player lists.")
		LabelSetText("YFirstSteps_Text2", L"Effigy Targets can be used to easily heal out-of-group ")
		LabelSetText("YFirstSteps_Text3", L"players or to focus fire on weaker enemies.")
		LabelSetText("YFirstSteps_Text4", L"Leftclick: Toggle enemy target list")
		LabelSetText("YFirstSteps_Text5", L"Rightclick: Toggle friendly target list")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "YakuiTTButton", "bottomleft", -5, 0)
		else
			yakui.Print("Effigy not found: skipping Targets element...")
		end]]--
	elseif tourcounter == 16 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		if Enemy then
		LabelSetText("YFirstSteps_Text1", L"Enemy provides a flexible usage of WARs assist")
		LabelSetText("YFirstSteps_Text2", L"mechanic and adds several features. Mainly it")
		LabelSetText("YFirstSteps_Text3", L"is an assist tool, thats works out-of-group as well.")
		LabelSetText("YFirstSteps_Text4", L"Leftclick: Intercom window")
		LabelSetText("YFirstSteps_Text5", L"Rightclick: More options")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "EnemyIcon", "bottomleft", -5, 0)
		else
			yakui.Print("Enemy not found: skipping Enemy element...")
		end
	elseif tourcounter == 17 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"The YakUI button, or y-button gives you access to")
		LabelSetText("YFirstSteps_Text2", L"most functions of the core addon(s) of YakUI.")
		LabelSetText("YFirstSteps_Text3", L"Leftclick: Toggles grp-icons on/off")
		LabelSetText("YFirstSteps_Text4", L"Middle Mouse Click: Show YakUI config")
		LabelSetText("YFirstSteps_Text5", L"Rightclick: Toggles the world mini map")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "YakuiButton", "bottomleft", -5, 0)
	elseif tourcounter == 18 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"With this little triangle in the corner of the")
		LabelSetText("YFirstSteps_Text2", L"chatframe the alpha of the transparent parts of")
		LabelSetText("YFirstSteps_Text3", L"YakUI can be toggled on/off.")
		LabelSetText("YFirstSteps_Text4", L"")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "YakuiATButton", "bottomleft", -5, 0)
	elseif tourcounter == 19 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"InfoBar abilities icon:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Leftclick: Toggles the abilities window")
		LabelSetText("YFirstSteps_Text4", L"Rightclick: Toggles the abilities training window")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_menu1_icon", "bottomleft", -5, 0)
	elseif tourcounter == 20 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"InfoBar character icon:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Leftclick: Toggles the character window")
		LabelSetText("YFirstSteps_Text4", L"Rightclick: Toggles the mastery training window")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_menu2_icon", "bottomleft", -5, 0)
	elseif tourcounter == 21 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"InfoBar renown & tome tactics:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Leftclick: Toggles the renown tactic window")
		LabelSetText("YFirstSteps_Text4", L"Rightclick: Toggles the tome tactic window")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_menu3_icon", "bottomleft", -5, 0)
	elseif tourcounter == 22 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"InfoBar bag icon:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Leftclick: Toggles your bags")
		LabelSetText("YFirstSteps_Text4", L"Rightclick: Toggles the loot options")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_menu4_icon", "bottomleft", -5, 0)
	elseif tourcounter == 23 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"InfoBar tome icon:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Leftclick: Toggles your tome")
		if DammazKron then 
		LabelSetText("YFirstSteps_Text4", L"Rightclick: Toggles tome to DammazKron page.")
		else
		LabelSetText("YFirstSteps_Text4", L"")
		end
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_menu5_icon", "bottomleft", -5, 0)
	elseif tourcounter == 24 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"InfoBar pc icon:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Leftclick: Toggles perfsettings custom1,custom2")
		LabelSetText("YFirstSteps_Text4", L"Rightclick: Toggles the addon manager")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_menu6_icon", "bottomleft", -5, 0)
	elseif tourcounter == 25 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"InfoBar RVR stats icon:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Leftclick: Toggles DaScore addon")
		LabelSetText("YFirstSteps_Text4", L"Rightclick: Toggles ScenarioStats addon")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_menu7_icon", "bottomleft", -5, 0)
	elseif tourcounter == 26 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"InfoBar search icon:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Leftclick: Open party and warband window")
		LabelSetText("YFirstSteps_Text4", L"Rightclick: server-wide search mode.")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_menu8_icon", "bottomleft", -5, 0)
	elseif tourcounter == 27 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"MoraleBar:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"It displays the amount of morale and the")
		LabelSetText("YFirstSteps_Text4", L"equipted morales. You also need it to alter")
		LabelSetText("YFirstSteps_Text5", L"the moralesets.")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "EA_MoraleBar", "bottomleft", -5, 0)
	elseif tourcounter == 28 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Minmap is a squared replacement of the minimap")
		LabelSetText("YFirstSteps_Text2", L"Most functions are bound to the WAR-symbol.")
		LabelSetText("YFirstSteps_Text3", L"Mouse over and read the tooltip!")
		LabelSetText("YFirstSteps_Text4", L"")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "UI_KwestorTrackerNub", "bottomleft", -50, -50)
	elseif tourcounter == 29 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"This button toggles your questtracker.")
		LabelSetText("YFirstSteps_Text2", L"The addon Kwestor enhances the display and you")
		LabelSetText("YFirstSteps_Text3", L"can access Kwestor options with rightclicking")
		LabelSetText("YFirstSteps_Text4", L"the quest tracker nub.")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "UI_KwestorTrackerNub", "bottomleft", -5, 0)
	elseif tourcounter == 30 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Infobar friendlist:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"This will display the number of your friends")
		LabelSetText("YFirstSteps_Text4", L"and how many of them are online. Mouse over to")
		LabelSetText("YFirstSteps_Text5", L"see a full list, click to open the FL-window.")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_fr_icon", "bottomleft", -5, 0)
	elseif tourcounter == 31 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Infobar guildlist:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"This will display the number of your guildies")
		LabelSetText("YFirstSteps_Text4", L"and how many of them are online. Mouse over to")
		LabelSetText("YFirstSteps_Text5", L"see a full list, click to open the guildwindow.")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_gl_icon", "bottomleft", -5, 0)
	elseif tourcounter == 32 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Infobar Time:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Servertime is displayed here.")
		LabelSetText("YFirstSteps_Text4", L"Leftclick toggles the War Report.")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "snt_info_time_icon", "bottomleft", -5, 0)
	elseif tourcounter == 33 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Infobar rank and realmrank:")
		LabelSetText("YFirstSteps_Text2", L"")
		LabelSetText("YFirstSteps_Text3", L"Note: The displays has the following syntax")
		LabelSetText("YFirstSteps_Text4", L"Realmrank:Percent%")
		LabelSetText("YFirstSteps_Text5", L"Level:Percent%")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "i_rn_icon", "bottomleft", -5, 0)
	elseif tourcounter == 34 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Actionbar 4 provides 7 more slots.")
		LabelSetText("YFirstSteps_Text2", L"I use them for potions and macros, but of")
		LabelSetText("YFirstSteps_Text3", L"course you can use them anyway you want.")
		LabelSetText("YFirstSteps_Text4", L"")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "EA_ActionBar4Action2Action", "bottomleft", -5, 0)
	elseif tourcounter == 35 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"Trakario:")
		LabelSetText("YFirstSteps_Text2", L"This addon keeps track of scenario")
		LabelSetText("YFirstSteps_Text3", L"timers and actions, like who is the flag")
		LabelSetText("YFirstSteps_Text4", L"carrier, or what object is attackable.")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topright", "TrakarioWindow", "bottomleft", -50, 80)
	elseif tourcounter == 36 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 255,255,255)
		LabelSetText("YFirstSteps_Text1", L"EA Trackers:")
		LabelSetText("YFirstSteps_Text2", L"In this area of your UI all the EA trackers")
		LabelSetText("YFirstSteps_Text3", L"like keep objective, battle field objective")
		LabelSetText("YFirstSteps_Text4", L"city tracker, etc. will be displayed.")
		LabelSetText("YFirstSteps_Text5", L"")
		WindowSetShowing("YPointer", true)
		WindowClearAnchors("YPointer")
		WindowAddAnchor("YPointer", "topleft", "EA_Window_KeepObjectiveTracker", "bottomleft", 100, 0)

	end
end

function yakuifirststeps.firststepshypertext5moff()
	if tourcounter == 0 then
		LabelSetTextColor("YFirstStepsHyperText5_Text", 204,192,43)
	else
		LabelSetTextColor("YFirstStepsHyperText5_Text", 160,0,0)
	end
	LabelSetText("YFirstSteps_Text1", L"YakUI has been installed to your characters profile.")
	LabelSetText("YFirstSteps_Text2", L"Now it is time to set the most important settings")
	LabelSetText("YFirstSteps_Text3", L"and have a look at the basic functions:")
	LabelSetText("YFirstSteps_Text4", L"")
	LabelSetText("YFirstSteps_Text5", L"")
	WindowSetShowing("YPointer", false)
end