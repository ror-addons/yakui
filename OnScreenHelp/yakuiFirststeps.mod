<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="YakUI FirstSteps" version="1.35a" date="05/24/2010" >

	<Author name="Yakar" email="" />
	<Description text="This mod introduces the settings and basic functions of YakUI. You may turn it off, if you don't need this anymore." />
	<VersionSettings gameVersion="1.4.1" />

	<Files>
		<File name="yakuiFirststeps.lua" />
		<File name="yakuiFirststeps.xml" />
	</Files>

	<OnInitialize/>

	<OnUpdate/>

	<OnShutdown/>   
	<WARInfo>
			<Categories>
				<Category name="SYSTEM" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
	</WARInfo>
	</UiMod>
</ModuleFile>