<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="YakUI Install Advisor" version="1.4a" date="11/11/2010" >

	<Author name="Wothor aka Philosound" email="" />
	<Description text="This mod is only needed at the first step of the YakUI installation and deactives automatically after a successful install." />
	<VersionSettings gameVersion="1.4.1" />
	
	<Dependencies>
		<Dependency name="EASystem_DialogManager" />
		<Dependency name="EA_UiProfilesWindow" />
		<Dependency name="EA_CurrentEventsWindow" />
		<Dependency name="YakUI" optional="false" forceEnable="true" />
	</Dependencies>

	<Files>
		<File name="yakuiFirstrun.lua" />	
	</Files>

	<OnInitialize>
		<CallFunction name="yakuiFirstrun.Initialize" />
	</OnInitialize>

	<OnUpdate/>

	<OnShutdown/>   
	<WARInfo>
			<Categories>
				<Category name="SYSTEM" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
	</WARInfo>
	</UiMod>
</ModuleFile>