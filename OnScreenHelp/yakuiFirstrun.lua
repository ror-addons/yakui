if not yakuiFirstrun then yakuiFirstrun = {} end

--[[function yakuiFirstrun.Initialize()
	--yakuiFirstrun.Delay()
end]]--

local WINDOWNAME = "EA_Window_UiProfileIntroDialog"
--local firstrundelay

--[[function yakuiFirstrun.Delay()
	firstrundelay = 1
	RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakuiFirstrun.Delay2")
end]]--

local hideWindows = {
	"EA_Window_CurrentEvents",
	"UiModVersionMismatchWindow",
	"EA_Window_UiProfileIntroDialog"
}
--function yakuiFirstrun.Delay2()
function yakuiFirstrun.Initialize()
	--firstrundelay = firstrundelay + 1
	--if firstrundelay == 50 then
		if not yakuiVar or (yakuiVar and not next(yakuiVar)) then
			--UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakuiFirstrun.Delay2")

			-- new stuff
			local uiProfile
			for i, profile in ipairs(CharacterSettingsGetAllUiProfiles()) do
				--[[O.34.character = "Monitor test"
				O.34.name = "YakUI test1"
				O.34.lastUpdated.year = 2011
				O.34.lastUpdated.month = 1
				O.34.lastUpdated.minute = 36
				O.34.lastUpdated.hour = 19
				O.34.lastUpdated.day = 6
				O.34.isShared = false
				O.34.isOwnedByActiveCharacter = false
				O.34.path = "user\settings\YakUI - 1.4 TEST\Monitor test\YakUI test1"
				O.34.server = "YakUI - 1.4 TEST"]]--
				
				-- note: server == L"YakUI" also on pre-1.4; name =="YakUI" not
				if profile.server == L"YakUI" and profile.character == L"YakUI" and profile.name == L"YakUI" then
					uiProfile = profile
					--uiProfile.index = i
				end
			end
			if uiProfile then
				EA_ChatWindow.Print(L"YakUI Settings found.")
				EA_Window_CurrentEvents.SetShowOnLogin( false )
				EA_Window_UiProfileImport.End()
				for _, windowName in ipairs(hideWindows) do
					if DoesWindowExist(windowName) then
						WindowSetShowing(windowName, false)
					end
				end
				--[[DialogManager.MakeTwoButtonDialog( dialogText, button1Text, buttonCallback1, button2Text, buttonCallback2,
                timer, autoRespondButton, warningIsChecked, neverWarnCallback, dialogType, dialogID, windowLayer )]]--
				--EA_Window_ManageUiProfiles.OnNewCharacterImportPromptDoImport(uiProfile)
				DialogManager.MakeTwoButtonDialog(
					L"Welcome to the YakUI Installation.<br>Import YakUI Settings now.",
					GetStringFromTable( "CustomizeUiStrings", StringTables.CustomizeUi.MANAGE_UI_PROFILES_INTRO_IMPORT_BUTTON ),
					function() EA_Window_ManageUiProfiles.OnNewCharacterImportPromptDoImport(uiProfile) end,
					GetStringFromTable( "CustomizeUiStrings", StringTables.CustomizeUi.MANAGE_UI_PROFILES_INTRO_CLOSE_BUTTON )
				)
			else
				DialogManager.MakeOneButtonDialog(
					L"YakUI Settings NOT found!",
					GetStringFromTable( "CustomizeUiStrings", StringTables.CustomizeUi.MANAGE_UI_PROFILES_INTRO_CLOSE_BUTTON ),
					function() ERROR(L"YakUI Settings NOT found!") end
				)
			end
		end
	--end
end

