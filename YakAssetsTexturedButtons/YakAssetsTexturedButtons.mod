<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="YakAssetsTexturedButtons" version="1.0" date="12/24/2010" >
        
	<Author name="Wothor" email="" />
	<Description text="YakUI ActionBar Textures for TexturedButtons" />
	<VersionSettings gameVersion="1.4.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
	<Dependencies>
			<Dependency name="TexturedButtons" />
	</Dependencies>
	
	<Files>
		<File name="TexturedButtons_YTextures.xml" />
		<File name="TexturedButtons_YTextures.lua" />
	</Files>
        
	<OnInitialize/> 
	<SavedVariables/>
	<OnUpdate/>
	<OnShutdown/>   
	<WARInfo>
			<Categories>
				<Category name="SYSTEM" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
	</WARInfo>
	</UiMod>
</ModuleFile>