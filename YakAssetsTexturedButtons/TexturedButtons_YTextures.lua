if TexturedButtons then

	-- Afaict this is a little different from the integrated ones; and I also prefer this one
	TexturedButtons.Register({
		Name = "Y_Gloss",
		Author = "Yakar. Possibly maybe.",
		Description = "Custom glossy textures",
		Textures = {
			Normal = "ActionbarButton_Y_Gloss",
			Highlighted = "ActionbarButton_Y_Gloss",
			Pressed = "ActionbarButton_Y_GlossDown",
			PressedHighlighted = "ActionbarButton_Y_Gloss",
		},
	});

	TexturedButtons.Register({
		Name = "Y_Analog",
		Author = "Yakar. Possibly maybe.",
		Description = "Custom analog textures",
		Textures = {
			Normal = "ActionbarButton_Y_Analog",
			Highlighted = "ActionbarButton_Y_Analog",
			Pressed = "ActionbarButton_Y_AnalogDown",
			PressedHighlighted = "ActionbarButton_Y_Analog",
		},
	});
end