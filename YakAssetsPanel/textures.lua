local textures = {

	[1]="YakSet11920x1200B",
	[2]="YakSet11920x1200P",
	[3]="YakSet11920x1200FX",

	[4]="YakSet11680x1050B",
	[5]="YakSet11680x1050P",
	[6]="YakSet11680x1050FX",

	[7]="YakSet11280x1024B",
	[8]="YakSet11280x1024P",
	[9]="YakSet11280x1024FX",

	[10]="YakSet21920x1200B",
	[11]="YakSet21920x1200P",
	[12]="YakSet21920x1200FX",
	
	[13]="YakSet21680x1050B",
	[14]="YakSet21680x1050P",
	[15]="YakSet21680x1050FX",

	[16]="YakSet21280x1024B",
	[17]="YakSet21280x1024P",
	[18]="YakSet21280x1024FX",

	[19]="YakSet31920x1200B",
	[20]="YakSet31920x1200P",
	[21]="YakSet31920x1200FX",

	[22]="YakSet31680x1050B",
	[23]="YakSet31680x1050P",
	[24]="YakSet31680x1050FX",

	[25]="YakSet31280x1024B",
	[26]="YakSet31280x1024P",
	[27]="YakSet31280x1024FX",

	[28]="YakSet41920x1200B",
	[29]="YakSet41920x1200P",
	[30]="YakSet41920x1200FX",

	[31]="YakSet41680x1050B",
	[32]="YakSet41680x1050P",
	[33]="YakSet41680x1050FX",

	[34]="YakSet41280x1024B",
	[35]="YakSet41280x1024P",
	[36]="YakSet41280x1024FX",

	[37]="YakSet51920x1200B",
	[38]="YakSet51920x1200P",
	[39]="YakSet51920x1200FX",

	[40]="YakSet51680x1050B",
	[41]="YakSet51680x1050P",
	[42]="YakSet51680x1050FX",

	[43]="YakSet51280x1024B",
	[44]="YakSet51280x1024P",
	[45]="YakSet51280x1024FX",
}

local displaynames = {
	[1]="YP_S1_1920x1200_B",
	[2]="YP_S1_1920x1200_P",
	[3]="YP_S1_1920x1200_FX",

	[4]="YP_S1_1680x1050_B",
	[5]="YP_S1_1680x1050_P",
	[6]="YP_S1_1680x1050_FX",

	[7]="YP_S1_1280x1024_B",
	[8]="YP_S1_1280x1024_P",
	[9]="YP_S1_1280x1024_FX",

	[10]="YP_S2_1920x1200_B",
	[11]="YP_S2_1920x1200_P",
	[12]="YP_S2_1920x1200_FX",
	
	[13]="YP_S2_1680x1050_B",
	[14]="YP_S2_1680x1050_P",
	[15]="YP_S2_1680x1050_FX",

	[16]="YP_S2_1280x1024_B",
	[17]="YP_S2_1280x1024_P",
	[18]="YP_S2_1280x1024_FX",

	[19]="YP_S3_1920x1200_B",
	[20]="YP_S3_1920x1200_P",
	[21]="YP_S3_1920x1200_FX",

	[22]="YP_S3_1680x1050_B",
	[23]="YP_S3_1680x1050_P",
	[24]="YP_S3_1680x1050_FX",

	[25]="YP_S3_1280x1024_B",
	[26]="YP_S3_1280x1024_P",
	[27]="YP_S3_1280x1024_FX",

	[28]="YP_S4_1920x1200_B",
	[29]="YP_S4_1920x1200_P",
	[30]="YP_S4_1920x1200_FX",

	[31]="YP_S4_1680x1050_B",
	[32]="YP_S4_1680x1050_P",
	[33]="YP_S4_1680x1050_FX",

	[34]="YP_S4_1280x1024_B",
	[35]="YP_S4_1280x1024_P",
	[36]="YP_S4_1280x1024_FX",

	[37]="YP_S5_1920x1200_B",
	[38]="YP_S5_1920x1200_P",
	[39]="YP_S5_1920x1200_FX",

	[40]="YP_S5_1680x1050_B",
	[41]="YP_S5_1680x1050_P",
	[42]="YP_S5_1680x1050_FX",

	[43]="YP_S5_1280x1024_B",
	[44]="YP_S5_1280x1024_P",
	[45]="YP_S5_1280x1024_FX",

}

local tiled = {
}

local dims = {

	["YakSet11920x1200B"] = {1920,225},
	["YakSet11920x1200P"] = {1920,225},
	["YakSet11920x1200FX"] = {1920,225},

	["YakSet11680x1050B"] = {1680,200},
	["YakSet11680x1050P"] = {1680,200},
	["YakSet11680x1050FX"] = {1680,200},

	["YakSet11280x1024B"] = {1280,1024},
	["YakSet11280x1024P"] = {1280,1024},
	["YakSet11280x1024FX"] = {1280,1024},

	["YakSet21920x1200B"] = {1920,225},
	["YakSet21920x1200P"] = {1920,225},
	["YakSet21920x1200FX"] = {1920,225},
	
	["YakSet21680x1050B"] = {1680,200},
	["YakSet21680x1050P"] = {1680,200},
	["YakSet21680x1050FX"] = {1680,200},

	["YakSet21280x1024B"] = {1280,1024},
	["YakSet21280x1024P"] = {1280,1024},
	["YakSet21280x1024FX"] = {1280,1024},

	["YakSet31920x1200B"] = {1920,225},
	["YakSet31920x1200P"] = {1920,225},
	["YakSet31920x1200FX"] = {1920,225},

	["YakSet31680x1050B"] = {1680,200},
	["YakSet31680x1050P"] = {1680,200},
	["YakSet31680x1050FX"] = {1680,200},

	["YakSet31280x1024B"] = {1280,1024},
	["YakSet31280x1024P"] = {1280,1024},
	["YakSet31280x1024FX"] = {1280,1024},

	["YakSet41920x1200B"] = {1920,225},
	["YakSet41920x1200P"] = {1920,225},
	["YakSet41920x1200FX"] = {1920,225},

	["YakSet41680x1050B"] = {1680,200},
	["YakSet41680x1050P"] = {1680,200},
	["YakSet41680x1050FX"] = {1680,200},

	["YakSet41280x1024B"] = {1280,1024},
	["YakSet41280x1024P"] = {1280,1024},
	["YakSet41280x1024FX"] = {1280,1024},

	["YakSet51920x1200B"] = {1920,225},
	["YakSet51920x1200P"] = {1920,225},
	["YakSet51920x1200FX"] = {1920,225},

	["YakSet51680x1050B"] = {1680,200},
	["YakSet51680x1050P"] = {1680,200},
	["YakSet51680x1050FX"] = {1680,200},

	["YakSet51280x1024B"] = {1280,1024},
	["YakSet51280x1024P"] = {1280,1024},
	["YakSet51280x1024FX"] = {1280,1024},
}


local tags = {

	["YakSet11920x1200B"] = {background=true, border=true, screenwidth = true},
	["YakSet11920x1200P"] = {background=true, screenwidth = true},
	["YakSet11920x1200FX"] = {screenwidth = true},

	["YakSet11680x1050B"] = {background=true, border=true, screenwidth = true},
	["YakSet11680x1050P"] = {background=true, screenwidth = true},
	["YakSet11680x1050FX"] = {screenwidth = true},

	["YakSet11280x1024B"] = {background=true, border=true, screenwidth = true},
	["YakSet11280x1024P"] = {background=true, screenwidth = true},
	["YakSet11280x1024FX"] = {screenwidth = true},

	["YakSet21920x1200B"] = {background=true, border=true, screenwidth = true},
	["YakSet21920x1200P"] = {background=true, screenwidth = true},
	["YakSet21920x1200FX"] = {screenwidth = true},
	
	["YakSet21680x1050B"] = {background=true, border=true, screenwidth = true},
	["YakSet21680x1050P"] = {background=true, screenwidth = true},
	["YakSet21680x1050FX"] = {screenwidth = true},

	["YakSet21280x1024B"] = {background=true, border=true, screenwidth = true},
	["YakSet21280x1024P"] = {background=true, screenwidth = true},
	["YakSet21280x1024FX"] = {screenwidth = true},

	["YakSet31920x1200B"] = {background=true, border=true, screenwidth = true},
	["YakSet31920x1200P"] = {background=true, screenwidth = true},
	["YakSet31920x1200FX"] = {screenwidth = true},

	["YakSet31680x1050B"] = {background=true, border=true, screenwidth = true},
	["YakSet31680x1050P"] = {background=true, screenwidth = true},
	["YakSet31680x1050FX"] = {screenwidth = true},

	["YakSet31280x1024B"] = {background=true, border=true, screenwidth = true},
	["YakSet31280x1024P"] = {background=true, screenwidth = true},
	["YakSet31280x1024FX"] = {screenwidth = true},

	["YakSet41920x1200B"] = {background=true, border=true, screenwidth = true},
	["YakSet41920x1200P"] = {background=true, screenwidth = true},
	["YakSet41920x1200FX"] = {screenwidth = true},

	["YakSet41680x1050B"] = {background=true, border=true, screenwidth = true},
	["YakSet41680x1050P"] = {background=true, screenwidth = true},
	["YakSet41680x1050FX"] = {screenwidth = true},

	["YakSet41280x1024B"] = {background=true, border=true, screenwidth = true},
	["YakSet41280x1024P"] = {background=true, screenwidth = true},
	["YakSet41280x1024FX"] = {screenwidth = true},

	["YakSet51920x1200B"] = {background=true, border=true, screenwidth = true},
	["YakSet51920x1200P"] = {background=true, screenwidth = true},
	["YakSet51920x1200FX"] = {screenwidth = true},

	["YakSet51680x1050B"] = {background=true, border=true, screenwidth = true},
	["YakSet51680x1050P"] = {background=true, screenwidth = true},
	["YakSet51680x1050FX"] = {screenwidth = true},

	["YakSet51280x1024B"] = {background=true, border=true, screenwidth = true},
	["YakSet51280x1024P"] = {background=true, screenwidth = true},
	["YakSet51280x1024FX"] = {screenwidth = true},
}

local LSA = LibStub("LibSharedAssets")
if not LSA then return end

for k,texName in ipairs(textures) do
	local metadata = {
			displayname = displaynames[k],
			size = dims[texName],
			tiled = tiled[texName] or false,
			tags = tags[texName],
		}
	local result = LSA:RegisterTexture(texName, metadata)
	if not result then
		LSA:AddMetadata(texName, {displayname = displaynames[k]})
	end
end