<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="YakAssetsPanel" version="1.0" date="03/12/2010" >

		<Author name="Wothor code Yakar textures" email="" />
		<Description text="Panel used by Effigy via LibSharedAssets." />
        <VersionSettings gameVersion="1.4.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Files>
			<File name="LibStub.lua" />
			<File name="LibSharedAssets.lua" />
			<File name="textures.xml" />
			<File name="textures.lua" />
		</Files>
		
		<OnInitialize/>
		<OnUpdate/>
		<OnShutdown/>
		
	</UiMod>
</ModuleFile>
