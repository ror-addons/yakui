function yakui.CreatePanel()
	if not Effigy then return end
	if not UFTemplates then return end
	if not UFTemplates.Bars then return end
	local Panel = Effigy.deepcopy(UFTemplates.Bars["_DefaultEmptyPanel"])
	Panel.show = false
	Panel.images =
	{
		Panel = 
		{
			alpha_group = "none",
			layer = "default",
			alpha = 0.7,
			width = 1920,
			show = true,
			visibility_group = "none",
			parent = "Bar",
			anchors = 
			{
				[1] = 
				{
					parentpoint = "bottomleft",
					x = 0,
					point = "bottomleft",
					parent = "Bar",
					y = 0,
				},
			},
			height = 225,
			colorsettings = 
			{
				ColorPreset = "none",
				alter = 
				{
					r = "no",
					g = "no",
					b = "no",
				},
				color = 
				{
					r = 16,
					g = 16,
					b = 16,
				},
				allow_overrides = false,
				color_group = "none",
			},
			scale = 1,
			texture = 
			{
				texture_group = "none",
				slice = "none",
				name = "YakSet51920x1200P",
				scale = 1,
				height = 0,
				y = 0,
				x = 0,
				width = 0,
			},
		},
		Border = 
		{
			alpha_group = "none",
			layer = "background",
			alpha = 0.8,
			width = 1920,
			show = true,
			visibility_group = "none",
			parent = "Bar",
			scale = 1,
			height = 225,
			colorsettings = 
			{
				ColorPreset = "none",
				alter = 
				{
					b = "no",
					g = "no",
					r = "no",
				},
				color = 
				{
					b = 24,
					g = 24,
					r = 24,
				},
				allow_overrides = false,
				color_group = "none",
			},
			anchors = 
			{
				[1] = 
				{
					parentpoint = "bottomleft",
					x = 0,
					point = "bottomleft",
					parent = "Bar",
					y = 0,
				},
			},
			texture = 
			{
				texture_group = "none",
				slice = "none",
				name = "YakSet51920x1200B",
				height = 0,
				scale = 1,
				x = 0,
				y = 0,
				width = 0,
			},
		},
		FX = 
		{
			alpha_group = "none",
			layer = "secondary",
			alpha = 0.6,
			width = 1920,
			show = true,
			visibility_group = "none",
			parent = "Bar",
			anchors = 
			{
				[1] = 
				{
					parentpoint = "bottomleft",
					x = 0,
					point = "bottomleft",
					parent = "Bar",
					y = 0,
				},
			},
			height = 225,
			colorsettings = 
			{
				ColorPreset = "none",
				alter = 
				{
					r = "no",
					g = "no",
					b = "no",
				},
				color = 
				{
					r = 180,
					g = 220,
					b = 225,
				},
				allow_overrides = false,
				color_group = "none",
			},
			scale = 1,
			texture = 
			{
				texture_group = "none",
				slice = "none",
				name = "YakSet51920x1200FX",
				scale = 1,
				height = 0,
				y = 0,
				x = 0,
				width = 0,
			},
		},
	}
	Panel.name = "YBackPanel1920x1200"
	UFTemplates.Bars[Panel.name] = Panel
	UFTemplates.Layouts.YakUI_140[Panel.name] = Panel
	if not Effigy.Bars[Panel.name] then
		if Effigy.isLoaded then
			local bar = Effigy.CreateBar(Panel.name, UFTemplates.Layouts.YakUI_140[Panel.name])
			bar:setup()
		else
			Effigy.Bars[Panel.name] = Effigy.deepcopy(Panel)
		end
	end
	
	Panel = Effigy.deepcopy(Panel)	-- uhm, should work shouldn�t it
	Panel.images["Border"].texture.name = "YakSet51680x1050B"
	Panel.images["Border"].width = 1680
	Panel.images["Border"].height = 200
	Panel.images["Panel"].texture.name = "YakSet51680x1050P"
	Panel.images["Panel"].width = 1680
	Panel.images["Panel"].height = 200
	Panel.images["FX"].texture.name = "YakSet51680x1050FX"
	Panel.images["FX"].width = 1680
	Panel.images["FX"].height = 200
	Panel.name = "YBackPanel1680x1050"
	UFTemplates.Bars[Panel.name] = Panel
	UFTemplates.Layouts.YakUI_140[Panel.name] = Panel
	if not Effigy.Bars[Panel.name] then
		if Effigy.isLoaded then
			local bar = Effigy.CreateBar(Panel.name, UFTemplates.Layouts.YakUI_140[Panel.name])
			bar:setup()
		else
			Effigy.Bars[Panel.name] = Effigy.deepcopy(Panel)
		end
	end
	
	Panel = Effigy.deepcopy(Panel)	-- uhm, should work shouldn�t it
	Panel.images["Border"].texture.name = "YakSet51280x1024B"
	Panel.images["Border"].width = 1280
	Panel.images["Border"].height = 1024
	Panel.images["Panel"].texture.name = "YakSet51280x1024P"
	Panel.images["Panel"].width = 1280
	Panel.images["Panel"].height = 1024
	Panel.images["FX"].texture.name = "YakSet51280x1024FX"
	Panel.images["FX"].width = 1280
	Panel.images["FX"].height = 1024
	Panel.name = "YBackPanel1280x1024"
	UFTemplates.Bars[Panel.name] = Panel
	UFTemplates.Layouts.YakUI_140[Panel.name] = Panel
	if not Effigy.Bars[Panel.name] then
		if Effigy.isLoaded then
			local bar = Effigy.CreateBar(Panel.name, UFTemplates.Layouts.YakUI_140[Panel.name])
			--bar:setup()
		else
			Effigy.Bars[Panel.name] = Effigy.deepcopy(Panel)
		end
	end
	yakui.RenderPanel()
end

--yakui.RenderPanel("1920x1200")
--yakui.RenderPanel("1680x1050")
function yakui.RenderPanel(active_resoset)
	if not active_resoset then
		if Vectors and Vectors.CalcRatio() == "1.25" then -- "5:4"
			active_resoset = "1280x1024"
		elseif SystemData.screenResolution.x > 1680 then
			active_resoset = "1920x1200"
		else
			active_resoset = "1680x1050"
		end
	end

	local bars = {"1920x1200", "1680x1050", "1280x1024"}
	for _, resoset in ipairs(bars) do
		local bar = Effigy.Bars["YBackPanel"..resoset]
		if bar then
			bar.images["Border"].texture.name = "Yak"..yakuiVar.Panel.tex..resoset.."B"
			bar.images["Panel"].texture.name = "Yak"..yakuiVar.Panel.tex..resoset.."P"
			bar.images["FX"].texture.name = "Yak"..yakuiVar.Panel.tex..resoset.."FX"
			local show = (resoset == active_resoset)
			if show or bar.show ~= show then
				bar.show = show
				if Effigy.isLoaded then
					bar:setup()
					bar:render()
				end
			end
		end
	end
	--if Vectors then Vectors.LoadProfile() end
end

local xOffsets = {
	["YakuiATButton"] = {
		["1920x1200"] = -0.17864583432674,
		["1680x1050"] = -0.2029515,
	},
	["EA_ActionBar1"] = {
		["1920x1200"] = -0.052491378039122,
		["1680x1050"] = -0.0796562,		
	},
	["i_menu1"] = {
		["1920x1200"] = -0.097255989909172,
		["1680x1050"] = -0.1238121,			
	},
	["Minmap"] = {
		["1920x1200"] = 0.016242504119873,
		["1680x1050"] = 0.0119842,	
	},
	["i_loc"] = {
		["1920x1200"] = 0.038231406360865,
		["1680x1050"] = 0.1596305,	
	},
	["i_gl"] = {
		["1920x1200"] = -0.2297967,
		["1680x1050"] = -0.20270973443985,
	},
	["snt_info_time"] = {
		["1920x1200"] = -0.1411085,
		["1680x1050"] = -0.12517488002777,
	},
	["EA_Window_ZoneControl"] = {
		["1920x1200"] = -0.12674887478352,
		["1680x1050"] = -0.1464260,
	},
	["EA_StanceBar"] = {
		["1920x1200"] = 0.19837239384651,
		["1680x1050"] = 0.1720052,
	},
}


--yakui.HandleReso("1920x1200")
--yakui.HandleReso("1680x1050")
function yakui.HandleReso(active_resoset, active_ratio)
	if not active_resoset then
		if Vectors and Vectors.CalcRatio() == "1.25" then -- "5:4"
			active_resoset = "1280x1024"
		elseif SystemData.screenResolution.x > 1680 then
			active_resoset = "1920x1200"
		else
			active_resoset = "1680x1050"
		end
	end
	if Vectors and not active_ratio then active_ratio = Vectors.CalcRatio() end
	
	-- reso dependend (only?)
	if Vectors and yakuiVar.active_resoset ~= active_resoset then
		if Squared and Squared.Settings then
			if SystemData.screenResolution.x > 1680 then
				Squared.Settings["spacing-group"] = 4
				Squared.Settings["icon-scale"] = 0.75
				Squared.Settings["text-font-center"] = "font_clear_medium_bold"
				Squared.Settings["text-font-bottom"] = "font_clear_small"
			else
				Squared.Settings["spacing-group"] = 3	-- integer only it seems
				Squared.Settings["icon-scale"] = 0.85
				Squared.Settings["text-font-center"] = "font_clear_large_bold"
				Squared.Settings["text-font-bottom"] = "font_clear_medium"
			end
			
			if active_resoset == "1280x1024" then
				Squared.Settings["boxheight"] = 33
			else
				Squared.Settings["boxheight"] = 25
			end
		end
		if Minmap then
			if SystemData.screenResolution.x > 1680 then
				Minmap.ScaleSliderChanged(1-(2/6))
			elseif SystemData.screenResolution.x > 1280 then
				Minmap.ScaleSliderChanged(1-(2/6))
			else
				Minmap.ScaleSliderChanged(1)
			end
		end
	end
	
	-- ratio dependend
	if Vectors and (active_resoset == "1920x1200" or active_resoset == "1680x1050") and (yakuiVar.active_ratio ~= active_ratio or yakuiVar.active_resoset ~= active_resoset) then
		local datasets = { "YakuiATButton", "EA_ActionBar1", "i_menu1", "Minmap", "i_loc", "i_gl", "snt_info_time", "EA_Window_ZoneControl", "EA_StanceBar"}
		local tt = {}
		for _,k in ipairs(datasets) do
			local dataset = Vectors.ExportData(k)
			if dataset then
				dataset.anchors[1].x = xOffsets[k][active_resoset]
				tt[k] = dataset
			end
		end
		
		Vectors.ImportData(tt)
	end
	
	-- if we changed anything, save and (re)load Vectors Profile
	if Vectors and (yakuiVar.active_resoset ~= active_resoset or ((active_resoset == "1920x1200" or active_resoset == "1680x1050") and yakuiVar.active_ratio ~= active_ratio)) then
		yakuiVar.active_resoset = active_resoset
		yakuiVar.active_ratio = active_ratio
		Vectors.LoadProfile()
	end
end

local DefaultCareerResourceWindowShowing = {
	[GameData.CareerLine.ENGINEER] = true,
	[GameData.CareerLine.SQUIG_HERDER] = true,
	[GameData.CareerLine.WHITE_LION] = true,
	[GameData.CareerLine.MAGUS] = true,
}
function yakui.CareerResourceHack(show)
	if show == nil then show = DefaultCareerResourceWindowShowing[GameData.Player.career.line] end
	if show then
		-- import Vectors settings here; note: same one for all ratios atm;
		Vectors.ImportData(
			{{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.048411466181278,
						point = "bottomleft",
						parent = "EA_GrantedAbilities",
						y = -0.042249988764524,
					},
				},
				name = "EA_CareerResourceWindow",
				scale = 0.65,
			}}
		)
		WindowSetShowing("EA_CareerResourceWindow", true)
		Vectors.LoadWindow("EA_CareerResourceWindow")
	else
		-- delete Vectors settings here
		Vectors.ImportData({"EA_CareerResourceWindow"})
		ActionBarClusterManager:RegisterClusterWithLayoutEditor ()
		LayoutEditor.UserHide("EA_CareerResourceWindow")
	end
end
