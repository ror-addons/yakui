if not yakuiconfig then yakuiconfig = {} end
local booltowstring={[false]=L"Off",[true]=L"On"} 

local function CreateCustomMenuWindow(name, showCheckBox, labeltext, leftclickhandler, mouseoverhandler)
	if showCheckBox then
		CreateWindowFromTemplate (name, "ChatContextMenuItemCheckBox", "Root")
		LabelSetText( name.."CheckBoxLabel", labeltext )
	else
		CreateWindowFromTemplate (name, "ChatContextMenuItemFontSelection", "Root")
		LabelSetText( name.."Label", labeltext )
	end
	
	WindowRegisterCoreEventHandler(name, "OnLButtonUp", leftclickhandler)
	if mouseoverhandler then WindowRegisterCoreEventHandler(name, "OnMouseOver", mouseoverhandler) end
	WindowSetShowing(name, false)
end

local function UpdateUnitFramesMenu()
	if not (Effigy and Effigy.Bars) then return end
	if Effigy.Bars["GroupMember2"] then ButtonSetPressedFlag("yconfigcontextmenugroupiconsCheckBox", Effigy.GetShowGroup(true)) end
	if Effigy.Bars["WorldGroupMember2"] then ButtonSetPressedFlag("yconfigcontextmenugroupframesCheckBox", Effigy.GetShowGroup(false)) end
	if Effigy.Bars["InfoPanel"] then ButtonSetPressedFlag("yconfigcontextmenuinfopanelCheckBox", Effigy.Bars["InfoPanel"].show) end
	if Effigy.Bars["HostileTarget"] and Effigy.Bars["HostileTarget"].labels["Range"] then
		ButtonSetPressedFlag("yconfigcontextmenuhostilerangeCheckBox", Effigy.Bars["HostileTarget"].labels["Range"].show)
	end
	if Effigy.Bars["FriendlyTarget"] and Effigy.Bars["FriendlyTarget"].labels["Range"] then
		ButtonSetPressedFlag("yconfigcontextmenufriendlyrangeCheckBox", Effigy.Bars["FriendlyTarget"].labels["Range"].show)
	end
	if Effigy.WindowSettings then ButtonSetPressedFlag("yconfigcontextmenucastbargcdCheckBox", Effigy.WindowSettings.castbar_show_gcd or false) end
	if Effigy.Bars["TargetsFriendly1"] then ButtonSetPressedFlag("yconfigcontextmenugrpCFCheckBox", Effigy.Bars["TargetsFriendly1"].show) end
	if Effigy.Bars["TargetsHostile1"] then ButtonSetPressedFlag("yconfigcontextmenugrpCHCheckBox", Effigy.Bars["TargetsHostile1"].show) end		
	if Effigy.Bars["MouseoverTarget"] then
		ButtonSetPressedFlag("yconfigcontextmenuMOTCheckBox", Effigy.Bars["MouseoverTarget"].show)
		ButtonSetPressedFlag("yconfigcontextmenuMOT_fixedCheckBox", Effigy.Bars["MouseoverTarget"].anchors[1].parent == "EA_Window_RvRTracker" and Effigy.Bars["MouseoverTarget"].pos_at_world_object == false)
		ButtonSetPressedFlag("yconfigcontextmenuMOT_CursorCheckBox", Effigy.Bars["MouseoverTarget"].anchors[1].parent == "CursorWindow" and Effigy.Bars["MouseoverTarget"].pos_at_world_object == false)
		ButtonSetPressedFlag("yconfigcontextmenuMOT_WObjCheckBox", Effigy.Bars["MouseoverTarget"].pos_at_world_object)
	end	
end

local presets = {L"Transparenz (light)", L"Transparenz (dark)", L"Vintage", L"Eeeeks!", L"Mariner", L"Dem Bones"}
function yakuiconfig.initializeButton()
	-- Init Color Selection
	for i,v in ipairs(presets) do
		CreateCustomMenuWindow("yconfigcontextmenucolorselect"..i, false, v, "yakuiconfig.colorselect"..i)
	end

	-- Init Effigy Options
	if Effigy then
		CreateCustomMenuWindow("yconfigcontextmenugroupicons", true, L"Group Icons", "yakuiconfig.actionbartogglegroupicons", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenugroupframes", true, L"Group Frames", "yakuiconfig.actionbartogglegroupframes", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenuinfopanel", true, L"InfoPanel", "yakuiconfig.infopaneltoggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenufriendlyrange", true, L"FT Range", "yakuiconfig.friendlyrangetoggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenuhostilerange", true, L"HT Range", "yakuiconfig.hostilerangetoggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenucastbargcd", true, L"Castbar GCD", "yakuiconfig.castbargcdtoggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		--CreateCustomMenuWindow("yconfigcontextmenuignoreeffigy", true, L"Ignore Effigy", "yakuiconfig.ignoreeffigytoggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenugrpCF", true, L"Friendlies Group", "yakuiconfig.grpCFtoggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenugrpCH", true, L"Hostiles Group", "yakuiconfig.grpCHtoggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenuMOT", true, L"MouseOver", "yakuiconfig.MOTtoggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenuMOT_fixed", true, L"MouseOver fixed", "yakuiconfig.MOT_fixed_toggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenuMOT_Cursor", true, L"MouseOver at Cursor", "yakuiconfig.MOT_atCursor_toggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenuMOT_WObj", true, L"MouseOver at Obj", "yakuiconfig.MOT_atWObj_toggle", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
		CreateCustomMenuWindow("yconfigcontextmenuEffigyToggleNameCase", true, L"Names Upper Case", "yakuiconfig.EffigyToggleNameCase", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
	end
	
	-- Misc Options
	CreateCustomMenuWindow("yconfigcontextmenulimitalert", true, L"Limit On Screen Alerts", "yakuiconfig.actionbartogglelimitalert", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
	CreateCustomMenuWindow("yconfigcontextmenufps", true, L"Show FPS", "yakuiconfig.actionbartogglefps", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
	CreateCustomMenuWindow("yconfigcontextmenuloc", true, L"Show Coordinates", "yakuiconfig.actionbartoggleloc", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
	CreateCustomMenuWindow("yconfigcontextmenubag", true, L"Show Bag Info", "yakuiconfig.actionbartogglebag", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
	CreateCustomMenuWindow("yconfigcontextmenuminmaphook", true, L"Minmap-Leftclick", "yakuiconfig.minmaphook", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
	CreateCustomMenuWindow("yconfigcontextmenumoraleCDsize", true, L"Larger Morale-CD", "yakuiconfig.moralecd", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
	CreateCustomMenuWindow("yconfigcontextmenusmoothout", true, L"Smooth Out", "yakuiconfig.togglesmoothout", "EA_Window_ContextMenu.OnMouseOverDefaultMenuItem")
	
	--[[if LibAddonButton then
		local button = LibAddonButton.Register("Y-Button", 48, 48, "Yakui", "Yakui", "Yakui", false)	-- change later to orig name "YakuiButton" for Vectors
		--local name = button.m_name
		--RegisterEventHandler(SystemData.Events.LOADING_END, "yakuiconfig.CreateYButton")
		yakuiconfig.CreateYButton()
	else]]--
		CreateWindow("YakuiButton", true)
		LayoutEditor.RegisterWindow( "YakuiButton", L"YakUI Button", L"YakUI Functions", false, false, true, nil )
	--end	
	
end

-- LibAddonButton - delayed until next version or so...
--[[function yakuiconfig.CreateYButton()
	--UnregisterEventHandler(SystemData.Events.LOADING_END, "yakuiconfig.CreateYButton")
	if not LibAddonButton then
		ERROR(L"LibAddonButton not found")
		return
	end
	local menu = LibAddonButton.AddCascadingMenuItem("Y-Button", L"Appearance Settings")
	LibAddonButton.AddMenuSubitem(menu, L"Edit Skin Settings" , yakuiconfig.editwindowbackgrounds)
	LibAddonButton.AddMenuSubitem(menu, L"Edit Custom Settings" , yakuiconfig.editcustomcolor)
	for i = 1,6 do
		LibAddonButton.AddUserDefinedMenuSubitem(menu, presets[i], "yconfigcontextmenucolorselect"..i)
		WindowSetShowing("yconfigcontextmenucolorselect"..i.."Check", (yakuiVar.Colors.col == "Preset"..i))
	end
	if Effigy then
		menu = LibAddonButton.AddCascadingMenuItem("Y-Button", L"Unitframes Settings")
		if Effigy.Bars["GroupMember2"] then
			ButtonSetPressedFlag("yconfigcontextmenugroupiconsCheckBox", Effigy.GetShowGroup(true))
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Group Icons", "yconfigcontextmenugroupicons")
		end
		if Effigy.Bars["WorldGroupMember2"] then
			ButtonSetPressedFlag("yconfigcontextmenugroupframesCheckBox", Effigy.GetShowGroup(false))
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Group Frames", "yconfigcontextmenugroupframes")
		end
		if Effigy.Bars["InfoPanel"] then
			ButtonSetPressedFlag("yconfigcontextmenuinfopanelCheckBox", Effigy.Bars["InfoPanel"].show)
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Info Panel", "yconfigcontextmenuinfopanel")
		end
		if Effigy.Bars["HostileTarget"] and Effigy.Bars["HostileTarget"].labels["Range"] then		
			ButtonSetPressedFlag("yconfigcontextmenuhostilerangeCheckBox", Effigy.Bars["HostileTarget"].labels["Range"].show)
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Hostile Range", "yconfigcontextmenuhostilerange")
		end
		if Effigy.Bars["FriendlyTarget"] and Effigy.Bars["FriendlyTarget"].labels["Range"] then
			ButtonSetPressedFlag("yconfigcontextmenufriendlyrangeCheckBox", Effigy.Bars["FriendlyTarget"].labels["Range"].show)
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Friendly Range", "yconfigcontextmenufriendlyrange")
		end
		if Effigy.WindowSettings then
			ButtonSetPressedFlag("yconfigcontextmenucastbargcdCheckBox", Effigy.WindowSettings.castbar_show_gcd or false)
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Castbar GCD", "yconfigcontextmenucastbargcd")
		end
		if Effigy.Bars["TargetsFriendly1"] then
			ButtonSetPressedFlag("yconfigcontextmenugrpCFCheckBox", Effigy.Bars["TargetsFriendly1"].show)
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Custom Friendly Group", "yconfigcontextmenugrpCF")
		end
		if Effigy.Bars["TargetsHostile1"] then
			ButtonSetPressedFlag("yconfigcontextmenugrpCHCheckBox", Effigy.Bars["TargetsHostile1"].show)
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Custom Hostile Group", "yconfigcontextmenugrpCH")
		end		
		if Effigy.Bars["MouseoverTarget"] then
			ButtonSetPressedFlag("yconfigcontextmenuMOTCheckBox", Effigy.Bars["MouseoverTarget"].show)
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Mouseover Target", "yconfigcontextmenuMOT")
			ButtonSetPressedFlag("yconfigcontextmenuMOT_fixedCheckBox", Effigy.Bars["MouseoverTarget"].anchors[1].parent == "EA_Window_RvRTracker" and Effigy.Bars["MouseoverTarget"].pos_at_world_object == false)
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Mouseover Target @Fixed", "yconfigcontextmenuMOT_fixed")
			ButtonSetPressedFlag("yconfigcontextmenuMOT_CursorCheckBox", Effigy.Bars["MouseoverTarget"].anchors[1].parent == "CursorWindow" and Effigy.Bars["MouseoverTarget"].pos_at_world_object == false)
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Mouseover Target @Cursor", "yconfigcontextmenuMOT_Cursor")
			ButtonSetPressedFlag("yconfigcontextmenuMOT_WObjCheckBox", Effigy.Bars["MouseoverTarget"].pos_at_world_object)
			LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Mouseover Target @WObj", "yconfigcontextmenuMOT_WObj")
		end
	end
	
	menu = LibAddonButton.AddCascadingMenuItem("Y-Button", L"Misc. Settings")
	ButtonSetPressedFlag("yconfigcontextmenulimitalertCheckBox", yakuiVar.limitalert)
	LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Limit Alerts", "yconfigcontextmenulimitalert")
	if alertMod then LibAddonButton.AddMenuSubitem(menu, L"ext. AlertMod", alertMod.SlashCommand) end
	ButtonSetPressedFlag("yconfigcontextmenufpsCheckBox", yakuiVar.fps)
	LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Show FPS", "yconfigcontextmenufps")
	ButtonSetPressedFlag("yconfigcontextmenulocCheckBox", yakuiVar.loc)
	LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Show Coordinates", "yconfigcontextmenuloc")
	if Map then
		ButtonSetPressedFlag("yconfigcontextmenuminmaphookCheckBox", Map.Settings.MiniMapHook)
		LibAddonButton.AddUserDefinedMenuSubitem(menu, L"MinMap-Leftclick", "yconfigcontextmenuminmaphook")
	end
	ButtonSetPressedFlag("yconfigcontextmenubagCheckBox", yakuiVar.bag)
	LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Show Bag Info", "yconfigcontextmenubag")
	ButtonSetPressedFlag("yconfigcontextmenumoraleCDsizeCheckBox", yakuiVar.moraleCDsize)
	LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Larger Morale CD", "yconfigcontextmenumoraleCDsize")
	ButtonSetPressedFlag("yconfigcontextmenusmoothoutCheckBox", yakuiVar.smoothout)
	LibAddonButton.AddUserDefinedMenuSubitem(menu, L"Smooth Out", "yconfigcontextmenusmoothout")
	LibAddonButton.AddMenuSubitem(menu, LibAddonButton.Localization.GetMapping()["Button.Manage"]..L" Button", function() LibAddonButton.GetButton("Y-Button"):Manage() end)
	
	menu = LibAddonButton.AddCascadingMenuItem("Y-Button", L"Core Addons")
	if DAoCBuffSettings then LibAddonButton.AddMenuSubitem(menu, L"DaoCBuff", DAoCBuffSettings.OpenOptionswindow) end
	if Effigy then LibAddonButton.AddMenuSubitem(menu, L"Effigy", function() Effigy.SlashHandler("") end) end
	if Phantom then LibAddonButton.AddMenuSubitem(menu, L"Phantom", Phantom.Show) end
	if SquaredConfigurator then LibAddonButton.AddMenuSubitem(menu, L"Squared", SquaredConfigurator.Show) end
	if TexturedButtons then LibAddonButton.AddMenuSubitem(menu, L"TexturedButtons", TexturedButtons.Setup.Show) end
	if TidyChat then LibAddonButton.AddMenuSubitem(menu, L"TidyChat", TidyChat.ToggleOptions) end
	if Vectors then LibAddonButton.AddMenuSubitem(menu, L"Vectors", Vectors.Settings.Open) end
	--if WSCT then LibAddonButton.AddMenuSubitem(menu, L"WSCT", yakuiconfig.showwsctoptions) end
	if WSCT then LibAddonButton.AddMenuSubitem(menu, L"WSCT", WSCT.OpenMenu) end
	
	if yakuifirststeps then LibAddonButton.AddMenuItem("Y-Button", L"YakUI First Steps", yakuifirststeps.show) end
	LibAddonButton.AddMenuItem("Y-Button", L"Reload Interface", InterfaceCore.ReloadUI)
	--d(button)
end]]--

local function UnsetOfflineStandardBearers()
	if not GuildWindowTabAdmin.GetGuildCommandPermissionForPlayer( SystemData.GuildPermissons.ASSIGN_BANNERS ) then return end
	for _, d in ipairs(GuildWindowTabRoster.memberListData) do
        if ( d.bearerStatus > 0 ) and ( d.zoneID == 0 ) then
			SendChatText(L"/GuildRemoveStandardBearer "..d.name, L"")
        end
    end
end
local function SetStandardBearer()
	if (GuildWindowTabRoster.GetMember()["bearerStatus"] > 0 ) or not GuildWindowTabAdmin.GetGuildCommandPermissionForPlayer( SystemData.GuildPermissons.ASSIGN_BANNERS ) then return end
	SendChatText(L"/GuildAddStandardBearer "..(GameData.Player.name), L"")
end

local function UnsetOfflineRealmCaptains()
	if not GuildWindowTabAdmin.GetGuildCommandPermissionForPlayer( SystemData.GuildPermissons.ASSIGN_REALM_CAPTAIN ) then return end
	for _, memberData in ipairs(GuildWindowTabRoster.memberListData) do
        if ( memberData.isRealmCaptain == true ) and not GuildWindowTabRoster.IsMemberOnline( memberData ) then
			SendChatText(L"/RemoveRealmCaptain "..memberData.name, L"")
        end
    end
end
local function SetRealmCaptain()
	if (GuildWindowTabRoster.GetMember()["isRealmCaptain"] == true) or not GuildWindowTabAdmin.GetGuildCommandPermissionForPlayer( SystemData.GuildPermissons.ASSIGN_REALM_CAPTAIN ) then return end
	SendChatText(L"/AddRealmCaptain "..(GameData.Player.name), L"")
end
--
-- Button Mouse Handlers
--
function yakuiconfig.YButtonOnLButtonUp()	-- Left Click Options
	EA_Window_ContextMenu.CreateContextMenu( "YakuiButton" )
	EA_Window_ContextMenu.AddMenuItem( L"Unset Offl. Standard Bearers" , UnsetOfflineStandardBearers, not GuildWindowTabAdmin.GetGuildCommandPermissionForPlayer( SystemData.GuildPermissons.ASSIGN_BANNERS ), true, EA_Window_ContextMenu.CONTEXT_MENU_1 )
	EA_Window_ContextMenu.AddMenuItem( L"Claim Standard Bearer" , SetStandardBearer, not GuildWindowTabAdmin.GetGuildCommandPermissionForPlayer( SystemData.GuildPermissons.ASSIGN_BANNERS ), true, EA_Window_ContextMenu.CONTEXT_MENU_1 )
	EA_Window_ContextMenu.AddMenuItem( L"Unset Offl. Realm Captains" , UnsetOfflineRealmCaptains, not GuildWindowTabAdmin.GetGuildCommandPermissionForPlayer( SystemData.GuildPermissons.ASSIGN_REALM_CAPTAIN ), true, EA_Window_ContextMenu.CONTEXT_MENU_1 )
	EA_Window_ContextMenu.AddMenuItem( L"Claim Realm Captain" , SetRealmCaptain, not GuildWindowTabAdmin.GetGuildCommandPermissionForPlayer( SystemData.GuildPermissons.ASSIGN_REALM_CAPTAIN ), true, EA_Window_ContextMenu.CONTEXT_MENU_1 )
	
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_1)
end
function yakuiconfig.toggle()				-- Middle Click Options
	EA_Window_ContextMenu.CreateContextMenu( "YakuiButton" )
	if Y_ActionBars and Y_ActionBars.Config then
		EA_Window_ContextMenu.AddCascadingMenuItem(L"ActionBars Settings", Y_ActionBars.Config.showcontextmenu, false, EA_Window_ContextMenu.CONTEXT_MENU_1)
	end
	EA_Window_ContextMenu.AddCascadingMenuItem(L"Appearance Settings", yakuiconfig.showcoloroptions, false, EA_Window_ContextMenu.CONTEXT_MENU_1)
	if Effigy then EA_Window_ContextMenu.AddCascadingMenuItem(L"Unitframe Settings", yakuiconfig.showunitframeoptions, false, EA_Window_ContextMenu.CONTEXT_MENU_1) end
	EA_Window_ContextMenu.AddCascadingMenuItem(L"Misc. Settings", yakuiconfig.showmiscellaneousoptions, false, EA_Window_ContextMenu.CONTEXT_MENU_1)
	EA_Window_ContextMenu.AddCascadingMenuItem(L"Core Addons", yakuiconfig.showunitframesextoptions, false, EA_Window_ContextMenu.CONTEXT_MENU_1)
	if yakuifirststeps then EA_Window_ContextMenu.AddMenuItem( L"YakUI First Steps" , yakuifirststeps.show , false, true, EA_Window_ContextMenu.CONTEXT_MENU_1 ) end
	EA_Window_ContextMenu.AddMenuItem( L"Reload Interface" , InterfaceCore.ReloadUI, false, true, EA_Window_ContextMenu.CONTEXT_MENU_1 )
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_1)
end
function yakuiconfig.YButtonOnRButtonUp()
	if yakuiVar.Panel.config == "on" then yakuiconfig.hide() end
	if Map then Map.ToggleMap() end
end
function yakuiconfig.YButtonOnMouseOver()
    local windowName	= SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, L"YakUI rocks!")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end
--
-- /Button Mouse Handlers
--

function yakuiconfig.showcoloroptions()
	EA_Window_ContextMenu.Hide(EA_Window_ContextMenu.CONTEXT_MENU_3)
	EA_Window_ContextMenu.CreateContextMenu("", EA_Window_ContextMenu.CONTEXT_MENU_2)
	EA_Window_ContextMenu.AddMenuItem( L"Edit Skin Settings" , yakuiconfig.editwindowbackgrounds, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 )
	EA_Window_ContextMenu.AddMenuItem( L"Edit Custom Settings" , yakuiconfig.editcustomcolor, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 )

	for i = 1,6 do
		EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenucolorselect"..i, EA_Window_ContextMenu.CONTEXT_MENU_2)
		WindowSetShowing("yconfigcontextmenucolorselect"..i.."Check", (yakuiVar.Colors.col == "Preset"..i))
	end
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_2)
end

function yakuiconfig.showmiscellaneousoptions()
	EA_Window_ContextMenu.Hide(EA_Window_ContextMenu.CONTEXT_MENU_3)
	EA_Window_ContextMenu.CreateContextMenu("", EA_Window_ContextMenu.CONTEXT_MENU_2)
	ButtonSetPressedFlag("yconfigcontextmenulimitalertCheckBox", yakuiVar.limitalert)
	EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenulimitalert", EA_Window_ContextMenu.CONTEXT_MENU_2)
	if alertMod then EA_Window_ContextMenu.AddMenuItem( L"ext. AlertMod" , alertMod.SlashCommand, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 ) end
	ButtonSetPressedFlag("yconfigcontextmenufpsCheckBox", yakuiVar.fps)
	EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenufps", EA_Window_ContextMenu.CONTEXT_MENU_2)
	ButtonSetPressedFlag("yconfigcontextmenulocCheckBox", yakuiVar.loc)
	EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenuloc", EA_Window_ContextMenu.CONTEXT_MENU_2)
	if Map then
		ButtonSetPressedFlag("yconfigcontextmenuminmaphookCheckBox", Map.Settings.MiniMapHook)
		EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenuminmaphook", EA_Window_ContextMenu.CONTEXT_MENU_2)
	end
	ButtonSetPressedFlag("yconfigcontextmenubagCheckBox", yakuiVar.bag)
	EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenubag", EA_Window_ContextMenu.CONTEXT_MENU_2)
	ButtonSetPressedFlag("yconfigcontextmenumoraleCDsizeCheckBox", yakuiVar.moraleCDsize)
	EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenumoraleCDsize", EA_Window_ContextMenu.CONTEXT_MENU_2)
	ButtonSetPressedFlag("yconfigcontextmenusmoothoutCheckBox", yakuiVar.smoothout)
	EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenusmoothout", EA_Window_ContextMenu.CONTEXT_MENU_2)
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_2)
end

function yakuiconfig.showunitframeoptions()
	if Effigy and Effigy.Bars then
		EA_Window_ContextMenu.Hide(EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.CreateContextMenu("", EA_Window_ContextMenu.CONTEXT_MENU_2)
		if Effigy.Bars["GroupMember2"] then
			ButtonSetPressedFlag("yconfigcontextmenugroupiconsCheckBox", Effigy.GetShowGroup(true))
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenugroupicons", EA_Window_ContextMenu.CONTEXT_MENU_2)
		end
		if Effigy.Bars["WorldGroupMember2"] then
			ButtonSetPressedFlag("yconfigcontextmenugroupframesCheckBox", Effigy.GetShowGroup(false))
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenugroupframes", EA_Window_ContextMenu.CONTEXT_MENU_2)
		end
		if Effigy.Bars["InfoPanel"] then
			ButtonSetPressedFlag("yconfigcontextmenuinfopanelCheckBox", Effigy.Bars["InfoPanel"].show)
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenuinfopanel", EA_Window_ContextMenu.CONTEXT_MENU_2)
		end
		if Effigy.Bars["HostileTarget"] and Effigy.Bars["HostileTarget"].labels["Range"] then		
			ButtonSetPressedFlag("yconfigcontextmenuhostilerangeCheckBox", Effigy.Bars["HostileTarget"].labels["Range"].show)
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenuhostilerange", EA_Window_ContextMenu.CONTEXT_MENU_2)
		end
		if Effigy.Bars["FriendlyTarget"] and Effigy.Bars["FriendlyTarget"].labels["Range"] then
			ButtonSetPressedFlag("yconfigcontextmenufriendlyrangeCheckBox", Effigy.Bars["FriendlyTarget"].labels["Range"].show)
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenufriendlyrange", EA_Window_ContextMenu.CONTEXT_MENU_2)
		end
		if Effigy.WindowSettings then
			ButtonSetPressedFlag("yconfigcontextmenucastbargcdCheckBox", Effigy.WindowSettings.castbar_show_gcd or false)
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenucastbargcd", EA_Window_ContextMenu.CONTEXT_MENU_2)
		end
		if Effigy.Bars["TargetsFriendly1"] then
			ButtonSetPressedFlag("yconfigcontextmenugrpCFCheckBox", Effigy.Bars["TargetsFriendly1"].show)
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenugrpCF", EA_Window_ContextMenu.CONTEXT_MENU_2)
		end
		if Effigy.Bars["TargetsHostile1"] then
			ButtonSetPressedFlag("yconfigcontextmenugrpCHCheckBox", Effigy.Bars["TargetsHostile1"].show)
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenugrpCH", EA_Window_ContextMenu.CONTEXT_MENU_2)
		end		
		if Effigy.Bars["MouseoverTarget"] then
			ButtonSetPressedFlag("yconfigcontextmenuMOTCheckBox", Effigy.Bars["MouseoverTarget"].show)
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenuMOT", EA_Window_ContextMenu.CONTEXT_MENU_2)
			ButtonSetPressedFlag("yconfigcontextmenuMOT_fixedCheckBox", Effigy.Bars["MouseoverTarget"].anchors[1].parent == "EA_Window_RvRTracker" and Effigy.Bars["MouseoverTarget"].pos_at_world_object == false)
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenuMOT_fixed", EA_Window_ContextMenu.CONTEXT_MENU_2)
			ButtonSetPressedFlag("yconfigcontextmenuMOT_CursorCheckBox", Effigy.Bars["MouseoverTarget"].anchors[1].parent == "CursorWindow" and Effigy.Bars["MouseoverTarget"].pos_at_world_object == false)
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenuMOT_Cursor", EA_Window_ContextMenu.CONTEXT_MENU_2)
			ButtonSetPressedFlag("yconfigcontextmenuMOT_WObjCheckBox", Effigy.Bars["MouseoverTarget"].pos_at_world_object)
			EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenuMOT_WObj", EA_Window_ContextMenu.CONTEXT_MENU_2)
		end	
		--[[ButtonSetPressedFlag("yconfigcontextmenuignoreeffigyCheckBox", yakuiVar.Huduf.ignore)
		EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenuignoreeffigy", EA_Window_ContextMenu.CONTEXT_MENU_2)]]--
		ButtonSetPressedFlag("yconfigcontextmenuEffigyToggleNameCaseCheckBox", (Effigy and Effigy.Bars["PlayerHP"] and Effigy.Bars["PlayerHP"].labels.Name.font.case == "upper"))
		EA_Window_ContextMenu.AddUserDefinedMenuItem("yconfigcontextmenuEffigyToggleNameCase", EA_Window_ContextMenu.CONTEXT_MENU_2)		
		
		EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_2)
	end
end
	
function yakuiconfig.showunitframesextoptions()
	EA_Window_ContextMenu.CreateContextMenu("", EA_Window_ContextMenu.CONTEXT_MENU_2)
	if yakuiVar.Warnings.DaocBuff == 1 then EA_Window_ContextMenu.AddMenuItem( L"DaoCBuff" , DAoCBuffSettings.OpenOptionswindow, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 ) end
	if Effigy then EA_Window_ContextMenu.AddMenuItem( L"Effigy" , function() Effigy.SlashHandler("") end , false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 ) end
	if RVMOD_GColorPresets then EA_Window_ContextMenu.AddMenuItem( L"Global Color Presets" , RVMOD_GColorPresets.SlashHandler , false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 ) end
	if yakuiVar.Warnings.Phantom == 1 then EA_Window_ContextMenu.AddMenuItem( L"Phantom" , Phantom.Show, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 ) end
	if yakuiVar.Warnings.Squared == 1 then EA_Window_ContextMenu.AddMenuItem( L"Squared" , SquaredConfigurator.Show, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 ) end
	if yakuiVar.Warnings.TChat == 1 then EA_Window_ContextMenu.AddMenuItem( L"TidyChat" , TidyChat.ToggleOptions, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 ) end
	if TexturedButtons then EA_Window_ContextMenu.AddMenuItem( L"TexturedButtons" , TexturedButtons.Setup.Show, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 ) end
	if Vectors then EA_Window_ContextMenu.AddMenuItem( L"Vectors" , Vectors.Settings.Open, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 ) end
	if WSCT then EA_Window_ContextMenu.AddMenuItem( L"WSCT" , WSCT.OpenMenu, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2 ) end
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_2)
end

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Config core functions


--
-- Effigy related
--

--[[function yakuiconfig.openeffigyoptions()
	if Effigy then Effigy.SlashHandler("") end
end]]--

--[[function yakuiconfig.ignoreeffigytoggle()
	yakuiVar.Huduf.ignore = not yakuiVar.Huduf.ignore
	ButtonSetPressedFlag("yconfigcontextmenuignoreeffigyCheckBox", yakuiVar.Huduf.ignore)
	if yakuiVar.Huduf.ignore == true then yakui.Print("YakUI will no longer change Effigy settings...")
	else yakui.Print("YakUI will change Effigy settings again. Caution! If you changed settings manually, this may result in errors or mixed up frames. Consider reloading the YakUI_135 layout in Effigy before proceeding.")
	end
end]]--

function yakuiconfig.hostilerangetoggle()
	if Effigy and Effigy.Bars["HostileTarget"] then
		local bar =  Effigy.Bars["HostileTarget"]
		local toggleTo = not bar.labels["Range"].show
		bar.labels["Range"].show = toggleTo
		bar:setup()
		bar:render()
		ButtonSetPressedFlag("yconfigcontextmenuhostilerangeCheckBox", toggleTo)
	end	
end

function yakuiconfig.friendlyrangetoggle()
	if Effigy and Effigy.Bars["FriendlyTarget"] then
		local bar =  Effigy.Bars["FriendlyTarget"]
		local toggleTo = not bar.labels["Range"].show
		bar.labels["Range"].show = toggleTo
		bar:setup()
		bar:render()
		ButtonSetPressedFlag("yconfigcontextmenufriendlyrangeCheckBox", toggleTo)
	end
end

function yakuiconfig.castbargcdtoggle()
	if Effigy and Effigy.WindowSettings then
		local onoff = not Effigy.WindowSettings.castbar_show_gcd
		Effigy.WindowSettings.castbar_show_gcd = onoff
		ButtonSetPressedFlag("yconfigcontextmenucastbargcdCheckBox", onoff)
	end
end

function yakuiconfig.infopaneltoggle()
	if Effigy.Bars["InfoPanel"] then
		local bar = Effigy.Bars["InfoPanel"]
		--bar.show = yakuiVar.Huduf.infopanel
		bar.show = not bar.show
		bar:setup()
		bar:render()
		bar = Effigy.Bars["InfoPanelExp"]
		--bar.show = yakuiVar.Huduf.infopanel
		bar.show = not bar.show
		bar:setup()
		bar:render()
		bar = Effigy.Bars["InfoPanelRenown"]
		--bar.show = yakuiVar.Huduf.infopanel
		bar.show = not bar.show
		bar:setup()
		bar:render()
		
		ButtonSetPressedFlag("yconfigcontextmenuinfopanelCheckBox", bar.show)
	end
end

function yakuiconfig.grpCFtoggle()
	if Effigy.Bars["TargetsFriendly1"] then
		local show = not Effigy.Bars["TargetsFriendly1"].show
		for i = 1,6 do
			bar =  Effigy.Bars["TargetsFriendly"..i]
			bar.show = show
			bar:setup()
			bar:render()
		end
		ButtonSetPressedFlag("yconfigcontextmenugrpCFCheckBox", bar.show)
		yakui.Print(L"Effigy Targets: Friendly "..booltowstring[show])
	end
end

function yakuiconfig.grpCHtoggle()
	if Effigy.Bars["TargetsHostile1"] then
		local show = not Effigy.Bars["TargetsHostile1"].show
		for i = 1,6 do
			bar =  Effigy.Bars["TargetsHostile"..i]
			bar.show = show
			bar:setup()
			bar:render()
		end
		ButtonSetPressedFlag("yconfigcontextmenugrpCHCheckBox", bar.show)
		yakui.Print(L"Effigy Targets: Hostile "..booltowstring[show])
	end
end

function yakuiconfig.actionbartogglegroupicons()
	--[[if yakuiVar.Huduf.ignore == false then
		yakuiVar.Huduf.grpico = not yakuiVar.Huduf.grpico
		for i = 1,5 do
			if Effigy.Bars["WorldGroupMember"..i] then
				bar =  Effigy.Bars["WorldGroupMember"..i]
				bar.show = yakuiVar.Huduf.grpico
				bar:setup()
				bar:render()
			end
		end
		if yakuiVar.Huduf.grpico == true then yakui.Print("Group Icons: On") else yakui.Print ("Group Icons: Off") end
		ButtonSetPressedFlag("yconfigcontextmenugroupiconsCheckBox", yakuiVar.Huduf.grpico)
	end]]--
	local onoff = not Effigy.GetShowGroup(true)
	Effigy.SetShowGroup(true, onoff)
	ButtonSetPressedFlag("yconfigcontextmenugroupiconsCheckBox", onoff)
	if onoff then
		yakui.Print("Group Icons: On")
	else
		yakui.Print("Group Icons: Off")
	end
end

function yakuiconfig.MOTtoggle()
	if Effigy and Effigy.Bars["MouseoverTarget"] then
		local bar = Effigy.Bars["MouseoverTarget"]
		local toggleTo = not bar.show
		bar.show = toggleTo
		bar:setup()
		bar:render()
		ButtonSetPressedFlag("yconfigcontextmenuMOTCheckBox", toggleTo)
	end
end

function yakuiconfig.MOT_fixed_toggle()
	if Effigy and Effigy.Bars["MouseoverTarget"] and not ButtonGetPressedFlag("yconfigcontextmenuMOT_fixedCheckBox") then
		local bar = Effigy.Bars["MouseoverTarget"]
		bar.anchors[1].parent = "EA_Window_RvRTracker"
		bar.pos_at_world_object = false
		bar:setup()
		bar:render()
		ButtonSetPressedFlag("yconfigcontextmenuMOT_fixedCheckBox", true)
		ButtonSetPressedFlag("yconfigcontextmenuMOT_CursorCheckBox", false)
		ButtonSetPressedFlag("yconfigcontextmenuMOT_WObjCheckBox", false)
	end
end

function yakuiconfig.MOT_atCursor_toggle()
	if Effigy and Effigy.Bars["MouseoverTarget"] and not ButtonGetPressedFlag("yconfigcontextmenuMOT_CursorCheckBox") then
		local bar = Effigy.Bars["MouseoverTarget"]
		bar.anchors[1].parent = "CursorWindow"
		bar.pos_at_world_object = false
		bar:setup()
		bar:render()
		ButtonSetPressedFlag("yconfigcontextmenuMOT_fixedCheckBox", false)
		ButtonSetPressedFlag("yconfigcontextmenuMOT_CursorCheckBox", true)
		ButtonSetPressedFlag("yconfigcontextmenuMOT_WObjCheckBox", false)
	end
end

function yakuiconfig.MOT_atWObj_toggle()
	if Effigy and Effigy.Bars["MouseoverTarget"] and not ButtonGetPressedFlag("yconfigcontextmenuMOT_WObjCheckBox") then
		local bar = Effigy.Bars["MouseoverTarget"]
		--bar.anchors[1].parent = "CursorWindow"
		bar.pos_at_world_object = true
		bar:setup()
		bar:render()
		ButtonSetPressedFlag("yconfigcontextmenuMOT_fixedCheckBox", false)
		ButtonSetPressedFlag("yconfigcontextmenuMOT_CursorCheckBox", false)
		ButtonSetPressedFlag("yconfigcontextmenuMOT_WObjCheckBox", true)
	end
end

function yakuiconfig.actionbartogglegroupframes()
	local onoff = not Effigy.GetShowGroup(false)
	Effigy.SetShowGroup(false, onoff)
	ButtonSetPressedFlag("yconfigcontextmenugroupframesCheckBox", onoff)
	if DAoCBuff then
		for i,k in ipairs(DAoCBuffVar.Frames) do
			if(k.buffTargetType==100) then k.active= onoff end 
		end 
		DAoCBuff.U()
	end
	-- InterfaceCore.ReloadUI()
end

function yakuiconfig.EffigyToggleNameCase()
	local labelsAffected = {"Name", "Guild", "Title"}
	for _,bar in pairs(Effigy.Bars) do
		for _,affectedLabel in ipairs(labelsAffected) do
			if bar.labels[affectedLabel] then
				if bar.labels[affectedLabel].font.case == "upper" then
					bar.labels[affectedLabel].font.case = "none"
				else
					bar.labels[affectedLabel].font.case = "upper"
				end
				bar:setup()
				bar:render()
			end
		end
	end
	ButtonSetPressedFlag("yconfigcontextmenuEffigyToggleNameCaseCheckBox", (Effigy and Effigy.Bars["PlayerHP"] and Effigy.Bars["PlayerHP"].labels.Name.font.case == "upper"))
end
--
-- /Effigy related
--

function yakuiconfig.actionbartogglebag()
	yakuiVar.bag = not yakuiVar.bag
	if yakuiVar.bag == true then
		WindowSetShowing("yInfo_bag", true)
	else
		WindowSetShowing("yInfo_bag", false)
	end
	ButtonSetPressedFlag("yconfigcontextmenubagCheckBox", yakuiVar.bag)
end

function yakuiconfig.togglesmoothout()
	yakuiVar.smoothout = not yakuiVar.smoothout
	ButtonSetPressedFlag("yconfigcontextmenusmoothoutCheckBox", yakuiVar.smoothout)
end

function yakuiconfig.minmaphook()
	Map.Settings.MiniMapHook = not Map.Settings.MiniMapHook
	ButtonSetPressedFlag("yconfigcontextmenuminmaphookCheckBox", Map.Settings.MiniMapHook)	--yakuiVar.minmaphook
end

function yakuiconfig.actionbartogglelimitalert()
	yakuiVar.limitalert = not yakuiVar.limitalert
	ButtonSetPressedFlag("yconfigcontextmenulimitalertCheckBox", yakuiVar.limitalert)
end

function yakuiconfig.moralecd()
	yakuiVar.moraleCDsize = not yakuiVar.moraleCDsize
	yakui.setmoraleCDsize()
	ButtonSetPressedFlag("yconfigcontextmenumoraleCDsizeCheckBox", yakuiVar.moraleCDsize)
end

function yakuiconfig.actionbartogglefps()
	yakuiVar.fps = not yakuiVar.fps
	if yakuiVar.fps == true then
		RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakuiInfo.fps_moduleupdate")
		WindowSetShowing("snt_info_fps", true)
	else
		WindowSetShowing("snt_info_fps", false)
		UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakuiInfo.fps_moduleupdate")
	end
	ButtonSetPressedFlag("yconfigcontextmenufpsCheckBox", yakuiVar.fps)
end

function yakuiconfig.actionbartoggleloc()
	yakuiVar.loc = not yakuiVar.loc
	if yakuiVar.loc == true then
		RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED,"yakuiInfo.loc_moduleupdate")
		WindowSetShowing("i_loc", true)
	else
		WindowSetShowing("i_loc", false)
		UnregisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED,"yakuiInfo.loc_moduleupdate")
	end
	ButtonSetPressedFlag("yconfigcontextmenulocCheckBox", yakuiVar.loc)
end

-- in fact they switch the whole layout, including textures
local function SetProfileChecked(profileNumber)
	for i = 1,6 do WindowSetShowing("yconfigcontextmenucolorselect"..i.."Check", i == profileNumber) end
end

local YAK_WHITE = {r = 220, g = 220, b = 220}

function yakuiconfig.colorselect1()
	SetProfileChecked(1)
	yakuiVar.Colors.colprintr = 64
	yakuiVar.Colors.colprintg = 64
	yakuiVar.Colors.colprintb = 80
	yakuiVar.Panel.Panelalpha = 0.5
	yakuiVar.Colors.background.r = 16
	yakuiVar.Colors.background.g = 16
	yakuiVar.Colors.background.b = 20
	yakuiVar.Panel.alpha = 0.7
	yakuiVar.Colors.fx.r = 255
	yakuiVar.Colors.fx.g = 255
	yakuiVar.Colors.fx.b = 255
	yakuiVar.Panel.fxalpha = 0.7
	yakuiVar.Colors.unitframes.player = YAK_WHITE
	yakuiVar.Colors.unitframes.hostile = YAK_WHITE
	yakuiVar.Colors.unitframes.castbar = YAK_WHITE
	yakuiVar.Colors.unitframes.apbar = YAK_WHITE
	yakuiVar.Colors.unitframes.friendly = YAK_WHITE
	yakuiVar.Colors.unitframes.careerbar = YAK_WHITE
	yakuiVar.Colors.col = "Preset1"
	yakuiVar.Panel.tex = "Set5"
	if yakuiVar.Skin.active == true then
		yakuiVar.Skin.newbackgrounds.r = 16
		yakuiVar.Skin.newbackgrounds.g = 16
		yakuiVar.Skin.newbackgrounds.b = 16
		yakuiVar.Skin.newbackgrounds.alpha = 0.9
		yakuiVar.Skin.newbackgrounds.linkwpanel = false
		yakuiconfig.setlinkwpanel()
	end
	yakuiconfig.colorsliderUpdate()
	yakui.color()
	yakui.texture()
end

function yakuiconfig.colorselect2()
	SetProfileChecked(2)
	yakuiVar.Colors.colprintr = 24
	yakuiVar.Colors.colprintg = 24
	yakuiVar.Colors.colprintb = 24
	yakuiVar.Panel.Panelalpha = 0.8
	yakuiVar.Colors.background.r = 16
	yakuiVar.Colors.background.g = 16
	yakuiVar.Colors.background.b = 16
	yakuiVar.Panel.alpha = 0.7
	yakuiVar.Colors.fx.r = 180
	yakuiVar.Colors.fx.g = 220
	yakuiVar.Colors.fx.b = 255
	yakuiVar.Panel.fxalpha = 0.6
	yakuiVar.Colors.unitframes.player = YAK_WHITE
	yakuiVar.Colors.unitframes.hostile.r = 192
	yakuiVar.Colors.unitframes.hostile.g = 48
	yakuiVar.Colors.unitframes.hostile.b = 160
	yakuiVar.Colors.unitframes.castbar = YAK_WHITE
	yakuiVar.Colors.unitframes.apbar = YAK_WHITE
	yakuiVar.Colors.unitframes.friendly.r = 0
	yakuiVar.Colors.unitframes.friendly.g = 192
	yakuiVar.Colors.unitframes.friendly.b = 160
	yakuiVar.Colors.unitframes.careerbar = YAK_WHITE
	yakuiVar.Colors.col = "Preset2"
	yakuiVar.Panel.tex = "Set5"
	if yakuiVar.Skin.active == true then
		yakuiVar.Skin.newbackgrounds.r = 16
		yakuiVar.Skin.newbackgrounds.g = 16
		yakuiVar.Skin.newbackgrounds.b = 20
		yakuiVar.Skin.newbackgrounds.alpha = 0.9
		yakuiVar.Skin.newbackgrounds.linkwpanel = false
		yakuiconfig.setlinkwpanel()
	end
	yakuiconfig.colorsliderUpdate()
	yakui.color()
	yakui.texture()
end

function yakuiconfig.colorselect3()
	SetProfileChecked(3)
	yakuiVar.Colors.colprintr = 64
	yakuiVar.Colors.colprintg = 64
	yakuiVar.Colors.colprintb = 80
	yakuiVar.Panel.Panelalpha = 1
	yakuiVar.Colors.background.r = 12
	yakuiVar.Colors.background.g = 3
	yakuiVar.Colors.background.b = 9
	yakuiVar.Panel.alpha = 0.8
	yakuiVar.Colors.fx.r = 92
	yakuiVar.Colors.fx.g = 92
	yakuiVar.Colors.fx.b = 92
	yakuiVar.Panel.fxalpha = 0.8
	yakuiVar.Colors.unitframes.player.r = 180
	yakuiVar.Colors.unitframes.player.g = 190
	yakuiVar.Colors.unitframes.player.b = 200
	yakuiVar.Colors.unitframes.hostile.r = 168
	yakuiVar.Colors.unitframes.hostile.g = 40
	yakuiVar.Colors.unitframes.hostile.b = 120
	yakuiVar.Colors.unitframes.castbar.r = 180
	yakuiVar.Colors.unitframes.castbar.g = 190
	yakuiVar.Colors.unitframes.castbar.b = 200
	yakuiVar.Colors.unitframes.apbar.r = 180
	yakuiVar.Colors.unitframes.apbar.g = 190
	yakuiVar.Colors.unitframes.apbar.b = 200
	yakuiVar.Colors.unitframes.friendly.r = 120
	yakuiVar.Colors.unitframes.friendly.g = 140
	yakuiVar.Colors.unitframes.friendly.b = 220
	yakuiVar.Colors.unitframes.careerbar.r = 180
	yakuiVar.Colors.unitframes.careerbar.g = 190
	yakuiVar.Colors.unitframes.careerbar.b = 200
	yakuiVar.Colors.col = "Preset3"
	yakuiVar.Panel.tex = "Set3"
	if yakuiVar.Skin.active == true then
		yakuiVar.Skin.newbackgrounds.r = 16
		yakuiVar.Skin.newbackgrounds.g = 0
		yakuiVar.Skin.newbackgrounds.b = 6
		yakuiVar.Skin.newbackgrounds.alpha = 0.8
		yakuiVar.Skin.newbackgrounds.linkwpanel = true
		yakuiconfig.setlinkwpanel()
	end
	yakuiconfig.colorsliderUpdate()
	yakui.color()
	yakui.texture()
end

function yakuiconfig.colorselect4()
	SetProfileChecked(4)
	yakuiVar.Colors.colprintr = 24
	yakuiVar.Colors.colprintg = 24
	yakuiVar.Colors.colprintb = 24
	yakuiVar.Panel.Panelalpha = 1
	yakuiVar.Colors.background.r = 0
	yakuiVar.Colors.background.g = 12
	yakuiVar.Colors.background.b = 6
	yakuiVar.Panel.alpha = 0.8
	yakuiVar.Colors.fx.r = 200
	yakuiVar.Colors.fx.g = 200
	yakuiVar.Colors.fx.b = 0
	yakuiVar.Panel.fxalpha = 0.7
	yakuiVar.Colors.unitframes.player.r = 180
	yakuiVar.Colors.unitframes.player.g = 190
	yakuiVar.Colors.unitframes.player.b = 200
	yakuiVar.Colors.unitframes.hostile.r = 180
	yakuiVar.Colors.unitframes.hostile.g = 190
	yakuiVar.Colors.unitframes.hostile.b = 200
	yakuiVar.Colors.unitframes.castbar.r = 180
	yakuiVar.Colors.unitframes.castbar.g = 190
	yakuiVar.Colors.unitframes.castbar.b = 200
	yakuiVar.Colors.unitframes.apbar.r = 180
	yakuiVar.Colors.unitframes.apbar.g = 190
	yakuiVar.Colors.unitframes.apbar.b = 200
	yakuiVar.Colors.unitframes.friendly.r = 180
	yakuiVar.Colors.unitframes.friendly.g = 190
	yakuiVar.Colors.unitframes.friendly.b = 200
	yakuiVar.Colors.unitframes.careerbar.r = 180
	yakuiVar.Colors.unitframes.careerbar.g = 190
	yakuiVar.Colors.unitframes.careerbar.b = 200
	yakuiVar.Colors.col = "Preset4"
	yakuiVar.Panel.tex = "Set2"
	if yakuiVar.Skin.active == true then
		yakuiVar.Skin.newbackgrounds.r = 0
		yakuiVar.Skin.newbackgrounds.g = 12
		yakuiVar.Skin.newbackgrounds.b = 6
		yakuiVar.Skin.newbackgrounds.alpha = 0.8
		yakuiVar.Skin.newbackgrounds.linkwpanel = true
		yakuiconfig.setlinkwpanel()
	end
	yakuiconfig.colorsliderUpdate()
	yakui.color()
	yakui.texture()
end

function yakuiconfig.colorselect5()
	SetProfileChecked(5)
	yakuiVar.Colors.colprintr = 20
	yakuiVar.Colors.colprintg = 30
	yakuiVar.Colors.colprintb = 60
	yakuiVar.Panel.Panelalpha = 1
	yakuiVar.Colors.background.r = 10
	yakuiVar.Colors.background.g = 15
	yakuiVar.Colors.background.b = 30
	yakuiVar.Panel.alpha = 0.7
	yakuiVar.Colors.fx.r = 255
	yakuiVar.Colors.fx.g = 255
	yakuiVar.Colors.fx.b = 255
	yakuiVar.Panel.fxalpha = 0.8
	yakuiVar.Colors.unitframes.player = YAK_WHITE
	yakuiVar.Colors.unitframes.hostile = YAK_WHITE
	yakuiVar.Colors.unitframes.castbar = YAK_WHITE
	yakuiVar.Colors.unitframes.apbar = YAK_WHITE
	yakuiVar.Colors.unitframes.friendly = YAK_WHITE
	yakuiVar.Colors.unitframes.careerbar = YAK_WHITE
	yakuiVar.Colors.col = "Preset5"
	yakuiVar.Panel.tex = "Set1"
	if yakuiVar.Skin.active == true then
		yakuiVar.Skin.newbackgrounds.r = 10
		yakuiVar.Skin.newbackgrounds.g = 15
		yakuiVar.Skin.newbackgrounds.b = 30
		yakuiVar.Skin.newbackgrounds.alpha = 0.7
		yakuiVar.Skin.newbackgrounds.linkwpanel = true
		yakuiconfig.setlinkwpanel()
	end
	yakuiconfig.colorsliderUpdate()
	yakui.color()
	yakui.texture()
end

function yakuiconfig.colorselect6()
	SetProfileChecked(6)
	
	yakuiVar.Colors.colprintr = 24
	yakuiVar.Colors.colprintg = 0
	yakuiVar.Colors.colprintb = 24
	yakuiVar.Panel.Panelalpha = 1
	yakuiVar.Colors.background.r = 8
	yakuiVar.Colors.background.g = 0
	yakuiVar.Colors.background.b = 4
	yakuiVar.Panel.alpha = 0.9
	yakuiVar.Colors.fx.r = 142
	yakuiVar.Colors.fx.g = 160
	yakuiVar.Colors.fx.b = 142
	yakuiVar.Panel.fxalpha = 0.8
	yakuiVar.Colors.unitframes.player = YAK_WHITE
	yakuiVar.Colors.unitframes.hostile = YAK_WHITE
	yakuiVar.Colors.unitframes.castbar = YAK_WHITE
	yakuiVar.Colors.unitframes.apbar = YAK_WHITE
	yakuiVar.Colors.unitframes.friendly = YAK_WHITE
	yakuiVar.Colors.unitframes.careerbar = YAK_WHITE
	yakuiVar.Colors.col = "Preset6"
	yakuiVar.Panel.tex = "Set4"
	if yakuiVar.Skin.active == true then
		yakuiVar.Skin.newbackgrounds.r = 8
		yakuiVar.Skin.newbackgrounds.g = 0
		yakuiVar.Skin.newbackgrounds.b = 4
		yakuiVar.Skin.newbackgrounds.alpha = 0.7
		yakuiVar.Skin.newbackgrounds.linkwpanel = true
		yakuiconfig.setlinkwpanel()
	end
	yakuiconfig.colorsliderUpdate()
	yakui.color()
	yakui.texture()
end
