-- yakuislash.lua build20100524

yakuislash = {}

local booltowstring={[false]=L"Off",[true]=L"On"}

function yakuislash.init()
	LibSlash.RegisterWSlashCmd("yui", function(args) yakuislash.yui(args) end)
	LibSlash.RegisterWSlashCmd("rl", function(args) yakui.reloadui(args) end)
end

function yakuislash.yui(args)
	local opt, val = args:match(L"([a-z0-9]+)[ ]?(.*)")

--version
	if opt == nil then
			yakui.Print("")
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, towstring(yakuiVar.ver))
			yakui.Print("For usage of the chat commands enter /yui help .")
	end
	
	if opt == L"ver" then
			yakui.Print("")
			local screenWidth, screenHeight = GetScreenResolution()
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, towstring(yakuiVar.ver))
			yakui.Print("UI Scale: "..InterfaceCore.GetScale().." | Resolution: "..screenWidth.."x"..screenHeight)
	end

	if opt == L"counter" then
		yakui.Print(L"The Interface has been loaded "..yakuiVar.counter..L" times after the installation.")
	end
	
--help
	if opt == L"help" then
		yakui.Print("")
		TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, towstring(yakuiVar.ver))
		yakui.Print("syntax: /yui <command> <value>")
		yakui.Print("")
		yakui.Print("usable commands:")
		yakui.Print("help   - shows help")
		yakui.Print("config   - opens the configuration")
		yakui.Print("ver   - shows YakUI version and settings")
		yakui.Print("scale   - scales the panel textures")
		yakui.Print("igeffigy    - YakUI will no longer change Effify settings")
		yakui.Print("")
	end		

-- open menu
	if opt ==L"config" then yakuiconfig.toggle() end

-- Scaling
	if opt == L"scale" then
		local panelxscale = yakui.panelxscale
		local panelyscale = yakui.panelyscale
		if val == L"" then
				yakui.Print("")
				TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, towstring(yakuiVar.ver))
				yakui.Print("This function toggles the panel scaling.")
				yakui.Print("Syntax: /yui scale <value>")
				yakui.Print("usable values: adjust, 0.5 - 1.5")
		else
		if val == L"adjust" then
		WindowSetDimensions("YBorder",math.ceil((panelxscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((panelyscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
		WindowSetDimensions("YFX",math.ceil((panelxscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((panelyscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
		WindowSetDimensions("YPanel",math.ceil((panelxscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((panelyscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
		else
			yakuiVar.Panel.scale = val
			yakuiVar.Panel.scale = tonumber(yakuiVar.Panel.scale)
			if yakuiVar.Panel.scale > 1.5 then yakuiVar.Panel.scale = 1.5 end
			if yakuiVar.Panel.scale < 0.5 then yakuiVar.Panel.scale = 0.5 end
			WindowSetDimensions("YBorder",math.ceil((panelxscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((panelyscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
			WindowSetDimensions("YPanel",math.ceil((panelxscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((panelyscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
			WindowSetDimensions("YFX",math.ceil((panelxscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((panelyscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
			--WindowSetDimensions("YMPanel",math.ceil((mwmxscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((mwmyscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
			WindowSetDimensions("YMBorder",math.ceil((mwmxscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((mwmyscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
			WindowSetDimensions("YMFX",math.ceil((mwmxscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((mwmyscale*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
			WindowClearAnchors("YMPanel")
			WindowClearAnchors("YMBorder")
			WindowClearAnchors("YMFX")
			WindowAddAnchor("YMPanel","bottomleft","Root","bottomleft",math.ceil((mwmxanch*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((mwmyanch*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
			WindowAddAnchor("YMBorder","bottomleft","Root","bottomleft",math.ceil((mwmxanch*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((mwmyanch*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
			WindowAddAnchor("YMFX","bottomleft","Root","bottomleft",math.ceil((mwmxanch*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)),math.ceil((mwmyanch*(1/InterfaceCore.GetScale()))*(1/yakuiVar.Panel.scale)))
		end
		if yakuiVar.Panel.config == "on" then
			yuiconfighide()
			yuiconfigshow()
		end
		yakui.Print(L"Scale: "..yakuiVar.Panel.scale)
		yakui.Print(L"X: "..math.ceil(panelxscale*(1/yakuiVar.Panel.scale)))
		yakui.Print(L"Y: "..math.ceil(panelyscale*(1/yakuiVar.Panel.scale)))
		end
	end
end
