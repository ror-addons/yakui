-- yakuiWHL.lua build20100524

yakuiWHL = {}
primary = 0
secondary = 0
itme = 0
function yakuiWHL.initialize()
		
		items ={L"All", L"Body", L"Gloves", L"Boots", L"Helm", L"Shoulders", L"belt" }
		dyes = {L"Azyr Blue", L"Bestial Brown", L"Blazing Orange", L"Blood Red", L"Bone Brown", L"Brazen Brass",
				L"Chainmail Grey", L"Chaos Black", L"Dark Ivory Dye", L"Dark Navy Blue", L"Dark Peach Dye", 
				L"Dark Purple", L"Dark Red", L"Dark Sage", L"Dark Sea Blue", L"Dark Seafoam Dye", L"Dark Warm Sage",
				L"Enchanted Blue", L"Fortress Grey", L"Goblin Green", L"Golden Yellow",	L"Graveyard Earth", 
				L"Liche Purple", L"Light Grey Dye", L"Light Ivory Dye", L"Light Sage Dye", 
				L"Medium Olive Dye", L"Medium Red Dye", L"Medium Sky Blue Dye", L"Medium Yellow Dye", L"Midnight Blue", 
				L"Orc Green", L"Red Gore", L"Regal Blue", L"Scab Red", L"Scaly Green", L"Scorched Brown", L"Seaguard Blue", 
				L"Shadow Grey",L"Shyish Purple", L"Skull White", L"Snot Green", L"Tanned Flesh", L"Ulgu Grey", 
				L"Vermin Brown", L"Warlock Purple"}
		
		dyenr = {213, 193, 198, 187, 192, 197, 179, 181, 40, 4, 25, 37, 31, 13, 7, 10, 61, 214, 183, 205, 196, 190, 
				216, 45, 42, 15, 17, 32, 2, 20, 212, 203, 188, 211, 219, 207, 185, 209, 208, 217, 180, 204, 194, 
				182, 195, 215}
		
		CreateWindow("yDye",false)
		CreateWindow("yDyeToogleButton",true) --yak		
		WindowSetParent("yDyeToogleButton", "CharacterWindow") --yak		
		
		-- Write text in the label
		LabelSetText("yDyePrimaryColor", L"Primary color")
		LabelSetText("yDyeSecondaryColor", L"Secondary color")
		LabelSetText("yDyeItems", L"Item to dye")
		ButtonSetText("yDyeBtn1", L"Default")
		
		--add items to dropdown boxes
		for _, v in ipairs (dyes)
			do
				ComboBoxAddMenuItem ("yDyeCombo", v)			
		end
		for _, v in ipairs (dyes)
			do
				ComboBoxAddMenuItem ("yDyeCombo2", v)
		end	
	   	for _, v in ipairs (items)
			do
				ComboBoxAddMenuItem ("yDyeCombo3", v)
		end
		ComboBoxSetSelectedMenuItem ("yDyeCombo3", 1)
		ComboBoxSetSelectedMenuItem ("yDyeCombo2", 1)
		ComboBoxSetSelectedMenuItem ("yDyeCombo", 1)
end




------------------------------------------------------------------------------------------------------

function DyeItem()
	DyeMerchantPreview( GameData.ItemLocs.EQUIPPED, item, primary, secondary)
end

function DyeAll()
	DyeMerchantPreviewAll(primary, secondary)
end 

function DyeIt()
	if ComboBoxGetSelectedMenuItem("yDyeCombo3") == 1 then
		setColor1()
		DyeAll()
	end
	if ComboBoxGetSelectedMenuItem("yDyeCombo3") == 2 then
		item = 6
		setColor1()
		DyeItem()
	elseif ComboBoxGetSelectedMenuItem("yDyeCombo3") == 3 then
		item = 7
		setColor1()
		DyeItem()
	elseif ComboBoxGetSelectedMenuItem("yDyeCombo3") == 4 then
		item = 8
		setColor1()
		DyeItem()
	elseif ComboBoxGetSelectedMenuItem("yDyeCombo3") == 5 then
		item = 9
		setColor1()
		DyeItem()
	elseif ComboBoxGetSelectedMenuItem("yDyeCombo3") == 6 then
		item = 10
		setColor1()
		DyeItem()
	elseif ComboBoxGetSelectedMenuItem("yDyeCombo3") == 7 then
		item = 14
		setColor1()
		DyeItem()
	end
end


function setColor1()
	primary = dyenr[ComboBoxGetSelectedMenuItem("yDyeCombo")]
	secondary = dyenr[ComboBoxGetSelectedMenuItem("yDyeCombo2")]
end	

function DyeCW()
	WindowSetShowing("yDye", false)
end

function DyeReset()
	RevertAllDyePreview()
end

function yakuiWHL.DyeToogle()
	WindowSetShowing("yDye", not WindowGetShowing("yDye"))
end