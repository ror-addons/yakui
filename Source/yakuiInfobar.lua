-- yakuiInfobar.lua build20100524

local VERSION     = "1.3.5"

yakuiInfo         = {}
yakuiInfo.informer = {}
yakuiInfo.itable   = {}
yakuiInfo.menu_module = {}
yakuiInfo.social_module = {}
yakuiInfo.time_module = {}
yakuiInfo.stat_module = {}
yakuiInfo.loc_module = {}
yakuiInfo.bag_info_module = {}

local default     = {
                     VERSION        = "1.3.1",
                     PRESET         = 0,
                     FONT           = 6,
                     ICON           = 2,
                     TEXT_COL       = {200,200,200},
                     BACK_COL       = {000,000,000},
                     BORD_COL       = {255,255,255},
                     SCALE          = 0.72839504480362,
                     BACK_ALPHA     = 0,
                     BORD_ALPHA     = 0,
                     STAT_ALPHA     = 1.0,
                     OLD_STYLE      = true,
                     SHOW_ICON      = true,
                     SHOW_STAT      = false,             
                     DEFAULT_SIZE   = true,
                     }
                     
local DB
local QUAD        = 32
local FONT_W      = 16      
local FONT_H      = 24
local SPACE       = 256
local OLD_COEFF   = 32

---------------------------------------------------------------------------------------------------------------------
local function chat(msg) TextLogAddEntry("Chat",2,StringToWString(msg)) end
local function alert(msg)
   SystemData.AlertText.VecType = {SystemData.AlertText.Types.STATUS_ACHIEVEMENTS_PURPLE}
   SystemData.AlertText.VecText = {StringToWString(msg)}
   AlertTextWindow.AddAlert()
end
---------------------------------------------------------------------------------------------------------------------
local CreateWindowFromTemplate,WindowSetDimensions,WindowClearAnchors,WindowAddAnchor,
      WindowRegisterCoreEventHandler,WindowSetLayer,DestroyWindow,RegisterEventHandler,
      DynamicImageSetTextureDimensions,DynamicImageSetTexture,
      StatusBarSetMaximumValue,StatusBarSetCurrentValue,StatusBarSetForegroundTint,
      LabelSetText,LabelSetTextColor =
     _G["CreateWindowFromTemplate"],_G["WindowSetDimensions"],_G["WindowClearAnchors"],_G["WindowAddAnchor"],
     _G["WindowRegisterCoreEventHandler"],_G["WindowSetLayer"],_G["DestroyWindow"],_G["RegisterEventHandler"],
     _G["DynamicImageSetTextureDimensions"],_G["DynamicImageSetTexture"],
     _G["StatusBarSetMaximumValue"],_G["StatusBarSetCurrentValue"],_G["StatusBarSetForegroundTint"],
     _G["LabelSetText"],_G["LabelSetTextColor"] 
---------------------------------------------------------------------------------------------------------------------
local chars = 
   {
   [0x30] = 000,[0x31] = 016,[0x32] = 032,[0x33] = 048,[0x34] = 064,[0x35] = 080,[0x36] = 096,
   [0x37] = 112,[0x38] = 128,[0x39] = 144,[0x2E] = 160,[0x2C] = 176,[0x3A] = 190,[0x5B] = 206,
   [0x5D] = 222,[0x25] = 240,[0x20] = 256,[0x2F] = 272,[0x2A] = 288,}

-----------------------------------------------------------------------------------------------------------------------
-- INFORMER CLASS (icon - num on snt icons file)
-----------------------------------------------------------------------------------------------------------------------
function yakuiInfo.informer:new(wname,slen,icon,ovf,lcf,rcf,reg)
   local instance    = {}
   instance.ctable   = {}
   instance.wname    = wname 
   instance.icon     = icon   
   instance.slen     = slen
   instance.ovf      = ovf  
   instance.lcf      = lcf 
   instance.rcf      = rcf
   instance.len = slen* FONT_W + QUAD + 1
   instance.ctable[0]= DB.TEXT_COL
   if(DB.OLD_STYLE) then      CreateWindowFromTemplate(wname,"YAKUI_INFO_OLD_TPL","Root")
   else                       CreateWindowFromTemplate(wname,"YAKUI_INFO_TPL","Root")
   end
   if(DB.DEFAULT_SIZE) then   WindowSetDimensions(wname,instance.len,QUAD) end
   local x,y = WindowGetDimensions(wname)
   local x_dist      = x/instance.len
   local y_dist      = y/QUAD
   local SIDE_LEN    = QUAD * x_dist
   local CENTER_LEN  = x - x_dist*QUAD*2

   if(DB.SHOW_ICON) then DynamicImageSetTexture(wname.."_icon","snt_info_icons",DB.ICON*QUAD,icon*QUAD) end
   if(ovf) then WindowRegisterCoreEventHandler(wname.."_icon","OnMouseOver",ovf)  end
   if(lcf) then WindowRegisterCoreEventHandler(wname,"OnLButtonDown" ,lcf)  end
   if(rcf) then WindowRegisterCoreEventHandler(wname,"OnRButtonDown" ,rcf)  end
   if(not DB.OLD_STYLE) then	-- unused in yakui
      WindowSetDimensions(wname.."_left",SIDE_LEN,y)  
      WindowSetDimensions(wname.."_center",CENTER_LEN,y) 
      WindowSetDimensions(wname.."_right",SIDE_LEN,y) 
      WindowSetDimensions(wname.."_icon",SIDE_LEN,y)
      WindowSetDimensions(wname.."_status",CENTER_LEN+SIDE_LEN-2,y)
      WindowClearAnchors(wname.."_left")  WindowClearAnchors(wname.."_center") 
      WindowClearAnchors(wname.."_right") WindowClearAnchors(wname.."_icon")
      WindowClearAnchors(wname.."_status")
      WindowAddAnchor(wname.."_icon","topleft",wname,"topleft",0,0) 
      WindowAddAnchor(wname.."_left","topleft",wname,"topleft",0,0)
      WindowAddAnchor(wname.."_status","right",wname.."_left","left",0,0) 
      WindowAddAnchor(wname.."_center","right",wname.."_left","left",0,0)
      WindowAddAnchor(wname.."_right","right",wname.."_center","left",0,0)
 
      DynamicImageSetTextureDimensions(wname.."_left",QUAD,QUAD)  DynamicImageSetTextureDimensions(wname.."_center",QUAD,QUAD)
      DynamicImageSetTextureDimensions(wname.."_right",QUAD,QUAD) DynamicImageSetTextureDimensions(wname.."_icon",QUAD,QUAD)

      DynamicImageSetTexture(wname.."_left"  , "snt_info_textures",0    ,DB.PRESET*QUAD)
      DynamicImageSetTexture(wname.."_center","snt_info_textures" ,32   ,DB.PRESET*QUAD)
      DynamicImageSetTexture(wname.."_right" ,"snt_info_textures" ,64   ,DB.PRESET*QUAD)
      WindowSetAlpha(wname.."_left",  DB.BACK_ALPHA)
      WindowSetAlpha(wname.."_right", DB.BACK_ALPHA)
      WindowSetAlpha(wname.."_center",DB.BACK_ALPHA)
      if(slen == 0) then 
         WindowSetShowing(wname.."_center",false) WindowSetShowing(wname.."_right",false)
      else
         for ndx = 1,instance.slen do
            CreateWindowFromTemplate(wname.."l"..ndx,"YAKUI_INFO_LETTER",wname)
            WindowSetDimensions(wname.."l"..ndx,FONT_W*x_dist,FONT_H*y_dist)
            WindowSetScale(wname.."l"..ndx,DB.SCALE)
            if(ndx == 1) then WindowAddAnchor(wname.."l"..ndx,"left",wname.."_center","left",0,0)
            else WindowAddAnchor(wname.."l"..ndx,"right",wname.."l"..ndx-1,"left",0,0)
            end
            DynamicImageSetTexture(wname.."l"..ndx,"snt_info_font",SPACE,DB.FONT*FONT_H)
            WindowSetTintColor(wname.."l"..ndx,unpack(DB.TEXT_COL))
         end
      end
   else	-- used in yakui
      LabelSetTextColor(wname.."_label",unpack(DB.TEXT_COL))
      WindowSetAlpha(wname.."_back",  DB.BACK_ALPHA)
      WindowSetAlpha(wname.."_border",DB.BORD_ALPHA)
      WindowSetTintColor(wname.."_back",unpack(DB.BACK_COL))
      WindowSetTintColor(wname.."_border",unpack(DB.BORD_COL))
   end
   WindowSetAlpha(wname.."_status",DB.STAT_ALPHA)
   WindowSetScale(wname,DB.SCALE)
   if(not reg) then LayoutEditor.RegisterWindow(wname,StringToWString(wname),StringToWString(wname),true,true,true,nil) end
   table.insert(yakuiInfo.itable,instance)
   setmetatable(instance,self)   
   self.__index   = self
   return instance
end
---------------------------------------------------------------------------------------------------------------------
function yakuiInfo.informer:set_text(txt) 
   if(DB.OLD_STYLE) then
      LabelSetText(self.wname.."_label",towstring(txt))
   else
      for ndx = 1,self.slen do DynamicImageSetTexture(self.wname.."l"..ndx,"snt_info_font",SPACE,DB.FONT*FONT_H) end
      if (type(txt) == "string") then
         for ndx = 1, string.len(txt) do
            local char = string.byte(txt,ndx)
            if(ndx <= self.slen) then
				local tex
				if(char > 0x60 and char < 0x7A) then tex = 304+FONT_W*(char-0x61)
				else tex = chars[char] or SPACE
				end
				DynamicImageSetTexture(self.wname.."l"..ndx,"snt_info_font",tex,DB.FONT*FONT_H) 
            end
         end
      end
   end
end
 
---------------------------------------------------------------------------------------------------------------------
function yakuiInfo.informer:set_text_col(r,g,b)    
   if(DB.OLD_STYLE) then
      LabelSetTextColor(self.wname.."_label",r,g,b)
   else
      for ndx = 1,self.slen do WindowSetTintColor(self.wname.."l"..ndx,r,g,b) end
   end
end
function yakuiInfo.informer:set_col_val(n,r,g,b) self.ctable[n] = {r,g,b} end    

---------------------------------------------------------------------------------------------------------------------
function yakuiInfo.informer:set_gradient(val,bad,good)
   local percent,r,g
   if (good > bad) then percent = val/(good-bad) else percent = 1-val/(bad-good) end
   if (percent > 1) then percent = 1 end if (percent < 0) then percent = 0 end
   if(percent < 0.5) then r,g = 1,2*percent   else  r,g = (1-percent)*2,1 end
   if(DB.OLD_STYLE) then
      LabelSetTextColor(self.wname.."_label",r*255,g*255,0)
   else
      for ndx = 1,self.slen do WindowSetTintColor(self.wname.."l"..ndx,r*255,g*255,0) end
   end
end
---------------------------------------------------------------------------------------------------------------------
function yakuiInfo.informer:set_status_val(val,val_max)
   if(DB.SHOW_STAT) then 
      StatusBarSetMaximumValue(self.wname.."_status",val_max)
      StatusBarSetCurrentValue(self.wname.."_status",val)
   end
end 
---------------------------------------------------------------------------------------------------------------------
function yakuiInfo.informer:set_status_col(r,g,b) StatusBarSetForegroundTint(self.wname.."_status",r,g,b) end

--
function yakuiInfo.update_informers()
   for ndx = 1 ,#yakuiInfo.itable do
      local self =  yakuiInfo.itable[ndx]
      WindowSetScale(self.wname,DB.SCALE)
      WindowSetAlpha(self.wname.."_status",DB.STAT_ALPHA)
      if(DB.SHOW_ICON)then DynamicImageSetTexture(self.wname.."_icon","snt_info_icons",DB.ICON*QUAD ,self.icon*QUAD) end
      if(DB.OLD_STYLE) then
         WindowSetAlpha(self.wname.."_back",  DB.BACK_ALPHA)
         WindowSetAlpha(self.wname.."_border",DB.BORD_ALPHA)
         WindowSetTintColor(self.wname.."_back",unpack(DB.BACK_COL))
         WindowSetTintColor(self.wname.."_border",unpack(DB.BORD_COL))
         LabelSetTextColor(self.wname.."_label",unpack(DB.TEXT_COL))  
      else
         WindowSetAlpha(self.wname.."_left",  DB.BACK_ALPHA)
         WindowSetAlpha(self.wname.."_right", DB.BACK_ALPHA)
         WindowSetAlpha(self.wname.."_center",DB.BACK_ALPHA)
         DynamicImageSetTexture(self.wname.."_left"  , "snt_info_textures",0    ,DB.PRESET*QUAD)
         DynamicImageSetTexture(self.wname.."_center","snt_info_textures" ,32   ,DB.PRESET*QUAD)
         DynamicImageSetTexture(self.wname.."_right" ,"snt_info_textures" ,64   ,DB.PRESET*QUAD) 
         for xxl = 1,self.slen do
            WindowSetScale(self.wname.."l"..xxl,DB.SCALE)
            WindowSetTintColor(self.wname.."l"..xxl,unpack(DB.TEXT_COL))
         end
      end
   end
end



---------------------------------------------------------------------------------------------------------------------
-- ENTRY POINT
---------------------------------------------------------------------------------------------------------------------

function yakuiInfo.entry_point()
   d("!> ENTRY POINT") 
   DB = default 

	RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakuiInfo.time_module.update")
	yakuiInfo.menu_module.entry_point()
	yakuiInfo.social_module.entry_point()
	yakuiInfo.time_module.entry_point()
	yakuiInfo.stat_module.entry_point()
	yakuiInfo.fps_moduleentry_point()
	yakuiInfo.bag_info_module.entry_point()
	if yakuiVar.fps == true then
		RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakuiInfo.fps_moduleupdate")
		WindowSetShowing("snt_info_fps", true)
	else
		WindowSetShowing("snt_info_fps", false)
	end
	-- yakuiInfo.loc_moduleentry_point() -- this function gets called in the yakui.lua, delayed after loading the ui
end   


--MENU---------------------------------------------------------------------------------------------------------------



function yakuiInfo.menu_module.lclick1() WindowUtils.ToggleShowing("AbilitiesWindow") end
function yakuiInfo.menu_module.rclick1() yakuiInfo.togglecareer() end
function yakuiInfo.menu_module.lclick2() MenuBarWindow.ToggleCharacterWindow() end
function yakuiInfo.menu_module.rclick2() yakuiInfo.togglemastery() end
function yakuiInfo.menu_module.lclick3() yakuiInfo.togglereown() end
function yakuiInfo.menu_module.rclick3() yakuiInfo.toggletome() end
function yakuiInfo.menu_module.lclick4() EA_Window_Backpack.ToggleShowing() end
function yakuiInfo.menu_module.rclick4() 
	WindowUtils.ToggleShowing("EA_Window_OpenParty")
	WindowSetShowing("EA_Window_OpenPartyNearby", not WindowGetShowing("EA_Window_OpenParty") )
	ButtonSetPressedFlag("EA_Window_OpenPartyTabNearby", not WindowGetShowing("EA_Window_OpenParty") )
	WindowSetShowing("EA_Window_OpenPartyWorld", not WindowGetShowing("EA_Window_OpenParty") )
	ButtonSetPressedFlag("EA_Window_OpenPartyTabWorld", not WindowGetShowing("EA_Window_OpenParty") )
	WindowSetShowing("EA_Window_OpenPartyLootRollOptions", WindowGetShowing("EA_Window_OpenParty") )
	ButtonSetPressedFlag("EA_Window_OpenPartyTabLootRollOptions", WindowGetShowing("EA_Window_OpenParty") )
	if DoesWindowExist("TidyRollOptions") == true then
		if WindowGetShowing("TidyRollOptions") == not WindowGetShowing("EA_Window_OpenParty") then TidyRoll.ToggleOptions() end
	else 
		yakui.Print("TidyRoll not found")
	end
end
function yakuiInfo.menu_module.lclick5()
	if WindowGetShowing("TomeWindow")==true then
		WindowUtils.ToggleShowing("TomeWindow") 
	else
		WindowUtils.ToggleShowing("TomeWindow") 
		TomeWindow.OnIntroductionBookmark()
	end
end
function yakuiInfo.menu_module.rclick5()
	if WindowGetShowing("TomeWindow")==true then
		WindowUtils.ToggleShowing("TomeWindow") 
	else
		WindowSetShowing( "TomeWindow", true )
		if DammazKron then TomeWindow.DK_OnBookmark() end
	end
end
function yakuiInfo.menu_module.lclick6() yakui.perftoggle() end
function yakuiInfo.menu_module.rclick6() WindowUtils.ToggleShowing("UiModWindow") end
function yakuiInfo.menu_module.lclick7()
	if Dascore then
		if WindowGetShowing("DascoreWin1Window") == false then Dascore.Win1.Show("")
		else 
			WindowSetShowing("DascoreWin1Window", false)
			WindowSetShowing("DascoreWin2Window", false)
		WindowSetShowing("ScenarioSummaryWindow", false)
		end
	else
		yakui.Print("DaScore not found")
	end
end
function yakuiInfo.menu_module.rclick7()
	if ScenarioStats then
		if WindowGetShowing("ScenarioStatsWindow") == false then ScenarioStats.ShowWindowByName("")
		else WindowSetShowing("ScenarioStatsWindow", false) end
	else
		yakui.Print("ScenarioStats not found")
	end
end
function yakuiInfo.menu_module.lclick8()
	if DoesWindowExist("TidyRollOptions") == true then
		if WindowGetShowing("TidyRollOptions") == true then TidyRoll.ToggleOptions() end
	end
	WindowUtils.ToggleShowing("EA_Window_OpenParty")
	WindowSetShowing("EA_Window_OpenPartyNearby", WindowGetShowing("EA_Window_OpenParty") )
	ButtonSetPressedFlag("EA_Window_OpenPartyTabNearby", WindowGetShowing("EA_Window_OpenParty") )
	WindowSetShowing("EA_Window_OpenPartyWorld", not WindowGetShowing("EA_Window_OpenParty") )
	ButtonSetPressedFlag("EA_Window_OpenPartyTabWorld", not WindowGetShowing("EA_Window_OpenParty") )
	WindowSetShowing("EA_Window_OpenPartyLootRollOptions", not WindowGetShowing("EA_Window_OpenParty") )
	ButtonSetPressedFlag("EA_Window_OpenPartyTabLootRollOptions", not WindowGetShowing("EA_Window_OpenParty") )
end
function yakuiInfo.menu_module.rclick8()
	if DoesWindowExist("TidyRollOptions") == true then
		if WindowGetShowing("TidyRollOptions") == true then TidyRoll.ToggleOptions() end
	end
	WindowUtils.ToggleShowing("EA_Window_OpenParty")
	WindowSetShowing("EA_Window_OpenPartyNearby", not WindowGetShowing("EA_Window_OpenParty") )
	ButtonSetPressedFlag("EA_Window_OpenPartyTabNearby", not WindowGetShowing("EA_Window_OpenParty") )
	WindowSetShowing("EA_Window_OpenPartyWorld", WindowGetShowing("EA_Window_OpenParty") )
	ButtonSetPressedFlag("EA_Window_OpenPartyTabWorld", WindowGetShowing("EA_Window_OpenParty") )
	WindowSetShowing("EA_Window_OpenPartyLootRollOptions", not WindowGetShowing("EA_Window_OpenParty") )
	ButtonSetPressedFlag("EA_Window_OpenPartyTabLootRollOptions", not WindowGetShowing("EA_Window_OpenParty") )
end


-----------------------------------------------------------------------------------------------------------------------

function yakuiInfo.menu_module.entry_point()
   LayoutEditor.UnregisterWindow("MenuBarWindow")
   --WindowSetShowing("MenuBarWindow",false)
   i_menu1 = yakuiInfo.informer:new("i_menu1",0,14,nil,"yakuiInfo.menu_module.lclick1","yakuiInfo.menu_module.rclick1")
   i_menu2 = yakuiInfo.informer:new("i_menu2",0,07,nil,"yakuiInfo.menu_module.lclick2","yakuiInfo.menu_module.rclick2")--,true)
   i_menu3 = yakuiInfo.informer:new("i_menu3",0,13,nil,"yakuiInfo.menu_module.lclick3","yakuiInfo.menu_module.rclick3")--,true)
   i_menu4 = yakuiInfo.informer:new("i_menu4",0,12,nil,"yakuiInfo.menu_module.lclick4","yakuiInfo.menu_module.rclick4")--,true)
   i_menu5 = yakuiInfo.informer:new("i_menu5",0,08,nil,"yakuiInfo.menu_module.lclick5","yakuiInfo.menu_module.rclick5")--,true)
   i_menu6 = yakuiInfo.informer:new("i_menu6",0,15,nil,"yakuiInfo.menu_module.lclick6","yakuiInfo.menu_module.rclick6")--,true)
   i_menu7 = yakuiInfo.informer:new("i_menu7",0,05,nil,"yakuiInfo.menu_module.lclick7","yakuiInfo.menu_module.rclick7")--,true)
   i_menu8 = yakuiInfo.informer:new("i_menu8",0,16,nil,"yakuiInfo.menu_module.lclick8","yakuiInfo.menu_module.rclick8")--,true)
   for ndx = 2,8 do
      WindowClearAnchors("i_menu"..ndx)
      WindowAddAnchor("i_menu"..ndx,"right","i_menu"..ndx-1,"left",0,0)
   end
end
    
--SOCIAL-------------------------------------------------------------------------------------------------------------

local Social = {}

local Career = {
   [20] = L"IRB", -- IronBreaker
   [100] = L"SWO", -- Swordmaster
   [60] = L"WIT", -- Witch Hunter
   [102] = L"LIO", -- White Lion
   [62] = L"WIZ", -- Bright Wizard
   [23] = L"ENG", -- Engineer
   [101] = L"SHD", -- Shadow Warrior
   [63] = L"WRP",  -- Warrior Priest
   [103] = L"ARC", -- Archmage
   [22] = L"RUP", -- Rune Priest
   [61] = L"KBS", -- Knight of the Blazing Sun
   [21] = L"SLY", --Slayer

   [104] = L"BKG", -- Blackguard
   [66] = L"ZEA", -- Zealot
   [26] = L"SHA", -- Shaman
   [67] = L"MAG", -- Magus
   [107] = L"SOC", -- Sorcerer
   [27] = L"SQH", -- Squig Herder
   [106] = L"DOK", -- Disciple of Khaine
   [65] = L"MAR", -- Marauder
   [105] = L"WEF", -- Witch Elf
   [24] = L"ORC", -- Black Orc
   [64] = L"CHO", -- Chosen
   [25] = L"CHP", --Choppa
}

local getn, ins = table.getn, table.insert

function  yakuiInfo.social_module.gl_update()
   local gl_list,online = GetGuildMemberData(),0
   local total = #gl_list
   for ndx = 1, total do if (gl_list[ndx].zoneID ~= 0) then online = online+1 end end
   i_gl:set_text(tostring(online).."/"..tostring(total))
end

function yakuiInfo.social_module.fr_update()
   local fr_list, online = GetFriendsList(),0
   local total = #fr_list
  for ndx = 1, total do if (fr_list[ndx].zoneID ~= 0) then online = online+1 end end
   i_fr:set_text(tostring(online).."/"..tostring(total))
end

function yakuiInfo.social_module.fr_click() 
	WindowUtils.ToggleShowing("SocialWindow")
	SocialWindow.ShowTab(SocialWindow.TABS_FRIENDS)
end

function yakuiInfo.social_module.gl_click() 
	if (not GameData.Guild.m_GuildRank) or GameData.Guild.m_GuildRank < 17 then
		WindowUtils.ToggleShowing("GuildWindow")
	else
		WindowUtils.ToggleShowing("GuildWindow")
		GuildWindow.SelectTab(3)
	end
end

local function sf(a,b) return a.name<b.name end
local TT = Tooltips.SetTooltipText
local TC = Tooltips.SetTooltipColor
local i,k

function yakuiInfo.social_module.gl_tooltip()
	local guildList, onlineGuildList = GetGuildMemberData(), {}
	for _,k in pairs(guildList) do
		if(k.zoneID~=0) then
			ins(onlineGuildList,k)
		end
	end
	if(#onlineGuildList==0)then return end --If no Guildies online don't show tooltips
	table.sort(onlineGuildList,sf)

	Tooltips.CreateTextOnlyTooltip(i_gl.wname.."_icon", nil)
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_TOP)
	TT( 1, 1, GameData.Guild.m_GuildName..L"\n" )
	TC( 1, 1, 0, 255, 0)

	local c1=L""
	local c2=L""
	local t=2
	for i,k in pairs(onlineGuildList) do
		if ( k.rank < 10 ) then -- Silly one, just for pretty printing
			c1=c1..L"Lv "..k.rank..L"    "..Career[k.careerID]..L"   "..k.name:sub(0,20)
		else
			c1=c1..L"Lv"..k.rank..L"   "..Career[k.careerID]..L"   "..k.name:sub(0,20)
		end
		c2=c2..towstring(k.note:sub(0,20):gsub(L"[\n|%s+]",L" "))
		if(i~=getn(onlineGuildList)) then
			c1=c1..L"\n"
			c2=c2..L"\n"
			if(c1:len()>800) then
				TT(t,1,c1)
				TT(t,3,c2)
				c1=L""
				c2=L""
				t=t+1
			end
		end
	end
	TT(t,1,c1)
	TT(t,3,c2)
	if GameData.Guild.Alliance.Id ~= 0 then
		local allianceName = towstring(GameData.Guild.Alliance.Name)
		local totalOnline = 0
		local total = 0
		for guildIndex, guild in ipairs( GuildWindowTabAlliance.guilds ) do
			totalOnline = totalOnline + guild.online
			total = total + guild.online + guild.offline
		end
		local allianceonline = towstring(totalOnline)..L" / "..towstring(total)
		TC(t+1, 1, 0, 255, 0)
		TT(t+1,1,L"\n"..allianceName)
		TC(t+2, 1, 255, 200, 0)
		TT(t+2,1,allianceonline)
		t=t+2
	end
	Tooltips.Finalize()
end

function yakuiInfo.social_module.fr_tooltip()
	local friendList, onlineFriendList = GetFriendsList(),{}
	for _,k in pairs(friendList)
	do
		if(k.zoneID~=0)
		then
			ins(onlineFriendList,k)
		end
	end
	if(#onlineFriendList==0)then return end
	table.sort(onlineFriendList,sf)

	Tooltips.CreateTextOnlyTooltip(i_fr.wname.."_icon",nil)
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_TOP)
	TT( 1, 1, L"Friends:\n" )
	TC( 1, 1, 0, 255, 0)

	local liststring = L""
	local t=2
	for i,k in pairs(onlineFriendList)
	do
		if ( k.rank < 10 ) then -- Silly one, just for pretty printing
			liststring = liststring..L"Lv "..k.rank..L"    "..Career[k.careerID]..L"   "..k.name:sub(0,20)
		else
			liststring = liststring..L"Lv"..k.rank..L"   "..Career[k.careerID]..L"   "..k.name:sub(0,20)
		end
		if ( i ~= getn(onlineFriendList) ) then
			liststring = liststring..L"\n"
			if(liststring:len()>800)
			then
				TT( t, 1, liststring )
				liststring=L""
				t=t+1
			end
		end
	end
	TT( t, 1, liststring )
	Tooltips.Finalize()
end

function yakuiInfo.social_module.entry_point()
   d("!> ENTRY")
   RegisterEventHandler(SystemData.Events.GUILD_MEMBER_UPDATED,   "yakuiInfo.social_module.gl_update")
   RegisterEventHandler(SystemData.Events.SOCIAL_FRIENDS_UPDATED, "yakuiInfo.social_module.fr_update")
   i_fr = yakuiInfo.informer:new("i_fr",5,4,"yakuiInfo.social_module.fr_tooltip","yakuiInfo.social_module.fr_click",nil)
   i_gl = yakuiInfo.informer:new("i_gl",7,5,"yakuiInfo.social_module.gl_tooltip","yakuiInfo.social_module.gl_click",nil)
   yakuiInfo.social_module.fr_update()
   yakuiInfo.social_module.gl_update()
end

--TIME--------------------------------------------------------------------------------------------------------------------

local cur_s,cur_m,cur_h 

function yakuiInfo.time_module.update(elapsed)
   local last_entry,last_time = TextLogGetNumEntries("Chat") - 1,0
   if (last_entry >= 0) then last_time = TextLogGetEntry("Chat",last_entry)
   else return end
   if (not yakuiInfo.time_module.clock_update) then yakuiInfo.time_module.clock_update = last_time end
   if (last_time ~= yakuiInfo.time_module.last_update) then
      yakuiInfo.time_module.last_update = last_time
      local h,m,s = last_time:match(L"([0-9]+):([0-9]+):([0-9]+)")
      cur_h = tonumber(h);cur_m = tonumber(m); cur_s = tonumber(s)
   else
      cur_s = cur_s + elapsed
      while (cur_s >= 60) do cur_m = cur_m+1; cur_s = cur_s-60; end
      while (cur_s >= 60) do cur_h = cur_h+1; cur_m = cur_m-60; end
      if(cur_h >= 24) then cur_h = (cur_h % 24) end
   end
   
   i_time:set_text(string.format("%02d:%02d:%02d",cur_h,cur_m,cur_s))
   i_time:set_status_val(cur_s,60)

end

function yakuiInfo.togglecurrentevent()
	EA_Window_CurrentEvents.SetCurrentTier(Player.GetTier())
	EA_Window_OverheadMap.ToggleCurrentEvents()
end

function yakuiInfo.time_module.entry_point()
   d("!> ENTRY")
   i_time = yakuiInfo.informer:new("snt_info_time",8,2,nil,"yakuiInfo.togglecurrentevent",nil)
   i_time:set_status_col(50,50,25)
end

--STATS------------------------------------------------------------------------------------------------------------------

local function get_xp() return GameData.Player.Experience.curXpEarned,GameData.Player.Experience.curXpNeeded,GameData.Player.Experience.restXp end
local function get_rn() return GameData.Player.Renown.curRenownEarned,GameData.Player.Renown.curRenownNeeded end

local SF = string.format

function yakuiInfo.stat_module.xp_update()
   if (GameData.Player.level == 40) then i_xp:set_text("levelcap") return end
   local xp,xp_need,xp_rest = get_xp() 
   i_xp:set_text(SF("%d:%02d%%",GameData.Player.level,xp/xp_need*100))
   i_xp:set_status_val(xp,xp_need)
end

function yakuiInfo.stat_module.rn_update()
	local rn,rn_need = get_rn()
	if(rn == rn_need)
	then
		i_rn:set_text("rr cap")
	else
		i_rn:set_text(SF("%d:%02d%%",GameData.Player.Renown.curRank,rn/rn_need*100))
		i_rn:set_status_val(rn,rn_need)
	end
end

function yakuiInfo.stat_module.click() end
---------------------------------------------------------------------------------------------------------------------

local TT = Tooltips.SetTooltipText
local TC = Tooltips.SetTooltipColor

function yakuiInfo.stat_module.xp_tooltip()
   local xp,xp_need,xp_rest = get_xp()
   Tooltips.CreateTextOnlyTooltip(i_xp.wname.."_icon",nil)
   Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_TOP)
   TC(1,1,0,255,0)  TT(1,1,L"XP Info:")   
   TT(2,1,L"XP: ") TC(2,3,255,200,0) TT(2,3,towstring(xp))
   TT(3,1,L"Needed: ")    TC(3,3,255,200,0) TT(3,3,towstring(xp_need))
   TT(4,1,L"Remaining: ") TC(4,3,255,200,0) TT(4,3,towstring(xp_need-xp))
   TT(5,1,L"Rested: ")    TC(5,3,255,200,0) TT(5,3,towstring(xp_rest))
   Tooltips.Finalize()
end

function yakuiInfo.stat_module.rn_tooltip()
   local rn,rn_need = get_rn()
   Tooltips.CreateTextOnlyTooltip(i_rn.wname.."_icon",nil)
   Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_TOP)
   TC(1,1,0,255,0)  TT(1,1,L"Renown Info:")  
   TT(2,1,L"Renown: ")    TC(2,3,255,200,0) TT(2,3,towstring(rn))
   TT(3,1,L"Needed: ")    TC(3,3,255,200,0) TT(3,3,towstring(rn_need))
   TT(4,1,L"Remaining: ") TC(4,3,255,200,0) TT(4,3,towstring(rn_need-rn))
   Tooltips.Finalize()
end


function yakuiInfo.stat_module.entry_point()
   d("!> ENTRY")
   RegisterEventHandler(SystemData.Events.PLAYER_EXP_UPDATED,   "yakuiInfo.stat_module.xp_update")
   RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_UPDATED,"yakuiInfo.stat_module.rn_update")
   i_xp = yakuiInfo.informer:new("i_xp",6,0,"yakuiInfo.stat_module.xp_tooltip","yakuiInfo.stat_module.click",nil)
   i_rn = yakuiInfo.informer:new("i_rn",6,1,"yakuiInfo.stat_module.rn_tooltip","yakuiInfo.stat_module.click",nil)
   i_xp:set_status_col(125,150,25)
   i_rn:set_status_col(150,25,150)  
   yakuiInfo.stat_module.xp_update()
   yakuiInfo.stat_module.rn_update()
end

function yakuiInfo.togglecareer()
	if( not WindowGetShowing( "EA_Window_InteractionCoreTraining" ) ) then
		EA_Window_InteractionCoreTraining.Show( )
	else
		EA_Window_InteractionCoreTraining.Hide( )
	end
end

function yakuiInfo.togglemastery()
	if( not WindowGetShowing( "EA_Window_InteractionSpecialtyTraining" ) ) then
		EA_Window_InteractionSpecialtyTraining.Show( )
	else
		EA_Window_InteractionSpecialtyTraining.Hide( )
	end
end

function yakuiInfo.togglereown()
	if( not WindowGetShowing( "EA_Window_InteractionRenownTraining" ) ) then
		EA_Window_InteractionRenownTraining.Show( )
	else
		EA_Window_InteractionRenownTraining.Hide( )
	end
end

function yakuiInfo.toggletome()
	if( not WindowGetShowing( "EA_Window_InteractionTomeTraining" ) ) then
		EA_Window_InteractionTomeTraining.Show( )
	else
		EA_Window_InteractionTomeTraining.Hide( )
	end
end


local delay,curr,fps = 2,4,0

function yakuiInfo.fps_moduleupdate(elapsed)
   fps = math.floor((fps + 1/elapsed)/2)
   curr = curr - elapsed
   if(curr > 0) then return end
   curr = delay
   i_fps:set_gradient(fps,0,45)
   i_fps:set_text(tostring(fps))
end

function yakuiInfo.fps_moduleentry_point()
   d("!> ENTRY")
   i_fps = yakuiInfo.informer:new("snt_info_fps",2,3,nil,nil,nil)
end


local oldpx = -1
local oldpy = -1
function  yakuiInfo.loc_moduleupdate()
   --local px,py = LibSurveyor.PlayerLocation.zoneX,LibSurveyor.PlayerLocation.zoneY
	local pp = MapMonsterAPI.GetPlayerPosition()
	if pp then
		local px = pp.zoneX
		local py = pp.zoneY
		if px and py then
			px = math.floor(px * 0.001)
			py = math.floor(py * 0.001)
			if px ~= oldpx or py ~= oldpy then
				i_loc:set_text(tostring(px).."."..tostring(py))
				oldpx = px
				oldpy = py
			end
		else
			i_loc:set_text("0.0")
		end
	else
		i_loc:set_text("0.0")
	end
end

local TT = Tooltips.SetTooltipText
local TC = Tooltips.SetTooltipColor

function yakuiInfo.loc_moduletooltip()
   --local px,py = LibSurveyor.PlayerLocation.zoneX,LibSurveyor.PlayerLocation.zoneY
	local pp = MapMonsterAPI.GetPlayerPosition()
	local px = 0
	local py = 0
	if pp then
		if pp.zoneX then px = math.floor(pp.zoneX) end 
		if pp.zoneY then py = math.floor(pp.zoneY) end
	end
   local data = GetAreaData()
   Tooltips.CreateTextOnlyTooltip(i_loc.wname.."_icon",nil)
   Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_TOP)
   TC(1,1,100,255,200)  TT(1,1,data[1].areaName) 
   TC(1,2,255,255,100)  TT(1,2,GetZoneName(GameData.Player.zone)) 
   TC(2,1,200,100,200)  TT(2,1,L"X position") TC(2,3,200,100,200) TT(2,3,towstring(px))
   TC(3,1,200,100,200)  TT(3,1,L"Y position") TC(3,3,200,100,200) TT(3,3,towstring(py))
   Tooltips.Finalize()
end

function yakuiInfo.loc_moduleclick() yakui.ybuttonr() end

function yakuiInfo.loc_moduleentry_point()
   d("!> ENTRY")
   i_loc = yakuiInfo.informer:new("i_loc",5,9,
                                "yakuiInfo.loc_moduletooltip",
                                "yakuiInfo.loc_moduleclick",nil)
   yakuiInfo.loc_moduleupdate()
end

-------------------------------------------------------------------------------------------------------

local LOC = "RU"

local i_items     = 0
local i_total     = 0
local q_items     = 0
local q_total     = 0
local cur_items   = 0
local cur_total   = 0
local craft_items = 0
local craft_total = 0


local function GS(id) return GetStringFromTable("BAG_LOCALIZE",id) end

---------------------------------------------------------------------------------------------------------------------

local function getMaxSlots()
    local totals = EA_Window_Backpack.numberOfSlots
    for i, total in ipairs(totals) do
        if i == 1 then
            q_total = total
        elseif i == 2 then
            i_total = total
        elseif i == 3 then
            cur_total = total
        elseif i == 4 then
            craft_total = total
        end
    end
end

local function get_num_items(mode)
   local count = 0
   local allitems = EA_Window_Backpack.GetItemsFromBackpack(mode)

   for slot, item in ipairs(allitems) do
      if (item.uniqueID > 0) then
         count = count + 1
      elseif (item.iconNum > 0) then
         count = count + 1
      end
   end
   return count
end

---------------------------------------------------------------------------------------------------------------------

function  yakuiInfo.bag_info_module.update()
    i_items,i_total = 0,0
    for ndx = 1,4  do
        if ndx == 1 then
            q_items = get_num_items(ndx)
        elseif ndx == 2 then
            i_items = get_num_items(ndx)
        elseif ndx == 3 then
            cur_items = get_num_items(ndx)
        elseif ndx == 4 then
            craft_items = get_num_items(ndx)
        end
    end
    getMaxSlots()
    
    i_bag_info:set_gradient(i_items,i_total,0)
    i_bag_info:set_text(string.format("%02d/%02d",i_items,i_total))
end

---------------------------------------------------------------------------------------------------------------------

function yakuiInfo.bag_info_module.click() EA_Window_Backpack.ToggleShowing()  end


local TT = Tooltips.SetTooltipText
local TC = Tooltips.SetTooltipColor

function yakuiInfo.bag_info_module.tooltip()
   Tooltips.CreateTextOnlyTooltip(i_bag_info.wname.."_icon",nil)
   Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_TOP)
   TC(1,1,0,255,0)  TT(1,1,L"Bags Info:")   
   TT(2,1,L"Total: ")            TC(2,3,255,200,0) TT(2,3,towstring(i_total))
   TT(3,1,L"Quest Items: ")      TC(3,3,255,200,0) TT(3,3,towstring(q_items))
   TT(4,1,L"Items: ")            TC(4,3,255,200,0) TT(4,3,towstring(i_items))
   TT(5,1,L"Empty: ")            TC(5,3,255,200,0) TT(5,3,towstring(i_total-i_items))
   TT(6,1,L"Currency Items: ")   TC(6,3,255,200,0) TT(6,3,(towstring(tostring(cur_items).."/"..tostring(cur_total))))
   TT(7,1,L"Crafting Items: ")   TC(7,3,255,200,0) TT(7,3,(towstring(tostring(craft_items).."/"..tostring(craft_total))))
   Tooltips.Finalize()
end

function yakuiInfo.bag_info_module.entry_point()
   d("!> ENTRY")
   RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, "yakuiInfo.bag_info_module.update")
   RegisterEventHandler(SystemData.Events.PLAYER_QUEST_ITEM_SLOT_UPDATED,"yakuiInfo.bag_info_module.update")
   RegisterEventHandler(SystemData.Events.PLAYER_CURRENCY_SLOT_UPDATED, "yakuiInfo.bag_info_module.update")
   RegisterEventHandler(SystemData.Events.PLAYER_CRAFTING_SLOT_UPDATED, "yakuiInfo.bag_info_module.update")
   i_bag_info = yakuiInfo.informer:new("yInfo_bag",5,12,"yakuiInfo.bag_info_module.tooltip","yakuiInfo.bag_info_module.click",nil)
   yakuiInfo.bag_info_module.update()
	if yakuiVar.bag == true then
		WindowSetShowing("yInfo_bag", true)
	else
		WindowSetShowing("yInfo_bag", false)
	end
end

