if not yakui then yakui = {} end

function yakui.InitializeVectorsSettings()
	if not Vectors then return end
	if not Vectors.data then Vectors.data = {} end
	Vectors.data.activeprofile = "YakUI"
	if not Vectors.data.profiles then Vectors.data.profiles = {} end
	Vectors.data.profiles.YakUI =
	{
		["1.60"] = 
		{
			ClosetGoblinOptionWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "MoraleEditor",
						y = 0,
					},
				},
				name = "ClosetGoblinOptionWindow",
				scale = 0.54166650772095,
			},
			SquaredAnchor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0.018359370529652,
						point = "topleft",
						parent = "Minmap",
						y = 0,
					},
				},
				name = "SquaredAnchor",
				scale = 0.95,
			},
			HostileTarget = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = 0.08203125,
						point = "topleft",
						parent = "Root",
						y = 0.050000000745058,
					},
				},
				name = "HostileTarget",
				scale = 1,
			},
			EA_Window_CampaignMap = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.17854166030884,
					},
				},
				name = "EA_Window_CampaignMap",
				scale = 0.75,
			},
			DAoCBuff_Self_Buffs = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025033624842763,
						point = "bottomleft",
						parent = "PlayerHP",
						y = 0,
					},
				},
				name = "DAoCBuff_Self_Buffs",
				scale = 0.60080701112747,
			},
			EA_Window_ScenarioTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topright",
						parent = "Root",
						y = 0,
					},
				},
				name = "EA_Window_ScenarioTracker",
			},
			snt_info_fps = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.17515894770622,
					},
				},
				minscale = 0.55,
				name = "snt_info_fps",
				scale = 0.7283947467804,
			},
			i_fr = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = -0.0041730953380466,
						point = "bottomright",
						parent = "i_gl",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_fr",
				scale = 0.7283947467804,
			},
			TargetsHostile1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = -0.1171875,
						point = "right",
						parent = "Root",
						y = -0.049375001341105,
					},
				},
				name = "TargetsHostile1",
				scale = 1,
			},
			EA_Window_PublicQuestTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topright",
						parent = "Root",
						y = 0,
					},
				},
				name = "EA_Window_PublicQuestTracker",
				scale = 0.65110123157501,
			},
			YakChatdisplay = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.010260295122862,
						point = "left",
						parent = "YakuiFWButton",
						y = 0,
					},
				},
				name = "YakChatdisplay",
				scale = 0.83500194549561,
			},
			TargetsHostile5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile4",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile5",
				scale = 1,
			},
			DAoCBuff_Hostile_All = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.0031119,
						point = "topleft",
						parent = "HostileTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Hostile_All",
				scale = 0.60177451372147,
			},
			YBackPanel1680x1050 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = 0,
					},
				},
				name = "YBackPanel1680x1050",
				scale = 1.1428571939468,
			},
			EveryBodyGuard_1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.196484375,
						point = "topleft",
						parent = "Root",
						y = -0.233125,
					},
				},
				name = "EveryBodyGuard_1",
				scale = 0.75,
			},
			EveryBodyGuard_2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_1",
						y = 0,
					},
				},
				name = "EveryBodyGuard_2",
				scale = 0.75,
			},
			EveryBodyGuard_3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_2",
						y = 0,
					},
				},
				name = "EveryBodyGuard_3",
				scale = 0.75,
			},
			EveryBodyGuard_4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_3",
						y = 0,
					},
				},
				name = "EveryBodyGuard_4",
				scale = 0.75,
			},
			EveryBodyGuard_5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_4",
						y = 0,
					},
				},
				name = "EveryBodyGuard_5",
				scale = 0.75,
			},
			DAoCBuff_Hostile_Morales = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topleft",
						parent = "HostileTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Hostile_Morales",
				scale = 0.62333405017853,
			},
			EA_TacticsEditor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = -0.1911664,
					},
				},
				minscale = 0.5,
				name = "EA_TacticsEditor",
				scale = 0.54458600282669,
			},
			WARCommander = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "bottom",
						parent = "EA_Window_ZoneControl",
						y = -0.016527500003576,
					},
				},
				name = "WARCommander",
				scale = 0.45074999332428,
			},
			GroupMember1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "Root",
						y = 0.046875,
					},
				},
				name = "GroupMember1",
				scale = 1,
			},
			GroupMember4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0,
						point = "left",
						parent = "Root",
						y = -0.15312500298023,
					},
				},
				name = "GroupMember4",
				scale = 1,
			},
			YakuiButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.069878496229649,
						point = "left",
						parent = "YakChatdisplay",
						y = 0,
					},
				},
				name = "YakuiButton",
				scale = 0.47916674613953,
			},
			GroupMember2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "Root",
						y = 0.140625,
					},
				},
				name = "GroupMember2",
				scale = 1,
			},
			i_xp = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.001451428513974,
						point = "bottomright",
						parent = "Root",
						y = -0.0037011718377471,
					},
				},
				minscale = 0.55,
				name = "i_xp",
				scale = 0.7283947467804,
			},
			DAoCBuff_GroupBuffs3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember4",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs3",
				scale = 0.59800070524216,
			},
			EA_ActionBar2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "EA_ActionBar1",
						y = 0.001931702834554,
					},
				},
				name = "EA_ActionBar2",
				scale = 0.57951086759567,
			},
			TargetsHostile4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile3",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile4",
				scale = 1,
			},
			TidyRollAnchor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = -0.1715741455555,
						point = "right",
						parent = "Root",
						y = 0.17468754947186,
					},
				},
				name = "TidyRollAnchor",
				scale = 0.88793182373047,
			},
			TargetsFriendly5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly4",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly5",
				scale = 1,
			},
			i_loc = 
			{
				scale = 0.7283947467804,
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = 0.038231406360865,
						point = "bottom",
						parent = "Root",
						y = -0.0030761719681323,
					},
				},
				minscale = 0.55,
				name = "i_loc",
			},
			DAoCBuff_Friendly_All = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.0031119,
						point = "topleft",
						parent = "FriendlyTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Friendly_All",
				scale = 0.60185325145721,
			},
			DAoCBuff_Hostile_Self_Debuff = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0031118865590543,
						point = "bottomleft",
						parent = "HostileTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Hostile_Self_Debuff",
				scale = 0.5974822640419,
			},
			EA_Window_RvRTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "bottom",
						parent = "EA_Window_CampaignMap",
						y = 0,
					},
				},
				name = "EA_Window_RvRTracker",
				scale = 0.75,
			},
			Twister = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.037199998895327,
						point = "bottomleft",
						parent = "EA_StanceBar",
						y = -0.0020668,
					},
				},
				name = "Twister",
				scale = 0.49602371454239,
			},
			Map = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.032421875745058,
						point = "left",
						parent = "Root",
						y = 0.15469208359718,
					},
				},
				name = "Map",
				scale = 0.74569725990295,
			},
			yAssistHelper = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "TextEntryAnchor",
						y = 0,
					},
				},
				name = "yAssistHelper",
				scale = 0.75,
			},
			EA_ActionBar1 = 
			{
				scale = 0.57951563596725,
				name = "EA_ActionBar1",
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.052491378039122,
						point = "bottom",
						parent = "Root",
						y = -0.034471534192562,
					},
				},
			},
			TargetsFriendly6 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly5",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly6",
				scale = 1,
			},
			TargetsHostile3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile2",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile3",
				scale = 1,
			},
			EA_ChatWindowGroup1 = 
			{
				anchors = 
				{
					
					{
						parentpoint = "bottomleft",
						x = -0.00052083336049691,
						point = "bottomleft",
						parent = "Root",
						y = -0.011874999850988,
					},
					
					{
						parentpoint = "topright",
						x = 0,
						point = "topright",
						parent = "YakuiATButton",
						y = 0,
					},
				},
				name = "EA_ChatWindowGroup1",
			},
			UI_KwestorTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.54862451553345,
					},
				},
				minscale = 0.66499996185303,
				name = "UI_KwestorTracker",
				scale = 0.665,
			},
			PlayerCareer = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "PlayerAP",
						y = 0,
					},
				},
				name = "PlayerCareer",
				scale = 1,
			},
			YakuiSORButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.0024956602137536,
						point = "left",
						parent = "EA_Window_ZoneControl",
						y = 0,
					},
				},
				name = "YakuiSORButton",
				scale = 0.47916674613953,
			},
			GroupMember5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0,
						point = "left",
						parent = "Root",
						y = -0.059374999254942,
					},
				},
				name = "GroupMember5",
				scale = 1,
			},
			InfoPanelRenown = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = 0.009375000372529,
						point = "bottomleft",
						parent = "InfoPanel",
						y = -0.0012499999720603,
					},
				},
				name = "InfoPanelRenown",
				scale = 1,
			},
			GroupSpotter = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.00092773593496531,
						point = "bottomleft",
						parent = "Minmap",
						y = -0.012864601798356,
					},
				},
				name = "GroupSpotter",
				scale = 0.59375101327896,
			},
			i_menu7 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu6",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu7",
				scale = 0.7283947467804,
			},
			MotionWindow = 
			{
			   anchors = 
			   {
				   [1] = 
				   {
					   parentpoint = "top",
					   x = -0.21484375,
					   point = "top",
					   parent = "Root",
					   y = 0,
				   },
			   },
			   name = "MotionWindow",
			   scale = 0.75,
			},
			FriendlyTarget = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = 0.08203125,
						point = "topleft",
						parent = "Root",
						y = 0.20000000298023,
					},
				},
				name = "FriendlyTarget",
				scale = 1,
			},
			i_menu5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu4",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu5",
				scale = 0.7283947467804,
			},
			YBackPanel1920x1200 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = 0,
					},
				},
				name = "YBackPanel1920x1200",
				scale = 1,
			},
			i_menu6 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu5",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu6",
				scale = 0.7283947467804,
			},
			EA_GrantedAbilities = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomleft",
						parent = "EA_StanceBar",
						y = 0,
					},
				},
				name = "EA_GrantedAbilities",
				scale = 0.40180602669716,
			},
			DAoCBuff_Friendly_Self_Buff = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0031119,
						point = "bottomleft",
						parent = "FriendlyTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Friendly_Self_Buff",
				scale = 0.5996955037117,
			},
			MotionIcon = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.001822916790843,
						point = "left",
						parent = "Root",
						y = 0.077083333333333,
					},
				},
				name = "MotionIcon",
				scale = 0.5,
			},
			EA_CareerResourceWindowActionBar = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.037199998895327,
						point = "bottomleft",
						parent = "EA_StanceBar",
						y = -0.0020668,
					},
				},
				name = "EA_CareerResourceWindowActionBar",
				scale = 0.4464213,
			},
			TextEntryAnchor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "bottom",
						parent = "EA_ActionBar3",
						y = -0.0088701397180557,
					},
				},
				name = "TextEntryAnchor",
				scale = 0.78526949882507,
			},
			TargetsHostile2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile1",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile2",
				scale = 1,
			},
			YakuiATButton = 
			{
				scale = 0.33333447575569,
				name = "YakuiATButton",
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.17864583432674,
						point = "bottom",
						parent = "Root",
						y = -0.12604166567326,
					},
				},
			},
			PlayerHP = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.08203125,
						point = "topright",
						parent = "Root",
						y = 0.050000004470348,
					},
				},
				name = "PlayerHP",
				scale = 1,
			},
			DAoCBuff_GroupBuffs1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember2",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs1",
				scale = 0.59437388181686,
			},
			DAoCBuff_Self_Morales = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topright",
						parent = "PlayerHP",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Self_Morales",
				scale = 0.62200063467026,
			},
			i_menu2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu1",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu2",
				scale = 0.7283947467804,
			},
			DAoCBuff_Self_AddInfo = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topleft",
						parent = "PlayerHP",
						y = 0.0078431628644466,
					},
				},
				name = "DAoCBuff_Self_AddInfo",
				scale = 0.62745296955109,
			},
			i_rn = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.0565072,
						point = "bottomright",
						parent = "Root",
						y = -0.0037012,
					},
				},
				minscale = 0.55,
				name = "i_rn",
				scale = 0.7283947467804,
			},
			snt_info_time = 
			{
				scale = 0.7283947467804,
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.1411085,
						point = "bottomright",
						parent = "Root",
						y = -0.0037012,
					},
				},
				minscale = 0.55,
				name = "snt_info_time",
			},
			DAoCBuff_Friendly_AddInfo = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topright",
						parent = "FriendlyTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Friendly_AddInfo",
				scale = 0.63281625509262,
			},
			EA_MoraleBar = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = -0.0092167,
						point = "bottom",
						parent = "Minmap",
						y = -0.0088457427918911,
					},
				},
				name = "EA_MoraleBar",
				scale = 0.58987188339233,
			},
			Castbar = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.05078125,
						point = "bottom",
						parent = "Root",
						y = -0.20000000298023,
					},
				},
				minscale = 0.75,
				name = "Castbar",
				scale = 1,
			},
			YakuiFWButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.0018878700211644,
						point = "left",
						parent = "YakuiBWButton",
						y = 0,
					},
				},
				name = "YakuiFWButton",
				scale = 0.47916826605797,
			},
			DAoCBuff_Friendly_Morales = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topleft",
						parent = "FriendlyTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Friendly_Morales",
				scale = 0.58705359697342,
			},
			UI_KwestorTrackerNub = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.00078125041909516,
						point = "bottomright",
						parent = "Minmap",
						y = -0.01041667163372,
					},
				},
				name = "UI_KwestorTrackerNub",
				scale = 0.50000023841858,
			},
			EA_Window_KeepObjectiveTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0.00018259683565702,
						point = "topright",
						parent = "Root",
						y = 0.029583340510726,
					},
				},
				name = "EA_Window_KeepObjectiveTracker",
			},
			DAoCBuff_GroupBuffs4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember5",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs4",
				scale = 0.59567582607269,
			},
			yInfo_bag = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0.018968613818288,
						point = "left",
						parent = "YakuiButton",
						y = 0.0036419737152755,
					},
				},
				minscale = 0.55,
				name = "yInfo_bag",
				scale = 0.7283947467804,
			},
			i_menu8 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu7",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu8",
				scale = 0.7283947467804,
			},
			i_menu4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu3",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu4",
				scale = 0.7283947467804,
			},
			TargetsFriendly4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly3",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly4",
				scale = 1,
			},
			EA_ActionBar4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.14248697459698,
					},
				},
				name = "EA_ActionBar4",
				scale = 0.40561673045158,
			},
			YakuiWCButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = -0.0024956641718745,
						point = "right",
						parent = "EA_Window_ZoneControl",
						y = 0,
					},
				},
				name = "YakuiWCButton",
				scale = 0.47916755080223,
			},
			MoraleEditor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "EA_TacticsEditor",
						y = 0,
					},
				},
				minscale = 0.5,
				name = "MoraleEditor",
				scale = 0.53216475248337,
			},
			EA_StanceBar = 
			{
				scale = 0.5625,
				name = "EA_StanceBar",
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = 0.19837239384651,
						point = "bottom",
						parent = "Root",
						y = -0.14854167401791,
					},
				},
			},
			i_menu1 = 
			{
				scale = 0.7283947467804,
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.097255989909172,
						point = "bottom",
						parent = "Root",
						y = -0.0030761719681323,
					},
				},
				minscale = 0.55,
				name = "i_menu1",
			},
			EA_Window_ZoneControl = 
			{
				scale = 0.41977798938751,
				name = "EA_Window_ZoneControl",
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.12674887478352,
						point = "bottomright",
						parent = "Root",
						y = -0.14529998600483,
					},
				},
			},
			PlayerAP = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "PlayerHP",
						y = 0,
					},
				},
				name = "PlayerAP",
				scale = 1,
			},
			InfoPanelExp = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.009375000372529,
						point = "bottomright",
						parent = "InfoPanel",
						y = -0.0012499999720603,
					},
				},
				name = "InfoPanelExp",
				scale = 1,
			},
			GroupMember3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "Root",
						y = 0.234375,
					},
				},
				name = "GroupMember3",
				scale = 1,
			},
			i_gl = 
			{
				scale = 0.7283947467804,
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.2297967,
						point = "bottomright",
						parent = "Root",
						y = -0.0037012,
					},
				},
				minscale = 0.55,
				name = "i_gl",
			},
			YakuiBWButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.03125,
						point = "bottomleft",
						parent = "Root",
						y = -0.14395843446255,
					},
				},
				name = "YakuiBWButton",
				scale = 0.47916755080223,
			},
			MouseoverTarget = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "EA_Window_RvRTracker",
						y = -1.862645149231e-009,
					},
				},
				name = "MouseoverTarget",
				scale = 1,
			},
			CDownWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "PlayerHP",
						y = 0.034038804471493,
					},
				},
				name = "CDownWindow",
				scale = 0.60068476200104,
			},
			ResToggle = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.00244140625,
						point = "left",
						parent = "Root",
						y = 0.09375,
					},
				},
				name = "ResToggle",
				scale = 0.46875,
			},
			DAoCBuff_GroupBuffs2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember3",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs2",
				scale = 0.5913337469101,
			},
			TargetsFriendly1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.1171875,
						point = "left",
						parent = "Root",
						y = -0.049375001341105,
					},
				},
				name = "TargetsFriendly1",
				scale = 1,
			},
			MouseOverTargetWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "right",
						parent = "Root",
						y = 0.16249999403954,
					},
				},
				name = "MouseOverTargetWindow",
				scale = 0.75,
			},
			TargetsFriendly2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly1",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly2",
				scale = 1,
			},
			EA_ActionBar3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "EA_ActionBar2",
						y = 0.0019314,
					},
				},
				name = "EA_ActionBar3",
				scale = 0.57941144704819,
			},
			EA_CareerResourceWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.048411466181278,
						point = "bottomleft",
						parent = "EA_GrantedAbilities",
						y = -0.042249988764524,
					},
				},
				name = "EA_CareerResourceWindow",
				scale = 0.65,
			},
			EA_AssistWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "yAssistHelper",
						y = 0,
					},
				},
				name = "EA_AssistWindow",
				scale = 0.79069727659225,
			},
			TargetsFriendly3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly2",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly3",
				scale = 1,
			},
			Minmap = 
			{
				scale = 0.58400022983551,
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0.016242504119873,
						point = "topleft",
						parent = "EA_ActionBar3",
						y = 0.0016546675469726,
					},
				},
				name = "Minmap",
				minscale = 0.4,
			},
			Pet = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.08203125,
						point = "topright",
						parent = "Root",
						y = 0.20000000298023,
					},
				},
				name = "Pet",
				scale = 1,
			},
			DAoCBuff_Hostile_AddInfo = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topright",
						parent = "HostileTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Hostile_AddInfo",
				scale = 0.62745296955109,
			},
			InfoPanel = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "top",
						parent = "Root",
						y = 0.0062500000931323,
					},
				},
				name = "InfoPanel",
				scale = 1,
			},
			DAoCBuff_GroupBuffs0 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember1",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs0",
				scale = 0.60333633422852,
			},
			WARCommanderInfo = 
			{
				anchors = 
				{
					
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "EA_Window_RvRTracker",
						y = -0.052271373569965,
					},
					
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "EA_Window_RvRTracker",
						y = -0.20385825634003,
					},
				},
				name = "WARCommanderInfo",
				scale = 0.62725651264191,
			},
			TargetsHostile6 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile5",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile6",
				scale = 1,
			},
			EA_Window_BattlefieldObjectiveTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topright",
						parent = "Root",
						y = 0.029999993741512,
					},
				},
				name = "EA_Window_BattlefieldObjectiveTracker",
			},
			DAoCBuff_Self_Debuffs = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.45703125,
						point = "topleft",
						parent = "Root",
						y = 0.71062487363815,
					},
				},
				name = "DAoCBuff_Self_Debuffs",
				scale = 0.71776050329208,
			},
			i_menu3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu2",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu3",
				scale = 0.7283947467804,
			},
		},
		["1.77"] = 
		{
			EveryBodyGuard_1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.196484375,
						point = "topleft",
						parent = "Root",
						y = -0.233125,
					},
				},
				name = "EveryBodyGuard_1",
				scale = 0.75,
			},
			EveryBodyGuard_2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_1",
						y = 0,
					},
				},
				name = "EveryBodyGuard_2",
				scale = 0.75,
			},
			EveryBodyGuard_3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_2",
						y = 0,
					},
				},
				name = "EveryBodyGuard_3",
				scale = 0.75,
			},
			EveryBodyGuard_4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_3",
						y = 0,
					},
				},
				name = "EveryBodyGuard_4",
				scale = 0.75,
			},
			EveryBodyGuard_5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_4",
						y = 0,
					},
				},
				name = "EveryBodyGuard_5",
				scale = 0.75,
			},
			TargetsFriendly3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly2",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly3",
				scale = 1,
			},
			SquaredAnchor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0.018359370529652,
						point = "topleft",
						parent = "Minmap",
						y = 0,
					},
				},
				name = "SquaredAnchor",
				scale = 0.95,
			},
			HostileTarget = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = 0.08203125,
						point = "topleft",
						parent = "Root",
						y = 0.0111111,
					},
				},
				name = "HostileTarget",
				scale = 1,
			},
			EA_Window_CampaignMap = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.19938515126705,
					},
				},
				name = "EA_Window_CampaignMap",
				scale = 0.75,
			},
			DAoCBuff_Self_Buffs = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025033624842763,
						point = "bottomleft",
						parent = "PlayerHP",
						y = 0,
					},
				},
				name = "DAoCBuff_Self_Buffs",
				scale = 0.60080701112747,
			},
			EA_Window_ScenarioTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topright",
						parent = "Root",
						y = 0,
					},
				},
				name = "EA_Window_ScenarioTracker",
			},
			snt_info_fps = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.20214971899986,
					},
				},
				minscale = 0.55,
				name = "snt_info_fps",
				scale = 0.7283947467804,
			},
			TargetsFriendly1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.1171875,
						point = "left",
						parent = "Root",
						y = -0.049375001341105,
					},
				},
				name = "TargetsFriendly1",
				scale = 1,
			},
			TargetsHostile1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = -0.1171875,
						point = "right",
						parent = "Root",
						y = -0.049375001341105,
					},
				},
				name = "TargetsHostile1",
				scale = 1,
			},
			EA_Window_PublicQuestTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topright",
						parent = "Root",
						y = 0,
					},
				},
				name = "EA_Window_PublicQuestTracker",
				scale = 0.58599102497101,
			},
			YakChatdisplay = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.010260295122862,
						point = "left",
						parent = "YakuiFWButton",
						y = 0,
					},
				},
				name = "YakChatdisplay",
				scale = 0.83500194549561,
			},
			TargetsHostile5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile4",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile5",
				scale = 1,
			},
			DAoCBuff_Hostile_All = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.0031119,
						point = "topleft",
						parent = "HostileTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Hostile_All",
				scale = 0.60177451372147,
			},
			DAoCBuff_Hostile_Morales = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topleft",
						parent = "HostileTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Hostile_Morales",
				scale = 0.62333405017853,
			},
			YBackPanel1680x1050 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = 0,
					},
				},
				name = "YBackPanel1680x1050",
				scale = 1.1428571939468,
			},
			WARCommander = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "bottom",
						parent = "EA_Window_ZoneControl",
						y = -0.016527500003576,
					},
				},
				name = "WARCommander",
				scale = 0.45074999332428,
			},
			GroupMember4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0,
						point = "left",
						parent = "Root",
						y = -0.15312500298023,
					},
				},
				name = "GroupMember4",
				scale = 1,
			},
			EA_ActionBar2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "EA_ActionBar1",
						y = 0.001931702834554,
					},
				},
				name = "EA_ActionBar2",
				scale = 0.57951086759567,
			},
			TargetsHostile4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile3",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile4",
				scale = 1,
			},
			DAoCBuff_Hostile_Self_Debuff = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0031118865590543,
						point = "bottomleft",
						parent = "HostileTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Hostile_Self_Debuff",
				scale = 0.5974822640419,
			},
			TargetsFriendly5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly4",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly5",
				scale = 1,
			},
			TidyRollAnchor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = -0.1715741455555,
						point = "right",
						parent = "Root",
						y = 0.17468754947186,
					},
				},
				name = "TidyRollAnchor",
				scale = 0.88793182373047,
			},
			EA_Window_BattlefieldObjectiveTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topright",
						parent = "Root",
						y = 0.029999990016222,
					},
				},
				name = "EA_Window_BattlefieldObjectiveTracker",
			},
			i_loc = 
			{
				scale = 0.7283947467804,
				name = "i_loc",
				minscale = 0.55,
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = 0.038231406360865,
						point = "bottom",
						parent = "Root",
						y = -0.0050507,
					},
				},
			},
			i_xp = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.001451428513974,
						point = "bottomright",
						parent = "Root",
						y = -0.0050507094711065,
					},
				},
				minscale = 0.55,
				name = "i_xp",
				scale = 0.7283947467804,
			},
			i_menu7 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu6",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu7",
				scale = 0.7283947467804,
			},
			EA_Window_RvRTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "bottom",
						parent = "EA_Window_CampaignMap",
						y = 0,
					},
				},
				name = "EA_Window_RvRTracker",
				scale = 0.75,
			},
			Twister = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.037199998895327,
						point = "bottomleft",
						parent = "EA_StanceBar",
						y = -0.0020668,
					},
				},
				name = "Twister",
				scale = 0.49602371454239,
			},
			WARCommanderInfo = 
			{
				anchors = 
				{
					
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "EA_Window_RvRTracker",
						y = -0.052271373569965,
					},
					
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "EA_Window_RvRTracker",
						y = -0.20385825634003,
					},
				},
				name = "WARCommanderInfo",
				scale = 0.62725651264191,
			},
			DAoCBuff_GroupBuffs3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember4",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs3",
				scale = 0.59800070524216,
			},
			EA_ActionBar1 = 
			{
				scale = 0.57951563596725,
				name = "EA_ActionBar1",
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.052491378039122,
						point = "bottom",
						parent = "Root",
						y = -0.039840050041676,
					},
				},
			},
			TargetsFriendly6 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly5",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly6",
				scale = 1,
			},
			DAoCBuff_GroupBuffs0 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember1",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs0",
				scale = 0.60333633422852,
			},
			EA_ChatWindowGroup1 = 
			{
				anchors = 
				{
					
					{
						parentpoint = "bottomleft",
						x = -0.00052083336049691,
						point = "bottomleft",
						parent = "Root",
						y = -0.011874999850988,
					},
					
					{
						parentpoint = "topright",
						x = 0,
						point = "topright",
						parent = "YakuiATButton",
						y = 0,
					},
				},
				name = "EA_ChatWindowGroup1",
			},
			DAoCBuff_Friendly_All = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.0031119,
						point = "topleft",
						parent = "FriendlyTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Friendly_All",
				scale = 0.60185325145721,
			},
			GroupMember2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "Root",
						y = 0.140625,
					},
				},
				name = "GroupMember2",
				scale = 1,
			},
			GroupMember1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "Root",
						y = 0.046875,
					},
				},
				name = "GroupMember1",
				scale = 1,
			},
			YakuiSORButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.0024956602137536,
						point = "left",
						parent = "EA_Window_ZoneControl",
						y = 0,
					},
				},
				name = "YakuiSORButton",
				scale = 0.47916674613953,
			},
			GroupMember5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0,
						point = "left",
						parent = "Root",
						y = -0.059374999254942,
					},
				},
				name = "GroupMember5",
				scale = 1,
			},
			YakuiButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.069878496229649,
						point = "left",
						parent = "YakChatdisplay",
						y = 0,
					},
				},
				name = "YakuiButton",
				scale = 0.47916674613953,
			},
			UI_KwestorTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.5966522693634,
					},
				},
				minscale = 0.665,
				name = "UI_KwestorTracker",
				scale = 0.665,
			},
			i_menu5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu4",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu5",
				scale = 0.7283947467804,
			},
			ClosetGoblinOptionWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "MoraleEditor",
						y = 0,
					},
				},
				name = "ClosetGoblinOptionWindow",
				scale = 0.54166650772095,
			},
			i_fr = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = -0.0041730953380466,
						point = "bottomright",
						parent = "i_gl",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_fr",
				scale = 0.7283947467804,
			},
			FriendlyTarget = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = 0.08203125,
						point = "topleft",
						parent = "Root",
						y = 0.1714813709259,
					},
				},
				name = "FriendlyTarget",
				scale = 1,
			},
			GroupSpotter = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.00092773593496531,
						point = "bottomleft",
						parent = "Minmap",
						y = -0.012864601798356,
					},
				},
				name = "GroupSpotter",
				scale = 0.59375101327896,
			},
			i_menu6 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu5",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu6",
				scale = 0.7283947467804,
			},
			EA_GrantedAbilities = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomleft",
						parent = "EA_StanceBar",
						y = 0,
					},
				},
				name = "EA_GrantedAbilities",
				scale = 0.40180602669716,
			},
			DAoCBuff_Friendly_Self_Buff = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0031119,
						point = "bottomleft",
						parent = "FriendlyTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Friendly_Self_Buff",
				scale = 0.5996955037117,
			},
			MotionIcon = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.0015625001396984,
						point = "left",
						parent = "Root",
						y = 0.055787030607462,
					},
				},
				name = "MotionIcon",
				scale = 0.5,
			},
			DAoCBuff_GroupBuffs2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember3",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs2",
				scale = 0.5913337469101,
			},
			MotionWindow = 
			{
			   anchors = 
			   {
				   [1] = 
				   {
					   parentpoint = "top",
					   x = -0.21484375,
					   point = "top",
					   parent = "Root",
					   y = 0,
				   },
			   },
			   name = "MotionWindow",
			   scale = 0.75,
			},
			TargetsHostile2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile1",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile2",
				scale = 1,
			},
			ResToggle = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.00244140625,
						point = "left",
						parent = "Root",
						y = 0.073784738779068,
					},
				},
				name = "ResToggle",
				scale = 0.46875,
			},
			PlayerHP = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.08203125,
						point = "topright",
						parent = "Root",
						y = 0.011111110448837,
					},
				},
				name = "PlayerHP",
				scale = 1,
			},
			DAoCBuff_GroupBuffs1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember2",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs1",
				scale = 0.59437388181686,
			},
			Map = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.032421875745058,
						point = "left",
						parent = "Root",
						y = 0.1187882,
					},
				},
				name = "Map",
				scale = 0.74569725990295,
			},
			i_menu2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu1",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu2",
				scale = 0.7283947467804,
			},
			TextEntryAnchor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "bottom",
						parent = "EA_ActionBar3",
						y = -0.0088701397180557,
					},
				},
				name = "TextEntryAnchor",
				scale = 0.78526949882507,
			},
			i_rn = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.0565072,
						point = "bottomright",
						parent = "Root",
						y = -0.0037012,
					},
				},
				minscale = 0.55,
				name = "i_rn",
				scale = 0.7283947467804,
			},
			snt_info_time = 
			{
				scale = 0.7283947467804,
				minscale = 0.55,
				name = "snt_info_time",
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.1411085,
						point = "bottomright",
						parent = "Root",
						y = -0.0037012,
					},
				},
			},
			EA_CareerResourceWindowActionBar = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.037199998895327,
						point = "bottomleft",
						parent = "EA_StanceBar",
						y = -0.0020668,
					},
				},
				name = "EA_CareerResourceWindowActionBar",
				scale = 0.4464213,
			},
			EA_MoraleBar = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = -0.0086023,
						point = "bottom",
						parent = "Minmap",
						y = -0.0104843,
					},
				},
				name = "EA_MoraleBar",
				scale = 0.58987188339233,
			},
			YakuiBWButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.03125,
						point = "bottomleft",
						parent = "Root",
						y = -0.1605605,
					},
				},
				name = "YakuiBWButton",
				scale = 0.47916755080223,
			},
			YakuiFWButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.0018878700211644,
						point = "left",
						parent = "YakuiBWButton",
						y = 0,
					},
				},
				name = "YakuiFWButton",
				scale = 0.47916826605797,
			},
			YBackPanel1920x1200 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = 0,
					},
				},
				name = "YBackPanel1920x1200",
				scale = 1,
			},
			UI_KwestorTrackerNub = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.00078125041909516,
						point = "bottomright",
						parent = "Minmap",
						y = -0.01041667163372,
					},
				},
				name = "UI_KwestorTrackerNub",
				scale = 0.50000023841858,
			},
			i_menu4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu3",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu4",
				scale = 0.7283947467804,
			},
			DAoCBuff_GroupBuffs4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember5",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs4",
				scale = 0.59567582607269,
			},
			yInfo_bag = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0.018968613818288,
						point = "left",
						parent = "YakuiButton",
						y = 0.0036419737152755,
					},
				},
				minscale = 0.55,
				name = "yInfo_bag",
				scale = 0.7283947467804,
			},
			i_menu8 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu7",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu8",
				scale = 0.7283947467804,
			},
			DAoCBuff_Friendly_Morales = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topleft",
						parent = "FriendlyTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Friendly_Morales",
				scale = 0.58705359697342,
			},
			TargetsFriendly4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly3",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly4",
				scale = 1,
			},
			DAoCBuff_Self_Morales = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topright",
						parent = "PlayerHP",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Self_Morales",
				scale = 0.62200063467026,
			},
			i_gl = 
			{
				scale = 0.7283947467804,
				name = "i_gl",
				minscale = 0.55,
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.2297967,
						point = "bottomright",
						parent = "Root",
						y = -0.0037012,
					},
				},
			},
			MoraleEditor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "EA_TacticsEditor",
						y = 0,
					},
				},
				minscale = 0.5,
				name = "MoraleEditor",
				scale = 0.53216475248337,
			},
			EA_StanceBar = 
			{
				scale = 0.5625,
				name = "EA_StanceBar",
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = 0.19837239384651,
						point = "bottom",
						parent = "Root",
						y = -0.16156889498234,
					},
				},
			},
			MouseoverTarget = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "EA_Window_RvRTracker",
						y = -1.862645149231e-009,
					},
				},
				name = "MouseoverTarget",
				scale = 1,
			},
			EA_Window_ZoneControl = 
			{
				scale = 0.41977798938751,
				name = "EA_Window_ZoneControl",
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.12674887478352,
						point = "bottomright",
						parent = "Root",
						y = -0.16474361717701,
					},
				},
			},
			YakuiWCButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = -0.0024956641718745,
						point = "right",
						parent = "EA_Window_ZoneControl",
						y = 0,
					},
				},
				name = "YakuiWCButton",
				scale = 0.47916755080223,
			},
			i_menu1 = 
			{
				scale = 0.7283947467804,
				name = "i_menu1",
				minscale = 0.55,
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.097255989909172,
						point = "bottom",
						parent = "Root",
						y = -0.0044257096014917,
					},
				},
			},
			GroupMember3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "Root",
						y = 0.234375,
					},
				},
				name = "GroupMember3",
				scale = 1,
			},
			InfoPanelExp = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.009375000372529,
						point = "bottomright",
						parent = "InfoPanel",
						y = -0.0012499999720603,
					},
				},
				name = "InfoPanelExp",
				scale = 1,
			},
			DAoCBuff_Friendly_AddInfo = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topright",
						parent = "FriendlyTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Friendly_AddInfo",
				scale = 0.63281625509262,
			},
			DAoCBuff_Self_AddInfo = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topleft",
						parent = "PlayerHP",
						y = 0.0078431628644466,
					},
				},
				name = "DAoCBuff_Self_AddInfo",
				scale = 0.62745296955109,
			},
			PlayerAP = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "PlayerHP",
						y = 0,
					},
				},
				name = "PlayerAP",
				scale = 1,
			},
			EA_ActionBar4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.15751717984676,
					},
				},
				name = "EA_ActionBar4",
				scale = 0.40561673045158,
			},
			EA_Window_KeepObjectiveTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topright",
						parent = "Root",
						y = 0.029583336785436,
					},
				},
				name = "EA_Window_KeepObjectiveTracker",
			},
			YakuiATButton = 
			{
				scale = 0.33333447575569,
				name = "YakuiATButton",
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.17864583432674,
						point = "bottom",
						parent = "Root",
						y = -0.13839341700077,
					},
				},
			},
			Castbar = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.05078125,
						point = "bottom",
						parent = "Root",
						y = -0.20000000298023,
					},
				},
				minscale = 0.75,
				name = "Castbar",
				scale = 1,
			},
			CDownWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "PlayerHP",
						y = 0.034038804471493,
					},
				},
				name = "CDownWindow",
				scale = 0.60068476200104,
			},
			MouseOverTargetWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "right",
						parent = "Root",
						y = 0.16249999403954,
					},
				},
				name = "MouseOverTargetWindow",
				scale = 0.75,
			},
			TargetsFriendly2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly1",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly2",
				scale = 1,
			},
			EA_ActionBar3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "EA_ActionBar2",
						y = 0.0019314,
					},
				},
				name = "EA_ActionBar3",
				scale = 0.57941144704819,
			},
			EA_CareerResourceWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.048411466181278,
						point = "bottomleft",
						parent = "EA_GrantedAbilities",
						y = -0.042249988764524,
					},
				},
				name = "EA_CareerResourceWindow",
				scale = 0.65,
			},
			EA_AssistWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "yAssistHelper",
						y = 0,
					},
				},
				name = "EA_AssistWindow",
				scale = 0.79069727659225,
			},
			DAoCBuff_Hostile_AddInfo = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topright",
						parent = "HostileTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Hostile_AddInfo",
				scale = 0.62745296955109,
			},
			Minmap = 
			{
				scale = 0.58400022983551,
				minscale = 0.4,
				name = "Minmap",
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0.016242504119873,
						point = "topleft",
						parent = "EA_ActionBar3",
						y = 0.0034932,
					},
				},
			},
			Pet = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.08203125,
						point = "topright",
						parent = "Root",
						y = 0.1714814,
					},
				},
				name = "Pet",
				scale = 1,
			},
			EA_TacticsEditor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = -0.1911664,
					},
				},
				minscale = 0.5,
				name = "EA_TacticsEditor",
				scale = 0.54458600282669,
			},
			InfoPanel = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "top",
						parent = "Root",
						y = 0.0062500000931323,
					},
				},
				name = "InfoPanel",
				scale = 1,
			},
			TargetsHostile3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile2",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile3",
				scale = 1,
			},
			InfoPanelRenown = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = 0.009375000372529,
						point = "bottomleft",
						parent = "InfoPanel",
						y = -0.0012499999720603,
					},
				},
				name = "InfoPanelRenown",
				scale = 1,
			},
			TargetsHostile6 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile5",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile6",
				scale = 1,
			},
			yAssistHelper = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "TextEntryAnchor",
						y = 0,
					},
				},
				name = "yAssistHelper",
				scale = 0.75,
			},
			DAoCBuff_Self_Debuffs = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.45703125,
						point = "topleft",
						parent = "Root",
						y = 0.71062487363815,
					},
				},
				name = "DAoCBuff_Self_Debuffs",
				scale = 0.71776050329208,
			},
			i_menu3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu2",
						y = 0,
					},
				},
				minscale = 0.55,
				name = "i_menu3",
				scale = 0.7283947467804,
			},
			PlayerCareer = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "PlayerAP",
						y = 0,
					},
				},
				name = "PlayerCareer",
				scale = 1,
			},
		},
		["1.25"] = 
		{
			EveryBodyGuard_1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.196484375,
						point = "topleft",
						parent = "Root",
						y = -0.233125,
					},
				},
				name = "EveryBodyGuard_1",
				scale = 0.75,
			},
			EveryBodyGuard_2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_1",
						y = 0,
					},
				},
				name = "EveryBodyGuard_2",
				scale = 0.75,
			},
			EveryBodyGuard_3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_2",
						y = 0,
					},
				},
				name = "EveryBodyGuard_3",
				scale = 0.75,
			},
			EveryBodyGuard_4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_3",
						y = 0,
					},
				},
				name = "EveryBodyGuard_4",
				scale = 0.75,
			},
			EveryBodyGuard_5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "EveryBodyGuard_4",
						y = 0,
					},
				},
				name = "EveryBodyGuard_5",
				scale = 0.75,
			},
			ClosetGoblinOptionWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "bottom",
						parent = "MoraleEditor",
						y = 0,
					},
				},
				name = "ClosetGoblinOptionWindow",
				scale = 0.6933331489563,
			},
			SquaredAnchor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0.022293332964182,
						point = "topleft",
						parent = "EA_ActionBar3",
						y = 0,
					},
				},
				name = "SquaredAnchor",
				scale = 0.95,
			},
			HostileTarget = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = 0.08203125,
						point = "topleft",
						parent = "Root",
						y = 0.050000000745058,
					},
				},
				name = "HostileTarget",
				scale = 1.2800000905991,
			},
			EA_Window_CampaignMap = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.19604164361954,
					},
				},
				name = "EA_Window_CampaignMap",
				scale = 0.95999997854233,
			},
			DAoCBuff_Self_Buffs = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025033624842763,
						point = "bottomleft",
						parent = "PlayerHP",
						y = 0,
					},
				},
				name = "DAoCBuff_Self_Buffs",
				scale = 0.76903295516968,
			},
			YBackPanel1920x1200_1280x1024 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = 0,
					},
				},
				name = "YBackPanel1920x1200_1280x1024",
				scale = 1.5,
			},
			EA_Window_ScenarioTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = -0.11849117279053,
						point = "topright",
						parent = "Root",
						y = 0.0024999976158142,
					},
				},
				name = "EA_Window_ScenarioTracker",
			},
			snt_info_fps = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.19518981873989,
					},
				},
				scale = 0.93234533071518,
				name = "snt_info_fps",
				minscale = 0.55,
			},
			TargetsFriendly1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.1171875,
						point = "left",
						parent = "Root",
						y = -0.049375001341105,
					},
				},
				name = "TargetsFriendly1",
				scale = 1.2800000905991,
			},
			TargetsHostile1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = -0.1171875,
						point = "right",
						parent = "Root",
						y = -0.049375001341105,
					},
				},
				name = "TargetsHostile1",
				scale = 1.2800000905991,
			},
			EA_Window_PublicQuestTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = -0.11842504143715,
						point = "topright",
						parent = "Root",
						y = 0,
					},
				},
				name = "EA_Window_PublicQuestTracker",
				scale = 0.83340954780579,
			},
			YakChatdisplay = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.010260295122862,
						point = "left",
						parent = "YakuiFWButton",
						y = 0,
					},
				},
				name = "YakChatdisplay",
				scale = 1.0688025951385,
			},
			TargetsHostile5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile4",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile5",
				scale = 1.2800000905991,
			},
			DAoCBuff_Hostile_All = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.0031119,
						point = "topleft",
						parent = "HostileTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Hostile_All",
				scale = 0.77027142047882,
			},
			UI_KwestorTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.58109527826309,
					},
				},
				scale = 0.85120004415512,
				name = "UI_KwestorTracker",
				minscale = 0.665,
			},
			EA_TacticsEditor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = -0.1911664,
					},
				},
				scale = 0.69707012176514,
				name = "EA_TacticsEditor",
				minscale = 0.5,
			},
			WARCommander = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = -0.030050000175834,
						point = "bottom",
						parent = "EA_Window_ZoneControl",
						y = -0.016527500003576,
					},
				},
				name = "WARCommander",
				scale = 0.57696002721786,
			},
			GroupMember4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0,
						point = "left",
						parent = "Root",
						y = -0.15312500298023,
					},
				},
				name = "GroupMember4",
				scale = 1.2800000905991,
			},
			YakuiButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.089045159518719,
						point = "left",
						parent = "YakChatdisplay",
						y = 0,
					},
				},
				name = "YakuiButton",
				scale = 0.61333346366882,
			},
			TargetsHostile4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile3",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile4",
				scale = 1.2800000905991,
			},
			DAoCBuff_Hostile_Self_Debuff = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0031118865590543,
						point = "bottomleft",
						parent = "HostileTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Hostile_Self_Debuff",
				scale = 0.764777302742,
			},
			GroupMember2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "Root",
						y = 0.140625,
					},
				},
				name = "GroupMember2",
				scale = 1.2800000905991,
			},
			TidyRollAnchor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = -0.1715741455555,
						point = "right",
						parent = "Root",
						y = 0.17468754947186,
					},
				},
				name = "TidyRollAnchor",
				scale = 1.1365526914597,
			},
			EA_Window_BattlefieldObjectiveTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = -0.11727342754602,
						point = "topright",
						parent = "Root",
						y = 0.043749999254942,
					},
				},
				name = "EA_Window_BattlefieldObjectiveTracker",
			},
			i_loc = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = 0,
						point = "top",
						parent = "Minmap",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_loc",
				minscale = 0.55,
			},
			GroupMember1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "Root",
						y = 0.046875,
					},
				},
				name = "GroupMember1",
				scale = 1.2800000905991,
			},
			TargetsFriendly5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly4",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly5",
				scale = 1.2800000905991,
			},
			EA_Window_RvRTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "bottom",
						parent = "EA_Window_CampaignMap",
						y = 0,
					},
				},
				name = "EA_Window_RvRTracker",
				scale = 0.95999997854233,
			},
			Twister = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.037199998895327,
						point = "bottomleft",
						parent = "EA_StanceBar",
						y = -0.0020668,
					},
				},
				name = "Twister",
				scale = 0.49602371454239,
			},
			Map = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.032421875745058,
						point = "left",
						parent = "Root",
						y = 0.15655642747879,
					},
				},
				name = "Map",
				scale = 0.95449250936508,
			},
			yAssistHelper = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "TextEntryAnchor",
						y = 0,
					},
				},
				name = "yAssistHelper",
				scale = 0.95999997854233,
			},
			EA_ActionBar1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.0011076452210546,
						point = "bottom",
						parent = "Root",
						y = -0.035437390208244,
					},
				},
				name = "EA_ActionBar1",
				scale = 0.74178004264832,
			},
			TargetsFriendly6 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly5",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly6",
				scale = 1.2800000905991,
			},
			DAoCBuff_GroupBuffs0 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember1",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs0",
				scale = 0.77227056026459,
			},
			EA_ChatWindowGroup1 = 
			{
				anchors = 
				{
					
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = -0.0056250002235174,
					},
					
					{
						parentpoint = "topright",
						x = 0,
						point = "topright",
						parent = "YakuiATButton",
						y = 0,
					},
				},
				name = "EA_ChatWindowGroup1",
			},
			DAoCBuff_Friendly_All = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.0031119,
						point = "topleft",
						parent = "FriendlyTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Friendly_All",
				scale = 0.77037221193314,
			},
			DAoCBuff_GroupBuffs3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember4",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs3",
				scale = 0.76544094085693,
			},
			YakuiSORButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.0024956602137536,
						point = "left",
						parent = "EA_Window_ZoneControl",
						y = 0,
					},
				},
				name = "YakuiSORButton",
				scale = 0.61333346366882,
			},
			InfoPanelRenown = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = 0.009375000372529,
						point = "bottomleft",
						parent = "InfoPanel",
						y = -0.0012499999720603,
					},
				},
				name = "InfoPanelRenown",
				scale = 1.2800000905991,
			},
			GroupMember5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0,
						point = "left",
						parent = "Root",
						y = -0.059374999254942,
					},
				},
				name = "GroupMember5",
				scale = 1.2800000905991,
			},
			i_xp = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = -0.008497966453433,
						point = "right",
						parent = "i_menu1",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_xp",
				minscale = 0.55,
			},
			TargetsHostile3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile2",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile3",
				scale = 1.2800000905991,
			},
			DAoCBuff_Hostile_AddInfo = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topright",
						parent = "HostileTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Hostile_AddInfo",
				scale = 0.80313986539841,
			},
			DAoCBuff_Hostile_Morales = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topleft",
						parent = "HostileTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Hostile_Morales",
				scale = 0.79786759614944,
			},
			i_menu7 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu6",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_menu7",
				minscale = 0.55,
			},
			FriendlyTarget = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = 0.08203125,
						point = "topleft",
						parent = "Root",
						y = 0.20000000298023,
					},
				},
				name = "FriendlyTarget",
				scale = 1.2800000905991,
			},
			EA_CareerResourceWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.048411466181278,
						point = "bottomleft",
						parent = "EA_GrantedAbilities",
						y = -0.042249988764524,
					},
				},
				name = "EA_CareerResourceWindow",
				scale = 0.65,
			},
			i_menu6 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu5",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_menu6",
				minscale = 0.55,
			},
			EA_GrantedAbilities = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "EA_StanceBar",
						y = 0,
					},
				},
				name = "EA_GrantedAbilities",
				scale = 0.59466874599457,
			},
			DAoCBuff_Friendly_Self_Buff = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0031119,
						point = "bottomleft",
						parent = "FriendlyTarget",
						y = 0,
					},
				},
				name = "DAoCBuff_Friendly_Self_Buff",
				scale = 0.76761025190353,
			},
			MotionIcon = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.0015625000232831,
						point = "left",
						parent = "Root",
						y = 0.057552084326744,
					},
				},
				minscale = 0.5,
				name = "MotionIcon",
				scale = 0.5,
			},
			TargetsHostile2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile1",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile2",
				scale = 1.2800000905991,
			},
			MotionWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0,
						point = "right",
						parent = "EA_Window_RvRTracker",
						y = 0,
					},
				},
				name = "MotionWindow",
				scale = 0.75,
			},
			DAoCBuff_GroupBuffs2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember3",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs2",
				scale = 0.75690722465515,
			},
			ResToggle = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0.002343749627471,
						point = "left",
						parent = "Root",
						y = 0.0791015625,
					},
				},
				minscale = 0.5,
				name = "ResToggle",
				scale = 0.46875,
			},
			PlayerHP = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.08203125,
						point = "topright",
						parent = "Root",
						y = 0.050000004470348,
					},
				},
				name = "PlayerHP",
				scale = 1.2800000905991,
			},
			DAoCBuff_GroupBuffs1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember2",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs1",
				scale = 0.76079857349396,
			},
			YBackPanel1280x1024 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = 0,
					},
				},
				name = "YBackPanel1280x1024",
				scale = 1.5,
			},
			i_menu2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu1",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_menu2",
				minscale = 0.55,
			},
			i_rn = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.018209867179394,
						point = "left",
						parent = "i_menu8",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_rn",
				minscale = 0.55,
			},
			DAoCBuff_Self_Morales = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topright",
						parent = "PlayerHP",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Self_Morales",
				scale = 0.7961608171463,
			},
			snt_info_time = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "topright",
						parent = "snt_info_fps",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "snt_info_time",
				minscale = 0.55,
			},
			EA_CareerResourceWindowActionBar = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.037199998895327,
						point = "bottomleft",
						parent = "EA_StanceBar",
						y = -0.0020668,
					},
				},
				name = "EA_CareerResourceWindowActionBar",
				scale = 0.4464213,
			},
			EA_MoraleBar = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomright",
						parent = "EA_StanceBar",
						y = 0,
					},
				},
				name = "EA_MoraleBar",
				scale = 0.75503593683243,
			},
			YakuiBWButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.034444451332092,
						point = "bottomleft",
						parent = "Root",
						y = -0.14395843446255,
					},
				},
				name = "YakuiBWButton",
				scale = 0.61333441734314,
			},
			YakuiFWButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0.0018878700211644,
						point = "left",
						parent = "YakuiBWButton",
						y = 0,
					},
				},
				name = "YakuiFWButton",
				scale = 0.61333537101746,
			},
			TargetsFriendly3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly2",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly3",
				scale = 1.2800000905991,
			},
			UI_KwestorTrackerNub = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.00078125041909516,
						point = "bottomright",
						parent = "Minmap",
						y = -0.01041667163372,
					},
				},
				name = "UI_KwestorTrackerNub",
				scale = 0.64000034332275,
			},
			i_menu8 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu7",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_menu8",
				minscale = 0.55,
			},
			DAoCBuff_GroupBuffs4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0.0025034,
						point = "bottomleft",
						parent = "GroupMember5",
						y = 0,
					},
				},
				name = "DAoCBuff_GroupBuffs4",
				scale = 0.76246511936188,
			},
			yInfo_bag = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0,
						point = "right",
						parent = "YakuiButton",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "yInfo_bag",
				minscale = 0.55,
			},
			YakuiATButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.16694584488869,
						point = "bottom",
						parent = "Root",
						y = -0.12251385301352,
					},
				},
				name = "YakuiATButton",
				scale = 0.55466824769974,
			},
			DAoCBuff_Friendly_Morales = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topleft",
						parent = "FriendlyTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Friendly_Morales",
				scale = 0.75142866373062,
			},
			TargetsFriendly4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly3",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly4",
				scale = 1.2800000905991,
			},
			CDownWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "PlayerHP",
						y = 0.034038804471493,
					},
				},
				name = "CDownWindow",
				scale = 0.76887649297714,
			},
			DAoCBuff_Self_AddInfo = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "topleft",
						parent = "PlayerHP",
						y = 0.0078431628644466,
					},
				},
				name = "DAoCBuff_Self_AddInfo",
				scale = 0.80313986539841,
			},
			EA_Window_KeepObjectiveTracker = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = -0.11826629936695,
						point = "topright",
						parent = "Root",
						y = 0.029374999925494,
					},
				},
				name = "EA_Window_KeepObjectiveTracker",
			},
			EA_StanceBar = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = 0.19825001060963,
						point = "bottom",
						parent = "Root",
						y = -0.14562499523163,
					},
				},
				name = "EA_StanceBar",
				scale = 0.72000002861023,
			},
			YakuiWCButton = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = -0.0024956641718745,
						point = "right",
						parent = "EA_Window_ZoneControl",
						y = 0,
					},
				},
				name = "YakuiWCButton",
				scale = 0.61333441734314,
			},
			EA_Window_ZoneControl = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = -0.16872677206993,
						point = "bottomright",
						parent = "Root",
						y = -0.14529998600483,
					},
				},
				name = "EA_Window_ZoneControl",
				scale = 0.5373158454895,
			},
			PlayerAP = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "PlayerHP",
						y = 0,
					},
				},
				name = "PlayerAP",
				scale = 1.2800000905991,
			},
			EA_ActionBar4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomright",
						x = 0,
						point = "bottomright",
						parent = "Root",
						y = -0.14248697459698,
					},
				},
				name = "EA_ActionBar4",
				scale = 0.51918941736221,
			},
			GroupMember3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "Root",
						y = 0.234375,
					},
				},
				name = "GroupMember3",
				scale = 1.2800000905991,
			},
			InfoPanelExp = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.009375000372529,
						point = "bottomright",
						parent = "InfoPanel",
						y = -0.0012499999720603,
					},
				},
				name = "InfoPanelExp",
				scale = 1.2800000905991,
			},
			DAoCBuff_Friendly_AddInfo = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "topright",
						parent = "FriendlyTarget",
						y = 0.0078432,
					},
				},
				name = "DAoCBuff_Friendly_AddInfo",
				scale = 0.81000488996506,
			},
			i_gl = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0,
						point = "right",
						parent = "snt_info_time",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_gl",
				minscale = 0.55,
			},
			i_menu1 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.057922560721636,
						point = "bottom",
						parent = "Root",
						y = -0.0030761719681323,
					},
				},
				scale = 0.93234533071518,
				name = "i_menu1",
				minscale = 0.55,
			},
			MoraleEditor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "EA_TacticsEditor",
						y = 0,
					},
				},
				scale = 0.68117088079453,
				name = "MoraleEditor",
				minscale = 0.5,
			},
			i_menu4 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu3",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_menu4",
				minscale = 0.55,
			},
			Castbar = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottom",
						x = -0.05078125,
						point = "bottom",
						parent = "Root",
						y = -0.20000000298023,
					},
				},
				scale = 1.2800000905991,
				name = "Castbar",
				minscale = 0.75,
			},
			TextEntryAnchor = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "bottom",
						parent = "EA_ActionBar3",
						y = -0.0088701397180557,
					},
				},
				name = "TextEntryAnchor",
				scale = 1.0051449537277,
			},
			MouseoverTarget = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "EA_Window_RvRTracker",
						y = -1.862645149231e-009,
					},
				},
				name = "MouseoverTarget",
				scale = 1.2800000905991,
			},
			MouseOverTargetWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "right",
						parent = "Root",
						y = 0.16249999403954,
					},
				},
				name = "MouseOverTargetWindow",
				scale = 0.95999997854233,
			},
			TargetsFriendly2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsFriendly1",
						y = 0.012500000186265,
					},
				},
				name = "TargetsFriendly2",
				scale = 1.2800000905991,
			},
			EA_ActionBar3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "EA_ActionBar2",
						y = 0.0019314,
					},
				},
				name = "EA_ActionBar3",
				scale = 0.74164664745331,
			},
			GroupSpotter = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0.00092773593496531,
						point = "bottomleft",
						parent = "Minmap",
						y = -0.012864601798356,
					},
				},
				name = "GroupSpotter",
				scale = 0.76000136137009,
			},
			EA_AssistWindow = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "yAssistHelper",
						y = 0,
					},
				},
				name = "EA_AssistWindow",
				scale = 1.0120924711227,
			},
			i_menu5 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu4",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_menu5",
				minscale = 0.55,
			},
			Minmap = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topright",
						x = -0.0040793353691697,
						point = "topright",
						parent = "Root",
						y = 0.0081441756337881,
					},
				},
				scale = 0.79232025146484,
				name = "Minmap",
				minscale = 0.4,
			},
			Pet = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = -0.08203125,
						point = "topright",
						parent = "Root",
						y = 0.20000000298023,
					},
				},
				name = "Pet",
				scale = 1.2800000905991,
			},
			PlayerCareer = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "PlayerAP",
						y = 0,
					},
				},
				name = "PlayerCareer",
				scale = 1.2800000905991,
			},
			InfoPanel = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "top",
						x = 0,
						point = "top",
						parent = "Root",
						y = 0.0062500000931323,
					},
				},
				name = "InfoPanel",
				scale = 1.2800000905991,
			},
			i_fr = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "left",
						x = 0,
						point = "right",
						parent = "i_gl",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_fr",
				minscale = 0.55,
			},
			EA_ActionBar2 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "topleft",
						x = 0,
						point = "bottomleft",
						parent = "EA_ActionBar1",
						y = 0.001931702834554,
					},
				},
				name = "EA_ActionBar2",
				scale = 0.74177396297455,
			},
			TargetsHostile6 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "topleft",
						parent = "TargetsHostile5",
						y = 0.012500000186265,
					},
				},
				name = "TargetsHostile6",
				scale = 1.2800000905991,
			},
			WARCommanderInfo = 
			{
				anchors = 
				{
					
					{
						parentpoint = "topright",
						x = 0,
						point = "bottomright",
						parent = "EA_Window_RvRTracker",
						y = -0.052271373569965,
					},
					
					{
						parentpoint = "topleft",
						x = 0,
						point = "topleft",
						parent = "EA_Window_RvRTracker",
						y = -0.20385825634003,
					},
				},
				name = "WARCommanderInfo",
				scale = 0.80288833379745,
			},
			DAoCBuff_Self_Debuffs = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "center",
						x = 0,
						point = "top",
						parent = "Root",
						y = 0.16747744055465,
					},
				},
				name = "DAoCBuff_Self_Debuffs",
				scale = 0.91873347759247,
			},
			i_menu3 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "right",
						x = 0,
						point = "left",
						parent = "i_menu2",
						y = 0,
					},
				},
				scale = 0.93234533071518,
				name = "i_menu3",
				minscale = 0.55,
			},
			YBackPanel1920x1200 = 
			{
				anchors = 
				{
					[1] = 
					{
						parentpoint = "bottomleft",
						x = 0,
						point = "bottomleft",
						parent = "Root",
						y = 0,
					},
				},
				name = "YBackPanel1920x1200",
				scale = 1.2800000905991,
			},
		},
	}
	Vectors.LoadProfile()
end