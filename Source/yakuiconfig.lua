-- this lua is now about the Skin Settings and the Custom Settings WINDOWs

if not yakuiconfig then yakuiconfig = {} end
local configside = 1
local booltowstring={[false]=L"Off",[true]=L"On"}

function yakuiconfig.initialize()
	yakuiconfig.initializeButton()
	-- Custom Color Sliders
	CreateWindow("yCustomcolorsliders",false)
	WindowSetShowing("yCustomcolorsliders",false)
	WindowSetHandleInput("yCustomcolorsliders",true)
	LabelSetText("yCustomcolorsliders_Headline1", L"Panel:")
	LabelSetText("yCustomcolorsliders_Headline2", L"Background:")
	LabelSetText("yCustomcolorsliders_Headline3", L"FX:")
	LabelSetText("yCustomcolorslidersTitleBarText", L"Appearance:")
	LabelSetText("yCustomcolorsliders_Red", L"Red: "..yakuiVar.Colors.colprintr)
	LabelSetText("yCustomcolorsliders_Green", L"Green: "..yakuiVar.Colors.colprintg)
	LabelSetText("yCustomcolorsliders_Blue", L"Blue: "..yakuiVar.Colors.colprintb)
	LabelSetText("yCustomcolorsliders_Alpha", L"Alpha: "..yakuiVar.Panel.Panelalpha)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderR" , yakuiVar.Colors.colprintr/256)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderG" , yakuiVar.Colors.colprintg/256)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderB" , yakuiVar.Colors.colprintb/256)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderA" , yakuiVar.Panel.Panelalpha)
	LabelSetText("yCustomcolorsliders_BGRed", L"Red: "..yakuiVar.Colors.background.r)
	LabelSetText("yCustomcolorsliders_BGGreen", L"Green: "..yakuiVar.Colors.background.g)
	LabelSetText("yCustomcolorsliders_BGBlue", L"Blue: "..yakuiVar.Colors.background.b)
	LabelSetText("yCustomcolorsliders_BGAlpha", L"Alpha: "..yakuiVar.Panel.alpha)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderR" , yakuiVar.Colors.background.r/256)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderG" , yakuiVar.Colors.background.g/256)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderB" , yakuiVar.Colors.background.b/256)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderA" , yakuiVar.Panel.alpha)
	LabelSetText("yCustomcolorsliders_FXRed", L"Red: "..yakuiVar.Colors.fx.r)
	LabelSetText("yCustomcolorsliders_FXGreen", L"Green: "..yakuiVar.Colors.fx.g)
	LabelSetText("yCustomcolorsliders_FXBlue", L"Blue: "..yakuiVar.Colors.fx.b)
	LabelSetText("yCustomcolorsliders_FXAlpha", L"Alpha: "..yakuiVar.Panel.fxalpha)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderR" , yakuiVar.Colors.fx.r/256)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderG" , yakuiVar.Colors.fx.g/256)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderB" , yakuiVar.Colors.fx.b/256)
	SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderA" , yakuiVar.Panel.fxalpha)
	LabelSetText("yCustomcolorsliders_AlphaToggleLabel", L"Enable Alpha")
	ButtonSetPressedFlag("yCustomcolorsliders_AlphaToggle", yakuiVar.Panel.alphatoggle)
	SliderBarSetDisabledFlag("yCustomcolorsliders_BGSliderA", not yakuiVar.Panel.alphatoggle )
	if yakuiVar.Panel.alphatoggle == false then 
		WindowSetTintColor("yCustomcolorsliders_BGSliderA", 120, 120, 120 )
		--LabelSetTextColor("yCustomcolorsliders_BGAlpha", 120, 120, 120 )
	else
		WindowSetTintColor("yCustomcolorsliders_BGSliderA", 255, 255, 255 )
		--LabelSetTextColor("yCustomcolorsliders_BGAlpha", 255, 255, 255 )
	end
	ComboBoxAddMenuItem ("yCustomcolorsliders_TextureCombo", L"Classic")
	ComboBoxAddMenuItem ("yCustomcolorsliders_TextureCombo", L"Snake Shake")
	ComboBoxAddMenuItem ("yCustomcolorsliders_TextureCombo", L"YakUI 1.3")
	ComboBoxAddMenuItem ("yCustomcolorsliders_TextureCombo", L"Skull&Bones")
	ComboBoxAddMenuItem ("yCustomcolorsliders_TextureCombo", L"Tranzparenz")
	if yakuiVar.Panel.tex == "Set1" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_TextureCombo", 1)
	elseif yakuiVar.Panel.tex == "Set2" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_TextureCombo", 2)
	elseif yakuiVar.Panel.tex == "Set3" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_TextureCombo", 3)
	elseif yakuiVar.Panel.tex == "Set4" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_TextureCombo", 4)
	elseif yakuiVar.Panel.tex == "Set5" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_TextureCombo", 5)
	end
	ComboBoxAddMenuItem ("yCustomcolorsliders_Side", L"Panel Color Settings")
	if Effigy then
		ComboBoxAddMenuItem ("yCustomcolorsliders_Side", L"Unitframe Settings 1")
		ComboBoxAddMenuItem ("yCustomcolorsliders_Side", L"Unitframe Settings 2")
	end
	ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Side", configside)
	ComboBoxAddMenuItem ("yCustomcolorsliders_Preset", L"Transparenz (light)")
	ComboBoxAddMenuItem ("yCustomcolorsliders_Preset", L"Transparenz (dark)")
	ComboBoxAddMenuItem ("yCustomcolorsliders_Preset", L"Vintage")
	ComboBoxAddMenuItem ("yCustomcolorsliders_Preset", L"Eeeeks!")
	ComboBoxAddMenuItem ("yCustomcolorsliders_Preset", L"Mariner")
	ComboBoxAddMenuItem ("yCustomcolorsliders_Preset", L"Dem Bones")
	ComboBoxAddMenuItem ("yCustomcolorsliders_Preset", L"Custom")
	if yakuiVar.Colors.col == "Preset1" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 1)
	elseif yakuiVar.Colors.col == "Preset2" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 2)
	elseif yakuiVar.Colors.col == "Preset3" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 3)
	elseif yakuiVar.Colors.col == "Preset4" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 4)
	elseif yakuiVar.Colors.col == "Preset5" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 5)
	elseif yakuiVar.Colors.col == "Preset6" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 6)
	else ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 7)
	end
	

	
	-- Skin Settings
	CreateWindow("ySkinBackgroundColor",false)
	WindowSetShowing("ySkinBackgroundColor",false)
	WindowSetHandleInput("ySkinBackgroundColor",true)
	ButtonSetPressedFlag("ySkinBackgroundColor_LinkwpanelCheckbox", yakuiVar.Skin.newbackgrounds.linkwpanel)
	ButtonSetPressedFlag("ySkinBackgroundColor_ActivateSkin", yakuiVar.Skin.active)
	if yakuiVar.Skin.newbackgrounds.linkwpanel == true then
		WindowSetTintColor("ySkinBackgroundColor_SliderR", 120, 120, 120 )
		WindowSetTintColor("ySkinBackgroundColor_SliderG", 120, 120, 120 )
		WindowSetTintColor("ySkinBackgroundColor_SliderB", 120, 120, 120 )
		WindowSetTintColor("ySkinBackgroundColor_SliderA", 120, 120, 120 )
		LabelSetTextColor("ySkinBackgroundColor_Red", 160, 160, 160)
		LabelSetTextColor("ySkinBackgroundColor_Green", 160, 160, 160)
		LabelSetTextColor("ySkinBackgroundColor_Blue", 160, 160, 160)
		LabelSetTextColor("ySkinBackgroundColor_Alpha", 160, 160, 160)
	else
		WindowSetTintColor("ySkinBackgroundColor_SliderR", 255, 255, 255 )
		WindowSetTintColor("ySkinBackgroundColor_SliderG", 255, 255, 255 )
		WindowSetTintColor("ySkinBackgroundColor_SliderB", 255, 255, 255 )
		WindowSetTintColor("ySkinBackgroundColor_SliderA", 255, 255, 255 )
		LabelSetTextColor("ySkinBackgroundColor_Red", 255, 255, 255)
		LabelSetTextColor("ySkinBackgroundColor_Green", 255, 255, 255)
		LabelSetTextColor("ySkinBackgroundColor_Blue", 255, 255, 255)
		LabelSetTextColor("ySkinBackgroundColor_Alpha", 255, 255, 255)
	end
	LabelSetText("ySkinBackgroundColor_Headline1", L"Link BG with panel")
	LabelSetText("ySkinBackgroundColor_Headline2", L"Activate Skin Mod")
	LabelSetText("ySkinBackgroundColorTitleBarText", L"ySkin:")
	LabelSetText("ySkinBackgroundColor_Red", L"Red: "..yakuiVar.Skin.newbackgrounds.r)
	LabelSetText("ySkinBackgroundColor_Green", L"Green: "..yakuiVar.Skin.newbackgrounds.g)
	LabelSetText("ySkinBackgroundColor_Blue", L"Blue: "..yakuiVar.Skin.newbackgrounds.b)
	LabelSetText("ySkinBackgroundColor_Alpha", L"Alpha: "..yakuiVar.Skin.newbackgrounds.alpha)
	SliderBarSetCurrentPosition( "ySkinBackgroundColor_SliderR" , yakuiVar.Skin.newbackgrounds.r/256)
	SliderBarSetCurrentPosition( "ySkinBackgroundColor_SliderG" , yakuiVar.Skin.newbackgrounds.g/256)
	SliderBarSetCurrentPosition( "ySkinBackgroundColor_SliderB" , yakuiVar.Skin.newbackgrounds.b/256)
	SliderBarSetCurrentPosition( "ySkinBackgroundColor_SliderA" , yakuiVar.Skin.newbackgrounds.alpha)
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderR", yakuiVar.Skin.newbackgrounds.linkwpanel )
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderG", yakuiVar.Skin.newbackgrounds.linkwpanel )
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderB", yakuiVar.Skin.newbackgrounds.linkwpanel )
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderA", yakuiVar.Skin.newbackgrounds.linkwpanel )
end

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Config core functions

function yakuiconfig.texturecombo()
	yakuiVar.Panel.tex = "Set"..ComboBoxGetSelectedMenuItem("yCustomcolorsliders_TextureCombo")
	yakui.texture()
	yakuiVar.Colors.col = "Custom"
	yakuiconfig.colorsliderUpdate()
end

function yakuiconfig.presetcombo()
	if ComboBoxGetSelectedMenuItem("yCustomcolorsliders_Preset") == 1 then
		yakuiconfig.colorselect1()
	elseif ComboBoxGetSelectedMenuItem("yCustomcolorsliders_Preset") == 2 then
		yakuiconfig.colorselect2()
	elseif ComboBoxGetSelectedMenuItem("yCustomcolorsliders_Preset") == 3 then
		yakuiconfig.colorselect3()
	elseif ComboBoxGetSelectedMenuItem("yCustomcolorsliders_Preset") == 4 then
		yakuiconfig.colorselect4()
	elseif ComboBoxGetSelectedMenuItem("yCustomcolorsliders_Preset") == 5 then
		yakuiconfig.colorselect5()
	elseif ComboBoxGetSelectedMenuItem("yCustomcolorsliders_Preset") == 6 then
		yakuiconfig.colorselect6()
	elseif ComboBoxGetSelectedMenuItem("yCustomcolorsliders_Preset") == 7 then
		yakuiVar.Colors.col = "Custom"
		yakuiconfig.colorsliderUpdate()
	end
end

function yakuiconfig.configsidecombo()
	configside = ComboBoxGetSelectedMenuItem("yCustomcolorsliders_Side")
	yakuiconfig.colorsliderUpdate()
end

function yakuiconfig.alphatoggle() yakui.alphatoggle() end

function yakuiconfig.activateskin()
	yakuiVar.Skin.active = not yakuiVar.Skin.active
	InterfaceCore.ReloadUI()
end

function yakuiconfig.editwindowbackgrounds() WindowSetShowing("ySkinBackgroundColor", not WindowGetShowing("ySkinBackgroundColor")) end	-- Edit Skin Settings

function yakuiconfig.linkwpanel()
	yakuiVar.Skin.newbackgrounds.linkwpanel = not yakuiVar.Skin.newbackgrounds.linkwpanel
	ButtonSetPressedFlag("ySkinBackgroundColor_LinkwpanelCheckbox", yakuiVar.Skin.newbackgrounds.linkwpanel)
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderR", yakuiVar.Skin.newbackgrounds.linkwpanel )
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderG", yakuiVar.Skin.newbackgrounds.linkwpanel )
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderB", yakuiVar.Skin.newbackgrounds.linkwpanel )
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderA", yakuiVar.Skin.newbackgrounds.linkwpanel )
	if yakuiVar.Skin.newbackgrounds.linkwpanel == true then
		WindowSetTintColor("ySkinBackgroundColor_SliderR", 120, 120, 120 )
		WindowSetTintColor("ySkinBackgroundColor_SliderG", 120, 120, 120 )
		WindowSetTintColor("ySkinBackgroundColor_SliderB", 120, 120, 120 )
		WindowSetTintColor("ySkinBackgroundColor_SliderA", 120, 120, 120 )
		LabelSetTextColor("ySkinBackgroundColor_Red", 160, 160, 160)
		LabelSetTextColor("ySkinBackgroundColor_Green", 160, 160, 160)
		LabelSetTextColor("ySkinBackgroundColor_Blue", 160, 160, 160)
		LabelSetTextColor("ySkinBackgroundColor_Alpha", 160, 160, 160)
	else
		WindowSetTintColor("ySkinBackgroundColor_SliderR", 255, 255, 255 )
		WindowSetTintColor("ySkinBackgroundColor_SliderG", 255, 255, 255 )
		WindowSetTintColor("ySkinBackgroundColor_SliderB", 255, 255, 255 )
		WindowSetTintColor("ySkinBackgroundColor_SliderA", 255, 255, 255 )
		LabelSetTextColor("ySkinBackgroundColor_Red", 255, 255, 255)
		LabelSetTextColor("ySkinBackgroundColor_Green", 255, 255, 255)
		LabelSetTextColor("ySkinBackgroundColor_Blue", 255, 255, 255)
		LabelSetTextColor("ySkinBackgroundColor_Alpha", 255, 255, 255)
	end
	yakuiSkin.newbackground()
end

function yakuiconfig.closewindowbackgrounds() WindowSetShowing("ySkinBackgroundColor", false) end

function yakuiconfig.editcustomcolor() WindowSetShowing("yCustomcolorsliders", not WindowGetShowing("yCustomcolorsliders")) end	-- Edit Custom Settings

function yakuiconfig.setlinkwpanel()
	ButtonSetPressedFlag("ySkinBackgroundColor_LinkwpanelCheckbox", yakuiVar.Skin.newbackgrounds.linkwpanel)
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderR", yakuiVar.Skin.newbackgrounds.linkwpanel )
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderG", yakuiVar.Skin.newbackgrounds.linkwpanel )
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderB", yakuiVar.Skin.newbackgrounds.linkwpanel )
	SliderBarSetDisabledFlag("ySkinBackgroundColor_SliderA", yakuiVar.Skin.newbackgrounds.linkwpanel )
	if yakuiVar.Skin.newbackgrounds.linkwpanel == true then
		WindowSetTintColor("ySkinBackgroundColor_SliderR", 120, 120, 120 )
		WindowSetTintColor("ySkinBackgroundColor_SliderG", 120, 120, 120 )
		WindowSetTintColor("ySkinBackgroundColor_SliderB", 120, 120, 120 )
		WindowSetTintColor("ySkinBackgroundColor_SliderA", 120, 120, 120 )
		LabelSetTextColor("ySkinBackgroundColor_Red", 160, 160, 160)
		LabelSetTextColor("ySkinBackgroundColor_Green", 160, 160, 160)
		LabelSetTextColor("ySkinBackgroundColor_Blue", 160, 160, 160)
		LabelSetTextColor("ySkinBackgroundColor_Alpha", 160, 160, 160)
	else
		WindowSetTintColor("ySkinBackgroundColor_SliderR", 255, 255, 255 )
		WindowSetTintColor("ySkinBackgroundColor_SliderG", 255, 255, 255 )
		WindowSetTintColor("ySkinBackgroundColor_SliderB", 255, 255, 255 )
		WindowSetTintColor("ySkinBackgroundColor_SliderA", 255, 255, 255 )
		LabelSetTextColor("ySkinBackgroundColor_Red", 255, 255, 255)
		LabelSetTextColor("ySkinBackgroundColor_Green", 255, 255, 255)
		LabelSetTextColor("ySkinBackgroundColor_Blue", 255, 255, 255)
		LabelSetTextColor("ySkinBackgroundColor_Alpha", 255, 255, 255)
	end
	yakuiSkin.newbackground()
end

function yakuiconfig.alphasliderpanel(pos)
	yakuiVar.Panel.Panelalpha = tonumber(wstring.format( L"%0.1f", towstring(pos) ))
	yakuiVar.Panel.alphatoggle = true
	for i = 1,6 do WindowSetShowing("yconfigcontextmenucolorselect"..i.."Check", false) end
	yakuiVar.Colors.col = "Custom"
	yakui.color()
	yakuiconfig.colorsliderUpdate()
end

function yakuiconfig.alphasliderbackground(pos)
	yakuiVar.Panel.alpha = tonumber(wstring.format( L"%0.1f", towstring(pos) ))
	for i = 1,6 do WindowSetShowing("yconfigcontextmenucolorselect"..i.."Check", false) end
	yakuiVar.Colors.col = "Custom"
	yakui.color()
	yakuiconfig.colorsliderUpdate()
end

function yakuiconfig.alphasliderfx(pos)
	yakuiVar.Panel.fxalpha = tonumber(wstring.format( L"%0.1f", towstring(pos) ))
	for i = 1,6 do WindowSetShowing("yconfigcontextmenucolorselect"..i.."Check", false) end
	yakuiVar.Colors.col = "Custom"
	yakui.color()
	yakuiconfig.colorsliderUpdate()
end

function yakuiconfig.windowsbackgroundsalphaslider(pos)
	yakuiVar.Skin.newbackgrounds.alpha = tonumber(wstring.format( L"%0.1f", towstring(pos) ))
	yakuiSkin.newbackground()
	yakuiconfig.windowsbackgroundssliderUpdate()
end

local function windowsbackgroundsslider(sel, pos)
	yakuiVar.Skin.newbackgrounds[sel] = math.floor(255 * pos + 0.5)
	yakuiSkin.newbackground()
	yakuiconfig.windowsbackgroundssliderUpdate()
end
function yakuiconfig.windowsbackgroundssliderR(pos) windowsbackgroundsslider("r", pos) end
function yakuiconfig.windowsbackgroundssliderG(pos) windowsbackgroundsslider("g", pos) end
function yakuiconfig.windowsbackgroundssliderB(pos) windowsbackgroundsslider("b", pos) end

function yakuiconfig.windowsbackgroundssliderUpdate()
	LabelSetText("ySkinBackgroundColor_Red", L"Red: "..yakuiVar.Skin.newbackgrounds.r)
	LabelSetText("ySkinBackgroundColor_Green", L"Green: "..yakuiVar.Skin.newbackgrounds.g)
	LabelSetText("ySkinBackgroundColor_Blue", L"Blue: "..yakuiVar.Skin.newbackgrounds.b)
	LabelSetText("ySkinBackgroundColor_Alpha", L"Alpha: "..yakuiVar.Skin.newbackgrounds.alpha)
	SliderBarSetCurrentPosition( "ySkinBackgroundColor_SliderR" , yakuiVar.Skin.newbackgrounds.r/256)
	SliderBarSetCurrentPosition( "ySkinBackgroundColor_SliderG" , yakuiVar.Skin.newbackgrounds.g/256)
	SliderBarSetCurrentPosition( "ySkinBackgroundColor_SliderB" , yakuiVar.Skin.newbackgrounds.b/256)
	SliderBarSetCurrentPosition( "ySkinBackgroundColor_SliderA" , yakuiVar.Skin.newbackgrounds.alpha)
end

function yakuiconfig.colorsliderClose() WindowSetShowing("yCustomcolorsliders", false) end

function yakuiconfig.colorsliderUpdate()
	if configside == 1 then
		LabelSetText("yCustomcolorsliders_Headline1", L"Panel:")
		LabelSetText("yCustomcolorsliders_Headline2", L"Background:")
		LabelSetText("yCustomcolorsliders_Headline3", L"FX:")
		LabelSetText("yCustomcolorsliders_Red", L"Red: "..yakuiVar.Colors.colprintr)
		LabelSetText("yCustomcolorsliders_Green", L"Green: "..yakuiVar.Colors.colprintg)
		LabelSetText("yCustomcolorsliders_Blue", L"Blue: "..yakuiVar.Colors.colprintb)
		WindowSetShowing("yCustomcolorsliders_Alpha", true)
		LabelSetText("yCustomcolorsliders_Alpha", L"Alpha: "..yakuiVar.Panel.Panelalpha)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderR" , yakuiVar.Colors.colprintr/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderG" , yakuiVar.Colors.colprintg/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderB" , yakuiVar.Colors.colprintb/256)
		WindowSetShowing("yCustomcolorsliders_SliderA" , true)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderA" , yakuiVar.Panel.Panelalpha)
		LabelSetText("yCustomcolorsliders_BGRed", L"Red: "..yakuiVar.Colors.background.r)
		LabelSetText("yCustomcolorsliders_BGGreen", L"Green: "..yakuiVar.Colors.background.g)
		LabelSetText("yCustomcolorsliders_BGBlue", L"Blue: "..yakuiVar.Colors.background.b)
		WindowSetShowing("yCustomcolorsliders_BGAlpha", true)
		LabelSetText("yCustomcolorsliders_BGAlpha", L"Alpha: "..yakuiVar.Panel.alpha)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderR" , yakuiVar.Colors.background.r/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderG" , yakuiVar.Colors.background.g/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderB" , yakuiVar.Colors.background.b/256)
		WindowSetShowing("yCustomcolorsliders_BGSliderA" , true)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderA" , yakuiVar.Panel.alpha)
		LabelSetText("yCustomcolorsliders_FXRed", L"Red: "..yakuiVar.Colors.fx.r)
		LabelSetText("yCustomcolorsliders_FXGreen", L"Green: "..yakuiVar.Colors.fx.g)
		LabelSetText("yCustomcolorsliders_FXBlue", L"Blue: "..yakuiVar.Colors.fx.b)
		WindowSetShowing("yCustomcolorsliders_FXAlpha", true)
		LabelSetText("yCustomcolorsliders_FXAlpha", L"Alpha: "..yakuiVar.Panel.fxalpha)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderR" , yakuiVar.Colors.fx.r/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderG" , yakuiVar.Colors.fx.g/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderB" , yakuiVar.Colors.fx.b/256)
		WindowSetShowing( "yCustomcolorsliders_FXSliderA" , true)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderA" , yakuiVar.Panel.fxalpha)
		WindowSetShowing("yCustomcolorsliders_AlphaToggleLabel", true)
		WindowSetShowing("yCustomcolorsliders_AlphaToggle", true)
		ButtonSetPressedFlag("yCustomcolorsliders_AlphaToggle", yakuiVar.Panel.alphatoggle)
		SliderBarSetDisabledFlag("yCustomcolorsliders_BGSliderA", not yakuiVar.Panel.alphatoggle )
		if yakuiVar.Panel.alphatoggle == false then 
			WindowSetTintColor("yCustomcolorsliders_BGSliderA", 120, 120, 120 )
		else
			WindowSetTintColor("yCustomcolorsliders_BGSliderA", 255, 255, 255 )
		end
	elseif configside == 2 then
		LabelSetText("yCustomcolorsliders_Headline1", L"Player:")
		LabelSetText("yCustomcolorsliders_Headline2", L"Hostile:")
		LabelSetText("yCustomcolorsliders_Headline3", L"Friendly:")
		LabelSetText("yCustomcolorsliders_Red", L"Red: "..yakuiVar.Colors.unitframes.player.r)
		LabelSetText("yCustomcolorsliders_Green", L"Green: "..yakuiVar.Colors.unitframes.player.g)
		LabelSetText("yCustomcolorsliders_Blue", L"Blue: "..yakuiVar.Colors.unitframes.player.b)
		WindowSetShowing("yCustomcolorsliders_Alpha", false)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderR" , yakuiVar.Colors.unitframes.player.r/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderG" , yakuiVar.Colors.unitframes.player.g/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderB" , yakuiVar.Colors.unitframes.player.b/256)
		WindowSetShowing("yCustomcolorsliders_SliderA" , false)
		LabelSetText("yCustomcolorsliders_BGRed", L"Red: "..yakuiVar.Colors.unitframes.hostile.r)
		LabelSetText("yCustomcolorsliders_BGGreen", L"Green: "..yakuiVar.Colors.unitframes.hostile.g)
		LabelSetText("yCustomcolorsliders_BGBlue", L"Blue: "..yakuiVar.Colors.unitframes.hostile.b)
		WindowSetShowing("yCustomcolorsliders_BGAlpha", false)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderR" , yakuiVar.Colors.unitframes.hostile.r/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderG" , yakuiVar.Colors.unitframes.hostile.g/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderB" , yakuiVar.Colors.unitframes.hostile.b/256)
		WindowSetShowing("yCustomcolorsliders_BGSliderA" , false)
		LabelSetText("yCustomcolorsliders_FXRed", L"Red: "..yakuiVar.Colors.unitframes.friendly.r)
		LabelSetText("yCustomcolorsliders_FXGreen", L"Green: "..yakuiVar.Colors.unitframes.friendly.g)
		LabelSetText("yCustomcolorsliders_FXBlue", L"Blue: "..yakuiVar.Colors.unitframes.friendly.b)
		WindowSetShowing("yCustomcolorsliders_FXAlpha", false)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderR" , yakuiVar.Colors.unitframes.friendly.r/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderG" , yakuiVar.Colors.unitframes.friendly.g/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderB" , yakuiVar.Colors.unitframes.friendly.b/256)
		WindowSetShowing( "yCustomcolorsliders_FXSliderA" , false)
		WindowSetShowing("yCustomcolorsliders_AlphaToggleLabel", false)
		WindowSetShowing("yCustomcolorsliders_AlphaToggle", false)
	elseif configside == 3 then
		LabelSetText("yCustomcolorsliders_Headline1", L"AP Bar:")
		LabelSetText("yCustomcolorsliders_Headline2", L"Career Bar:")
		LabelSetText("yCustomcolorsliders_Headline3", L"Castbar:")
		LabelSetText("yCustomcolorsliders_Red", L"Red: "..yakuiVar.Colors.unitframes.apbar.r)
		LabelSetText("yCustomcolorsliders_Green", L"Green: "..yakuiVar.Colors.unitframes.apbar.g)
		LabelSetText("yCustomcolorsliders_Blue", L"Blue: "..yakuiVar.Colors.unitframes.apbar.b)
		WindowSetShowing("yCustomcolorsliders_Alpha", false)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderR" , yakuiVar.Colors.unitframes.apbar.r/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderG" , yakuiVar.Colors.unitframes.apbar.g/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_SliderB" , yakuiVar.Colors.unitframes.apbar.b/256)
		WindowSetShowing("yCustomcolorsliders_SliderA" , false)
		LabelSetText("yCustomcolorsliders_BGRed", L"Red: "..yakuiVar.Colors.unitframes.careerbar.r)
		LabelSetText("yCustomcolorsliders_BGGreen", L"Green: "..yakuiVar.Colors.unitframes.careerbar.g)
		LabelSetText("yCustomcolorsliders_BGBlue", L"Blue: "..yakuiVar.Colors.unitframes.careerbar.b)
		WindowSetShowing("yCustomcolorsliders_BGAlpha", false)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderR" , yakuiVar.Colors.unitframes.careerbar.r/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderG" , yakuiVar.Colors.unitframes.careerbar.g/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_BGSliderB" , yakuiVar.Colors.unitframes.careerbar.b/256)
		WindowSetShowing("yCustomcolorsliders_BGSliderA" , false)
		LabelSetText("yCustomcolorsliders_FXRed", L"Red: "..yakuiVar.Colors.unitframes.castbar.r)
		LabelSetText("yCustomcolorsliders_FXGreen", L"Green: "..yakuiVar.Colors.unitframes.castbar.g)
		LabelSetText("yCustomcolorsliders_FXBlue", L"Blue: "..yakuiVar.Colors.unitframes.castbar.b)
		WindowSetShowing("yCustomcolorsliders_FXAlpha", false)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderR" , yakuiVar.Colors.unitframes.castbar.r/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderG" , yakuiVar.Colors.unitframes.castbar.g/256)
		SliderBarSetCurrentPosition( "yCustomcolorsliders_FXSliderB" , yakuiVar.Colors.unitframes.castbar.b/256)
		WindowSetShowing( "yCustomcolorsliders_FXSliderA" , false)
		WindowSetShowing("yCustomcolorsliders_AlphaToggleLabel", false)
		WindowSetShowing("yCustomcolorsliders_AlphaToggle", false)	
	end
	if yakuiVar.Colors.col == "Preset1" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 1)
	elseif yakuiVar.Colors.col == "Preset2" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 2)
	elseif yakuiVar.Colors.col == "Preset3" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 3)
	elseif yakuiVar.Colors.col == "Preset4" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 4)
	elseif yakuiVar.Colors.col == "Preset5" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 5)
	elseif yakuiVar.Colors.col == "Preset6" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 6)
	else ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_Preset", 7)
	end
	if yakuiVar.Panel.tex == "Set1" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_TextureCombo", 1)
	elseif yakuiVar.Panel.tex == "Set2" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_TextureCombo", 2)
	elseif yakuiVar.Panel.tex == "Set3" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_TextureCombo", 3)
	elseif yakuiVar.Panel.tex == "Set4" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_TextureCombo", 4)
	elseif yakuiVar.Panel.tex == "Set5" then ComboBoxSetSelectedMenuItem ("yCustomcolorsliders_TextureCombo", 5)
	end
end

local function colorslider(sel, pos)
	if configside == 1 then
		yakuiVar.Colors["colprint"..sel] = math.floor(255 * pos + 0.5)
	elseif configside == 2 then
		yakuiVar.Colors.unitframes.player[sel]  = math.floor(255 * pos + 0.5)
	elseif configside == 3 then
		yakuiVar.Colors.unitframes.apbar[sel]  = math.floor(255 * pos + 0.5)
	end
	for i = 1,6 do WindowSetShowing("yconfigcontextmenucolorselect"..i.."Check", false) end
	yakuiVar.Colors.col = "Custom"
	yakui.color()
	yakuiconfig.colorsliderUpdate()
end
function yakuiconfig.colorsliderR(pos) colorslider("r", pos) end
function yakuiconfig.colorsliderG(pos) colorslider("g", pos) end
function yakuiconfig.colorsliderB(pos) colorslider("b", pos) end

local function bgcolorslider(sel, pos)
	if configside == 1 then
		yakuiVar.Colors.background[sel] = math.floor(255 * pos + 0.5)
	elseif configside == 2 then
		yakuiVar.Colors.unitframes.hostile[sel] = math.floor(255 * pos + 0.5)
	elseif configside == 3 then
		yakuiVar.Colors.unitframes.careerbar[sel] = math.floor(255 * pos + 0.5)
	end
	for i = 1,6 do WindowSetShowing("yconfigcontextmenucolorselect"..i.."Check", false) end
	yakuiVar.Colors.col = "Custom"
	yakuiconfig.colorsliderUpdate()
	yakui.color()
end
function yakuiconfig.bgcolorsliderR(pos) bgcolorslider("r", pos) end
function yakuiconfig.bgcolorsliderG(pos) bgcolorslider("g", pos) end
function yakuiconfig.bgcolorsliderB(pos) bgcolorslider("b", pos) end

local function fxcolorslider(sel, pos)
	if configside == 1 then
		yakuiVar.Colors.fx[sel] = math.floor(255 * pos + 0.5)
	elseif configside == 2 then
		yakuiVar.Colors.unitframes.friendly[sel] = math.floor(255 * pos + 0.5)
	elseif configside == 3 then
		yakuiVar.Colors.unitframes.castbar[sel] = math.floor(255 * pos + 0.5)
	end
	for i = 1,6 do WindowSetShowing("yconfigcontextmenucolorselect"..i.."Check", false) end
	yakuiVar.Colors.col = "Custom"
	yakuiconfig.colorsliderUpdate()
	yakui.color()
end
function yakuiconfig.fxcolorsliderR(pos) fxcolorslider ("r", pos) end
function yakuiconfig.fxcolorsliderG(pos) fxcolorslider ("g", pos) end
function yakuiconfig.fxcolorsliderB(pos) fxcolorslider ("b", pos) end
