-- yakuiSkin.lua build20100524

yakuiSkin = {}

function yakuiSkin.initialize()
	local mainmenuitems = 	{	"MainMenuWindowLogOutItem", "MainMenuWindowExitGameItem", "MainMenuWindowUserSettingsItem", "MainMenuWindowKeyMappingItem", "MainMenuWindowCustomizeInterfaceItem", 
							"MainMenuWindowMacrosItem" 
						}
	yakuiSkin.cornerimgs = 		{	"CharacterWindowCornerImage","AbilitiesWindowCornerIcon","EA_Window_InteractionCoreTrainingCornerImage","EA_Window_BackpackCornerImage",
							"EA_Window_OpenPartySocialImage", "EA_Window_InteractionRenownTrainingCornerImage", "EA_Window_InteractionTomeTrainingCornerImage", 
							"EA_Window_InteractionStoreCornerImage", "GuildWindowCornerImage", "EA_Window_InteractionSpecialtyTrainingCornerImage", "ClosetGoblinCharacterWindowCornerImage",
							"alertModWindowImage", "SettingsWindowTabbedWindowImage", "MailWindowCornerImage", "BankWindowCornerImage", "GuildVaultWindowCornerImage", "MailWindowTabMessageCornerImage",
							"RVMOD_ManagerWindowCornerImage", "EA_Window_HelpCornerImage"
						  }
	yakuiSkin.bgtex = 	{	"CharacterWindowBackgroundBackground", "CharacterWindowImage", "CharacterWindowContentsStatsFrameBackgroundImage", "CharacterWindowContentsStatsFrame", "CharacterWindowContentsBGImageLabel",
							"CharacterWindowContentsBackgroundImageLabel", "CharacterWindowContentsBG", "AbilitiesWindowBackgroundTabBackground", "AbilitiesWindowBackgroundTabBar",
							"AbilitiesWindowBackgroundStoneTablet", "AbilitiesWindowModeTab1ActiveImage", "AbilitiesWindowModeTab1InactiveImage", "AbilitiesWindowModeTab2ActiveImage",
							"AbilitiesWindowModeTab2InactiveImage", "AbilitiesWindowModeTab3ActiveImage", "AbilitiesWindowModeTab3InactiveImage", "AbilitiesWindowModeTab4ActiveImage",
							"AbilitiesWindowModeTab4InactiveImage", "AbilitiesWindowTopBookend", "AbilitiesWindowBottomBookend", "ySkinBackgroundColorBackgroundBackground", "yCustomcolorslidersBackgroundBackground",
							"EA_Window_InteractionCoreTrainingFooterSeparator", "EA_Window_InteractionCoreTrainingBackgroundBackground", "EA_Window_InteractionSpecialtyTrainingBackgroundBackground",
							"EA_Window_InteractionSpecialtyTrainingTabs", "EA_Window_InteractionSpecialtyTrainingPicture", "EA_Window_InteractionSpecialtyTrainingFooterSeparator", 
							"EA_Window_InteractionRenownTrainingBackgroundBackground", "EA_Window_InteractionRenownTrainingButtonSeparator", "EA_Window_InteractionRenownTrainingMainScrollChildPicture",
							"EA_Window_InteractionRenownTrainingFooterSeparator", "EA_Window_InteractionTomeTrainingBackgroundBackground", "EA_Window_InteractionTomeTrainingButtonSeparator",
							"EA_Window_InteractionTomeTrainingFooterSeparator", "EA_Window_BackpackBackgroundBackground", "EA_Window_OpenPartyBackgroundBackground", "EA_Window_OpenPartyLootRollOptionsUsableEquipmentBackground",
							"EA_Window_OpenPartyLootRollOptionsUnusableEquipmentBackground", "EA_Window_OpenPartyLootRollOptionsCraftingBackground", "EA_Window_OpenPartyLootRollOptionsCurrencyBackground",
							"EA_Window_OpenPartyLootRollOptionsPotionsBackground", "EA_Window_OpenPartyLootRollOptionsTalismansBackground", "EA_Window_OpenPartyManageConvertToWarbandBackground",
							"EA_Window_OpenPartyManageHeaderSeparator", "EA_Window_OpenPartyManageLegendLegendSeparator", "EA_Window_OpenPartyManageVertSeparator", "EA_Window_OpenPartyWorldButtonBackground",
							"EA_Window_OpenPartyWorldSeparator", "EA_Window_OpenPartyNearbyLegendLegendSeparator", "EA_Window_OpenPartyNearbySeparator", "EA_Window_OpenPartyNearbyButtonBackground", 
							"DebugWindowBackgroundBackground", "DebugWindowButtonBackground", "EA_Window_InteractionStoreBackgroundBackground", "EA_Window_InteractionStoreMiddleDivider",
							"EA_Window_InteractionStoreBottomDivider", "EA_Window_InteractionStoreButtonBackground", "SocialWindowBuddyListBackgroundBackground", "SocialWindowBuddyListSeparator",
							"SocialWindowBuddyListTopBackgroundFill", "GuildWindowBackgroundBackground", "GWProfileMOTDSeperator", "GWProfileOverviewSeperator", "GWProfileAboutUsVertSeperator",
							"GWCalendarAppointmentSeperator", "GWCalendarVertSeparator", "GWRosterButtonBackground", "GWRewardsListHeaderBackground", "GWRewardsBannerPicture", "GWAdminTitlesFrame",
							"GWAdminButtonBackground", "GWAdminEditTitlesFrame", "GWRecruitSearchHorizRule", "GWRecruitSearchVertRule", "GWRecruitProfileHorizRule", "GuildXPBarWindowBackground",
							"ClosetGoblinCharacterWindowContentsBG", "ClosetGoblinCharacterWindowBackgroundBackground", "DascoreWin1WindowBarBackground", "DascoreWin1WindowBackground", "ScenarioStatsWindowBackground",
							"yDyeBGBackground", "yDyeBGFrame", "CaVESWindowBackgroundBackground", "CaVESWindowButtonBackground", "CaVESWindowOptionsWindowBackground", "CaVESWindowBottomSeperator", "alertModBackgroundBackground",
							"alertModOptionsTopSeparator", "alertModOptionsButtonBackground", "PQLootWindowBackgroundBackground", "PQLootWindowBottomDivider1", "PQLootWindowBottomDivider2", 
							"PQLootWindowTransitionWindowBackground", "EA_Window_ScenarioLobbyBackgroundBackground", "EA_Window_ScenarioLobbyBackbroundFrame", "EA_Window_ScenarioLobbyDivider1", "EA_Window_ScenarioLobbyDivider2",
							"EA_Window_ScenarioLobbyDivider3", "EA_Window_ScenarioLobbyButtonBackground", "TidyQueueBackgroundBackground", "EA_Window_CityCaptureJoinPromptWindowBoxBackgroundBackground", 
							"EA_Window_CityCaptureJoinPromptWindowBoxBackgroundFrame", "EA_Window_CityCaptureJoinPromptWindowBoxTitleBar", "EA_Window_ScenarioJoinPromptBoxBackgroundBackground", "EA_Window_ScenarioJoinPromptBoxBackgroundFrame", 
							"EA_Window_ScenarioJoinPromptBoxTitleBar", "ScenarioSummaryWindowBackground", "ScenarioGroupWindowBackgroundBackground", "ScenarioGroupWindowGroup1BG", "ScenarioGroupWindowGroup2BG", "ScenarioGroupWindowGroup3BG",
							"ScenarioGroupWindowGroup4BG", "ScenarioGroupWindowGroup5BG", "ScenarioGroupWindowGroup6BG", "ScenarioGroupWindowGroup1NameBG", "ScenarioGroupWindowGroup2NameBG", "ScenarioGroupWindowGroup3NameBG",
							"ScenarioGroupWindowGroup4NameBG", "ScenarioGroupWindowGroup5NameBG", "ScenarioGroupWindowGroup6NameBG", "ScenarioGroupWindowMiddleBarBG", "ScenarioGroupWindowBottomBarBG", 
							"EA_Window_ScenarioStartingBackgroundBackground", "ItemEnhancementWindowBackgroundBackground", "ItemEnhancementWindowSeperator", "ItemEnhancementWindowButtonBackground",
							"ItemStackingWindowWindowBackgroundBackground", "TwoButtonDlgBoxBackgroundBackground", "EA_Window_MacroBackgroundBackground", "KeyMappingWindowBackgroundBackground", "SettingsWindowTabbedBackgroundBackground",
							"SettingsWindowTabbedButtonBackground", "SWTabGeneralContentsScrollChildSettingsGamePlayBackground", "SWTabGeneralContentsScrollChildSettingsHelpTipsBackground", 
							"SWTabGeneralContentsScrollChildSettingsDialogWarningsBackground", "SWTabGeneralContentsScrollChildSettingsCameraBackground", "SWTabVideoContentsScrollChildSettingsResolutionBackground",
							"SWTabVideoContentsScrollChildSettingsResolutionBackground", "SWTabVideoContentsScrollChildSettingsGlobalUIScaleBackground", "SWTabVideoContentsScrollChildSettingsPerformanceBackground",
							"SWTabVideoContentsScrollChildSettingsColorBackground", "SWTabSoundContentsScrollChildSettingsSoundBackground", "SWTabChatContentsScrollChildSettingsChatBackground", 
							"SWTabChatContentsScrollChildSettingsChatBubblesBackground", "SWTabTargettingContentsScrollChildSettingsHealthbarsBackground", "SWTabTargettingContentsScrollChildSettingsNamesBackground",
							"SWTabTargettingContentsScrollChildSettingsReticlesBackground", "SWTabInterfaceContentsScrollChildSettingsSubSystemsBackground", "SWTabInterfaceContentsScrollChildSettingsActionBarsBackground", 
							"SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsBackground", "DyeMerchantButtonsBackground", "DyeMerchantDivider", "DyeMerchantBackground", "CharacterWindowContentsInstructionFrame",
							"CharacterWindowContentsInstructionBackground", "AuctionHouseWindowBackgroundBackground", "AuctionHouseWindowCreateAuctionTitleBar", "AuctionHouseWindowCreateAuctionBackground",
							"AuctionHouseWindowCreateAuctionBottomDivider2", "AuctionHouseWindowCreateAuctionButtonBackground", "AuctionHouseWindowCreateSearchTitleBar", "AuctionHouseWindowSearchResultsTitleBar",
							"AuctionHouseWindowCreateSearchBackground", "AuctionHouseWindowCreateSearchBottomDivider2", "AuctionHouseWindowCreateSearchButtonBackground", "AuctionHouseWindowSearchResultsBackground", 
							"AuctionHouseWindowSearchResultsBottomDivider2", "AuctionHouseWindowSearchResultsButtonBackground", "MailWindowBackgroundBackground", "MailWindowTabInboxDisplaySeperator", 
							"MailWindowTabInboxBarBackground", "MailWindowTabSendDisplaySeperatorTop", "MailWindowTabSendBackground", "MailWindowTabSendDisplaySeperatorMiddle", "MailWindowTabSendDisplaySeperatorBottom",
							"MailWindowTabSendBarBackground", "MailWindowTabAuctionDisplaySeperator", "MailWindowTabAuctionBarBackground", "BankWindowBackgroundBackground", "GuildVaultWindowBackgroundBackground", 
							"GuildVaultWindowBarBackground", "MailWindowTabMessageBackgroundBackground", "MailWindowTabMessageSeparator1", "MailWindowTabMessageDisplaySeperator", "MailWindowTabMessageAttachmentBackground", 
							"MailWindowTabMessageBarBackground", "GWAllianceInAllianceButtonBackground", "YFirstStepsBackgroundBackground", "EnemyIntercomDialogBackgroundBackground", "EnemyIntercomDialogDivider1",
							"EnemyIntercomDialogButtonBackground", "WARCommander_ConfigWindowBackgroundBackground", "SocialWindowBackgroundBackground", "SocialWindowTabSearchTopHalfBorder", 
							"SocialWindowTabSearchOptionsBackground", "SocialWindowTabFriendsTopHalfBorder", "SocialWindowTabFriendsSearchBackground", "SocialWindowTabIgnoreTopHalfBorder", "SocialWindowTabIgnoreSearchBackground",
							"SocialWindowTabOptionsTopHalfBorder", "SocialWindowTabOptionsAFKBackground", "SocialWindowTabOptionsOptionsBackground", "EnemyIntercomJoinDialogBackgroundBackground",
							"EnemyIntercomJoinDialogButtonBackground", "EA_Window_HelpBackgroundBackground", "EA_Window_HelpLeftSeperator", "EA_Window_HelpRightSeperator", "EA_Window_HelpLeftWindowSeperator",
							"EA_Window_HelpCenterWindowSeperator", "EA_Window_HelpCenterWindowSeperator", "EA_Window_HelpRightWindowSeperator", "UiModVersionMismatchWindowBackgroundBackground", "UiModVersionMismatchWindowModListSeperator", "UiModVersionMismatchWindowButtonBackground", "UiModWindowBackgroundBackground",
							"UiModWindowTopSeperator", "UiModWindowVerticalSeperator", "UiModWindowBottomSeperator", "UiModWindowButtonBackground", "UiModWindowModDetailsScrollChildNameBackground",
							"EA_Window_CustomizePerformanceBackgroundBackground", "EA_Window_CustomizePerformanceButtonBackground", "EA_Window_CustomizePerformanceOptionsTopSeparator", 
							"EA_Window_CustomizePerformanceEnvironmentSection", "EA_Window_CustomizePerformanceEffectsSection", "EA_Window_CustomizePerformanceLightingSection", "EA_Window_CustomizePerformanceMiscSection",
							"EA_Window_CustomizePerformanceGPUSection", "TidyChatCopyBackgroundBackground", "TidyChatCopyButtonBackground", --[["SOR.Options.BackgroundBackground", "SOR.Options.ButtonBackground",
							"SOR.Options.TitleBar"]]--
						  }
	---------------syntax: origninalwindow[v1], topleftcorner[v2], topxoffset[v3], topyoffset[v4], bottomrightcorner[v5], bottomxoffset[v6], bottomyoffset[v7], hastitlebar[v8], hasborder[v9]
	yakuiSkin.newbackgrounds =  { {"AbilitiesWindow", "AbilitiesWindowBackgroundTabBackground",0,-4,"AbilitiesWindowBackgroundStoneTablet",0,0,true,true},
						{"CharacterWindow", "CharacterWindowBackgroundBackground",8,-4,"CharacterWindowBackgroundBackground",-8,-8,true,true},
						{"ySkinBackgroundColor", "ySkinBackgroundColorBackgroundBackground",8,2,"ySkinBackgroundColorBackgroundBackground",-8,-8,true,true},
						{"yCustomcolorsliders", "yCustomcolorslidersBackgroundBackground",8,2,"yCustomcolorslidersBackgroundBackground",-8,-8,true,true},
						{"EA_Window_InteractionCoreTraining", "EA_Window_InteractionCoreTrainingBackgroundBackground",8,-4,"EA_Window_InteractionCoreTrainingBackgroundBackground",-4,-8,true,true},
						{"EA_Window_InteractionSpecialtyTraining", "EA_Window_InteractionSpecialtyTrainingBackgroundBackground",8,-4,"EA_Window_InteractionSpecialtyTrainingBackgroundBackground",-8,-8,true,true},
						{"EA_Window_InteractionRenownTraining", "EA_Window_InteractionRenownTrainingBackgroundBackground",8,-4,"EA_Window_InteractionRenownTrainingBackgroundBackground",-8,-8,true,true},
						{"EA_Window_InteractionTomeTraining", "EA_Window_InteractionTomeTrainingBackgroundBackground",8,-4,"EA_Window_InteractionTomeTrainingBackgroundBackground",-8,-8,true,true},
						{"EA_Window_Backpack", "EA_Window_BackpackBackgroundBackground",8,-4,"EA_Window_BackpackBackgroundBackground",-8,-8,true,true},
						{"EA_Window_OpenParty", "EA_Window_OpenPartyBackgroundBackground",8,-4,"EA_Window_OpenPartyBackgroundBackground",-8,-8,true,true},
						{"DebugWindow", "DebugWindowBackgroundBackground",8,-4,"DebugWindowBackgroundBackground",-8,-8,true,true},
						{"EA_Window_InteractionStore", "EA_Window_InteractionStoreBackgroundBackground",8,-4,"EA_Window_InteractionStoreBackgroundBackground",-8,-8,true,true},
						{"SocialWindowBuddyList", "SocialWindowBuddyListBackgroundBackground",8,12,"SocialWindowBuddyListBackgroundBackground",0,-8,false,true},
						{"GuildWindow", "GuildWindowBackgroundBackground",8,-4,"GuildWindowBackgroundBackground",-8,-8,true,true},
						{"MainMenuWindow", "MainMenuWindowBackgroundBackground",0,-4,"MainMenuWindowBackgroundBackground",0,0,true,true},
						{"ClosetGoblinCharacterWindow", "ClosetGoblinCharacterWindowBackgroundBackground", 8,-4, "ClosetGoblinCharacterWindowBackgroundBackground",-8,-8, true,true},
						{"DascoreWin1Window", "DascoreWin1WindowBackgroundBackground", 8,-2, "DascoreWin1WindowBackgroundBackground",0,-16, true,true},
						{"ScenarioStatsWindow", "ScenarioStatsWindowBackground", 0,32, "ScenarioStatsWindowBackground",0,0,false,false},
						{"yDye", "yDyeBGBackground", 0,28, "yDyeBGBackground", -8,-4, true, true},
						{"CaVESWindow", "CaVESWindowBackground", 8,0, "CaVESWindowBackground", -8,-8, true, true},
						{"alertMod", "alertModBackground", 8,2, "alertModBackground", -8,0, true, true},
						{"PQLootWindow", "PQLootWindowBackgroundBackground", 8, -4, "PQLootWindowBackgroundBackground", -8,-8, true, true},
						{"EA_Window_ScenarioLobby", "EA_Window_ScenarioLobbyBackgroundBackground", 8,-4, "EA_Window_ScenarioLobbyBackgroundBackground", -8,-8, true, true},
						{"TidyQueue", "TidyQueueBackgroundBackground", 8,-4, "TidyQueueBackgroundBackground", -8,-8, false, true},
						{"EA_Window_ScenarioJoinPrompt", "EA_Window_ScenarioJoinPromptBoxBackgroundBackground", 8,-4, "EA_Window_ScenarioJoinPromptBoxBackgroundBackground", -8,-8,false, true},
						{"EA_Window_CityCaptureJoinPromptWindow", "EA_Window_CityCaptureJoinPromptWindowBoxBackgroundBackground", 8,-4, "EA_Window_CityCaptureJoinPromptWindowBoxBackgroundBackground", -8,-8, false, true},
						{"ScenarioSummaryWindow", "ScenarioSummaryWindowBackground", 8,32, "ScenarioSummaryWindowBackground", -8,-8, false, false},
						{"ScenarioGroupWindow", "ScenarioGroupWindowBackgroundBackground", 8,-4, "ScenarioGroupWindowBackgroundBackground", -8,-8, true, true},
						{"EA_Window_ScenarioStarting", "EA_Window_ScenarioStartingBackgroundBackground", 8,-4, "EA_Window_ScenarioStartingBackgroundBackground", -8,-8, false, true},
						{"ItemEnhancementWindow", "ItemEnhancementWindowBackgroundBackground", 8,-4, "ItemEnhancementWindowBackgroundBackground", -8,-8, true, true},
						{"ItemStackingWindow", "ItemStackingWindowWindowBackgroundBackground", 8,-4, "ItemStackingWindowWindowBackgroundBackground", -8,-8, true, true},
						{"TwoButtonDlg1Box", "TwoButtonDlg1BoxBackgroundBackground", 8,-4, "TwoButtonDlg1BoxBackgroundBackground", -8,-8, false, true},
						{"EA_Window_Macro", "EA_Window_MacroBackgroundBackground", 8,-4, "EA_Window_MacroBackgroundBackground", -8,-8, true, true},
						{"KeyMappingWindow", "KeyMappingWindowBackgroundBackground", 8,-4, "KeyMappingWindowBackgroundBackground", -8,-8, true, true},
						{"SettingsWindowTabbed", "SettingsWindowTabbedBackgroundBackground", 8,-4, "SettingsWindowTabbedBackgroundBackground", -8,-8, true, true},
						{"AuctionHouseWindow", "AuctionHouseWindowBackgroundBackground", 8,-4, "AuctionHouseWindowBackgroundBackground", -8,-8, true, true},
						{"MailWindow", "MailWindowBackgroundBackground", 8,-4, "MailWindowBackgroundBackground", -8,-8, true, true},
						{"MailWindowTabMessage", "MailWindowTabMessageBackgroundBackground", 8,-4, "MailWindowTabMessageBackgroundBackground", -8,-8, true, true},
						{"BankWindow", "BankWindowBackgroundBackground", 8,-4, "BankWindowBackgroundBackground", -8,-8, true, true},
						{"GuildVaultWindow", "GuildVaultWindowBackgroundBackground", 8,-4, "GuildVaultWindowBackgroundBackground", -8,-8, true, true},
						{"RVMOD_ManagerWindow", "RVMOD_ManagerWindowBackgroundBackground", 8,-4, "RVMOD_ManagerWindowBackgroundBackground", -8,-8, true, true},
						{"EnemyIntercomDialog", "EnemyIntercomDialogBackgroundBackground", 8,-4, "EnemyIntercomDialogBackgroundBackground", -8,-8, true, true},
						--[[{"EnemyChooseChannelDialog", "EnemyChooseChannelDialogBackgroundBackground", 8,-4, "EnemyIntercomChooseChannelBackgroundBackground", -8,-8, true, true},]]--
						{"WARCommander_ConfigWindow", "WARCommander_ConfigWindowBackgroundBackground", 8,-4, "WARCommander_ConfigWindowBackgroundBackground", -8,-8, true, true},
						{"SocialWindow", "SocialWindowBackgroundBackground", 8,-4, "SocialWindowBackgroundBackground", -8,0, true, true},
						{"EnemyIntercomJoinDialog", "EnemyIntercomJoinDialogBackgroundBackground", 8,-4, "EnemyIntercomJoinDialogBackgroundBackground", -8,-8, true, true},
						{"EA_Window_Help", "EA_Window_HelpBackgroundBackground", 8,-4, "EA_Window_HelpBackgroundBackground", -8,-8, true, true},
						--{"SOR.Options", "SOR.Options.BackgroundBackground", 8,-4, "SOR.Options.BackgroundBackground", -8,-8, false, true},
						{"UiModVersionMismatchWindow", "UiModVersionMismatchWindowBackgroundBackground", 8,-4, "UiModVersionMismatchWindowBackgroundBackground", -8,-8, true, false},
						{"UiModWindow", "UiModWindowBackgroundBackground", 8,-4, "UiModWindowBackgroundBackground", -8,-8, true, true},
						{"EA_Window_CustomizePerformance", "EA_Window_CustomizePerformanceBackgroundBackground", 8,-4, "EA_Window_CustomizePerformanceBackgroundBackground", -8,-8, true, true},
						{"TidyChatCopy", "TidyChatCopyBackgroundBackground", 8,-4, "TidyChatCopyBackgroundBackground", -8,-8, true, true}
					  }
	
	-- fonts
	local fontssmallbold = {"EA_Window_PublicQuestTrackerObjective1Quest1TimerValue", "EA_Window_PublicQuestTrackerObjective1Quest2TimerValue", "EA_Window_PublicQuestTrackerObjective1Quest3TimerValue"
							, "EA_Window_PublicQuestTrackerObjective2Quest1TimerValue", "EA_Window_PublicQuestTrackerObjective2Quest2TimerValue", "EA_Window_PublicQuestTrackerObjective2Quest3TimerValue"}
	local fontsmediumbold =	{	"EA_Window_PublicQuestTrackerLocationPairingLabel", "EA_Window_PublicQuestTrackerLocationChapterLabel", "CharacterWindowTimeoutsTitleLabel",
							"CharacterWindowBragsHeader", "SocialWindowBuddyListSectionTitle", "GWProfilePersonalStatisticsTitle", "CharacterWindowRvRStatsTitle", "EA_Window_CityTrackerOverviewDescription",
							"EA_Window_CityTrackerCaptureNameName", 
							"EA_Window_CityTrackerObjective1Quest1TimerValue", "EA_Window_CityTrackerObejctive1Quest1Label", "EA_Window_CityTrackerMainQuest1DataCondition2Counter", "EA_Window_CityTrackerMainQuest1DataCondition2Name",
							"EA_Window_CityTrackerMainQuest1DataCondition3Counter", "EA_Window_CityTrackerMainQuest1DataCondition3Name", "EA_Window_CityTrackerMainQuest1DataCondition3Counter", "EA_Window_CityTrackerMainQuest1DataCondition3Name",
							"EA_Window_CityTrackerMainQuest1DataCondition5Counter", "EA_Window_CityTrackerMainQuest1DataCondition5Name",
							"EA_Window_CityTrackerMainQuest2DataCondition1Counter", "EA_Window_CityTrackerMainQuest2DataCondition1Name", "EA_Window_CityTrackerMainQuest2DataCondition2Counter", "EA_Window_CityTrackerMainQuest2DataCondition2Name",
							"EA_Window_CityTrackerMainQuest2DataCondition3Counter", "EA_Window_CityTrackerMainQuest2DataCondition3Name", "EA_Window_CityTrackerMainQuest2DataCondition3Counter", "EA_Window_CityTrackerMainQuest2DataCondition3Name",
							"EA_Window_CityTrackerMainQuest2DataCondition5Counter", "EA_Window_CityTrackerMainQuest2DataCondition5Name", "PQLootWindowTimeText", "PQLootWindowPlayerResultsText", "PQLootWindowTransitionWindowText",
							"EA_Window_PublicQuestTrackerObjective1Quest1DataCondition1Counter", "EA_Window_PublicQuestTrackerObjective1Quest1DataCondition1Name",
							"EA_Window_PublicQuestTrackerObjective1Quest1DataCondition2Counter", "EA_Window_PublicQuestTrackerObjective1Quest1DataCondition2Name",
							"EA_Window_PublicQuestTrackerObjective1Quest1DataCondition3Counter", "EA_Window_PublicQuestTrackerObjective1Quest1DataCondition3Name",
							"EA_Window_PublicQuestTrackerObjective1Quest1DataCondition4Counter", "EA_Window_PublicQuestTrackerObjective1Quest1DataCondition4Name",
							"EA_Window_PublicQuestTrackerObjective1Quest1DataCondition5Counter", "EA_Window_PublicQuestTrackerObjective1Quest1DataCondition5Name",
							"EA_Window_PublicQuestTrackerObjective1Quest2DataCondition1Counter", "EA_Window_PublicQuestTrackerObjective1Quest2DataCondition1Name",
							"EA_Window_PublicQuestTrackerObjective1Quest2DataCondition2Counter", "EA_Window_PublicQuestTrackerObjective1Quest2DataCondition2Name",
							"EA_Window_PublicQuestTrackerObjective1Quest2DataCondition3Counter", "EA_Window_PublicQuestTrackerObjective1Quest2DataCondition3Name",
							"EA_Window_PublicQuestTrackerObjective1Quest2DataCondition4Counter", "EA_Window_PublicQuestTrackerObjective1Quest2DataCondition4Name",
							"EA_Window_PublicQuestTrackerObjective1Quest2DataCondition5Counter", "EA_Window_PublicQuestTrackerObjective1Quest2DataCondition5Name",							
							"EA_Window_PublicQuestTrackerObjective1Quest3DataCondition1Counter", "EA_Window_PublicQuestTrackerObjective1Quest3DataCondition1Name",
							"EA_Window_PublicQuestTrackerObjective1Quest3DataCondition2Counter", "EA_Window_PublicQuestTrackerObjective1Quest3DataCondition2Name",
							"EA_Window_PublicQuestTrackerObjective1Quest3DataCondition3Counter", "EA_Window_PublicQuestTrackerObjective1Quest3DataCondition3Name",
							"EA_Window_PublicQuestTrackerObjective1Quest3DataCondition4Counter", "EA_Window_PublicQuestTrackerObjective1Quest3DataCondition4Name",
							"EA_Window_PublicQuestTrackerObjective1Quest3DataCondition5Counter", "EA_Window_PublicQuestTrackerObjective1Quest3DataCondition5Name",
							"EA_Window_PublicQuestTrackerObjective2Quest1DataCondition1Counter", "EA_Window_PublicQuestTrackerObjective2Quest1DataCondition1Name",
							"EA_Window_PublicQuestTrackerObjective2Quest1DataCondition2Counter", "EA_Window_PublicQuestTrackerObjective2Quest1DataCondition2Name",
							"EA_Window_PublicQuestTrackerObjective2Quest1DataCondition3Counter", "EA_Window_PublicQuestTrackerObjective2Quest1DataCondition3Name",
							"EA_Window_PublicQuestTrackerObjective2Quest1DataCondition4Counter", "EA_Window_PublicQuestTrackerObjective2Quest1DataCondition4Name",
							"EA_Window_PublicQuestTrackerObjective2Quest1DataCondition5Counter", "EA_Window_PublicQuestTrackerObjective2Quest1DataCondition5Name",
							"EA_Window_PublicQuestTrackerObjective2Quest2DataCondition1Counter", "EA_Window_PublicQuestTrackerObjective2Quest2DataCondition1Name",
							"EA_Window_PublicQuestTrackerObjective2Quest2DataCondition2Counter", "EA_Window_PublicQuestTrackerObjective2Quest2DataCondition2Name",
							"EA_Window_PublicQuestTrackerObjective2Quest2DataCondition3Counter", "EA_Window_PublicQuestTrackerObjective2Quest2DataCondition3Name",
							"EA_Window_PublicQuestTrackerObjective2Quest2DataCondition4Counter", "EA_Window_PublicQuestTrackerObjective2Quest2DataCondition4Name",
							"EA_Window_PublicQuestTrackerObjective2Quest2DataCondition5Counter", "EA_Window_PublicQuestTrackerObjective2Quest2DataCondition5Name",							
							"EA_Window_PublicQuestTrackerObjective2Quest3DataCondition1Counter", "EA_Window_PublicQuestTrackerObjective2Quest3DataCondition1Name",
							"EA_Window_PublicQuestTrackerObjective2Quest3DataCondition2Counter", "EA_Window_PublicQuestTrackerObjective2Quest3DataCondition2Name",
							"EA_Window_PublicQuestTrackerObjective2Quest3DataCondition3Counter", "EA_Window_PublicQuestTrackerObjective2Quest3DataCondition3Name",
							"EA_Window_PublicQuestTrackerObjective2Quest3DataCondition4Counter", "EA_Window_PublicQuestTrackerObjective2Quest3DataCondition4Name",
							"EA_Window_PublicQuestTrackerObjective2Quest3DataCondition5Counter", "EA_Window_PublicQuestTrackerObjective2Quest3DataCondition5Name",
							"EA_Window_PublicQuestResultsDataTimerLabel", "EA_Window_PublicQuestResultsDataTimerText", 
							"EA_Window_ScenarioStartingTimer", "EA_Window_ScenarioStartingInCombatText", "ItemEnhancementWindowItemName", "KeyMappingWindowActionListLabel", 
							"KeyMappingWindowActionListBoundKeysLabel", "EA_Window_KeepObjectiveTrackerDuringActionTimerValue", "EA_Window_KeepObjectiveTrackerDuringActionQuest",
							"SWTabInterfaceContentsScrollChildSettingsActionBarsCustomizeTitle", "AuctionHouseWindowCreateAuctionTitleBarText", "AuctionHouseWindowCreateSearchTitleBarText",
							"AuctionHouseWindowSearchResultsTitleBarText", "EA_Window_BattlefieldObjectiveTrackerActionLabel", "EA_Window_BattlefieldObjectiveTrackerLabel",
							"EA_Window_BattlefieldObjectiveTrackerTimerValue", 
							"EA_Window_BattlefieldObjectiveTrackerMainQuest1Label", "EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCounter", "EA_Window_BattlefieldObjectiveTrackerMainQuest1DataName",
							"EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCondition1Counter", "EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCondition1Name", 
							"EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCondition2Counter", "EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCondition2Name",
							"EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCondition3Counter", "EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCondition3Name",
							"EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCondition4Counter", "EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCondition4Name",
							"EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCondition5Counter", "EA_Window_BattlefieldObjectiveTrackerMainQuest1DataCondition5Name",
							"EA_Window_BattlefieldObjectiveTrackerLockedLocation", "EA_Window_BattlefieldObjectiveTrackerLockedText", "EnemyIntercomDialogMessage",
							"SocialWindowTabOptionsOptionsLabel", "SocialWindowTabOptionsAFKLabel"
						}
	local fontsmedium		=	{	"CharacterWindowContentsImageTitleLabel", "CharacterWindowBragsFooter", "AbilitiesWindowPageNumber", "EA_Window_InteractionCoreTrainingHint",
							"EA_Window_InteractionCoreTrainingPurseLabel", "EA_Window_InteractionCoreTrainingNoAbiltitiesText", "EA_Window_InteractionCoreTrainingAvailabilityFilterLabel",
							"EA_Window_InteractionSpecialtyTrainingDisplayPaneMasteryPointPurse", "EA_Window_BackpackCheckBoxLabel", "EA_Window_OpenPartyLootRollOptionsRarityLabelCommon",
							"EA_Window_OpenPartyLootRollOptionsRarityLabelCommon", "EA_Window_OpenPartyLootRollOptionsRarityLabelUncommon", "EA_Window_OpenPartyLootRollOptionsRarityLabelRare",
							"EA_Window_OpenPartyLootRollOptionsRarityLabelVeryRare", "EA_Window_OpenPartyLootRollOptionsRarityLabelArtifact", "EA_Window_OpenPartyLootRollOptionsUsableEquipmentTitle",
							"EA_Window_OpenPartyLootRollOptionsUnusableEquipmentTitle", "EA_Window_OpenPartyLootRollOptionsCraftingTitle", "EA_Window_OpenPartyLootRollOptionsCurrencyTitle",
							"EA_Window_OpenPartyLootRollOptionsPotionsTitle", "EA_Window_OpenPartyLootRollOptionsTalismansTitle", "EA_Window_OpenPartyLootRollOptionsTrashTitle",
							"EA_Window_OpenPartyLootRollOptionsHelpText", "EA_Window_OpenPartyManageLootHeaderText", "EA_Window_OpenPartyManageLootModeTitle", "EA_Window_OpenPartyManageNeedOnUseButtonLabel",
							"EA_Window_OpenPartyManageAutoLootRvRLabel", "EA_Window_OpenPartyManageLootThresholdTitle", "EA_Window_OpenPartyManageMasterLooterTitle",
							"EA_Window_OpenPartyManageLegendLeaderText", "EA_Window_OpenPartyManageLegendLeaderText", "EA_Window_OpenPartyManageLegendAssistantText",
							"EA_Window_OpenPartyManageLegendMainAssistText", "EA_Window_OpenPartyManageLegendMasterLooterText", "EA_Window_OpenPartyWorldHelpText", "EA_Window_OpenPartyWorldLocationSelectLabel",
							"EA_Window_OpenPartyWorldInterestSelectLabel", "EA_Window_OpenPartyWorldSearchableCheckLabel", "EA_Window_OpenPartyWorldHideFullPartiesCheckLabel",
							"EA_Window_OpenPartyNearbyHelpText", "EA_Window_OpenPartyNearbyOpenPartyFlagLabel", "EA_Window_OpenPartyNearbyLegendGuildText", "EA_Window_OpenPartyNearbyLegendAllianceText",
							"EA_Window_OpenPartyNearbyLegendFriendText", "EA_Window_OpenPartyNearbyLegendIgnoredText", "EA_Window_InteractionRenownTrainingPurseLabel", 
							"EA_Window_InteractionRenownTrainingMainScrollChildHint", "EA_Window_InteractionTomeTrainingHint", "EA_Window_InteractionTomeTrainingAvailabilityFilterLabel", 
							"EA_Window_InteractionStoreMoneyAvailableHeader", "EA_Window_InteractionStoreFilterArmorLabel", "EA_Window_InteractionStoreFilterWeaponsLabel", "EA_Window_InteractionStoreFilterMiscLabel",
							"EA_Window_InteractionStoreFilterByUsableLabel", "CharacterWindowRvRStatsLifetimeK", "CharacterWindowRvRStatsLifetimeDB", "CharacterWindowRvRStatsLifetimeD", 
							"CharacterWindowRvRStatsLifetimeRatioKD", "CharacterWindowRvRStatsLifetimeRatioDBD", "CharacterWindowRvRStatsSessionK", "CharacterWindowRvRStatsXPBonus",
							"CharacterWindowRvRStatsRenownBonus", "CharacterWindowRvRStatsClassKills", "CharacterWindowRvRStatsClassA", "CharacterWindowRvRStatsClassB", "CharacterWindowRvRStatsClassC",
							"CharacterWindowRvRStatsClassD", "CharacterWindowRvRStatsClassE", "CharacterWindowRvRStatsClassF", "CharacterWindowRvRStatsClassG", "CharacterWindowRvRStatsClassH",
							"CharacterWindowRvRStatsClassI", "CharacterWindowRvRStatsClassJ", "CharacterWindowRvRStatsClassK", "CharacterWindowRvRStatsClassL", "CaVESWindowIgnoreTitle", 
							"CaVESWindowToggleEventSlotLabel", "CaVESWindowToggleTalismansLabel", "EA_Window_ScenarioLobbyJoinMode1Label", "EA_Window_ScenarioLobbyJoinMode2Label", "EA_Window_ScenarioJoinPromptBoxText", 
							"EA_Window_ScenarioJoinPromptBoxRespondTime", "EA_Window_ScenarioJoinPromptBoxInCombatText", "EA_Window_CityCaptureJoinPromptWindowBoxText", "EA_Window_CityCaptureJoinPromptWindowBoxRespondTime",
							"EA_Window_CityCaptureJoinPromptWindowBoxLowLevel", "ScenarioGroupWindowGroup1Name", "ScenarioGroupWindowGroup2Name", "ScenarioGroupWindowGroup3Name", "ScenarioGroupWindowGroup4Name",
							"ScenarioGroupWindowGroup5Name", "ScenarioGroupWindowGroup6Name", "ScenarioGroupWindowMiddleBarInstructions", "ScenarioGroupWindowMiddleBarGroupToggleButtonText", 
							"EA_Window_ScenarioTrackerOverviewPoint1Name", "EA_Window_ScenarioTrackerOverviewPoint1Time", "EA_Window_ScenarioTrackerOverviewPoint2Name", "EA_Window_ScenarioTrackerOverviewPoint2Time",
							"EA_Window_ScenarioTrackerOverviewPoint3Name", "EA_Window_ScenarioTrackerOverviewPoint3Time", "EA_Window_ScenarioTrackerOverviewPoint4Name", "EA_Window_ScenarioTrackerOverviewPoint4Time",
							"EA_Window_ScenarioTrackerOverviewPoint5Name", "EA_Window_ScenarioTrackerOverviewPoint5Time", "EA_Window_ScenarioTrackerOverviewPoint6Name", "EA_Window_ScenarioTrackerOverviewPoint6Time",
							"EA_Window_ScenarioTrackerOverviewPoint7Name", "EA_Window_ScenarioTrackerOverviewPoint7Time", "EA_Window_ScenarioTrackerOverviewPoint8Name", "EA_Window_ScenarioTrackerOverviewPoint8Time",
							"TwoButtonDlgBoxText", "TwoButtonDlg1BoxTimer", "EA_Window_MacroDetailsNameTitle", "EA_Window_MacroDetailsTextTitle", "SWTabGeneralContentsScrollChildSettingsGamePlayAutoLootAllLabel",
							"SWTabGeneralContentsScrollChildSettingsGamePlayStaticAbilityTooltipsLabel", "SWTabGeneralContentsScrollChildSettingsGamePlayClickThroughSelfLabel",
							"SWTabGeneralContentsScrollChildSettingsGamePlayAutoLootInRvRLabel", "SWTabGeneralContentsScrollChildSettingsGamePlayShowRvRAlertsLabel", "SWTabGeneralContentsScrollChildSettingsGamePlayStaticTooltipsLabel",
							"SWTabGeneralContentsScrollChildSettingsGamePlayShowToolTipsLabel", "SWTabGeneralContentsScrollChildSettingsGamePlayPlayerStatusFadeLabel",
							"SWTabGeneralContentsScrollChildSettingsGamePlayAllowAutoQueueingLabel", "SWTabGeneralContentsScrollChildSettingsGamePlayClickToMoveLabel", 
							"SWTabGeneralContentsScrollChildSettingsGamePlayPreventPlayerStatusFadeLabel", "SWTabGeneralContentsScrollChildSettingsHelpTipsShowTutorialsLabel",
							"SWTabGeneralContentsScrollChildSettingsHelpTipsHideAdvancedWindowsUntilNeededLabel", "SWTabGeneralContentsScrollChildSettingsHelpTipsShowBeginnerTipsLabel",
							"SWTabGeneralContentsScrollChildSettingsHelpTipsShowGameplayTipsLabel", "SWTabGeneralContentsScrollChildSettingsHelpTipsShowUiTipsLabel", 
							"SWTabGeneralContentsScrollChildSettingsHelpTipsShowAdvancedTipsLabel", "SWTabGeneralContentsScrollChildSettingsDialogWarningsWarnBuyLabel",
							"SWTabGeneralContentsScrollChildSettingsDialogWarningsWarnSellLabel", "SWTabGeneralContentsScrollChildSettingsDialogWarningsWarnRepairLabel",
							"SWTabGeneralContentsScrollChildSettingsDialogWarningsWarnRefineLabel", "SWTabGeneralContentsScrollChildSettingsDialogWarningsWarnRefineCurrencyLabel",
							"SWTabGeneralContentsScrollChildSettingsDialogWarningsWarnPerfLevelLabel", "SWTabGeneralContentsScrollChildSettingsCameraUseFirstPersonViewLabel",
							"SWTabGeneralContentsScrollChildSettingsCameraInvertMouselookLabel", "SWTabGeneralContentsScrollChildSettingsCameraPlayerFadeLabel",
							"SWTabGeneralContentsScrollChildSettingsCameraSmartFollowLabel", "SWTabVideoContentsScrollChildSettingsResolutionWindowedResLabel", "SWTabVideoContentsScrollChildSettingsResolutionFullScreenResLabel",
							"SWTabVideoContentsScrollChildSettingsResolutionShowFrameLabel", "SWTabVideoContentsScrollChildSettingsResolutionUseFullscreenLabel", 
							"SWTabVideoContentsScrollChildSettingsGlobalUIScaleUiScaleSliderMinLabel", "SWTabVideoContentsScrollChildSettingsGlobalUIScaleUiScaleSliderMaxLabel",
							"SWTabVideoContentsScrollChildSettingsGlobalUIScaleUiScaleSliderMidLabel", "SWTabVideoContentsScrollChildSettingsPerformancePerformanceLabel", "SWTabVideoContentsScrollChildSettingsPerformancePerformanceLabel",
							"SWTabVideoContentsScrollChildSettingsPerformanceShaderCachingLabel", "SWTabVideoContentsScrollChildSettingsColorBrightnessLabel", "SWTabVideoContentsScrollChildSettingsColorGammaLabel",
							"SWTabVideoContentsScrollChildSettingsColorInstructionsLabel", "SWTabVideoContentsScrollChildSettingsColorDisabledLabel", "SWTabSoundContentsScrollChildSettingsSoundMasterSoundLabel",
							"SWTabSoundContentsScrollChildSettingsSoundEnableMasterSoundLabel", "SWTabSoundContentsScrollChildSettingsSoundEffectsSoundLabel", "SWTabSoundContentsScrollChildSettingsSoundEnableEffectsSoundLabel",
							"SWTabSoundContentsScrollChildSettingsSoundMusicSoundLabel", "SWTabSoundContentsScrollChildSettingsSoundEnableMusicSoundLabel", "SWTabSoundContentsScrollChildSettingsSoundEAXLabel",
							"SWTabSoundContentsScrollChildSettingsSoundEnableEAXLabel", "SWTabChatContentsScrollChildSettingsChatFadeTextLabel", "SWTabChatContentsScrollChildSettingsChatProfanityFilterLabel",
							"SWTabChatContentsScrollChildSettingsChatVisTimeLabel", "SWTabChatContentsScrollChildSettingsChatLimitLabel", "SWTabChatContentsScrollChildSettingsChatLimitLowLabel", "SWTabChatContentsScrollChildSettingsChatLimitMediumLabel", 
							"SWTabChatContentsScrollChildSettingsChatLimitHighLabel", "SWTabChatContentsScrollChildSettingsChatLimitVeryHighLabel", "SWTabChatContentsScrollChildSettingsChatBubblesShowPlayerChatBubblesLabel",
							"SWTabChatContentsScrollChildSettingsChatBubblesShowNPCChatBubblesLabel", "SWTabChatContentsScrollChildSettingsChatBubblesShowPartyChatBubblesLabel", "SWTabTargettingContentsScrollChildSettingsHealthbarsYourHealthbar",
							"SWTabTargettingContentsScrollChildSettingsHealthbarsTargetHealthbar", "SWTabTargettingContentsScrollChildSettingsHealthbarsGroupHealthbar", "SWTabTargettingContentsScrollChildSettingsHealthbarsFriendlyPlayerHealthbar",
							"SWTabTargettingContentsScrollChildSettingsHealthbarsMonsterHealthbar", "SWTabTargettingContentsScrollChildSettingsNamesFont", "SWTabTargettingContentsScrollChildSettingsNamesYourNameLabel", 
							"SWTabTargettingContentsScrollChildSettingsNamesYourTitleLabel", "SWTabTargettingContentsScrollChildSettingsNamesYourGuildLabel", "SWTabTargettingContentsScrollChildSettingsNamesTargetNameLabel",
							"SWTabTargettingContentsScrollChildSettingsNamesTargetTitleLabel", "SWTabTargettingContentsScrollChildSettingsNamesTargetGuildLabel", "SWTabTargettingContentsScrollChildSettingsNamesFriendlyPlayerNameLabel",
							"SWTabTargettingContentsScrollChildSettingsNamesFriendlyPlayerTitleLabel", "SWTabTargettingContentsScrollChildSettingsNamesFriendlyPlayerGuildLabel", "SWTabTargettingContentsScrollChildSettingsNamesEnemyPlayerNameLabel",
							"SWTabTargettingContentsScrollChildSettingsNamesEnemyPlayerTitleLabel", "SWTabTargettingContentsScrollChildSettingsNamesEnemyPlayerGuildLabel", "SWTabTargettingContentsScrollChildSettingsNamesYourPetNameLabel",
							"SWTabTargettingContentsScrollChildSettingsNamesYourPetNameLabel", "SWTabTargettingContentsScrollChildSettingsNamesNPCTitleLabel", "SWTabTargettingContentsScrollChildSettingsNamesFriendlyNPCNameLabel", 
							"SWTabTargettingContentsScrollChildSettingsNamesEnemyNPCNameLabel", "SWTabTargettingContentsScrollChildSettingsReticlesTargetLabel", "SWTabTargettingContentsScrollChildSettingsReticlesMouseOverLabel",
							"SWTabTargettingContentsScrollChildSettingsReticlesOverheadArrowLabel", "SWTabTargettingContentsScrollChildSettingsReticlesTargetPointerLabel", "SWTabTargettingContentsScrollChildSettingsReticlesGroupPointerLabel",
							"SWTabTargettingContentsScrollChildSettingsReticlesDamagePointerLabel", "SWTabInterfaceContentsScrollChildSettingsActionBarsLockActionBarsLabel", "SWTabInterfaceContentsScrollChildSettingsActionBarsShowCooldownTextLabel",
							"SWTabInterfaceContentsScrollChildSettingsActionBarsLayoutModeTitle", "SWTabInterfaceContentsScrollChildSettingsActionBarsLayoutMode1BarLabel", "SWTabInterfaceContentsScrollChildSettingsActionBarsLayoutMode2BarsLabel", 
							"SWTabInterfaceContentsScrollChildSettingsActionBarsLayoutMode2BarsStackedLabel", "SWTabInterfaceContentsScrollChildSettingsActionBarsLayoutMode3BarsLabel", "SWTabInterfaceContentsScrollChildSettingsActionBarsLayoutMode4BarsLabel",
							"SWTabInterfaceContentsScrollChildSettingsActionBarsLayoutMode5BarsLabel", "SWTabInterfaceContentsScrollChildSettingsActionBarsCustomizeMakeVertical", "SWTabInterfaceContentsScrollChildSettingsActionBarsCustomizeRows",
							"SWTabInterfaceContentsScrollChildSettingsActionBarsCustomizeNumberButtons", "SWTabInterfaceContentsScrollChildSettingsActionBarsCustomizeVisibility", "SWTabInterfaceContentsScrollChildSettingsActionBarsCustomizeBar1Name",
							"SWTabInterfaceContentsScrollChildSettingsActionBarsCustomizeBar2Name", "SWTabInterfaceContentsScrollChildSettingsActionBarsCustomizeBar3Name", "SWTabInterfaceContentsScrollChildSettingsActionBarsCustomizeBar4Name",
							"SWTabInterfaceContentsScrollChildSettingsActionBarsCustomizeBar5Name", "SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsEnableLabel", "SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsCooldownLabel",
							"SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsAlertCombatLabel", "SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsAlertRvRLabel", "SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsAlertGeoLabel",
							"SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsAlertAchievLabel", "SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsAlertQuestLabel", "SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsAlertSocialLabel",
							"SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsAlertDefaultLabel", "DyeMerchantCostLabelTotal", "DyeMerchantDyeAllLabel", "DyeMerchantCurrentColorDescLabel", "DyeMerchantCurrentColorLabel", 
							"DyeMerchantMarketingSlotDescLabel", "DyeMerchantButtonsCostLabelTotal", "CharacterWindowContentsInstructionText", "AuctionHouseWindowCreateAuctionItemName", "AuctionHouseWindowCreateAuctionDurationComboBoxHeader",
							"AuctionHouseWindowCreateAuctionBidPriceHeader", "AuctionHouseWindowCreateAuctionBuyOutPriceHeader", "AuctionHouseWindowCreateAuctionDepositHeader", "AuctionHouseWindowCreateAuctionVendorPriceHeader", 
							"AuctionHouseWindowCreateAuctionMoneyAvailableHeader", "AuctionHouseWindowCreateSearchRankHeader", "AuctionHouseWindowCreateSearchRankSeparator", "AuctionHouseWindowCreateSearchTradeSkillLevelHeader",
							"AuctionHouseWindowCreateSearchTradeSkillLevelSeparator", "AuctionHouseWindowCreateSearchUsableOnlyHeader", "AuctionHouseWindowCreateSearchMaxPriceHeader", "AuctionHouseWindowSearchResultsNoResultsText",
							"AuctionHouseWindowSearchResultsBrowseOptionsMyBidPriceHeader", "AuctionHouseWindowSearchResultsBrowseOptionsMoneyAvailableHeader", "MailWindowTabInboxMessageNumberText", "MailWindowTabInboxResultText",
							"MailWindowTabSendToHeader", "MailWindowTabSendSubjectHeader", "MailWindowTabSendMessageBodyHeader", "MailWindowTabSendAttachmentHeader", "MailWindowTabSendCODCheckBoxButtonHeader", "MailWindowTabSendMoneyInBackpackHeader", 
							"MailWindowTabSendPostageHeader", "MailWindowTabSendResultText", "MailWindowTabAuctionMessageNumberText", "MailWindowTabAuctionResultText", "MailWindowTabMessageFromHeader", "MailWindowTabMessageFromText", 
							"MailWindowTabMessageSubjectText", "MailWindowTabMessageBodyScrollChildText", "MailWindowTabMessageAttachmentHeader", "SocialWindowTabSearchEditBoxPlayerNameHeader", "SocialWindowTabSearchEditBoxGuildNameHeader",
							"SocialWindowTabSearchEditBoxCareerNameHeader", "SocialWindowTabSearchComboBoxZoneNamesHeader", "SocialWindowTabSearchEditBoxMinRankHeader", "SocialWindowTabSearchEditBoxMaxRankHeader", "SocialWindowTabFriendsPlayerSearchLabel",
							"SocialWindowTabFriendsFilterLabel", "SocialWindowTabIgnorePlayerSearchLabel", "SocialWindowTabOptionsAnonymousLabel", "SocialWindowTabOptionsHiddenLabel", "SocialWindowTabOptionsPrivatePartyLabel", 
							"SocialWindowTabOptionsDisableBuddyListLabel", "SocialWindowTabOptionsAdvisorLabel", "EA_Window_HelpFAQDescriptionText", "EA_Window_HelpManualDescriptionText", "EA_Window_HelpAppealHeader", "EA_Window_HelpAppealDescriptionText",
							"EA_Window_HelpReportBugDescriptionText", "EA_Window_HelpFeedbackDescriptionText", "EA_Window_HelpTipsDescriptionText", "UiModVersionMismatchWindowCurrentVersionText"

						}	
	local fontslargebold	=	{	"MainMenuWindowTitleBarText", "yCustomcolorslidersTitleBarText", "CharacterWindowContentsImageNameLabel", "AbilitiesWindowTitleBarText", "ySkinBackgroundColorTitleBarText",
							"EA_Window_InteractionCoreTrainingTitleBarText", "EA_Window_InteractionSpecialtyTrainingTitleBarText", "EA_Window_BackpackTitleBarText",
							"EA_Window_OpenPartyTitleBarText", "EA_Window_InteractionRenownTrainingTitleBarText", "EA_Window_InteractionTomeTrainingTitleBarText", "ScenarioSummaryWindowScenarioName",
							"ScenarioSummaryWindowVictorText", "EA_Window_InteractionStoreTitleBarText", "DebugWindowTitleBarText", "GuildWindowTitleBarText", "CharacterWindowRvRStatsHeader",
							"ClosetGoblinCharacterWindowTitleBarText", "DascoreWin1WindowTitleBarText", "CaVESWindowTitleBarText", "alertModTitleBarText", "EA_Window_CityTrackerOverviewStatus",
							"EA_Window_CityTrackerOverviewTimerValue", "PQLootWindowTitleBarText", "EA_Window_CityTrackerMainQuest1Label", "EA_Window_CityTrackerMainQuest1TimerValue", 
							"EA_Window_CityTrackerMainQuest2Label", "EA_Window_CityTrackerMainQuest2TimerValue", 
							"EA_Window_PublicQuestTrackerObjective1Quest1Label", "EA_Window_PublicQuestTrackerObjective1Quest2Label",
							"EA_Window_PublicQuestTrackerObjective1Quest3Label",
							"EA_Window_PublicQuestTrackerObjective2Quest1Label", "EA_Window_PublicQuestTrackerObjective2Quest2Label",
							"EA_Window_PublicQuestTrackerObjective2Quest3Label", 
							"EA_Window_PublicQuestResultsDataPQName", "EA_Window_ScenarioLobbyTitleBarText", "EA_Window_ScenarioLobbyScenarioName", "EA_Window_ScenarioJoinPromptBoxTitleBarText", "EA_Window_ScenarioJoinPromptBoxName", 
							"EA_Window_CityCaptureJoinPromptWindowBoxTitleBarText", "EA_Window_CityCaptureJoinPromptWindowBoxName", "ScenarioGroupWindowTitleBarText", "EA_Window_ScenarioStartingName", "ItemEnhancementWindowTitleBarText",
							"EA_Window_ScenarioTrackerLocationScenarioName", "ItemStackingWindowTitleBarText", "EA_Window_MacroTitleBarText", "KeyMappingWindowTitleBarText", "SettingsWindowTabbedTitleBarText", "EA_Window_KeepObjectiveTrackerLabel",
							"AuctionHouseWindowTitleBarText", "MailWindowTitleBarText", "BankWindowTitleBarText", "GuildVaultWindowTitleBarText", "MailWindowTabMessageTitleBarText", "RVMOD_ManagerWindowTitleBarText", "EnemyIntercomDialogTitleBarText",
							"WARCommander_ConfigWindowTitleBarText", "SocialWindowTitleBarText", "EnemyIntercomJoinDialogTitleBarText", "EA_Window_HelpTitleBarText", "UiModVersionMismatchWindowTitleBarText", "UiModWindowTitleBarText",
							"EA_Window_CustomizePerformanceTitleBarText", "TidyChatCopyTitleBarText"--, "SOR.Options.Title"
						}	
	local fontslarge		=	{	"EA_Window_OpenPartyManageWarbandHeaderText", "EA_Window_OpenPartyManageWarband1Label", "EA_Window_OpenPartyManageWarband2Label", "EA_Window_OpenPartyManageWarband3Label", 
							"EA_Window_OpenPartyManageWarband4Label", "SWTabGeneralContentsScrollChildSettingsGamePlayTitle", "SWTabGeneralContentsScrollChildSettingsHelpTipsTitle", 
							"SWTabGeneralContentsScrollChildSettingsDialogWarningsTitle", "SWTabGeneralContentsScrollChildSettingsCameraTitle", "SWTabVideoContentsScrollChildSettingsResolutionTitle", "SWTabVideoContentsScrollChildSettingsGlobalUIScaleTitle",
							"SWTabVideoContentsScrollChildSettingsPerformanceTitle", "SWTabVideoContentsScrollChildSettingsColorTitle", "SWTabSoundContentsScrollChildSettingsSoundTitle", "SWTabChatContentsScrollChildSettingsChatTitle",
							"SWTabChatContentsScrollChildSettingsChatBubblesTitle", "SWTabTargettingContentsScrollChildSettingsHealthbarsTitle", "SWTabTargettingContentsScrollChildSettingsNamesTitle", "SWTabTargettingContentsScrollChildSettingsReticlesTitle",
							"SWTabInterfaceContentsScrollChildSettingsSubSystemsTitle", "SWTabInterfaceContentsScrollChildSettingsActionBarsTitle", "SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsTitle", "EA_Window_HelpFAQHeader", 
							"EA_Window_HelpManualHeader", "EA_Window_HelpAppealHeader", "EA_Window_HelpReportBugHeader", "EA_Window_HelpFeedbackHeader", "EA_Window_HelpTipsHeader", "UiModWindowModDetailsScrollChildName"
						}	

	RegisterEventHandler(SystemData.Events.TOGGLE_BACKPACK_WINDOW, "yakuiSkin.backpacktex")
	
	-- set cornerimages
	for _, v in ipairs (yakuiSkin.cornerimgs) do
		if yakuiVar.Skin.cornerimages == true then 
			if DoesWindowExist(v) == true then WindowSetAlpha(v, 1) end	
		else 
			if DoesWindowExist(v) == true then WindowSetAlpha(v, 0) end 
		end
	end
	-- backgroundtextures
	for _, v in ipairs (yakuiSkin.bgtex) do
		if yakuiVar.Skin.backgroundtex == true then 
			if DoesWindowExist(v) == true then WindowSetAlpha(v, 1) end	
		else 
			if DoesWindowExist(v) == true then WindowSetAlpha(v, 0) end 
		end
	end
	--create new backgrounds
	for _, v in ipairs (yakuiSkin.newbackgrounds) do
		if DoesWindowExist(v[1]) == true then 
			CreateWindowFromTemplate(v[1].."NewBackground","EA_FullResizeImage_TintableSolidBackground",v[1])
			WindowSetShowing(v[1].."NewBackground",true)
			WindowSetHandleInput(v[1].."NewBackground",false)
			WindowSetLayer(v[1].."NewBackground",0)
			--if v[9] ~= nil then WindowSetParent(v[1].."NewBackground", v[9]) end
			WindowClearAnchors(v[1].."NewBackground")
			WindowAddAnchor(v[1].."NewBackground","topleft",v[2],"topleft",v[3],v[4])
			WindowAddAnchor(v[1].."NewBackground","bottomright",v[5],"bottomright",v[6],v[7])
			if DoesWindowExist(v[1]) == true then
				if yakuiVar.Skin.newbackgrounds.linkwpanel == false then
					WindowSetTintColor(v[1].."NewBackground",yakuiVar.Skin.newbackgrounds.r,yakuiVar.Skin.newbackgrounds.g,yakuiVar.Skin.newbackgrounds.b)
					if yakuiVar.Panel.alphatoggle == false then
						WindowSetAlpha(v[1].."NewBackground", 1)
					else
						WindowSetAlpha(v[1].."NewBackground", yakuiVar.Skin.newbackgrounds.alpha)
					end
				else
					WindowSetTintColor(v[1].."NewBackground",yakuiVar.Colors.background.r,yakuiVar.Colors.background.g,yakuiVar.Colors.background.b)
					if yakuiVar.Panel.alphatoggle == false then
						WindowSetAlpha(v[1].."NewBackground", 1)
					else
						WindowSetAlpha(v[1].."NewBackground", yakuiVar.Panel.alpha)
					end
				end
			end		
		end
	end

	-- create new borders
	for _, v in ipairs (yakuiSkin.newbackgrounds) do
		if DoesWindowExist(v[1]) == true then yakuiSkin.createborder(v[1],0,0,0,0,v[8],v[9]) end
	end

	-- setfonts
	if yakuiVar.Skin.fonts == true then
		for _, v in ipairs (fontssmallbold) do
			if DoesWindowExist(v) == true then LabelSetFont(v, "font_clear_small_bold", 10) end
		end
		for _, v in ipairs (fontsmediumbold) do
			if DoesWindowExist(v) == true then LabelSetFont(v, "font_clear_medium_bold", 10) end
		end
		for _, v in ipairs (fontsmedium) do
			if DoesWindowExist(v) == true then LabelSetFont(v, "font_clear_medium", 10) end
		end
		for _, v in ipairs (fontslargebold) do
			if DoesWindowExist(v) == true then LabelSetFont(v, "font_clear_large_bold", 10) end
		end
		for _, v in ipairs (fontslarge) do
			if DoesWindowExist(v) == true then LabelSetFont(v, "font_clear_large", 10) end
		end
	end

	-- icon overlay
	if yakuiVar.Skin.equipmentoverlay == true then
		--character window
		for v =1,30 do
			if DoesWindowExist("CharacterWindowContentsEquipmentSlot"..tostring(v)) then
				CreateWindowFromTemplate("EquipmentOverlay"..tostring(v),"EA_DynamicImage_DefaultSeparatorRight","Root")
				WindowSetShowing("EquipmentOverlay"..tostring(v),true)
				WindowSetAlpha("EquipmentOverlay"..tostring(v), 1)
				WindowSetHandleInput("EquipmentOverlay"..tostring(v),false)
				WindowSetParent("EquipmentOverlay"..tostring(v), "CharacterWindowContentsEquipmentSlot"..tostring(v))
				WindowClearAnchors("EquipmentOverlay"..tostring(v))	
				WindowAddAnchor("EquipmentOverlay"..tostring(v),"topleft","CharacterWindowContentsEquipmentSlot"..tostring(v),"topleft",0,0)
				WindowAddAnchor("EquipmentOverlay"..tostring(v),"bottomright","CharacterWindowContentsEquipmentSlot"..tostring(v),"bottomright",0,0)
				DynamicImageSetTexture("EquipmentOverlay"..tostring(v), "EquipmentGloss", 0, 0)
				DynamicImageSetTextureDimensions("EquipmentOverlay"..tostring(v), 64, 64)
			end
		end
		--mainmenu
		for _, v in ipairs (mainmenuitems) do
			if DoesWindowExist(tostring(v)) then
				CreateWindowFromTemplate("EquipmentOverlay"..tostring(v),"EA_DynamicImage_DefaultSeparatorRight","Root")
				WindowSetShowing("EquipmentOverlay"..tostring(v),true)
				WindowSetAlpha("EquipmentOverlay"..tostring(v), 1)
				WindowSetHandleInput("EquipmentOverlay"..tostring(v),false)
				WindowSetParent("EquipmentOverlay"..tostring(v), tostring(v).."Icon")
				WindowClearAnchors("EquipmentOverlay"..tostring(v))	
				WindowAddAnchor("EquipmentOverlay"..tostring(v),"topleft",tostring(v).."Icon","topleft",0,0)
				WindowAddAnchor("EquipmentOverlay"..tostring(v),"bottomright",tostring(v).."Icon","bottomright",0,0)
				DynamicImageSetTexture("EquipmentOverlay"..tostring(v), "EquipmentGloss", 0, 0)
				DynamicImageSetTextureDimensions("EquipmentOverlay"..tostring(v), 64, 64)
				WindowSetShowing(tostring(v).."Frame",false)
			end
		end
		--closet goblin
		for v =1,30 do
			if DoesWindowExist("ClosetGoblinCharacterWindowContentsEquipmentSlot"..tostring(v)) then
				CreateWindowFromTemplate("ClosetGoblinEquipmentOverlay"..tostring(v),"EA_DynamicImage_DefaultSeparatorRight","Root")
				WindowSetShowing("ClosetGoblinEquipmentOverlay"..tostring(v),true)
				WindowSetAlpha("ClosetGoblinEquipmentOverlay"..tostring(v), 1)
				WindowSetHandleInput("ClosetGoblinEquipmentOverlay"..tostring(v),false)
				WindowSetParent("ClosetGoblinEquipmentOverlay"..tostring(v), "ClosetGoblinCharacterWindowContentsEquipmentSlot"..tostring(v))
				WindowClearAnchors("ClosetGoblinEquipmentOverlay"..tostring(v))	
				WindowAddAnchor("ClosetGoblinEquipmentOverlay"..tostring(v),"topleft","ClosetGoblinCharacterWindowContentsEquipmentSlot"..tostring(v),"topleft",0,0)
				WindowAddAnchor("ClosetGoblinEquipmentOverlay"..tostring(v),"bottomright","ClosetGoblinCharacterWindowContentsEquipmentSlot"..tostring(v),"bottomright",0,0)
				DynamicImageSetTexture("ClosetGoblinEquipmentOverlay"..tostring(v), "EquipmentGloss", 0, 0)
				DynamicImageSetTextureDimensions("ClosetGoblinEquipmentOverlay"..tostring(v), 64, 64)
			end
		end
	end
	
	--repair various stuff
	WindowSetLayer("EA_Window_InteractionSpecialtyTrainingInteractivePane", 1)
	WindowSetLayer("EA_Window_InteractionSpecialtyTrainingDisplayPane", 1)
	WindowSetLayer("ItemEnhancementWindowSlot1Id", 1)
	WindowSetLayer("ItemEnhancementWindowSlot2Id", 1)
	WindowSetLayer("ItemEnhancementWindowSlot3Id", 1)
	WindowSetLayer("KeyMappingWindowActionsListBackground", 1)
	WindowSetLayer("KeyMappingWindowActionListLabel", 1)
	WindowSetLayer("KeyMappingWindowActionListBoundKeysLabel", 1)
	WindowSetLayer("SocialWindowTabSeparatorRight", 1)
	WindowSetShowing("CharacterWindowContentsInstructionText", false)
	WindowClearAnchors("EA_Window_KeepObjectiveTrackerDuringActionTimerValue")
	WindowAddAnchor("EA_Window_KeepObjectiveTrackerDuringActionTimerValue","topleft","EA_Window_KeepObjectiveTrackerDuringActionQuest","topleft",250,35)
	WindowSetDimensions("SWTabInterfaceContentsScrollChildCustomizeUiMessageSettingsCooldownLabel", 225, 20)
	if DoesWindowExist("ScenarioStatsWindowTitle") == true then 
		WindowClearAnchors("ScenarioStatsWindowTitle")	
		WindowAddAnchor("ScenarioStatsWindowTitle","topleft","ScenarioStatsWindow","topleft",0,4)
	end
	if DoesWindowExist("CaVESWindow") == true then
		WindowClearAnchors("CaVESWindow")
		WindowAddAnchor("CaVESWindow", "topright", "CharacterWindowClose", "topleft", 16, 0)
	end
	if DoesWindowExist("TidyQueue") == true then
		WindowClearAnchors("TidyQueue")
		WindowAddAnchor("TidyQueue", "bottomright", "EA_Window_ScenarioLobby", "bottomleft", 16, 0)
	end
	--LabelSetText("RVMOD_ManagerWindowTitleBarText", L"ModsManager")
end

function yakuiSkin.FirstStepsCreate()
	CreateWindowFromTemplate("YFirstStepsNewBackground","EA_FullResizeImage_TintableSolidBackground","YFirstSteps")
	WindowSetShowing("YFirstStepsNewBackground",true)
	WindowSetHandleInput("YFirstStepsNewBackground",false)
	WindowSetLayer("YFirstStepsNewBackground",0)
	WindowClearAnchors("YFirstStepsNewBackground")
	WindowAddAnchor("YFirstStepsNewBackground","topleft","YFirstStepsBackgroundBackground","topleft",8,-4)
	WindowAddAnchor("YFirstStepsNewBackground","bottomright","YFirstStepsBackgroundBackground","bottomright",-8, -8)
		if DoesWindowExist("YFirstSteps") == true then
			if yakuiVar.Skin.newbackgrounds.linkwpanel == false then
				WindowSetTintColor("YFirstStepsNewBackground",yakuiVar.Skin.newbackgrounds.r,yakuiVar.Skin.newbackgrounds.g,yakuiVar.Skin.newbackgrounds.b)
				if yakuiVar.Panel.alphatoggle == false then
					WindowSetAlpha("YFirstStepsNewBackground", 1)
				else
					WindowSetAlpha("YFirstStepsNewBackground", yakuiVar.Skin.newbackgrounds.alpha)
				end
			else
				WindowSetTintColor("YFirstStepsNewBackground",yakuiVar.Colors.background.r,yakuiVar.Colors.background.g,yakuiVar.Colors.background.b)
				if yakuiVar.Panel.alphatoggle == false then
					WindowSetAlpha("YFirstStepsNewBackground", 1)
				else
					WindowSetAlpha("YFirstStepsNewBackground", yakuiVar.Panel.alpha)
				end
			end
		end
	yakuiSkin.createborder("YFirstSteps",0,0,0,0,true,true)
end

function yakuiSkin.FirstStepsDestroy()
	DestroyWindow("YFirstStepsNewBackground")
	DestroyWindow("YFirstStepsBackgroundyBorder")
end
	
function yakuiSkin.backpacktex()
	UnregisterEventHandler(SystemData.Events.TOGGLE_BACKPACK_WINDOW, "yakuiSkin.backpacktex")
	for x=1,EA_Window_Backpack.numberOfPockets[1] do 
		if (DoesWindowExist("EA_Window_BackpackQuestViewSection"..x.."Background")) then WindowSetAlpha("EA_Window_BackpackQuestViewSection"..x.."Background", 0) end
		if (DoesWindowExist("EA_Window_BackpackQuestViewSection"..x.."BottomDivider")) then WindowSetAlpha("EA_Window_BackpackQuestViewSection"..x.."BottomDivider", 0) end
	end
	for x=1,EA_Window_Backpack.numberOfPockets[2] do 
		if (DoesWindowExist("EA_Window_BackpackIconViewSection"..x.."Background")) then WindowSetAlpha("EA_Window_BackpackIconViewSection"..x.."Background", 0) end
		if (DoesWindowExist("EA_Window_BackpackIconViewSection"..x.."BottomDivider")) then WindowSetAlpha("EA_Window_BackpackIconViewSection"..x.."BottomDivider", 0) end
	end
	for x=1,EA_Window_Backpack.numberOfPockets[3] do 
		if (DoesWindowExist("EA_Window_BackpackCurrencyViewSection"..x.."Background")) then WindowSetAlpha("EA_Window_BackpackCurrencyViewSection"..x.."Background", 0) end
		if (DoesWindowExist("EA_Window_BackpackCurrencyViewSection"..x.."BottomDivider")) then WindowSetAlpha("EA_Window_BackpackCurrencyViewSection"..x.."BottomDivider", 0) end
	end
	for x=1,EA_Window_Backpack.numberOfPockets[4] do 
		if (DoesWindowExist("EA_Window_BackpackCraftingViewSection"..x.."Background")) then WindowSetAlpha("EA_Window_BackpackCraftingViewSection"..x.."Background", 0) end
		if (DoesWindowExist("EA_Window_BackpackCurrencyViewSection"..x.."BottomDivider")) then WindowSetAlpha("EA_Window_BackpackCurrencyViewSection"..x.."BottomDivider", 0) end
	end
end

function yakuiSkin.UpdateAllBorders()
	if yakuiSkin.newbackgrounds then
		for _, v in ipairs (yakuiSkin.newbackgrounds) do
			if DoesWindowExist(v[1]) == true then yakuiSkin.updateborder(v[1]) end
		end
	end
	if DoesWindowExist("YFirstSteps") == true then yakuiSkin.updateborder("YFirstSteps") end
end

function yakuiSkin.cornerimages()
	if yakuiSkin.cornerimgs then
		for _, v in ipairs (yakuiSkin.cornerimgs) do
			if yakuiVar.Skin.cornerimages == true then 
				if DoesWindowExist(v) == true then WindowStartAlphaAnimation(v,Window.AnimationType.SINGLE_NO_RESET,WindowGetAlpha(v),1,1.5,true, 0,0) end
			else 
				if DoesWindowExist(v) == true then WindowStartAlphaAnimation(v,Window.AnimationType.SINGLE_NO_RESET,WindowGetAlpha(v),0,1.5,true, 0,0) end
			end
		end
	end
end

function yakuiSkin.backgroundtex()
	if yakuiSkin.bgtex then
		for _, v in ipairs (yakuiSkin.bgtex) do
			if yakuiVar.Skin.backgroundtex == true then 
				if DoesWindowExist(v) == true then 
					WindowStartAlphaAnimation(v,Window.AnimationType.SINGLE_NO_RESET,WindowGetAlpha(v),1,1.5,true, 0,0) 
				end
			else
				if DoesWindowExist(v) == true then 
					WindowStartAlphaAnimation(v,Window.AnimationType.SINGLE_NO_RESET,WindowGetAlpha(v),0,1.5,true, 0,0) 
				end
			end
		end
	end
	if yakuiSkin.newbackgrounds then
		for _, v in ipairs (yakuiSkin.newbackgrounds) do
			if yakuiVar.Skin.backgroundtex == true then
				WindowStartAlphaAnimation(v[1].."NewBackground",Window.AnimationType.SINGLE_NO_RESET,WindowGetAlpha(v[1].."NewBackground"),0,1.5,true, 0,0)
			else
				WindowStartAlphaAnimation(v[1].."NewBackground",Window.AnimationType.SINGLE_NO_RESET,WindowGetAlpha(v[1].."NewBackground"),1,1.5,true, 0,0)
			end
		end
	end
end

function yakuiSkin.newbackground()
	if yakuiSkin.newbackgrounds then
		for _, v in ipairs (yakuiSkin.newbackgrounds) do
			if DoesWindowExist(v[1]) == true then
				if yakuiVar.Skin.newbackgrounds.linkwpanel == false then
					WindowSetTintColor(v[1].."NewBackground",yakuiVar.Skin.newbackgrounds.r,yakuiVar.Skin.newbackgrounds.g,yakuiVar.Skin.newbackgrounds.b)
					if yakuiVar.Panel.alphatoggle == true then 
						WindowSetAlpha(v[1].."NewBackground", yakuiVar.Skin.newbackgrounds.alpha)
					else 
						WindowSetAlpha(v[1].."NewBackground", 1)
					end
				else
					WindowSetTintColor(v[1].."NewBackground",yakuiVar.Colors.background.r,yakuiVar.Colors.background.g,yakuiVar.Colors.background.b)
					if yakuiVar.Panel.alphatoggle == true then 
						WindowSetAlpha(v[1].."NewBackground", yakuiVar.Panel.alpha)
					else 
						WindowSetAlpha(v[1].."NewBackground", 1)
					end
				end
			end
		end
	end
	if DoesWindowExist("YFirstSteps") == true then
		if yakuiVar.Skin.newbackgrounds.linkwpanel == false then
			WindowSetTintColor("YFirstStepsNewBackground",yakuiVar.Skin.newbackgrounds.r,yakuiVar.Skin.newbackgrounds.g,yakuiVar.Skin.newbackgrounds.b)
			if yakuiVar.Panel.alphatoggle == true then 
				WindowSetAlpha("YFirstStepsNewBackground", yakuiVar.Skin.newbackgrounds.alpha)
			else 
				WindowSetAlpha("YFirstStepsNewBackground", 1)
			end
		else
			WindowSetTintColor("YFirstStepsNewBackground",yakuiVar.Colors.background.r,yakuiVar.Colors.background.g,yakuiVar.Colors.background.b)
			if yakuiVar.Panel.alphatoggle == true then 
				WindowSetAlpha("YFirstStepsNewBackground", yakuiVar.Panel.alpha)
			else 
				WindowSetAlpha("YFirstStepsNewBackground", 1)
			end
		end
	end	
end

function yakuiSkin.createborder(wnd,topxoff,topyoff,bottomxoff,bottomyoff,hastitlebar,hasborder)
	CreateWindowFromTemplate(wnd.."BackgroundyBorder","ySkinTemplate",wnd)
	WindowSetShowing(wnd.."BackgroundyBorder",true)
	WindowSetHandleInput(wnd.."BackgroundyBorder",false)
	WindowClearAnchors(wnd.."BackgroundyBorder")
	WindowAddAnchor(wnd.."BackgroundyBorder","topleft",wnd.."NewBackground","topleft",topxoff,topyoff)
	WindowAddAnchor(wnd.."BackgroundyBorder","bottomright",wnd.."NewBackground","bottomright",bottomxoff,bottomyoff)
	WindowSetParent(wnd.."BackgroundyBorder", wnd)
	
	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_TopBorder", 545, 27)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_TopBorder", "Yak"..yakuiVar.Panel.tex.."SkinTitleBarBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TopBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_TopBorder",yakuiVar.Panel.Panelalpha)
	
	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_TopBorderFX", 545, 27)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_TopBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinTitleBarFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TopBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_TopBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_BottomBorder", 350, 10)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_BottomBorder", "Yak"..yakuiVar.Panel.tex.."SkinHorizontalBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BottomBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_BottomBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_BottomBorderFX", 350, 10)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_BottomBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinHorizontalFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BottomBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_BottomBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_LeftBorder", 10, 350)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_LeftBorder", "Yak"..yakuiVar.Panel.tex.."SkinLVerticalBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_LeftBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_LeftBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_LeftBorderFX", 10, 350)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_LeftBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinLVerticalFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_LeftBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_LeftBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_RightBorder", 10, 350)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_RightBorder", "Yak"..yakuiVar.Panel.tex.."SkinRVerticalBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_RightBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_RightBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_RightBorderFX", 10, 350)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_RightBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinRVerticalFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_RightBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_RightBorderFX",yakuiVar.Panel.fxalpha)
	
	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_TRCBorder", 10, 27)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_TRCBorder", "Yak"..yakuiVar.Panel.tex.."SkinTRCornerBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TRCBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_TRCBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_TRCBorderFX", 10, 27)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_TRCBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinTRCornerFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TRCBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_TRCBorderFX",yakuiVar.Panel.fxalpha)
	
	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_BRCBorder", 10, 10)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_BRCBorder", "Yak"..yakuiVar.Panel.tex.."SkinBRCornerBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BRCBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_BRCBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_BRCBorderFX", 10, 10)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_BRCBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinBRCornerFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BRCBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_BRCBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_BLCBorder", 10, 10)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_BLCBorder", "Yak"..yakuiVar.Panel.tex.."SkinBLCornerBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BLCBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_BLCBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_BLCBorderFX", 10, 10)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_BLCBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinBLCornerFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BLCBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_BLCBorderFX",yakuiVar.Panel.fxalpha)
	
	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_TLCBorder", 10, 27)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_TLCBorder", "Yak"..yakuiVar.Panel.tex.."SkinTLCornerBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TLCBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_TLCBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTextureDimensions(wnd.."BackgroundyBorder_TLCBorderFX", 10, 27)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_TLCBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinTLCornerFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TLCBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_TLCBorderFX",yakuiVar.Panel.fxalpha)

	if hastitlebar == true then WindowSetAlpha(wnd.."TitleBar", 0) end
	if DoesWindowExist(wnd.."BackgroundFrame") == true then WindowSetAlpha(wnd.."BackgroundFrame", 0) end
	--WindowSetTintColor(wnd.."Background",yakuiVar.Colors.background.r,yakuiVar.Colors.background.g,yakuiVar.Colors.background.b)
end


function yakuiSkin.updateborder(wnd)
	DynamicImageSetTexture(wnd.."BackgroundyBorder_TopBorder", "Yak"..yakuiVar.Panel.tex.."SkinTitleBarBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TopBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_TopBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_TopBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinTitleBarFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TopBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_TopBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_BottomBorder", "Yak"..yakuiVar.Panel.tex.."SkinHorizontalBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BottomBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_BottomBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_BottomBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinHorizontalFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BottomBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_BottomBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_LeftBorder", "Yak"..yakuiVar.Panel.tex.."SkinLVerticalBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_LeftBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_LeftBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_LeftBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinLVerticalFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_LeftBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_LeftBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_RightBorder", "Yak"..yakuiVar.Panel.tex.."SkinRVerticalBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_RightBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_RightBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_RightBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinRVerticalFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_RightBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_RightBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_TRCBorder", "Yak"..yakuiVar.Panel.tex.."SkinTRCornerBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TRCBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_TRCBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_TRCBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinTRCornerFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TRCBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_TRCBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_BRCBorder", "Yak"..yakuiVar.Panel.tex.."SkinBRCornerBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BRCBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_BRCBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_BRCBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinBRCornerFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BRCBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_BRCBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_BLCBorder", "Yak"..yakuiVar.Panel.tex.."SkinBLCornerBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BLCBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_BLCBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_BLCBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinBLCornerFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_BLCBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_BLCBorderFX",yakuiVar.Panel.fxalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_TLCBorder", "Yak"..yakuiVar.Panel.tex.."SkinTLCornerBG", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TLCBorder",yakuiVar.Colors.colprintr,yakuiVar.Colors.colprintg,yakuiVar.Colors.colprintb)
	WindowSetAlpha(wnd.."BackgroundyBorder_TLCBorder",yakuiVar.Panel.Panelalpha)

	DynamicImageSetTexture(wnd.."BackgroundyBorder_TLCBorderFX", "Yak"..yakuiVar.Panel.tex.."SkinTLCornerFX", 0, 0)
	WindowSetTintColor(wnd.."BackgroundyBorder_TLCBorderFX",yakuiVar.Colors.fx.r,yakuiVar.Colors.fx.g,yakuiVar.Colors.fx.b)
	WindowSetAlpha(wnd.."BackgroundyBorder_TLCBorderFX",yakuiVar.Panel.fxalpha)
end