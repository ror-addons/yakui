-- yakuiUpdate.lua build20100524

-- From the lua wiki: http://lua-users.org/wiki/CopyTable
local function deepcopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return setmetatable(new_table, _copy( getmetatable(object)))
    end
    return _copy(object)
end

local function LogOutAfterUpdate()
	TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"YakUI will now log off to complete the update.")
	CreateWindow("YOnScreenMessage",true)
	LabelSetText("YOnScreenMessage_Label", L"YakUI will now log off to complete the update.")
	BroadcastEvent( SystemData.Events.LOG_OUT )
end

yakuiUpdate = {}

function yakuiUpdate.load()
	-- variables
	if yakuiVar == nil then 
		TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, L"Warning!!! No YakUI variables found!")	
	else
		if yakuiVar.counter == nil then yakuiVar.counter = 0 end
		yakuiVar.counter = yakuiVar.counter + 1
	end
	if yakuiVar.ver == "YakUI 1.35a (beta)" or yakuiVar.ver == "YakUI 1.35a" then
		TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, towstring(yakuiVar.ver)..L" found! Updating...")
		--[[if Effigy and Effigy.Bars and next(Effigy.Bars) ~= 0 and UFTemplates and UFTemplates.Layouts and UFTemplates.Layouts.YakUI_135 then
			TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, L"Updating: Effigy...")
			-- add the additional bars
			for i = 1,6 do
				Effigy.Bars["TargetsHostile"..i] = deepcopy(UFTemplates.Layouts.YakUI_135["TargetsHostile"..i])
				Effigy.Bars["TargetsFriendly"..i] = deepcopy(UFTemplates.Layouts.YakUI_135["TargetsFriendly"..i])
			end
		end]]--
		yakuiVar.ver = "YakUI 1.36a (beta)"
		TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, L"YakUI updated. New Version: YakUI 1.36a (beta)")
	end
	if yakuiVar.ver == "YakUI 1.36a (beta)" then
		TextLogAddEntry("Chat", SystemData.ChatLogFilters.SHOUT, towstring(yakuiVar.ver)..L" found! Updating...")
		
		-- Load Effigy layout, backup current
		Effigy.SaveLayout("YakUI_Update_14_bck")
		Effigy.LoadLayout("YakUI_140")

		-- yakuiVar.Panel.Panelalpha ->Border
		--[[	if yakuiVar.Panel.alphatoggle == true then 
			WindowSetAlpha("YPanel",yakuiVar.Panel.alpha)
		else				
			WindowSetAlpha("YPanel",1)
		end]]--
		-- yakuiVar.Panel.fxalpha
		--[[WindowSetTintColor("YBorder", yakuiVar.Colors.colprintr, yakuiVar.Colors.colprintg, yakuiVar.Colors.colprintb)
		WindowSetTintColor("YPanel", yakuiVar.Colors.background.r, yakuiVar.Colors.background.g, yakuiVar.Colors.background.b)
		WindowSetTintColor("YFX", yakuiVar.Colors.fx.r, yakuiVar.Colors.fx.g, yakuiVar.Colors.fx.b)]]--
		--Todo yakui:color() texture() to inject to MiniWorldMap, above could be unneeded by injecting effigy too

		if yakuiVar.minmaphook ~= nil then
			Map.Settings.MiniMapHook = yakuiVar.minmaphook
			yakuiVar.minmaphook = nil
		end
		if DAoCBuffVar and DAoCBuffVar.Tables and DAoCBuffVar.Tables.BattleMaster then
			DAoCBuffVar.Tables.BattleMaster[14491] =
			{
				abilityId = 14491,
				effectText = L"All of your Mastery Levels are increased by 1.",
				name = L"Schlachtvollendung^f",
				iconNum = 0,
			}
			DAoCBuffVar.Tables.BattleMaster[14492] =
			{
				abilityId = 14492,
				effectText = L"All of your Mastery Levels are increased by 1.",
				name = L"Schlachtprestige^n",
				iconNum = 0,
			}
			DAoCBuffVar.Tables.BattleMaster[14493] =
			{
				abilityId = 14493,
				effectText = L"All of your Mastery Levels are increased by 1.",
				name = L"Schlachtvortrefflichkeit^f",
				iconNum = 0,
			}
			DAoCBuffVar.Tables.BattleMaster[14494] =
			{
				abilityId = 14494,
				effectText = L"All of your Mastery Levels are increased by 1.",
				name = L"Schlachtvorherrschaft^f",
				iconNum = 0,
			}
			DAoCBuffVar.Tables.BattleMaster[14495] =
			{
				abilityId = 14495,
				effectText = L"All of your Mastery Levels are increased by 1.",
				name = L"Schlachtübermacht^f",
				iconNum = 0,
			}
			for i,frame in ipairs(DAoCBuffVar.Frames) do
				if frame.name == L"Self_Debuffs" then
					frame.rowcount = 2
					frame.maxBuffCount = 10
					frame.buffRowStride = 1
				elseif frame.name == L"Hostile_All" then
					frame.rowcount = 1
					frame.maxBuffCount = 7
					frame.buffRowStride = 3
				elseif frame.name == L"Friendly_All" then
					frame.rowcount = 1
					frame.maxBuffCount = 7
					frame.buffRowStride = 3
				elseif frame.name == L"Self_Morales" then
					frame.rowcount = 1
					frame.maxBuffCount = 5
					frame.buffRowStride = 1
				elseif frame.name == L"Friendly_Morales" then
					frame.rowcount = 1
					frame.maxBuffCount = 5
					frame.buffRowStride = 1
				elseif frame.name == L"Hostile_Morales" then
					frame.rowcount = 1
					frame.maxBuffCount = 5
					frame.buffRowStride = 1
				elseif frame.name == L"Self_Buffs" then
					frame.rowcount = 3
					frame.maxBuffCount = 21
					frame.buffRowStride = 3
				elseif frame.name == L"Hostile_Self_Debuff" then
					frame.rowcount = 2
					frame.maxBuffCount = 14
					frame.buffRowStride = 3
				elseif frame.name == L"Friendly_Self_Buff" then
					frame.rowcount = 2
					frame.maxBuffCount = 14
					frame.buffRowStride = 3
				end
			end
			DAoCBuff.U()
		end
		
		-- Crestors Squared changes
		if Squared and Squared.Settings then
			Squared.Settings.boxheight = 25
			Squared.Settings.boxwidth = 103
			Squared.Settings["spacing-group"] = 4
			Squared.Settings["spacing-member"] = 5
			Squared.Settings["icon-offset-x"] = 0
			Squared.Settings["icon-offset-y"] = 3
			Squared.Settings["icon-scale"] = 0.75
		end
		
		if TexturedButtons and TexturedButtons.Settings then
			if TexturedButtons.Settings.Misc then
				TexturedButtons.Settings.Misc.HideQuicklock = true
			end
			if TexturedButtons.Settings.Preset then
				TexturedButtons.Settings.Preset.Name = "Y_Gloss"
				TexturedButtons.Settings.Preset.Enabled = true
			end
			if TexturedButtons.Settings.Tint then
				TexturedButtons.Settings.Tint.Enabled = true
			end
		end
		
		--[[local warnings = {}
		if BlackBox then table.insert(warnings, "BlackBox") end			-- ToDo: Turn off Blackbox?
		if Moth then table.insert(warnings, "Moth") end					-- ToDo: Turn off Moth?
		]]--
		
		yakui.InitializeVectorsSettings()
		
		yakuiVar.ver = "YakUI 1.4"
		RegisterEventHandler(SystemData.Events.LOADING_END, "yakui.OnLoadingEndAfterUpdate")
	end
end
function yakui.OnLoadingEndAfterUpdate()
	yakui.texture()
	yakui.color()
	LayoutEditor.UserHide("PlayerWindow")
	LayoutEditor.UserHide("GroupWindow")
	LayoutEditor.UserHide("PrimaryTargetLayoutWindow")
	LayoutEditor.UserHide("SecondaryTargetLayoutWindow")
	LayoutEditor.UserHide("EA_Window_OverheadMap")
	for groupIndex = 1,4 do
		LayoutEditor.UserHide("BattlegroupHUDGroup"..groupIndex.."LayoutWindow")
	end
	for groupIndex = 1,6 do
		LayoutEditor.UserHide("FloatingScenarioGroup"..groupIndex.."Window")
	end
	
	yakui.CareerResourceHack()
	LogOutAfterUpdate()
end

function yakuiUpdate.afterloading()
	WindowUnregisterEventHandler("EA_Window_CurrentEvents", SystemData.Events.LOADING_END)
	KwestorTracker.NubLButtonUp()
	if yakuifirststeps then yakuifirststeps.show() end
	if (DK_Config and DK_Config.OnUpdateSettingButton) then DK_Config.ToggleSetting( "MiscHTScore" , 0 ) end
end