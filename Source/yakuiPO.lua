local PO_STATUS_NONE = 0
local PO_STATUS_LOGOUT = 1
local PO_STATUS_EXITGAME = 2
local PO_TIMEDELAY = 0.1
local PO_Status = PO_STATUS_NONE
local PO_Val = 0
local PO_ValRange = 0
local PO_Time = PO_TIMEDELAY
local PO_Msg, PO_MsgCnt
local oldEA_ChatWindow_OnKeyEnter = nil

local checkbreak = false

local function yakuiPO_SmoothOut()
	if yakuiVar.DEVmode == true then yakui.Print("YakUI: PO_SmoothOut") end
	if DoesWindowExist("ySmoothOut") == true then DestroyWindow("ySmoothOut") end	
	CreateWindowFromTemplate("ySmoothOut","EA_FullResizeImage_TintableSolidBackground","Root")
	WindowSetShowing("ySmoothOut",true)
	WindowSetAlpha("ySmoothOut", 0)
	WindowSetLayer("ySmoothOut",3)
	WindowAddAnchor("ySmoothOut","topleft","Root","topleft",0,0)
	WindowAddAnchor("ySmoothOut","bottomright","Root","bottomright",0,0)
	WindowSetTintColor("ySmoothOut",0,0,0)
	WindowStartAlphaAnimation("ySmoothOut",Window.AnimationType.SINGLE_NO_RESET,0,1,4.5,true, 0,0)
end

local function yakuiPO_SmoothOutCancel()
	if yakuiVar.DEVmode == true then yakui.Print("YakUI: PO_SmoothOutCancel") end
	if DoesWindowExist("ySmoothOut") == true then DestroyWindow("ySmoothOut") end
	checkbreak = false
end

local function yakuiPO_UpdateLabel()
	if yakuiVar.DEVmode == true then yakui.Print("YakUI: PO_UpdateLabel") end
	if yakuiVar.smoothout == true then
		if ((20 - math.floor(PO_Val)) == 5) and (checkbreak == false) then
			checkbreak = true
			yakuiPO_SmoothOut() 
		end
	end
	if PO_Status == PO_STATUS_LOGOUT then
		LabelSetText("YakuiPOInfo", towstring(20 - math.floor(PO_Val))..L"s")
		LabelSetText("YakuiPOInfoShadow", towstring(20 - math.floor(PO_Val))..L"s")
	elseif PO_Status == PO_STATUS_EXITGAME then
		if towstring("You will finish logging out in " .. 20 - math.floor(PO_Val) .. " seconds.") == GetFormatStringFromTable("Hardcoded", 456, {20 - math.floor(PO_Val)}) then
			LabelSetText("YakuiPOInfo", towstring(20 - math.floor(PO_Val))..L"s")
			LabelSetText("YakuiPOInfoShadow", towstring(20 - math.floor(PO_Val))..L"s")
		else
			LabelSetText("YakuiPOInfo", towstring(20 - math.floor(PO_Val))..L"s")
			LabelSetText("YakuiPOInfoShadow", towstring(20 - math.floor(PO_Val))..L"s")
		end
	end
end

local function yakuiPO_Tick()
	if yakuiVar.DEVmode == true then yakui.Print("YakUI: PO_Tick") end
	local msgcount = TextLogGetNumEntries("System")
	if msgcount == 0 then
		return
	end
	local msg = select(3, TextLogGetEntry("System", TextLogGetNumEntries("System") - 1))
	if msg ~= PO_Msg then
		PO_Msg = msg
		if msg == GetFormatStringFromTable("Hardcoded", 456, {20}) then
			PO_Val = 0.5
			PO_ValRange = 0
			yakuiPO_UpdateLabel()
			WindowSetShowing("YakuiPO", true)
		elseif msg == GetFormatStringFromTable("Hardcoded", 456, {15}) then
			PO_Val = 5
			PO_ValRange = 5
			yakuiPO_UpdateLabel()
			WindowSetShowing("YakuiPO", true)
		elseif msg == GetFormatStringFromTable("Hardcoded", 456, {10}) then
			PO_Val = 10
			PO_ValRange = 10
			yakuiPO_UpdateLabel()
			WindowSetShowing("YakuiPO", true)
		elseif msg == GetFormatStringFromTable("Hardcoded", 456, {5}) then
			PO_Val = 15
			PO_ValRange = 15
			yakuiPO_UpdateLabel()
			WindowSetShowing("YakuiPO", true)
		elseif msg == GetStringFromTable("Hardcoded", 457) then
			UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakui.POTickCheck")
			PO_Status = PO_STATUS_NONE
			if yakuiVar.smoothout == true then yakuiPO_SmoothOutCancel() end
			WindowSetShowing("YakuiPO", false)
			return
		end
	end
	if math.floor(PO_Val) ~= PO_ValRange and math.floor(PO_Val) < PO_ValRange + 5 then
		yakuiPO_UpdateLabel()
	end
end

local function yakuiPO_SetupAction(state)
	if yakuiVar.DEVmode == true then yakui.Print("YakUI: PO_SetupAction") end
	if PO_Status > PO_STATUS_NONE then
	    return
	end
	WindowSetShowing("MainMenuWindow", false)
	RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "yakui.POTickCheck")
	PO_Status = state
	yakuiPO_Tick()
end

local function EA_ChatWindow_OnKeyEnter(...)
	local input = WStringToString(EA_TextEntryGroupEntryBoxTextInput.Text)
	input = string.lower(input)
	local cmd = string.match(input, "^/%l+")
	if  cmd == "/logout" or cmd == "/camp" then
		yakui.POLogOut()
	elseif cmd == "/quit" or cmd == "/exit" or cmd == "/q" then
		yakui.POExitGame()
	end
	oldEA_ChatWindow_OnKeyEnter(...)
end

function yakui.POInit()
	if yakuiVar.DEVmode == true then yakui.Print("YakUI: POInit") end
	CreateWindow("YakuiPO", false)
	WindowSetLayer("YakuiPO", 4)
	RegisterEventHandler(SystemData.Events.LOG_OUT, "yakui.POLogOut")
	RegisterEventHandler(SystemData.Events.EXIT_GAME, "yakui.POExitGame")
	oldEA_ChatWindow_OnKeyEnter = EA_ChatWindow.OnKeyEnter
	EA_ChatWindow.OnKeyEnter = EA_ChatWindow_OnKeyEnter
	WindowSetTintColor("YakuiPOBg", 255, 255, 255)
	DynamicImageSetTexture("YakuiPOBg", "SmoothOutBG", 0, 0)
	DynamicImageSetTextureDimensions("YakuiPOBg", 300, 300)
	LabelSetText("YakuiPOInfo", L"")
	LabelSetText("YakuiPOInfoShadow", L"")
	WindowSetAlpha("YakuiPO", 1)
	WindowSetShowing("YakuiPO", false)
end

function yakui.POTickCheck(elapsed)
	if yakuiVar.DEVmode == true then yakui.Print("YakUI: POTickCheck") end
	PO_Time = PO_Time - elapsed
	PO_Val = PO_Val + elapsed
	if PO_Time > 0 then
		return
	end
	PO_Time = PO_TIMEDELAY
	yakuiPO_Tick()
end

function yakui.POLogOut()
	WindowSetAlpha("YakuiPO", 1)
	WindowSetShowing("YakuiPO", true)
	yakuiPO_SetupAction(PO_STATUS_LOGOUT)
end

function yakui.POExitGame()
	WindowSetAlpha("YakuiPO", 1)
	WindowSetShowing("YakuiPO", true)
	yakuiPO_SetupAction(PO_STATUS_EXITGAME)
end 

function yakui.POCancel()
	BroadcastEvent( SystemData.Events.LOG_OUT )
	if yakuiVar.smoothout == true then yakuiPO_SmoothOutCancel() end
	LabelSetText("YakuiPOInfo", L"")
	LabelSetText("YakuiPOInfoShadow", L"")	
	WindowSetShowing("YakuiPO", false)
end

function yakui.POOnShown()
	WindowUtils.OnShown( yakui.POCancel, WindowUtils.Cascade.MODE_NONE )
end

function yakui.POOnHidden()
	if yakuiVar.DEVmode == true then yakui.Print("YakUI: POOnHidden") end
	if PO_Status > PO_STATUS_NONE then
		BroadcastEvent( SystemData.Events.LOG_OUT )
	end
	WindowUtils.OnHidden()
end
