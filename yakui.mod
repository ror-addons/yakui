<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="YakUI" version="1.4" date="01/26/2011" >
        
	<Author name="Yakar, Wothor, Crestor" email="" />
	<Description text="YakUI 1.4a Core - main mod of the YakUI compilation" />
	<VersionSettings gameVersion="1.4.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
	<Dependencies>
		<Dependency name="EA_CharacterWindow" optional="false" forceEnable="true" />
		<Dependency name="EA_InteractionWindow" optional="false" forceEnable="true" />
		<Dependency name="EA_RvRTracker" optional="false" forceEnable="true" />
		<Dependency name="EA_CareerResourcesWindow" optional="true" forceEnable="true" />
		<Dependency name="EA_AlertTextWindow"/>
		<Dependency name="YakAssetsPanel" optional="false" forceEnable="true" />
		<Dependency name="LibSlash" optional="false" forceEnable="true" />
		<Dependency name="Vectors" optional="false" forceEnable="true" />
		<Dependency name="Effigy" optional="false" forceEnable="true" />
		<Dependency name="DAoCBuff" optional="true" forceEnable="true" />
		<Dependency name="MapMonster" optional="true" forceEnable="true" />
		<Dependency name="Map" optional="true" forceEnable="true" />
		<Dependency name="TexturedButtons" optional="true" forceEnable="true" />
	</Dependencies>
	
	<Files>
		<File name="yakui.lua" />
		<File name="yakui.xml" />
		<File name="Source/yakui_ybutton.lua" />
		<File name="Source/yakuiconfig.xml" />
		<File name="Source/yakuiconfig.lua" />
		<File name="Source/yakuislash.lua" />
		<File name="Source/yakuiTacticMorale.lua" />
		<File name="Source/yakuiInfobar.lua" />
		<File name="Source/yakuiInfobar.xml" />
		<File name="Source/yakuiUpdateVectorsSettings.lua" />
		<File name="Source/yakuiUpdate.lua" />
		<File name="Source/yakuiPO.xml" />
		<File name="Source/yakuiPO.lua" />
		<File name="Source/yakuiWHL.lua" />
		<File name="Source/yakuiWHL.xml" />
		<File name="Source/yakuiSkin.lua" />
		<File name="Source/yakuiSkin.xml" />
		<File name="Source/Fader.lua" />
		<File name="Source/Panel.lua" />
	</Files>
        
	<OnInitialize>
		<CallFunction name="yakui.initialize" />
	</OnInitialize> 

	<SavedVariables>
		<SavedVariable name="yakuiVar" />
	</SavedVariables>

	<OnUpdate/>

	<OnShutdown/>   
	<WARInfo>
		<Categories>
			<Category name="SYSTEM" />
		</Categories>
		<Careers>
			<Career name="BLACKGUARD" />
			<Career name="WITCH_ELF" />
			<Career name="DISCIPLE" />
			<Career name="SORCERER" />
			<Career name="IRON_BREAKER" />
			<Career name="SLAYER" />
			<Career name="RUNE_PRIEST" />
			<Career name="ENGINEER" />
			<Career name="BLACK_ORC" />
			<Career name="CHOPPA" />
			<Career name="SHAMAN" />
			<Career name="SQUIG_HERDER" />
			<Career name="WITCH_HUNTER" />
			<Career name="KNIGHT" />
			<Career name="BRIGHT_WIZARD" />
			<Career name="WARRIOR_PRIEST" />
			<Career name="CHOSEN" />
			<Career name= "MARAUDER" />
			<Career name="ZEALOT" />
			<Career name="MAGUS" />
			<Career name="SWORDMASTER" />
			<Career name="SHADOW_WARRIOR" />
			<Career name="WHITE_LION" />
			<Career name="ARCHMAGE" />
		</Careers>
	</WARInfo>
	</UiMod>
</ModuleFile>